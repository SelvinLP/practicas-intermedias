-- MySQL Workbench Forward Engineering

SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0;
SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0;
SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='ONLY_FULL_GROUP_BY,STRICT_TRANS_TABLES,NO_ZERO_IN_DATE,NO_ZERO_DATE,ERROR_FOR_DIVISION_BY_ZERO,NO_ENGINE_SUBSTITUTION';

-- -----------------------------------------------------
-- Schema mydb
-- -----------------------------------------------------

-- -----------------------------------------------------
-- Schema mydb
-- -----------------------------------------------------
CREATE SCHEMA IF NOT EXISTS `mydb` DEFAULT CHARACTER SET utf8 ;
USE `mydb` ;

-- -----------------------------------------------------
-- Table `mydb`.`Sede`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `mydb`.`Sede` (
  `idSede` INT NOT NULL AUTO_INCREMENT,
  `Alias` VARCHAR(60) NULL,
  `Direccion` VARCHAR(50) NULL,
  `Departamento` VARCHAR(50) NULL,
  `Municipio` VARCHAR(50) NULL,
  `Encargado` INT NULL,
  PRIMARY KEY (`idSede`))
ENGINE = MyISAM;


-- -----------------------------------------------------
-- Table `mydb`.`Usuario`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `mydb`.`Usuario` (
  `idUsuario` INT NOT NULL AUTO_INCREMENT,
  `DPI` DOUBLE NOT NULL,
  `Nombre` VARCHAR(60) NULL,
  `FechaNacimiento` VARCHAR(50) NULL,
  `Correo` VARCHAR(45) NULL,
  `Password` VARCHAR(45) NULL,
  PRIMARY KEY (`idUsuario`))
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `mydb`.`Rol`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `mydb`.`Rol` (
  `idRol` INT NOT NULL AUTO_INCREMENT,
  `Descripcion` VARCHAR(60) NULL,
  `Usuario_idUsuario` INT NOT NULL,
  PRIMARY KEY (`idRol`),
  INDEX `fk_Rol_Usuario1_idx` (`Usuario_idUsuario` ASC) VISIBLE,
  CONSTRAINT `fk_Rol_Usuario1`
    FOREIGN KEY (`Usuario_idUsuario`)
    REFERENCES `mydb`.`Usuario` (`idUsuario`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `mydb`.`Bodega`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `mydb`.`Bodega` (
  `idBodega` INT NOT NULL AUTO_INCREMENT,
  `Nombre` VARCHAR(60) NULL,
  `Descripcion` VARCHAR(100) NULL,
  `Estado` VARCHAR(20) NULL,
  `Sede_idSede` INT NOT NULL,
  `Usuario_idUsuario` INT NOT NULL,
  PRIMARY KEY (`idBodega`),
  INDEX `fk_Bodega_Sede1_idx` (`Sede_idSede` ASC) VISIBLE,
  INDEX `fk_Bodega_Usuario1_idx` (`Usuario_idUsuario` ASC) VISIBLE,
  CONSTRAINT `fk_Bodega_Sede1`
    FOREIGN KEY (`Sede_idSede`)
    REFERENCES `mydb`.`Sede` (`idSede`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_Bodega_Usuario1`
    FOREIGN KEY (`Usuario_idUsuario`)
    REFERENCES `mydb`.`Usuario` (`idUsuario`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `mydb`.`Producto`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `mydb`.`Producto` (
  `idProducto` INT NOT NULL AUTO_INCREMENT,
  `SKU` VARCHAR(45) NOT NULL,
  `CodigoBarras` VARCHAR(45) NULL,
  `Nombre` VARCHAR(60) NULL,
  `Descripcion` VARCHAR(100) NULL,
  `Precio` DOUBLE NULL,
  PRIMARY KEY (`idProducto`))
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `mydb`.`Categoria`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `mydb`.`Categoria` (
  `idCategoria` INT NOT NULL AUTO_INCREMENT,
  `Descripcion` VARCHAR(100) NULL,
  `Producto_idProducto` INT NOT NULL,
  PRIMARY KEY (`idCategoria`),
  INDEX `fk_Categoria_Producto1_idx` (`Producto_idProducto` ASC) VISIBLE,
  CONSTRAINT `fk_Categoria_Producto1`
    FOREIGN KEY (`Producto_idProducto`)
    REFERENCES `mydb`.`Producto` (`idProducto`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `mydb`.`Cliente`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `mydb`.`Cliente` (
  `idCliente` INT NOT NULL AUTO_INCREMENT,
  `Nombre` VARCHAR(45) NULL,
  `Nit` VARCHAR(45) NULL,
  `DPI` DOUBLE NULL,
  `Direccion` VARCHAR(60) NULL,
  `Sede_idSede` INT NOT NULL,
  PRIMARY KEY (`idCliente`),
  INDEX `fk_Cliente_Sede1_idx` (`Sede_idSede` ASC) VISIBLE,
  CONSTRAINT `fk_Cliente_Sede1`
    FOREIGN KEY (`Sede_idSede`)
    REFERENCES `mydb`.`Sede` (`idSede`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `mydb`.`Venta`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `mydb`.`Venta` (
  `idVenta` INT NOT NULL AUTO_INCREMENT,
  `FechaFacturacion` VARCHAR(45) NULL,
  `FechaEntrega` VARCHAR(45) NULL,
  `Usuario_idUsuario` INT NOT NULL,
  `Cliente_idCliente` INT NOT NULL,
  PRIMARY KEY (`idVenta`),
  INDEX `fk_Venta_Usuario1_idx` (`Usuario_idUsuario` ASC) VISIBLE,
  INDEX `fk_Venta_Cliente1_idx` (`Cliente_idCliente` ASC) VISIBLE,
  CONSTRAINT `fk_Venta_Usuario1`
    FOREIGN KEY (`Usuario_idUsuario`)
    REFERENCES `mydb`.`Usuario` (`idUsuario`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_Venta_Cliente1`
    FOREIGN KEY (`Cliente_idCliente`)
    REFERENCES `mydb`.`Cliente` (`idCliente`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `mydb`.`Inventario`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `mydb`.`Inventario` (
  `idInventario` INT NOT NULL AUTO_INCREMENT,
  `Cantidad` DOUBLE NULL,
  `Descripcion` VARCHAR(45) NULL,
  `Bodega_idBodega` INT NOT NULL,
  `Producto_idProducto` INT NOT NULL,
  PRIMARY KEY (`idInventario`),
  INDEX `fk_Inventario_Bodega1_idx` (`Bodega_idBodega` ASC) VISIBLE,
  INDEX `fk_Inventario_Producto1_idx` (`Producto_idProducto` ASC) VISIBLE,
  CONSTRAINT `fk_Inventario_Bodega1`
    FOREIGN KEY (`Bodega_idBodega`)
    REFERENCES `mydb`.`Bodega` (`idBodega`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_Inventario_Producto1`
    FOREIGN KEY (`Producto_idProducto`)
    REFERENCES `mydb`.`Producto` (`idProducto`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `mydb`.`ListaProductos`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `mydb`.`ListaProductos` (
  `idListaProductos` INT NOT NULL,
  `Descripcion` VARCHAR(45) NULL,
  `Cantidad` VARCHAR(45) NULL,
  `Producto_idProducto` INT NOT NULL,
  `Venta_idVenta` INT NOT NULL,
  PRIMARY KEY (`idListaProductos`),
  INDEX `fk_ListaProductos_Producto1_idx` (`Producto_idProducto` ASC) VISIBLE,
  INDEX `fk_ListaProductos_Venta1_idx` (`Venta_idVenta` ASC) VISIBLE,
  CONSTRAINT `fk_ListaProductos_Producto1`
    FOREIGN KEY (`Producto_idProducto`)
    REFERENCES `mydb`.`Producto` (`idProducto`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_ListaProductos_Venta1`
    FOREIGN KEY (`Venta_idVenta`)
    REFERENCES `mydb`.`Venta` (`idVenta`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `mydb`.`Transferencias`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `mydb`.`Transferencias` (
  `idTransferencias` INT NOT NULL,
  `Descripcion` VARCHAR(45) NULL,
  `Estado` VARCHAR(45) NULL,
  `CreadorPeticion` VARCHAR(45) NULL,
  `Bodega_idBodega` INT NOT NULL,
  PRIMARY KEY (`idTransferencias`),
  INDEX `fk_Transferencias_Bodega1_idx` (`Bodega_idBodega` ASC) VISIBLE,
  CONSTRAINT `fk_Transferencias_Bodega1`
    FOREIGN KEY (`Bodega_idBodega`)
    REFERENCES `mydb`.`Bodega` (`idBodega`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;


SET SQL_MODE=@OLD_SQL_MODE;
SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS;
SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS;
