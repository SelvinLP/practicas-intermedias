const { Router } = require('express');
const { login, get_password, update_dataUsu, ver_Usuario, del_Usuario, add_Usuario,asignarRol,quitarRol,activarBodega } = require('../controller/usuarios');
const router = Router();

// LOGIN
router.post('/login', login);
// RECUPERAR CONTRASEÑA
router.post('/recpass', get_password);
// EDITAR DATOS PERSONALES
router.post('/editprofile', update_dataUsu);

// CREAR NUEVO USUARIO
router.post('/signup', add_Usuario)

//ELIMINAR USUARIO
router.delete('/deleteUser', del_Usuario)

//VER DATA USUARIO
router.get('/user:id',ver_Usuario)


//  asignar rol
router.post('/asignarRol', asignarRol)
//  quitar rol
router.post('/quitarRol', quitarRol)


//  activar bodega
router.post('/activarBodega', activarBodega)

module.exports = router;