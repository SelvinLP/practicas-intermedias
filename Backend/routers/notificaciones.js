const { Router } = require('express');
const { send_notificacion } = require('../controller/notificacion');
const router = Router();

// LOGIN
router.post('/notificacion', send_notificacion);

module.exports = router;