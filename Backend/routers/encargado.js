const { Router } = require('express');
const CRUD_sedes = require('../controller/sedes');
const bodega = require('../controller/bodega');
const router = Router();

//CREAR SEDE
router.post('/crearSede', CRUD_sedes.crear)

//LISTAR SEDES
router.get('/sedes',CRUD_sedes.leer)

//LISTAR SEDES POR USUARIO
router.get('/sede:id',CRUD_sedes.leer_user)

//ACTUALIZAR SEDE
router.put('/updateSede',CRUD_sedes.actualizar)

//ELIMINAR SEDE
router.post('/deleteSede',CRUD_sedes.eliminar)

//CREAR BODEGA
router.post('/crearBodega', bodega.crear)

//LISTAR BODEGAS
router.get('/bodegas',bodega.leer)

//LISTAR BODEGAS por usuario
router.get('/bodegasUser:id',bodega.leer_user)

//LISTAR BODEGAS por sede
router.get('/bodegasSede:id',bodega.leer_sede)



module.exports = router