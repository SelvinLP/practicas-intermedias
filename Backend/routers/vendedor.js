const { Router } = require('express');
const { registerClient, registerSale, requestTransfer, updateStocktaking, getClients, getSales } = require('../controller/vendedor');
const router = Router();

router.post('/registrarCliente', registerClient);
router.post('/registrarVenta', registerSale);
//router.get('/reporteDeVentas', getSalesReport);
router.post('/actualizarInventario', updateStocktaking);
router.post('/solicitarTransferencia', requestTransfer);
router.get('/getClientes', getClients);
router.post('/getSales', getSales);

module.exports = router;    