const { Router } = require('express');
const { verOrdenesVentaPendintes, verDetalleVenta, verOrdenesTransferencia } = require('../controller/repartidor');
const router = Router();

router.get('/repartidor/ventas:id', verOrdenesVentaPendintes);
router.get('/repartidor/detalle-venta:id', verDetalleVenta);
router.get('/repartidor/transferencias:id', verOrdenesTransferencia);



module.exports = router;