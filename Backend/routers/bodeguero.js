const { Router } = require('express');
const { verOrdenesTransferenciaExternas, obtenerBodegaSaliente,
    obtenerDatosProducto, obtenerDatosRepartidor, actualizarEstadoTransferencia,
    verOrdenesTransferenciaInternas, verInfoBodegas,verProductosInventario } = require('../controller/bodeguero');
const router = Router();

router.get('/bodeguero/ordenes_externas:id', verOrdenesTransferenciaExternas);
router.get('/bodeguero/bodega-saliente', obtenerBodegaSaliente);
router.get('/bodeguero/datos-producto', obtenerDatosProducto);
router.get('/bodeguero/datos-repartidor', obtenerDatosRepartidor);
router.get('/bodeguero/actualizar_transferencia:id', actualizarEstadoTransferencia);
router.get('/bodeguero/ordenes_internas:id', verOrdenesTransferenciaInternas);
router.get('/bodeguero/info-bodegas', verInfoBodegas);
router.get('/bodeguero/productos_inventario:id', verProductosInventario);

module.exports = router;