var mysql = require('mysql');
const pool = require('./conexiondb');

/***
 * Funciónn para visualizar las ventas de un usuario 
 * repartidor, el body del request tiene como atributo el 
 * id del usuario
 */
const verOrdenesVentaPendintes = (req, res) => {
    
    pool.query('select * from Venta as V where V.id_repart = ?',
        [req.params.id], (err, resquery) => {
            if (err)
                throw err;

            results = JSON.stringify(resquery); //remove RowDataPacket
            console.log(results);
            let estado = results.replace("[", "").replace("]", "") == "" ? 100 : 300;
            if (estado == 100) {
                res.send({
                    'status': 100
                });
                return
            }
            resquery = JSON.parse(results);

            res.send({
                'status': estado,
                'ventas': resquery
            });
        });
}

/***
 * Función para ver el detalle de una venta en específico
 * el body del request tiene como atributo el id de la venta
 * de la cual se quiere obtener el detalle
 */
const verDetalleVenta = (req, res) => {
    let body = req.body;
    pool.query(`select LP.Descripcion,LP.Cantidad, P.Nombre, P.Precio from mydb.listaproductos as LP
    join mydb.producto as P on LP.Producto_idProducto = P.idProducto
    where LP.Venta_idVenta = ?`,
        [req.params.id], (err, resquery) => {
            if (err)
                throw err;

            results = JSON.stringify(resquery); //remove RowDataPacket
            console.log(results);
            let estado = results.replace("[", "").replace("]", "") == "" ? 100 : 300;
            if (estado == 100) {
                res.send({
                    'status': 100
                });
                return
            }
            resquery = JSON.parse(results);

            res.send({
                'status': estado,
                'detalle_venta': resquery
            });
        });
}

/***
 * Función para obtener las ordenes de transferencia pendientes
 */
const verOrdenesTransferencia = (req, res) => {

    pool.query('select * from Transferencias as T where T.Estado = \'Pendiente\' and T.CreadorPeticion=?',
    [req.params.id], (err, resquery) => {
        if (err)
            throw err;

        results = JSON.stringify(resquery); //remove RowDataPacket
        console.log(results);
        let estado = results.replace("[", "").replace("]", "") == "" ? 100 : 300;
        if (estado == 100) {
            res.send({
                'status': 100
            });
            return
        }
        resquery = JSON.parse(results);

        res.send({
            'status': estado,
            'trans': resquery
        });
    });
}

module.exports = {
    verOrdenesVentaPendintes,
    verDetalleVenta,
    verOrdenesTransferencia
}