var mysql = require('mysql');
var cred = require('../credentials/credintials');
var pool = mysql.createConnection(cred);


/**
 * Función utilizada para registrar 'Clientes' en la base de datos.
 */
const registerClient = ( req, res ) => {
    let body = req.body;
    console.log(body);
    pool.query('INSERT INTO Cliente (Nombre, Nit, DPI, Direccion, Sede_idSede) VALUES (?);',
        [[body.nombre, body.nit, body.dpi, body.direccion, body.sede]], (err, resquery) => {
            if ( err ) {
                console.log(err);
                res.send({"descripcion":`Error al conectarse al servidor. Error: ${err}`});
                return;
            }
            res.send({"descripcion":"Usuario registrado exitosamente."});
        });
}

/**
 * Función que registra una 'venta'
 * {
 *  "fechafacturacion"
 *  "fechaentrega"
 *  "id_usuario"
 *  "id_cliente"
 *  "total"
 *  "estado"
 *  "r_venta"
 *  "id_repartidor"
 *  "ubicacion"
 * }
 */
const registerSale = ( req, res ) => {
    let body = req.body;
    pool.query('INSERT INTO Venta ( FechaFacturacion, FechaEntrega, Usuario_idUsuario, Cliente_idCliente, Total, Estado, R_Venta, id_repart, Ubicacion ) VALUES (?);',
        [[body.fechafacturacion, body.fechaentrega, body.id_usuario, body.id_cliente, body.total, body.estado, body.r_venta, body.id_repartidor, body.ubicacion]], (err, resquery) => {
            if ( err ) {
                res.send({"descripcion":"Error al conectarse al servidor."});
                return;
            }
            // Insertar lista de productos...
            /*
            pool.query('SELECT LAST_INSERT_ID() AS id;',[], (err_, resquery_) => {
                if ( err ) {
                    res.send({"descripcion":"Error al conectarse al servidor."});
                    return;
                }
                if ( resquery_ == 0 ) return response.send({"descripcion": "Error al insertar venta."});
                let idv = resquery_[0].id;
                for ( let x of body.productos ) {
                    x.idventa = idv;
                }
                let q = `INSERT ignore INTO ListaProductos(Descripcion, Cantidad, Producto_idProducto, Venta_idVenta) VALUES (?)`
                pool.query(q, [body.productos], async (error, result) => {
                    console.log(error || result);
                });

            })*/
            res.send({"descripcion":"Venta registrada exitosamente."});
        });
} 

/**
 * Función para extraer todos los clientes
 */

const getClients = (req, res) => {
    pool.query('SELECT * FROM Cliente',
    [], (err, resquery) => {
        if (err) {
            res.send({"msj": err})
            return
        }
        res.send(resquery)
    })
}

/**
 * Función que envía los datos necesarios para generar reporte de ventas
 */
/* const getSalesReport = ( req, res ) => {
    let body = req.body;
    pool.query();
} */

/** ESTAS FUNCIONES CORRESPONDEN AL ROL: BODEGUERO **/
/**
 * Función que actualiza el inventario
 */
const updateStocktaking = ( req, res ) => {
    let body = req.body;
    console.log(body);
    pool.query('UPDATE Inventario SET Cantidad = ? WHERE idInventario = ?;',
        [body.cantidad, body.inventario], (err, resquery) => {
            if ( err ) {
                console.log(err);
                res.send({"descripcion":"Error al conectarse al servidor."});
                return;
            }
            res.send({"descripcion":"Inventario actualizado exitosamente."});
        });
}

/**
 * Función que solicita una transferencia
 */
const requestTransfer = ( req, res ) => {
    let body = req.body;
    //console.log(body);
    pool.query('INSERT INTO Transferencias (Descripcion, Estado, CreadorPeticion, Bodega_idBodega,Bodega_dest,Tipo ) VALUES (?)',
        [[body.descripcion, body.estado, body.creador, body.bodega,body.bodega_dest,body.tipo]], (err, resquery) => {
            if ( err ) {
                console.log(err);
                res.send({"descripcion":"Error al conectarse al servidor."});
                return;
            }
            res.send({"descripcion":"Solicitud de transferencia realizada."});
        });
}

/**
 * Funcion para extraer todos las ventas del usuario
 */

const getSales = ( req, res ) => {
    let body = req.body;
    pool.query(`SELECT * FROM Venta WHERE Usuario_idUsuario = ?`,
    [body.cliente], (err, resquery) => {
        if (err) {
            res.send({"msj": err})
            return
        }
        res.send(resquery)
    })
}

module.exports = {
    registerClient,
    registerSale,
    requestTransfer,
    updateStocktaking,
    getClients,
    getSales
    /*getSalesReport */
}