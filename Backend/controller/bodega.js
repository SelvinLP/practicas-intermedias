var pool = require('./conexiondb')

const bodega = {}

bodega.crear = (req, res) =>{
    let body = req.body
    pool.query('INSERT INTO Bodega (Nombre, Descripcion, Estado, Sede_idSede, Usuario_idUsuario) VALUES (?)', 
    [[body.nombre, body.descripcion, body.estado, body.sede, body.usuario]], (err, resquery) => {
        if (err){
            console.log(err)
            res.send({"msj": "Error: no se pudo conectar al servidor"})
            return
        }
        res.send({
            'msj': 'Bodega guardada con exito!'
        })
    })
}

bodega.leer = (req, res) =>{
    pool.query('SELECT B.idBodega AS id,B.Nombre AS Bodega, B.Descripcion, B.Estado, S.Alias AS Sede, U.Nombre AS Encargado FROM Sede AS S, Bodega AS B, Usuario AS U WHERE S.Encargado = U.idUsuario AND B.Sede_idSede = S.idSede', 
    [], (err, resquery) => {
        if (err) {
            res.send({"msj": "Error: no se pudo conectar al servidor"})
            return
        }
        res.send(resquery)
    })
}

bodega.leer_user = (req, res) =>{
    pool.query('SELECT B.idBodega AS id,B.Nombre AS Bodega FROM Bodega AS B where B.Usuario_idUsuario = ?', 
    [req.params.id], (err, resquery) => {
        if (err) {
            res.send({"msj": "Error: no se pudo conectar al servidor"})
            return
        }
        res.send(resquery)
    })
}

bodega.leer_sede = (req, res) =>{
    pool.query('SELECT B.idBodega AS id,B.Nombre AS Bodega, B.Descripcion, B.Estado, S.Alias AS Sede, U.Nombre AS Encargado FROM Sede AS S, Bodega AS B, Usuario AS U WHERE B.Usuario_idUsuario = U.idUsuario AND B.Sede_idSede = S.idSede AND S.idSede = ?', 
    [req.params.id], (err, resquery) => {
        if (err) {
            res.send({"msj": "Error: no se pudo conectar al servidor"})
            return
        }
        res.send(resquery)
    })
}

module.exports = bodega