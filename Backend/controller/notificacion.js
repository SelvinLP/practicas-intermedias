var mysql = require('mysql');
const pool = require('./conexiondb');
const enviar_correo = require('./correo');

// MANDAR NOTIFICACION
const send_notificacion = (req, res) => {
    let body = req.body;
    enviar_correo(body.email, body.descripcion, "Notificacion")
    res.send({"descripcion":"Notificacion enviada correctamente."});
}

module.exports = {
    send_notificacion
}