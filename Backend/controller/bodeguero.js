var mysql = require('mysql');
const pool = require('./conexiondb');

/**
 * Función para obtener órdenes de transferencia externas pendientes
 * en el body del request viene el atributo idSedePropia para que la
 * búsqueda de ordenes de transferencia tome en cuenta los id de sede 
 * diferente al id de la sede propia
 */
const verOrdenesTransferenciaExternas = (req, res) => {
    
   
    pool.query('select T.idTransferencias as Id,T.Descripcion as Descripcion,T.estado as Estado,T.CreadorPeticion as Creador,B1.Nombre as Bodega_solicitante from Transferencias as T join Bodega as B1 on T.Bodega_idBodega = B1.idBodega where T.Estado = \'Pendiente\' and T.tipo = \'Externa\' and T.Bodega_dest = ?',
    [req.params.id], (err, resquery) => {
        if (err) {
            res.send({"msj": "Error: no se pudo conectar al servidor"})
            return
        }
        res.send(resquery)
    })
}

/**
 * Función que obtiene una bodega de la sede propia con
 * disponibilidad del producto que se desea transferir.
 *el body del request tiene como atributos el id de sede, 
 id de producto y cantidad que se 
 * necesita transferir
 */
const obtenerBodegaSaliente = (req, res) => {
    let body = req.body;
    pool.query(`select B.idBodega, B.Nombre, B.Descripcion, B.Estado, B.Sede_idSede, B.Usuario_idUsuario from mydb.bodega as B
    join mydb.inventario as I on B.idBodega = I.Bodega_idBodega
    where B.Sede_idSede = ? and I.Producto_idProducto = ? and I.Cantidad >=?`,
        [body.idSedePropia, body.idProducto, body.cantidad], (err, resquery) => {
            if (err)
                throw err;

            results = JSON.stringify(resquery); //remove RowDataPacket
            console.log(results);
            let estado = results.replace("[", "").replace("]", "") == "" ? 100 : 300;
            if (estado == 100) {
                res.send({
                    'status': 100
                });
                return
            }
            resquery = JSON.parse(results);

            res.send({
                'status': estado,
                'bodegas': resquery
            });

        });
}
//------------------------- inventario ---------------------------

const verProductosInventario = (req, res) => {
    let body = req.body;
    pool.query(`Select I.idInventario, p.idProducto, p.Nombre, I.cantidad from Inventario as I  inner join Producto as p on p.idProducto = I.Producto_idProducto  where I.Bodega_idBodega = ?`,
    [req.params.id], (err, resquery) => {
        if (err) {
            res.send({"msj": "Error: no se pudo conectar al servidor"})
            return
        }
        res.send(resquery)
    })
}


/**
 * Funcion para obtener todos los datos del producto
 */
const obtenerDatosProducto = (req, res) => {
    let body = req.body;
    pool.query(`select * from mydb.producto as P where P.idProducto = ?`,
        [body.idProducto], (err, resquery) => {
            if (err)
                throw err;

            results = JSON.stringify(resquery); //remove RowDataPacket
            console.log(results);
            let estado = results.replace("[", "").replace("]", "") == "" ? 100 : 300;
            if (estado == 100) {
                res.send({
                    'status': 100
                });
                return
            }
            resquery = JSON.parse(results);

            res.send({
                'status': estado,
                'datos_producto': resquery
            });

        });
}

/**
 * Función para obtener los datos de un repartidor
 */
const obtenerDatosRepartidor = (req, res) => {
    let body = req.body;
    pool.query(`select * from mydb.usuario as U where U.idUsuario = ?`,
        [body.idUsuario], (err, resquery) => {
            if (err)
                throw err;

            results = JSON.stringify(resquery); //remove RowDataPacket
            console.log(results);
            let estado = results.replace("[", "").replace("]", "") == "" ? 100 : 300;
            if (estado == 100) {
                res.send({
                    'status': 100
                });
                return
            }
            resquery = JSON.parse(results);

            res.send({
                'status': estado,
                'datos_repartidor': resquery
            });
        });
}

/**
 * Función para cambiar el estado de una transferencia 
 * el body del request tiene como atributos el estado nuevo de la transferencia
 * y el id de la transferencia que se va a actualizar
 */
const actualizarEstadoTransferencia = (req, res) => {
    pool.query('UPDATE Transferencias set Estado = \'Aceptada\' where idTransferencias = ?',
    [req.params.id], (err, resquery) => {
        if (err) {
            res.send({"msj": "Error: no se pudo conectar al servidor"})
            return
        }

        
        res.send({"msj":"Transferencia Actualizada"})
    })
}


/**
 * Función para obtener órdenes de transferencia internas pendientes
 * en el body del request viene el atributo idSedePropia para que la
 * búsqueda de ordenes de transferencia tome en cuenta los id de sede 
 * igual al id de la sede propia
 */
const verOrdenesTransferenciaInternas = (req, res) => {
   
    pool.query('select T.idTransferencias as Id,T.Descripcion as Descripcion,T.estado as Estado,T.CreadorPeticion as Creador,B1.Nombre as Bodega_solicitante from Transferencias as T join Bodega as B1 on T.Bodega_idBodega = B1.idBodega where T.Estado = \'Pendiente\' and T.tipo = \'Interna\' and T.Bodega_dest = ?',
    [req.params.id], (err, resquery) => {
        if (err) {
            res.send({"msj": "Error: no se pudo conectar al servidor"})
            return
        }
        res.send(resquery)
    })
}

/**
 * Función que muestra las bodegas de una sede
 * el body del request tiene como atributo el id de la sede
 */
const verInfoBodegas = (req, res) => {
    let body = req.body;
    pool.query(`select * 
    from mydb.bodega as B
    where B.Sede_idSede = ?`,
        [body.idSede], (err, resquery) => {
            if (err)
                throw err;

            results = JSON.stringify(resquery); //remove RowDataPacket
            console.log(results);
            let estado = results.replace("[", "").replace("]", "") == "" ? 100 : 300;
            if (estado == 100) {
                res.send({
                    'status': 100
                });
                return
            }
            resquery = JSON.parse(results);

            res.send({
                'status': estado,
                'bodegas': resquery
            });
        });
}

module.exports = {
    verOrdenesTransferenciaExternas,
    obtenerBodegaSaliente,
    obtenerDatosProducto,
    obtenerDatosRepartidor,
    actualizarEstadoTransferencia,
    verOrdenesTransferenciaInternas,
    verInfoBodegas,
    verProductosInventario
}