var nodemailer = require('nodemailer');

async function enviar_correo(correo, contraseña, encabezado){
    var transporter = nodemailer.createTransport({
        service: 'gmail',
        auth: {
          user: 'practicasintermedias2021@gmail.com',
          pass: '123practicas!'
        }
    });
      
    var mensaje = contraseña;
      
    var mailOptions = {
        from: 'practicasintermedias2021@gmail.com',
        to: correo,
        subject: encabezado,
        text: mensaje
    };
    
    transporter.sendMail(mailOptions, function(error, info){
        if (error) {
          console.log(error);
        } else {
          console.log('Email enviado: ' + info.response);
        }
      });
      
}

module.exports = enviar_correo;
