var mysql = require('mysql');
const pool = require('./conexiondb');
const enviar_correo = require('./correo');



//----------------activar bodega ------------------------------

const activarBodega= (req, res) =>{
    let body = req.body
    pool.query('UPDATE Bodega SET Estado = ? WHERE idBodega = ?', 
    [body.estado, body.idBodega], (err, resquery) => {
      
        if (err){
            res.send({"msj": "Error: no se pudo conectar al servidor"})
            return
        }
        res.send({
            'msj': 'Estado actualizado exitosamente!'
        })
     
    })
}



//----------------asiganar rol ------------------------------

const asignarRol= (req, res) =>{
    let body = req.body
    pool.query('INSERT INTO Rol (Descripcion, Usuario_idUsuario) VALUES (?)', 
    [[body.rol, body.idUsuario]], (err, resquery) => {
        if (err){
            res.send({"msj": "Error: no se pudo conectar al servidor"})
            return
        }
        res.send({
            'msj': 'Rol guardado exitosamente!'
        })
     
    })
}

//----------------quitar rol ------------------------------

const quitarRol= (req, res) =>{
    let body = req.body
    pool.query('DELETE FROM Rol WHERE Usuario_idUsuario = ? AND Descripcion = ?', 
    [body.idUsuario, body.rol], (err, resquery) => {
      
        if (err){
            res.send({"msj": "Error: no se pudo conectar al servidor"})
            return
        }
        res.send({
            'msj': 'Rol eliminado exitosamente!'
        })
     
    })
}



//----------------quitar rol --------------------------------





// LOGIN
const login = (req, res) => {
    let body = req.body;
    pool.query('SELECT * FROM Usuario as Usu, Rol as rol WHERE Usu.Correo = ? AND Usu.Password = ? AND Usu.idUsuario = rol.Usuario_idUsuario;', 
        [body.email, body.password], (err, resquery) => {
            if (err)
                throw err;

            results = JSON.stringify(resquery); //remove RowDataPacket
            console.log(results);
            let estado = results.replace("[", "").replace("]", "") == "" ? 100 : 300;
            if (estado == 100) {
                res.send({
                    'status': 100
                });
                return
            }
            resquery = JSON.parse(results);
            let roles = [];
            for (let dato of resquery) {
                roles.push({ "descripcion": dato.Descripcion });
            }
            res.send({
                'status': estado,
                "id": resquery[0].idUsuario,
                "dpi": resquery[0].DPI,
                "nombre": resquery[0].Nombre,
                "fechanacimiento": resquery[0].FechaNacimiento,
                "correo": resquery[0].Correo,
                "rol": roles
            });

        });

}

//OBTENER CONTRASEÑA POR CORREO
const get_password = (req, res) => {
    let body = req.body;
    pool.query('SELECT Password FROM mydb.Usuario WHERE Correo = ?;', body.email, (err, resquery) => {
        if (err) throw err;

        results = JSON.stringify(resquery); //remove RowDataPacket
        let estado = results.replace("[", "").replace("]", "") == "" ? 100 : 300;
        if (estado == 100) {
            res.send({
                'status': 100,
                "descripcion": "El usuario no existe"
            });
            return
        }
        resquery = JSON.parse(results);
        enviar_correo(body.email, "Tu contraseña es: " + resquery[0].Password, "Recuperacion de Contraseña")

        res.send({
            'status': 300,
            "descripcion": "Correo enviado correctamente"
        });
    });
}

// EDITAR DATOS PERSONALES
const update_dataUsu = (req, res) => {
    let body = req.body;
    pool.query('UPDATE mydb.Usuario SET Nombre=?, FechaNacimiento=? , Correo=?, Password=? WHERE DPI = ?;',
        [body.nombre, body.fechanacimiento, body.correo, body.pass, body.dpi], (err, resquery) => {
            if (err) throw err;

            results = JSON.stringify(resquery); //remove RowDataPacket
            let estado = results.replace("[", "").replace("]", "") == "" ? 100 : 300;
            if (estado == 100) {
                res.send({
                    'status': 100,
                    "descripcion": "No se pudo modificar Correctamente"
                });
                return
            }
            res.send({
                'status': 300,
                "descripcion": "Modificado Correctamente"
            });
        });
}

//CREAR USUARIO
const add_Usuario= (req, res) =>{
    let body = req.body
    pool.query('INSERT INTO Usuario (DPI, Nombre, FechaNacimiento, Correo, Password) VALUES (?)', 
    [[body.dpi, body.nombre, body.fechanac, body.correo , body.password]], (err, resquery) => {
        if (err){
            res.send({"msj": "Error: no se pudo conectar al servidor"})
            return
        }
        let id = resquery.insertId
        pool.query('INSERT INTO Rol (Descripcion, Usuario_idUsuario) VALUES (?)', [[body.rol, id]], (errr, resquery2) => {
            if (errr){
                console.log(errr)
                res.send({"msj": "Error: no se pudo conectar al servidor"})
                return
            }
            res.send({
                'msj': 'Usuario nuevo guardado exitosamente!'
            })
        })
    })
}

//ELIMINAR USUARIO
const del_Usuario = (req, res) =>{
    body = req.body
    pool.query('DELETE FROM Usuario WHERE idUsuario = ?', 
    [body.id], (err, resquery) => {
        if (err) {
            res.send({"msj": "Error: no se pudo conectar al servidor"})
            return
        }
        res.send({
            "msj":"Usuario eliminado!"
        })
    })
}

//VER USUARIO
const ver_Usuario = (req, res) =>{
    pool.query('SELECT DPI, Nombre, FechaNacimiento, Correo, Password FROM Usuario WHERE idUsuario = ?', 
    [req.params.id], (err, resquery) => {
        if (err) {
            res.send({"msj": "Error: no se pudo conectar al servidor"})
            return
        }
        res.send(resquery)
    })
}

module.exports = {
    login,
    get_password,
    update_dataUsu,
    ver_Usuario,
    del_Usuario,
    add_Usuario,
    asignarRol,
    quitarRol,
    activarBodega
}