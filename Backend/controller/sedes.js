var pool = require('./conexiondb')

const CRUD_sedes = {}

CRUD_sedes.crear = (req, res) =>{
    let body = req.body
    pool.query('INSERT INTO Sede (Alias, Direccion, Departamento, Municipio, Encargado) VALUES (?)', 
    [[body.alias, body.direccion, body.departamento, body.municipio , body.usuario]], (err, resquery) => {
        if (err){
            res.send({"msj": "Error: no se pudo conectar al servidor"})
            return
        }
        res.send({
            'msj': 'Sede guardada con exito!'
        })
    })
}

CRUD_sedes.leer = (req, res) =>{
    pool.query('SELECT S.idSede AS id, S.Alias AS Sede, S.Direccion, S.Departamento, S.Municipio, U.Nombre AS Encargado FROM Sede AS S, Usuario AS U WHERE S.Encargado = U.idUsuario', 
    [], (err, resquery) => {
        if (err) {
            res.send({"msj": "Error: no se pudo conectar al servidor"})
            return
        }
        res.send(resquery)
    })
}

CRUD_sedes.leer_user = (req, res) =>{
    pool.query('SELECT S.idSede AS id, S.Alias AS Sede, S.Direccion, S.Departamento, S.Municipio, U.Nombre AS Encargado FROM Sede AS S, Usuario AS U WHERE S.Encargado = U.idUsuario AND S.Encargado = ?', 
    [req.params.id], (err, resquery) => {
        if (err) {
            res.send({"msj": "Error: no se pudo conectar al servidor"})
            return
        }
        res.send(resquery)
    })
}

CRUD_sedes.actualizar = (req, res) =>{
    body = req.body
    pool.query('UPDATE Sede SET Alias = ?, Direccion = ?, Departamento = ?, Municipio = ? WHERE idSede = ?', 
    [body.alias, body.direccion, body.departamento, body.municipio, body.id], (err, resquery) => {
        if (err) {
            res.send({"msj": "Error: no se pudo conectar al servidor"})
            return
        }
        res.send({
            "msj":"Sede actualizada!"
        })
    })
}

CRUD_sedes.eliminar = (req, res) =>{
    body = req.body
    pool.query('DELETE FROM Sede WHERE idSede = ?', 
    [body.id], (err, resquery) => {
        if (err) {
            res.send({"msj": "Error: no se pudo conectar al servidor"})
            return
        }
        res.send({
            "msj":"Sede eliminada!"
        })
    })
}

module.exports = CRUD_sedes
