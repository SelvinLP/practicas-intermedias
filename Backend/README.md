# Backend
### Modelo 
![alt text](https://gitlab.com/SelvinLP/practicas-intermedias/-/raw/feature/Database/Backend/images/Base%20de%20datos%20Practicas%20Intermedias.png)
### Endpoints
Login POST ```18.225.36.192:3000/login```
* Entrada
    ```
    {
        "email": "",
        "password": ""
    }
    ```
* Salida
    ```
    {
        "status": Int (300 = ok, 100 = error),
        "id": Int,
        "dpi": Int,
        "nombre": "",
        "fechanacimiento": "",
        "correo": "",
        "rol": [
            {
                "descripcion":""
            }
        ]
    }
    ```

Recuperar Contraseña POST ```18.225.36.192:3000/recpass```
* Entrada
    ```
    {
        "email": ""
    }
    
    ```
* Salida
    ```
    {
        "status": Int (200 = ok, 100 = error),
        "descripcion": ""
    }
    ```

Crear Usuario POST ```18.225.36.192:3000/signup```
* Entrada
    ```
    {
        "dpi":INT,
        "nombre":"",
        "fechanac" : "",
        "correo" : "",
        "password" : ""
        "rol": ""
    }
    ```
* Salida
    ```
    {
        'msj': 'Usuario nuevo guardado exitosamente!'
    }
    ```

Editar Datos Personales POST ```18.225.36.192:3000/editprofile```
* Entrada
    ```
    {
        "dpi": Int, // no puede cambiar de dpi, este sera solo de referencia
        "nombre": "",
        "fechanacimiento": "",
        "correo": "",
        "pass": ""
    }
    
    ```
* Salida
    ```
    {
        "status": Int (200 = ok, 100 = error),
        "descripcion": ""
    }
    ```

Eliminar Usuario DELETE ```18.225.36.192:3000//deleteUser```
* Entrada
    ```
    {
        "id" : INT  id del usuario a eliminar
    }
    ```
* Salida
    ```
    {
        'msj': 'Usuario eliminada!'
    }
    ```

Ver Usuario logueado GET ```18.225.36.192:3000/user:id```
id es el id del usuario que esta logueado
* Salida
    ```
    {
        "dpi":int,
        "nombre":"",
        "fechanac" : "",
        "correo" : "",
        "password" : ""
    }

***
***
***

Crear Sede POST ```18.225.36.192:3000/encargado/crearSede```
* Entrada
    ```
    {
        "alias":"",
        "direccion":"",
        "departamento":"",
        "municipio": "",
        "usuario" : INT  id del usuario que esta logueado
    }
    ```
* Salida
    ```
    {
        'msj': 'Sede guardada con exito!'
    }
    ```

Actualizar Sede PUT ```18.225.36.192:3000/encargado/updateSede```
* Entrada
    ```
    {
        "alias":"",
        "direccion":"",
        "departamento":"",
        "municipio": "",
        "id" : INT  id del usuario que esta logueado
    }
    ```
* Salida
    ```
    {
        'msj': 'Sede actualizada!'
    }
    ```

Eliminar Sede DELETE ```18.225.36.192:3000/encargado/deleteSede```
* Entrada
    ```
    {
        "id" : INT  id de la sede a eliminar
    }
    ```
* Salida
    ```
    {
        'msj': 'Sede eliminada!'
    }
    ```

Ver Sedes GET ```18.225.36.192:3000/encargado/sedes```
* Salida
    ```
    [
        {
            "id" : int,
            "Sede":"",
            "Direccion":"",
            "Departamento":"",
            "Municipio": "",
            "Encargado": ""
        },
        {
            "id" : int,
            "Sede":"",
            "Direccion":"",
            "Departamento":"",
            "Municipio": "",
            "Encargado": ""
        }
    ]
    ```

    Ver Sedes por Usuario GET ```18.225.36.192:3000/encargado/sede:id```
    id del usuario que esta logueado
* Salida
    ```
    [
        {
            "id" : int,
            "Sede":"",
            "Direccion":"",
            "Departamento":"",
            "Municipio": "",
            "Encargado": ""
        },
        {
            "id" : int,
            "Sede":"",
            "Direccion":"",
            "Departamento":"",
            "Municipio": "",
            "Encargado": ""
        }
    ]
    ```
***
***
***

Crear Bodega POST ```18.225.36.192:3000/encargado/crearBodega```
* Entrada
    ```
    {
        "nombre":"",
        "descripcion":"",
        "estado":"",
        "sede": int id de la sede a la que pertenecera la bodega,
        "usuario" : id del usuario logueado
    }
    ```
* Salida
    ```
    {
        'msj': 'Sede guardada con exito!'
    }
    ```

Ver Bodegas GET ```18.225.36.192:3000/encargado/bodegas```
* Salida
    ```
    [
        {
            "id": int,
            "Bodega": "",
            "Descripcion": "",
            "Estado": "",
            "Sede": "",
            "Encargado": ""
        },
        {
            "id": int,
            "Bodega": "",
            "Descripcion": "",
            "Estado": "",
            "Sede": "",
            "Encargado": ""
        }
    ]
    ```

Ver Bodegas por usuario GET ```18.225.36.192:3000/encargado/bodegasUser:id```
 id del usuario logueado
* Salida
    ```
    [
        {
            "id": int,
            "Bodega": "",
            "Descripcion": "",
            "Estado": "",
            "Sede": "",
            "Encargado": ""
        },
        {
            "id": int,
            "Bodega": "",
            "Descripcion": "",
            "Estado": "",
            "Sede": "",
            "Encargado": ""
        }
    ]
    ```

Ver Bodegas por sede GET ```18.225.36.192:3000/encargado/bodegasSede:id```
id de la sede a detallar sus bodegas
* Salida
    ```
    [
        {
            "id": int,
            "Bodega": "",
            "Descripcion": "",
            "Estado": "",
            "Sede": "",
            "Encargado": ""
        },
        {
            "id": int,
            "Bodega": "",
            "Descripcion": "",
            "Estado": "",
            "Sede": "",
            "Encargado": ""
        }
    ]
    ```

### Endpoint /registrarCliente

`POST 18.225.36.192:3000/registrarCliente` 

```
{
	"nombre": "",
	"nit": "",
	"dpi": (Según la BD double),
	"direccion": "",
	"sede": INT
}
```

Salida:

```
{
	"descripcion":"Usuario registrado exitosamente."
}
```

### Endopoint /registrarVenta

`POST 18.225.36.192:3000/registrarVenta` 

```
{
	"fechafacturacion": "",
	"fechaentrega": "",
	"usuario": INT,
	"cliente": INT,
	"Productos": [
		{
			"Descripcion": "",
			"Cantidad": "",
			"producto": INT,
			"idventa": INT
		},..., { } (lista de productos...)
	]
}
```

Salida:

```
{ 
	"descripcion":"Venta registrada exitosamente."
}
```

### Endopoint /actualizarInventario

❗ Usuario con rol bodeguero

`POST 18.225.36.192:3000/actualizarInventario`

```
{
	"inventario": INT
	"cantidad": DOUBLE
}
```

Salida:

```
{
	descripcion":"Inventario actualizado exitosamente.
}
```

### Endpoint /solicitarTransferencia

❗ Usuario con rol bodeguero

`POST 18.225.36.192:3000/solicitarTransferencia`

```
{
	"id": INT, 
	"descripcion": ""
   "estado": "", 
   "creador": "",
   "bodega": INT
}
```

Salida:

```
{ 
	"descripcion":"Solicitud de transferencia realizada."
}
```

