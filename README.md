# Practicas Intermedias
Repositorio de Practicas Intermedias

## Integrantes
Integrantes de desarrollo
| Nombre | Carnet | Correo |
|:--------------|:-------------:|:--------------:|
| Selvin Aragón | 201701133 | selvin.lisandro@gmail.com |
| Luis Arana | 201700988 | luarana631@gmail.com |
| Edy Galicia | 201709029 | edyfgalicia@gmail.com |
| Brandon Vargas | 201709343 | vargasbrandon805@gmail.com |
| Luis Pineda | 201700995 | luispinedagonzalez33@gmail.com |
| Gabriela Raymundo | 201500332 | xiorb.2@gmail.com |
| Yadira Ferrer | 201503442 | yyfm.95@gmail.com |
| Ramon Osvaldo Patzán Caballeros | 201216022 | ramon.os.pc@gmail.com|


## Tabla de Prácticas y Proyectos
1. Proyecto

### Proyecto 
***
Aplicación web orientada a la facturación de la misma con una base de datos centralizada para 
poder llevar un mejor control del crecimiento de cada sede.

## Distribución del Repositorio
> El repositorio se distribuyó por carpetas de los diferentes
> proyectos y prácticas que se realizaron en el transcurso.
> del curso de practicas intermedias.

