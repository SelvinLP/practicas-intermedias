(window["webpackJsonp"] = window["webpackJsonp"] || []).push([["main"],{

/***/ "/ztn":
/*!******************************************************************************************!*\
  !*** ./node_modules/raw-loader/dist/cjs.js!./src/app/layouts/admin/admin.component.html ***!
  \******************************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = ("<!-- Sidenav -->\r\n<app-sidebar-admin></app-sidebar-admin>\r\n<div class=\"main-content\">\r\n  <!-- Top navbar -->\r\n \r\n  <!-- Pages -->\r\n  <router-outlet></router-outlet>\r\n  <div class=\"container-fluid\">\r\n    <app-footer></app-footer>\r\n  </div>\r\n</div>");

/***/ }),

/***/ 0:
/*!***************************!*\
  !*** multi ./src/main.ts ***!
  \***************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

module.exports = __webpack_require__(/*! C:\Users\Luis Pineda\Desktop\PRACTICAS\practicas-intermedias\App-Practicas\src\main.ts */"zUnb");


/***/ }),

/***/ "3ASV":
/*!*******************************************************************************************************************!*\
  !*** ./node_modules/raw-loader/dist/cjs.js!./src/app/components/sidebar-vendedor/sidebar-vendedor.component.html ***!
  \*******************************************************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = ("<nav class=\"navbar navbar-vertical navbar-expand-md navbar-light bg-white\" id=\"sidenav-main\">\r\n    <div class=\"container-fluid\">\r\n      <!-- Toggler -->\r\n      <button class=\"navbar-toggler\" type=\"button\" (click)=\"isCollapsed=!isCollapsed\"\r\n         aria-controls=\"sidenav-collapse-main\">\r\n        <span class=\"navbar-toggler-icon\"></span>\r\n      </button>\r\n      <!-- Brand -->\r\n      <a class=\"navbar-brand pt-0\" routerLinkActive=\"active\">\r\n        <img src=\"https://us.123rf.com/450wm/dragomirescu/dragomirescu1807/dragomirescu180700290/104917812-gold-alphabet-letter-sb-s-b-logo-combination-design-suitable-for-a-company-or-business.jpg\" class=\"navbar-brand-img\" alt=\"...\">\r\n      </a>\r\n      <!-- User -->\r\n      <ul class=\"nav align-items-center d-md-none\">\r\n        <li class=\"nav-item\" ngbDropdown placement=\"bottom-right\">\r\n          <a class=\"nav-link nav-link-icon\" role=\"button\" ngbDropdownToggle>\r\n            <i class=\"ni ni-bell-55\"></i>\r\n          </a>\r\n          <div class=\"dropdown-menu-arrow dropdown-menu-right\" ngbDropdownMenu>\r\n            <a class=\"dropdown-item\" href=\"javascript:void(0)\">Action</a>\r\n            <a class=\"dropdown-item\" href=\"javascript:void(0)\">Another action</a>\r\n            <div class=\"dropdown-divider\"></div>\r\n            <a class=\"dropdown-item\" href=\"javascript:void(0)\">Something else here</a>\r\n          </div>\r\n        </li>\r\n        <li class=\"nav-item\" ngbDropdown placement=\"bottom-right\">\r\n          <a class=\"nav-link\" role=\"button\" ngbDropdownToggle>\r\n            <div class=\"media align-items-center\">\r\n              <span class=\"avatar avatar-sm rounded-circle\">\r\n                <img alt=\"Image placeholder\" src=\"./assets/img/theme/team-1-800x800.jpg\">\r\n              </span>\r\n            </div>\r\n          </a>\r\n          <div class=\"dropdown-menu-arrow dropdown-menu-right\" ngbDropdownMenu>\r\n            <div class=\" dropdown-header noti-title\">\r\n              <h6 class=\"text-overflow m-0\">Welcome!</h6>\r\n            </div>\r\n            <a routerLinkActive=\"active\" [routerLink]=\"['/user-profile']\" class=\"dropdown-item\">\r\n              <i class=\"ni ni-single-02\"></i>\r\n              <span>My profile</span>\r\n            </a>\r\n            <a routerLinkActive=\"active\" [routerLink]=\"['/user-profile']\" class=\"dropdown-item\">\r\n              <i class=\"ni ni-settings-gear-65\"></i>\r\n              <span>Settings</span>\r\n            </a>\r\n            <a routerLinkActive=\"active\" [routerLink]=\"['/user-profile']\" class=\"dropdown-item\">\r\n              <i class=\"ni ni-calendar-grid-58\"></i>\r\n              <span>Activity</span>\r\n            </a>\r\n            <a routerLinkActive=\"active\" [routerLink]=\"['/user-profile']\" class=\"dropdown-item\">\r\n              <i class=\"ni ni-support-16\"></i>\r\n              <span>Support</span>\r\n            </a>\r\n            <div class=\"dropdown-divider\"></div>\r\n            <a href=\"#!\" class=\"dropdown-item\">\r\n              <i class=\"ni ni-user-run\"></i>\r\n              <span>Logout</span>\r\n            </a>\r\n          </div>\r\n        </li>\r\n      </ul>\r\n      <!-- Collapse -->\r\n      <div class=\"collapse navbar-collapse\"  [ngbCollapse]=\"isCollapsed\" id=\"sidenav-collapse-main\">\r\n        <!-- Collapse header -->\r\n        <div class=\"navbar-collapse-header d-md-none\">\r\n          <div class=\"row\">\r\n            <div class=\"col-6 collapse-brand\">\r\n              <a  routerLinkActive=\"active\" [routerLink]=\"['/dashboard']\">\r\n                <img src=\"./assets/img/brand/blue.png\">\r\n              </a>\r\n            </div>\r\n            <div class=\"col-6 collapse-close\">\r\n              <button type=\"button\" class=\"navbar-toggler\" (click)=\"isCollapsed=!isCollapsed\">\r\n                <span></span>\r\n                <span></span>\r\n              </button>\r\n            </div>\r\n          </div>\r\n        </div>\r\n        <!-- Form -->\r\n        <form class=\"mt-4 mb-3 d-md-none\">\r\n          <div class=\"input-group input-group-rounded input-group-merge\">\r\n            <input type=\"search\" class=\"form-control form-control-rounded form-control-prepended\" placeholder=\"Search\" aria-label=\"Search\">\r\n            <div class=\"input-group-prepend\">\r\n              <div class=\"input-group-text\">\r\n                <span class=\"fa fa-search\"></span>\r\n              </div>\r\n            </div>\r\n          </div>\r\n        </form>\r\n        <!-- Navigation -->\r\n        <ul class=\"navbar-nav\">\r\n            <li *ngFor=\"let menuItem of menuItems\" class=\"{{menuItem.class}} nav-item\">\r\n                <a routerLinkActive=\"active\" [routerLink]=\"[menuItem.path]\" class=\"nav-link\">\r\n                    <i class=\"ni {{menuItem.icon}}\"></i>\r\n                    {{menuItem.title}}\r\n                </a>\r\n            </li>\r\n        </ul>\r\n        <!-- Divider -->\r\n        <hr class=\"my-3\">\r\n        <!-- Heading -->\r\n       \r\n       \r\n      </div>\r\n    </div>\r\n  </nav>\r\n  ");

/***/ }),

/***/ "3TnI":
/*!**************************************************************!*\
  !*** ./src/app/layouts/auth-layout/auth-layout.component.ts ***!
  \**************************************************************/
/*! exports provided: AuthLayoutComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "AuthLayoutComponent", function() { return AuthLayoutComponent; });
/* harmony import */ var _raw_loader_auth_layout_component_html__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! raw-loader!./auth-layout.component.html */ "WSaj");
/* harmony import */ var _auth_layout_component_scss__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./auth-layout.component.scss */ "FkQd");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/core */ "fXoL");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/router */ "tyNb");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};




var AuthLayoutComponent = /** @class */ (function () {
    function AuthLayoutComponent(router) {
        this.router = router;
        this.test = new Date();
        this.isCollapsed = true;
    }
    AuthLayoutComponent.prototype.ngOnInit = function () {
        var _this = this;
        var html = document.getElementsByTagName("html")[0];
        html.classList.add("auth-layout");
        var body = document.getElementsByTagName("body")[0];
        body.classList.add("bg-default");
        this.router.events.subscribe(function (event) {
            _this.isCollapsed = true;
        });
    };
    AuthLayoutComponent.prototype.ngOnDestroy = function () {
        var html = document.getElementsByTagName("html")[0];
        html.classList.remove("auth-layout");
        var body = document.getElementsByTagName("body")[0];
        body.classList.remove("bg-default");
    };
    AuthLayoutComponent.ctorParameters = function () { return [
        { type: _angular_router__WEBPACK_IMPORTED_MODULE_3__["Router"] }
    ]; };
    AuthLayoutComponent = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_2__["Component"])({
            selector: 'app-auth-layout',
            template: _raw_loader_auth_layout_component_html__WEBPACK_IMPORTED_MODULE_0__["default"],
            styles: [_auth_layout_component_scss__WEBPACK_IMPORTED_MODULE_1__["default"]]
        }),
        __metadata("design:paramtypes", [_angular_router__WEBPACK_IMPORTED_MODULE_3__["Router"]])
    ], AuthLayoutComponent);
    return AuthLayoutComponent;
}());



/***/ }),

/***/ "6vv9":
/*!***************************************************************************!*\
  !*** ./src/app/components/navbar-bodeguero/navbar-bodeguero.component.ts ***!
  \***************************************************************************/
/*! exports provided: NavbarBodegueroComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "NavbarBodegueroComponent", function() { return NavbarBodegueroComponent; });
/* harmony import */ var _raw_loader_navbar_bodeguero_component_html__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! raw-loader!./navbar-bodeguero.component.html */ "NdXD");
/* harmony import */ var _navbar_bodeguero_component_css__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./navbar-bodeguero.component.css */ "7T+n");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/core */ "fXoL");
/* harmony import */ var _sidebar_sidebar_component__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ../sidebar/sidebar.component */ "zBoC");
/* harmony import */ var _angular_common__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @angular/common */ "ofXK");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! @angular/router */ "tyNb");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};






var NavbarBodegueroComponent = /** @class */ (function () {
    function NavbarBodegueroComponent(location, element, router) {
        this.element = element;
        this.router = router;
        this.location = location;
    }
    NavbarBodegueroComponent.prototype.ngOnInit = function () {
        this.listTitles = _sidebar_sidebar_component__WEBPACK_IMPORTED_MODULE_3__["ROUTES"].filter(function (listTitle) { return listTitle; });
    };
    NavbarBodegueroComponent.prototype.getTitle = function () {
        var titlee = this.location.prepareExternalUrl(this.location.path());
        if (titlee.charAt(0) === '#') {
            titlee = titlee.slice(1);
        }
        for (var item = 0; item < this.listTitles.length; item++) {
            if (this.listTitles[item].path === titlee) {
                return this.listTitles[item].title;
            }
        }
        return titlee.replace('/', '');
    };
    NavbarBodegueroComponent.prototype.btnLogout = function () {
        localStorage.removeItem('log');
        this.router.navigate(['/login']);
    };
    NavbarBodegueroComponent.ctorParameters = function () { return [
        { type: _angular_common__WEBPACK_IMPORTED_MODULE_4__["Location"] },
        { type: _angular_core__WEBPACK_IMPORTED_MODULE_2__["ElementRef"] },
        { type: _angular_router__WEBPACK_IMPORTED_MODULE_5__["Router"] }
    ]; };
    NavbarBodegueroComponent = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_2__["Component"])({
            selector: 'app-navbar-bodeguero',
            template: _raw_loader_navbar_bodeguero_component_html__WEBPACK_IMPORTED_MODULE_0__["default"],
            styles: [_navbar_bodeguero_component_css__WEBPACK_IMPORTED_MODULE_1__["default"]]
        }),
        __metadata("design:paramtypes", [_angular_common__WEBPACK_IMPORTED_MODULE_4__["Location"], _angular_core__WEBPACK_IMPORTED_MODULE_2__["ElementRef"], _angular_router__WEBPACK_IMPORTED_MODULE_5__["Router"]])
    ], NavbarBodegueroComponent);
    return NavbarBodegueroComponent;
}());



/***/ }),

/***/ "7FMQ":
/*!***************************************************************************************************************!*\
  !*** ./node_modules/raw-loader/dist/cjs.js!./src/app/pages/vendedor/sales-report/sales-report.component.html ***!
  \***************************************************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = ("<div class=\"header pb-8 pt-5 pt-lg-8 d-flex align-items-center\"\r\n  style=\"min-height: 200px; background-image: url(https://geoinnova.org/cursos/wp-content/uploads/2016/10/1-11-1024x551.jpg); background-size: cover; background-position: center top;\">\r\n  <!-- Mask -->\r\n  <span class=\"mask bg-gradient-danger opacity-8\"></span>\r\n\r\n</div>\r\n<div class=\"container-fluid mt--7\">\r\n  <div class=\"row\">\r\n    <div class=\"col\">\r\n      <div class=\"card shadow\">\r\n        <div class=\"card-header border-0\">\r\n          <h3 class=\"mb-0\">Ventas registradas</h3>\r\n        </div>\r\n        <div class=\"table-responsive\">\r\n          <table class=\"table align-items-center table-flush\">\r\n            <thead class=\"thead-light\">\r\n              <tr>\r\n                <th scope=\"col\">ID Venta</th>\r\n                <th scope=\"col\">Fecha Entrega</th>\r\n                <th scope=\"col\">Fecha Factura</th>\r\n                <th scope=\"col\">Estado</th>\r\n                <th scope=\"col\">Total</th>\r\n                <th scope=\"col\">Ubicación</th>\r\n                <th scope=\"col\">ID Cliente</th>\r\n                <th scope=\"col\">ID Repartidor</th>\r\n              </tr>\r\n            </thead>\r\n            <tbody>\r\n              <tr *ngFor=\"let venta of ventas.ventas\">\r\n                <th scope=\"row\">\r\n                      <span class=\"mb-0 text-sm\">{{venta.idVenta}}</span>\r\n                </th>\r\n                <td>\r\n                  {{venta.FechaEntrega}}\r\n                </td>\r\n                <td>\r\n                    {{venta.FechaFacturacion}}\r\n                </td>\r\n                <td>\r\n                  {{venta.Estado}}\r\n                </td>\r\n                <td>\r\n                  {{venta.Total}}\r\n                </td>\r\n                <td>\r\n                  {{venta.Ubicacion}}\r\n                </td>\r\n                <td>\r\n                  {{venta.Cliente_idCliente}}\r\n                </td>\r\n                <td>\r\n                  {{venta.id_repart}}\r\n                </td>\r\n              </tr>\r\n            </tbody>\r\n          </table>\r\n        </div>\r\n      </div>\r\n    </div>\r\n  </div>\r\n</div>\r\n");

/***/ }),

/***/ "7T+n":
/*!****************************************************************************!*\
  !*** ./src/app/components/navbar-bodeguero/navbar-bodeguero.component.css ***!
  \****************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = ("\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IiIsImZpbGUiOiJzcmMvYXBwL2NvbXBvbmVudHMvbmF2YmFyLWJvZGVndWVyby9uYXZiYXItYm9kZWd1ZXJvLmNvbXBvbmVudC5jc3MifQ== */");

/***/ }),

/***/ "AK6u":
/*!********************************************************************************************************!*\
  !*** ./node_modules/raw-loader/dist/cjs.js!./src/app/layouts/admin-layout/admin-layout.component.html ***!
  \********************************************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = ("<!-- Sidenav -->\r\n<app-sidebar></app-sidebar>\r\n<div class=\"main-content\">\r\n  <!-- Top navbar -->\r\n  <app-navbar></app-navbar>\r\n  <!-- Pages -->\r\n  <router-outlet></router-outlet>\r\n  <div class=\"container-fluid\">\r\n    <app-footer></app-footer>\r\n  </div>\r\n</div>\r\n");

/***/ }),

/***/ "AytR":
/*!*****************************************!*\
  !*** ./src/environments/environment.ts ***!
  \*****************************************/
/*! exports provided: environment */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "environment", function() { return environment; });
// This file can be replaced during build by using the `fileReplacements` array.
// `ng build --prod` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.
var environment = {
    production: false
};
/*
 * For easier debugging in development mode, you can import the following file
 * to ignore zone related error stack frames such as `zone.run`, `zoneDelegate.invokeTask`.
 *
 * This import should be commented out in production mode because it will have a negative impact
 * on performance if an error is thrown.
 */
// import 'zone.js/dist/zone-error';  // Included with Angular CLI.


/***/ }),

/***/ "C75L":
/*!***************************************************************************!*\
  !*** ./src/app/components/sidebar-vendedor/sidebar-vendedor.component.ts ***!
  \***************************************************************************/
/*! exports provided: ROUTES, SidebarVendedorComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "ROUTES", function() { return ROUTES; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "SidebarVendedorComponent", function() { return SidebarVendedorComponent; });
/* harmony import */ var _raw_loader_sidebar_vendedor_component_html__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! raw-loader!./sidebar-vendedor.component.html */ "3ASV");
/* harmony import */ var _sidebar_vendedor_component_css__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./sidebar-vendedor.component.css */ "v5Bj");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/core */ "fXoL");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/router */ "tyNb");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};




var ROUTES = [
    { path: '/profile-vendedor', title: 'User profile', icon: 'ni-single-02 text-yellow', class: '' },
    { path: '/form-clients', title: 'Register client', icon: 'ni-circle-08 text-yellow', class: '' },
    { path: '/form-sales', title: 'Register sale', icon: 'ni-books text-yellow', class: '' },
    { path: '/sales-report', title: 'Sales report', icon: 'ni-money-coins text-yellow', class: '' },
];
var SidebarVendedorComponent = /** @class */ (function () {
    function SidebarVendedorComponent(router) {
        this.router = router;
        this.isCollapsed = true;
    }
    SidebarVendedorComponent.prototype.ngOnInit = function () {
        var _this = this;
        this.menuItems = ROUTES.filter(function (menuItem) { return menuItem; });
        this.router.events.subscribe(function (event) {
            _this.isCollapsed = true;
        });
    };
    SidebarVendedorComponent.ctorParameters = function () { return [
        { type: _angular_router__WEBPACK_IMPORTED_MODULE_3__["Router"] }
    ]; };
    SidebarVendedorComponent = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_2__["Component"])({
            selector: 'app-sidebar-vendedor',
            template: _raw_loader_sidebar_vendedor_component_html__WEBPACK_IMPORTED_MODULE_0__["default"],
            styles: [_sidebar_vendedor_component_css__WEBPACK_IMPORTED_MODULE_1__["default"]]
        }),
        __metadata("design:paramtypes", [_angular_router__WEBPACK_IMPORTED_MODULE_3__["Router"]])
    ], SidebarVendedorComponent);
    return SidebarVendedorComponent;
}());



/***/ }),

/***/ "Cwie":
/*!*****************************************************************************!*\
  !*** ./src/app/components/sidebar-bodeguero/sidebar-bodeguero.component.ts ***!
  \*****************************************************************************/
/*! exports provided: ROUTES, SidebarBodegueroComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "ROUTES", function() { return ROUTES; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "SidebarBodegueroComponent", function() { return SidebarBodegueroComponent; });
/* harmony import */ var _raw_loader_sidebar_bodeguero_component_html__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! raw-loader!./sidebar-bodeguero.component.html */ "l0hG");
/* harmony import */ var _sidebar_bodeguero_component_css__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./sidebar-bodeguero.component.css */ "HSUz");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/core */ "fXoL");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/router */ "tyNb");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};




var ROUTES = [
    { path: '/profile-bodeguero', title: 'User profile', icon: 'ni-single-02 text-yellow', class: '' },
    { path: '/actualizacion-bodeguero', title: 'Actualizacion Inventario', icon: 'ni-single-02 text-yellow', class: '' },
    { path: '/transferencia-bodeguero', title: 'Transferencias', icon: 'ni-single-02 text-yellow', class: '' },
    { path: '/visualizar-interna-bodeguero', title: 'Visualizar', icon: 'ni-single-02 text-yellow', class: '' },
];
var SidebarBodegueroComponent = /** @class */ (function () {
    function SidebarBodegueroComponent(router) {
        this.router = router;
        this.isCollapsed = true;
    }
    SidebarBodegueroComponent.prototype.ngOnInit = function () {
        var _this = this;
        this.menuItems = ROUTES.filter(function (menuItem) { return menuItem; });
        this.router.events.subscribe(function (event) {
            _this.isCollapsed = true;
        });
    };
    SidebarBodegueroComponent.ctorParameters = function () { return [
        { type: _angular_router__WEBPACK_IMPORTED_MODULE_3__["Router"] }
    ]; };
    SidebarBodegueroComponent = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_2__["Component"])({
            selector: 'app-sidebar-bodeguero',
            template: _raw_loader_sidebar_bodeguero_component_html__WEBPACK_IMPORTED_MODULE_0__["default"],
            styles: [_sidebar_bodeguero_component_css__WEBPACK_IMPORTED_MODULE_1__["default"]]
        }),
        __metadata("design:paramtypes", [_angular_router__WEBPACK_IMPORTED_MODULE_3__["Router"]])
    ], SidebarBodegueroComponent);
    return SidebarBodegueroComponent;
}());



/***/ }),

/***/ "FkQd":
/*!****************************************************************!*\
  !*** ./src/app/layouts/auth-layout/auth-layout.component.scss ***!
  \****************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = ("\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IiIsImZpbGUiOiJzcmMvYXBwL2xheW91dHMvYXV0aC1sYXlvdXQvYXV0aC1sYXlvdXQuY29tcG9uZW50LnNjc3MifQ== */");

/***/ }),

/***/ "G9cc":
/*!*********************************************************!*\
  !*** ./src/app/layouts/vendedor/vendedor.component.css ***!
  \*********************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = ("\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IiIsImZpbGUiOiJzcmMvYXBwL2xheW91dHMvdmVuZGVkb3IvdmVuZGVkb3IuY29tcG9uZW50LmNzcyJ9 */");

/***/ }),

/***/ "GXi5":
/*!***********************************************************************************************************************!*\
  !*** ./node_modules/raw-loader/dist/cjs.js!./src/app/components/sidebar-repartidor/sidebar-repartidor.component.html ***!
  \***********************************************************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = ("<nav class=\"navbar navbar-vertical navbar-expand-md navbar-light bg-white\" id=\"sidenav-main\">\r\n    <div class=\"container-fluid\">\r\n      <!-- Toggler -->\r\n      <button class=\"navbar-toggler\" type=\"button\" (click)=\"isCollapsed=!isCollapsed\"\r\n         aria-controls=\"sidenav-collapse-main\">\r\n        <span class=\"navbar-toggler-icon\"></span>\r\n      </button>\r\n      <!-- Brand -->\r\n      <a class=\"navbar-brand pt-0\" routerLinkActive=\"active\">\r\n        <img src=\"https://us.123rf.com/450wm/dragomirescu/dragomirescu1807/dragomirescu180700290/104917812-gold-alphabet-letter-sb-s-b-logo-combination-design-suitable-for-a-company-or-business.jpg\" class=\"navbar-brand-img\" alt=\"...\">\r\n      </a>\r\n      <!-- User -->\r\n      <ul class=\"nav align-items-center d-md-none\">\r\n        <li class=\"nav-item\" ngbDropdown placement=\"bottom-right\">\r\n          <a class=\"nav-link nav-link-icon\" role=\"button\" ngbDropdownToggle>\r\n            <i class=\"ni ni-bell-55\"></i>\r\n          </a>\r\n          <div class=\"dropdown-menu-arrow dropdown-menu-right\" ngbDropdownMenu>\r\n            <a class=\"dropdown-item\" href=\"javascript:void(0)\">Action</a>\r\n            <a class=\"dropdown-item\" href=\"javascript:void(0)\">Another action</a>\r\n            <div class=\"dropdown-divider\"></div>\r\n            <a class=\"dropdown-item\" href=\"javascript:void(0)\">Something else here</a>\r\n          </div>\r\n        </li>\r\n        <li class=\"nav-item\" ngbDropdown placement=\"bottom-right\">\r\n          <a class=\"nav-link\" role=\"button\" ngbDropdownToggle>\r\n            <div class=\"media align-items-center\">\r\n              <span class=\"avatar avatar-sm rounded-circle\">\r\n                <img alt=\"Image placeholder\" src=\"./assets/img/theme/team-1-800x800.jpg\">\r\n              </span>\r\n            </div>\r\n          </a>\r\n          <div class=\"dropdown-menu-arrow dropdown-menu-right\" ngbDropdownMenu>\r\n            <div class=\" dropdown-header noti-title\">\r\n              <h6 class=\"text-overflow m-0\">Welcome!</h6>\r\n            </div>\r\n         \r\n            <div class=\"dropdown-divider\"></div>\r\n            <a href=\"#!\" class=\"dropdown-item\">\r\n              <i class=\"ni ni-user-run\"></i>\r\n              <span>Logout</span>\r\n            </a>\r\n          </div>\r\n        </li>\r\n      </ul>\r\n      <!-- Collapse -->\r\n      <div class=\"collapse navbar-collapse\"  [ngbCollapse]=\"isCollapsed\" id=\"sidenav-collapse-main\">\r\n        <!-- Collapse header -->\r\n        <div class=\"navbar-collapse-header d-md-none\">\r\n          <div class=\"row\">\r\n            <div class=\"col-6 collapse-brand\">\r\n              <a  routerLinkActive=\"active\" [routerLink]=\"['/dashboard']\">\r\n                <img src=\"./assets/img/brand/blue.png\">\r\n              </a>\r\n            </div>\r\n            <div class=\"col-6 collapse-close\">\r\n              <button type=\"button\" class=\"navbar-toggler\" (click)=\"isCollapsed=!isCollapsed\">\r\n                <span></span>\r\n                <span></span>\r\n              </button>\r\n            </div>\r\n          </div>\r\n        </div>\r\n        <!-- Form -->\r\n        <form class=\"mt-4 mb-3 d-md-none\">\r\n          <div class=\"input-group input-group-rounded input-group-merge\">\r\n            <input type=\"search\" class=\"form-control form-control-rounded form-control-prepended\" placeholder=\"Search\" aria-label=\"Search\">\r\n            <div class=\"input-group-prepend\">\r\n              <div class=\"input-group-text\">\r\n                <span class=\"fa fa-search\"></span>\r\n              </div>\r\n            </div>\r\n          </div>\r\n        </form>\r\n        <!-- Navigation -->\r\n        <ul class=\"navbar-nav\">\r\n            <li *ngFor=\"let menuItem of menuItems\" class=\"{{menuItem.class}} nav-item\">\r\n                <a routerLinkActive=\"active\" [routerLink]=\"[menuItem.path]\" class=\"nav-link\">\r\n                    <i class=\"ni {{menuItem.icon}}\"></i>\r\n                    {{menuItem.title}}\r\n                </a>\r\n            </li>\r\n        </ul>\r\n        <!-- Divider -->\r\n        <hr class=\"my-3\">\r\n        <!-- Heading -->\r\n       \r\n       \r\n      </div>\r\n    </div>\r\n  </nav>\r\n  ");

/***/ }),

/***/ "HSUz":
/*!******************************************************************************!*\
  !*** ./src/app/components/sidebar-bodeguero/sidebar-bodeguero.component.css ***!
  \******************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = ("\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IiIsImZpbGUiOiJzcmMvYXBwL2NvbXBvbmVudHMvc2lkZWJhci1ib2RlZ3Vlcm8vc2lkZWJhci1ib2RlZ3Vlcm8uY29tcG9uZW50LmNzcyJ9 */");

/***/ }),

/***/ "Hysb":
/*!*********************************************************************!*\
  !*** ./src/app/components/sidebar-admin/sidebar-admin.component.ts ***!
  \*********************************************************************/
/*! exports provided: ROUTES, SidebarAdminComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "ROUTES", function() { return ROUTES; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "SidebarAdminComponent", function() { return SidebarAdminComponent; });
/* harmony import */ var _raw_loader_sidebar_admin_component_html__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! raw-loader!./sidebar-admin.component.html */ "zRj8");
/* harmony import */ var _sidebar_admin_component_css__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./sidebar-admin.component.css */ "TK9H");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/core */ "fXoL");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/router */ "tyNb");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};




var ROUTES = [
    { path: '/form-users', title: 'users', icon: 'ni-tv-2 text-primary', class: '' },
    { path: '/view-sedes', title: 'sedes', icon: 'ni-tv-2 text-primary', class: '' },
    { path: '/rol', title: 'rol', icon: 'ni-tv-2 text-primary', class: '' },
];
var SidebarAdminComponent = /** @class */ (function () {
    function SidebarAdminComponent(router) {
        this.router = router;
        this.isCollapsed = true;
    }
    SidebarAdminComponent.prototype.ngOnInit = function () {
        var _this = this;
        this.menuItems = ROUTES.filter(function (menuItem) { return menuItem; });
        this.router.events.subscribe(function (event) {
            _this.isCollapsed = true;
        });
    };
    SidebarAdminComponent.prototype.btnLogout = function () {
        localStorage.removeItem('log');
        this.router.navigate(['/login']);
    };
    SidebarAdminComponent.ctorParameters = function () { return [
        { type: _angular_router__WEBPACK_IMPORTED_MODULE_3__["Router"] }
    ]; };
    SidebarAdminComponent = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_2__["Component"])({
            selector: 'app-sidebar-admin',
            template: _raw_loader_sidebar_admin_component_html__WEBPACK_IMPORTED_MODULE_0__["default"],
            styles: [_sidebar_admin_component_css__WEBPACK_IMPORTED_MODULE_1__["default"]]
        }),
        __metadata("design:paramtypes", [_angular_router__WEBPACK_IMPORTED_MODULE_3__["Router"]])
    ], SidebarAdminComponent);
    return SidebarAdminComponent;
}());



/***/ }),

/***/ "J6UQ":
/*!*************************************************************!*\
  !*** ./src/app/layouts/repartidor/repartidor.component.css ***!
  \*************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = ("\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IiIsImZpbGUiOiJzcmMvYXBwL2xheW91dHMvcmVwYXJ0aWRvci9yZXBhcnRpZG9yLmNvbXBvbmVudC5jc3MifQ== */");

/***/ }),

/***/ "KKA+":
/*!*************************************************************************************************!*\
  !*** ./node_modules/raw-loader/dist/cjs.js!./src/app/components/sidebar/sidebar.component.html ***!
  \*************************************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = ("<nav class=\"navbar navbar-vertical navbar-expand-md navbar-light bg-white\" id=\"sidenav-main\">\r\n  <div class=\"container-fluid\">\r\n    <!-- Toggler -->\r\n    <button class=\"navbar-toggler\" type=\"button\" (click)=\"isCollapsed=!isCollapsed\"\r\n       aria-controls=\"sidenav-collapse-main\">\r\n      <span class=\"navbar-toggler-icon\"></span>\r\n    </button>\r\n    <!-- Brand -->\r\n    <a class=\"navbar-brand pt-0\" routerLinkActive=\"active\">\r\n      <img src=\"https://us.123rf.com/450wm/dragomirescu/dragomirescu1807/dragomirescu180700290/104917812-gold-alphabet-letter-sb-s-b-logo-combination-design-suitable-for-a-company-or-business.jpg\" class=\"navbar-brand-img\" alt=\"...\">\r\n    </a>\r\n    <!-- User -->\r\n    <ul class=\"nav align-items-center d-md-none\">\r\n      <li class=\"nav-item\" ngbDropdown placement=\"bottom-right\">\r\n        <a class=\"nav-link nav-link-icon\" role=\"button\" ngbDropdownToggle>\r\n          <i class=\"ni ni-bell-55\"></i>\r\n        </a>\r\n        <div class=\"dropdown-menu-arrow dropdown-menu-right\" ngbDropdownMenu>\r\n          <a class=\"dropdown-item\" href=\"javascript:void(0)\">Action</a>\r\n          <a class=\"dropdown-item\" href=\"javascript:void(0)\">Another action</a>\r\n          <div class=\"dropdown-divider\"></div>\r\n          <a class=\"dropdown-item\" href=\"javascript:void(0)\">Something else here</a>\r\n        </div>\r\n      </li>\r\n      <li class=\"nav-item\" ngbDropdown placement=\"bottom-right\">\r\n        <a class=\"nav-link\" role=\"button\" ngbDropdownToggle>\r\n          <div class=\"media align-items-center\">\r\n            <span class=\"avatar avatar-sm rounded-circle\">\r\n              <img alt=\"Image placeholder\" src=\"./assets/img/theme/team-1-800x800.jpg\">\r\n            </span>\r\n          </div>\r\n        </a>\r\n        <div class=\"dropdown-menu-arrow dropdown-menu-right\" ngbDropdownMenu>\r\n          <div class=\" dropdown-header noti-title\">\r\n            <h6 class=\"text-overflow m-0\">Welcome!</h6>\r\n          </div>\r\n          <a routerLinkActive=\"active\" [routerLink]=\"['/user-profile']\" class=\"dropdown-item\">\r\n            <i class=\"ni ni-single-02\"></i>\r\n            <span>My profile</span>\r\n          </a>\r\n          <a routerLinkActive=\"active\" [routerLink]=\"['/user-profile']\" class=\"dropdown-item\">\r\n            <i class=\"ni ni-settings-gear-65\"></i>\r\n            <span>Settings</span>\r\n          </a>\r\n          <a routerLinkActive=\"active\" [routerLink]=\"['/user-profile']\" class=\"dropdown-item\">\r\n            <i class=\"ni ni-calendar-grid-58\"></i>\r\n            <span>Activity</span>\r\n          </a>\r\n          <a routerLinkActive=\"active\" [routerLink]=\"['/user-profile']\" class=\"dropdown-item\">\r\n            <i class=\"ni ni-support-16\"></i>\r\n            <span>Support</span>\r\n          </a>\r\n          <div class=\"dropdown-divider\"></div>\r\n          <a href=\"#!\" class=\"dropdown-item\">\r\n            <i class=\"ni ni-user-run\"></i>\r\n            <span>Logout</span>\r\n          </a>\r\n        </div>\r\n      </li>\r\n    </ul>\r\n    <!-- Collapse -->\r\n    <div class=\"collapse navbar-collapse\"  [ngbCollapse]=\"isCollapsed\" id=\"sidenav-collapse-main\">\r\n      <!-- Collapse header -->\r\n      <div class=\"navbar-collapse-header d-md-none\">\r\n        <div class=\"row\">\r\n          <div class=\"col-6 collapse-brand\">\r\n            <a  routerLinkActive=\"active\" [routerLink]=\"['/dashboard']\">\r\n              <img src=\"./assets/img/brand/blue.png\">\r\n            </a>\r\n          </div>\r\n          <div class=\"col-6 collapse-close\">\r\n            <button type=\"button\" class=\"navbar-toggler\" (click)=\"isCollapsed=!isCollapsed\">\r\n              <span></span>\r\n              <span></span>\r\n            </button>\r\n          </div>\r\n        </div>\r\n      </div>\r\n      <!-- Form -->\r\n      <form class=\"mt-4 mb-3 d-md-none\">\r\n        <div class=\"input-group input-group-rounded input-group-merge\">\r\n          <input type=\"search\" class=\"form-control form-control-rounded form-control-prepended\" placeholder=\"Search\" aria-label=\"Search\">\r\n          <div class=\"input-group-prepend\">\r\n            <div class=\"input-group-text\">\r\n              <span class=\"fa fa-search\"></span>\r\n            </div>\r\n          </div>\r\n        </div>\r\n      </form>\r\n      <!-- Navigation -->\r\n      <ul class=\"navbar-nav\">\r\n          <li *ngFor=\"let menuItem of menuItems\" class=\"{{menuItem.class}} nav-item\">\r\n              <a routerLinkActive=\"active\" [routerLink]=\"[menuItem.path]\" class=\"nav-link\">\r\n                  <i class=\"ni {{menuItem.icon}}\"></i>\r\n                  {{menuItem.title}}\r\n              </a>\r\n          </li>\r\n      </ul>\r\n      <!-- Divider -->\r\n      <hr class=\"my-3\">\r\n      <!-- Heading -->\r\n     \r\n     \r\n    </div>\r\n  </div>\r\n</nav>\r\n");

/***/ }),

/***/ "LmEr":
/*!*******************************************************!*\
  !*** ./src/app/components/footer/footer.component.ts ***!
  \*******************************************************/
/*! exports provided: FooterComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "FooterComponent", function() { return FooterComponent; });
/* harmony import */ var _raw_loader_footer_component_html__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! raw-loader!./footer.component.html */ "WwN9");
/* harmony import */ var _footer_component_scss__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./footer.component.scss */ "yZN6");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/core */ "fXoL");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};



var FooterComponent = /** @class */ (function () {
    function FooterComponent() {
        this.test = new Date();
    }
    FooterComponent.prototype.ngOnInit = function () {
    };
    FooterComponent.ctorParameters = function () { return []; };
    FooterComponent = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_2__["Component"])({
            selector: 'app-footer',
            template: _raw_loader_footer_component_html__WEBPACK_IMPORTED_MODULE_0__["default"],
            styles: [_footer_component_scss__WEBPACK_IMPORTED_MODULE_1__["default"]]
        }),
        __metadata("design:paramtypes", [])
    ], FooterComponent);
    return FooterComponent;
}());



/***/ }),

/***/ "Ls9r":
/*!*********************************************************!*\
  !*** ./src/app/components/navbar/navbar.component.scss ***!
  \*********************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = ("\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IiIsImZpbGUiOiJzcmMvYXBwL2NvbXBvbmVudHMvbmF2YmFyL25hdmJhci5jb21wb25lbnQuc2NzcyJ9 */");

/***/ }),

/***/ "NdXD":
/*!*******************************************************************************************************************!*\
  !*** ./node_modules/raw-loader/dist/cjs.js!./src/app/components/navbar-bodeguero/navbar-bodeguero.component.html ***!
  \*******************************************************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = ("<nav class=\"navbar navbar-top navbar-expand-md navbar-dark\" id=\"navbar-main\">\r\n    <div class=\"container-fluid\">\r\n      <!-- Brand -->\r\n      <a class=\"h4 mb-0 text-white text-uppercase d-none d-lg-inline-block\" routerLinkActive=\"active\" >{{getTitle()}}</a>\r\n      <!-- Form -->\r\n      \r\n      <!-- User -->\r\n      <ul class=\"navbar-nav align-items-center d-none d-md-flex\">\r\n        <li class=\"nav-item\" ngbDropdown placement=\"bottom-right\">\r\n          <a class=\"nav-link pr-0\" role=\"button\" ngbDropdownToggle>\r\n            <div class=\"media align-items-center\">\r\n              <span class=\"avatar avatar-sm rounded-circle\">\r\n                <img alt=\"Image placeholder\" src=\"https://www.softzone.es/app/uploads-softzone.es/2018/04/guest.png\">\r\n              </span>\r\n              <div class=\"media-body ml-2 d-none d-lg-block\">\r\n                <span class=\"mb-0 text-sm  font-weight-bold\">USUARIO</span>\r\n              </div>\r\n            </div>\r\n          </a>\r\n          <div class=\"dropdown-menu-arrow dropdown-menu-right\" ngbDropdownMenu>\r\n            <div class=\" dropdown-header noti-title\">\r\n              <h6 class=\"text-overflow m-0\">Welcome!</h6>\r\n            </div>\r\n                   \r\n            <div class=\"dropdown-divider\"></div>\r\n            <a href=\"#!\" (click) = \"btnLogout()\" class=\"dropdown-item\">\r\n              <i class=\"ni ni-user-run\"></i>\r\n              <span>Logout</span>\r\n            </a>\r\n          </div>\r\n        </li>\r\n      </ul>\r\n    </div>\r\n  </nav>");

/***/ }),

/***/ "P6kD":
/*!****************************************************************!*\
  !*** ./src/app/layouts/admin-layout/admin-layout.component.ts ***!
  \****************************************************************/
/*! exports provided: AdminLayoutComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "AdminLayoutComponent", function() { return AdminLayoutComponent; });
/* harmony import */ var _raw_loader_admin_layout_component_html__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! raw-loader!./admin-layout.component.html */ "AK6u");
/* harmony import */ var _admin_layout_component_scss__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./admin-layout.component.scss */ "vtrx");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/core */ "fXoL");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};



var AdminLayoutComponent = /** @class */ (function () {
    function AdminLayoutComponent() {
    }
    AdminLayoutComponent.prototype.ngOnInit = function () {
    };
    AdminLayoutComponent.ctorParameters = function () { return []; };
    AdminLayoutComponent = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_2__["Component"])({
            selector: 'app-admin-layout',
            template: _raw_loader_admin_layout_component_html__WEBPACK_IMPORTED_MODULE_0__["default"],
            styles: [_admin_layout_component_scss__WEBPACK_IMPORTED_MODULE_1__["default"]]
        }),
        __metadata("design:paramtypes", [])
    ], AdminLayoutComponent);
    return AdminLayoutComponent;
}());



/***/ }),

/***/ "Ptez":
/*!********************************************************************!*\
  !*** ./src/app/pages/vendedor/form-sales/form-sales.component.css ***!
  \********************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = ("\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IiIsImZpbGUiOiJzcmMvYXBwL3BhZ2VzL3ZlbmRlZG9yL2Zvcm0tc2FsZXMvZm9ybS1zYWxlcy5jb21wb25lbnQuY3NzIn0= */");

/***/ }),

/***/ "Qqav":
/*!*******************************************************************************!*\
  !*** ./src/app/components/sidebar-repartidor/sidebar-repartidor.component.ts ***!
  \*******************************************************************************/
/*! exports provided: ROUTES, SidebarRepartidorComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "ROUTES", function() { return ROUTES; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "SidebarRepartidorComponent", function() { return SidebarRepartidorComponent; });
/* harmony import */ var _raw_loader_sidebar_repartidor_component_html__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! raw-loader!./sidebar-repartidor.component.html */ "GXi5");
/* harmony import */ var _sidebar_repartidor_component_css__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./sidebar-repartidor.component.css */ "dB3f");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/core */ "fXoL");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/router */ "tyNb");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};




var ROUTES = [
    { path: '/profile-repartidor', title: 'User profile', icon: 'ni-single-02 text-yellow', class: '' },
    { path: '/repartidor-ventas', title: 'ORDENES', icon: 'ni-single-02 text-yellow', class: '' },
];
var SidebarRepartidorComponent = /** @class */ (function () {
    function SidebarRepartidorComponent(router) {
        this.router = router;
        this.isCollapsed = true;
    }
    SidebarRepartidorComponent.prototype.ngOnInit = function () {
        var _this = this;
        this.menuItems = ROUTES.filter(function (menuItem) { return menuItem; });
        this.router.events.subscribe(function (event) {
            _this.isCollapsed = true;
        });
    };
    SidebarRepartidorComponent.ctorParameters = function () { return [
        { type: _angular_router__WEBPACK_IMPORTED_MODULE_3__["Router"] }
    ]; };
    SidebarRepartidorComponent = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_2__["Component"])({
            selector: 'app-sidebar-repartidor',
            template: _raw_loader_sidebar_repartidor_component_html__WEBPACK_IMPORTED_MODULE_0__["default"],
            styles: [_sidebar_repartidor_component_css__WEBPACK_IMPORTED_MODULE_1__["default"]]
        }),
        __metadata("design:paramtypes", [_angular_router__WEBPACK_IMPORTED_MODULE_3__["Router"]])
    ], SidebarRepartidorComponent);
    return SidebarRepartidorComponent;
}());



/***/ }),

/***/ "R+tk":
/*!**************************************************!*\
  !*** ./src/app/layouts/admin/admin.component.ts ***!
  \**************************************************/
/*! exports provided: AdminComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "AdminComponent", function() { return AdminComponent; });
/* harmony import */ var _raw_loader_admin_component_html__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! raw-loader!./admin.component.html */ "/ztn");
/* harmony import */ var _admin_component_css__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./admin.component.css */ "cefF");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/core */ "fXoL");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};



var AdminComponent = /** @class */ (function () {
    function AdminComponent() {
        this.isCollapsed = true;
    }
    AdminComponent.prototype.ngOnInit = function () {
    };
    AdminComponent.ctorParameters = function () { return []; };
    AdminComponent = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_2__["Component"])({
            selector: 'app-admin',
            template: _raw_loader_admin_component_html__WEBPACK_IMPORTED_MODULE_0__["default"],
            styles: [_admin_component_css__WEBPACK_IMPORTED_MODULE_1__["default"]]
        }),
        __metadata("design:paramtypes", [])
    ], AdminComponent);
    return AdminComponent;
}());



/***/ }),

/***/ "S6iF":
/*!***********************************************************************************************!*\
  !*** ./node_modules/raw-loader/dist/cjs.js!./src/app/components/navbar/navbar.component.html ***!
  \***********************************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = ("<nav class=\"navbar navbar-top navbar-expand-md navbar-dark\" id=\"navbar-main\">\r\n  <div class=\"container-fluid\">\r\n    <!-- Brand -->\r\n    <a class=\"h4 mb-0 text-white text-uppercase d-none d-lg-inline-block\" routerLinkActive=\"active\" >{{getTitle()}}</a>\r\n    <!-- Form -->\r\n    \r\n    <!-- User -->\r\n    <ul class=\"navbar-nav align-items-center d-none d-md-flex\">\r\n      <li class=\"nav-item\" ngbDropdown placement=\"bottom-right\">\r\n        <a class=\"nav-link pr-0\" role=\"button\" ngbDropdownToggle>\r\n          <div class=\"media align-items-center\">\r\n            <span class=\"avatar avatar-sm rounded-circle\">\r\n              <img alt=\"Image placeholder\" src=\"https://www.softzone.es/app/uploads-softzone.es/2018/04/guest.png\">\r\n            </span>\r\n            <div class=\"media-body ml-2 d-none d-lg-block\">\r\n              <span class=\"mb-0 text-sm  font-weight-bold\">USUARIO</span>\r\n            </div>\r\n          </div>\r\n        </a>\r\n        <div class=\"dropdown-menu-arrow dropdown-menu-right\" ngbDropdownMenu>\r\n          <div class=\" dropdown-header noti-title\">\r\n            <h6 class=\"text-overflow m-0\">Welcome!</h6>\r\n          </div>\r\n          <a routerLinkActive=\"active\" [routerLink]=\"['/user-profile']\" class=\"dropdown-item\">\r\n            <i class=\"ni ni-single-02\"></i>\r\n            <span>My profile</span>\r\n          </a>\r\n          <a routerLinkActive=\"active\" [routerLink]=\"['/user-profile']\" class=\"dropdown-item\">\r\n            <i class=\"ni ni-settings-gear-65\"></i>\r\n            <span>Settings</span>\r\n          </a>\r\n          \r\n          <div class=\"dropdown-divider\"></div>\r\n          <a href=\"#!\" (click) = \"btnLogout()\" class=\"dropdown-item\">\r\n            <i class=\"ni ni-user-run\"></i>\r\n            <span>Logout</span>\r\n          </a>\r\n        </div>\r\n      </li>\r\n    </ul>\r\n  </div>\r\n</nav>\r\n");

/***/ }),

/***/ "Sjf0":
/*!********************************************************!*\
  !*** ./src/app/layouts/vendedor/vendedor.component.ts ***!
  \********************************************************/
/*! exports provided: VendedorComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "VendedorComponent", function() { return VendedorComponent; });
/* harmony import */ var _raw_loader_vendedor_component_html__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! raw-loader!./vendedor.component.html */ "iHt8");
/* harmony import */ var _vendedor_component_css__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./vendedor.component.css */ "G9cc");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/core */ "fXoL");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};



var VendedorComponent = /** @class */ (function () {
    function VendedorComponent() {
    }
    VendedorComponent.prototype.ngOnInit = function () {
    };
    VendedorComponent.ctorParameters = function () { return []; };
    VendedorComponent = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_2__["Component"])({
            selector: 'app-vendedor',
            template: _raw_loader_vendedor_component_html__WEBPACK_IMPORTED_MODULE_0__["default"],
            styles: [_vendedor_component_css__WEBPACK_IMPORTED_MODULE_1__["default"]]
        }),
        __metadata("design:paramtypes", [])
    ], VendedorComponent);
    return VendedorComponent;
}());



/***/ }),

/***/ "Sy1n":
/*!**********************************!*\
  !*** ./src/app/app.component.ts ***!
  \**********************************/
/*! exports provided: AppComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "AppComponent", function() { return AppComponent; });
/* harmony import */ var _raw_loader_app_component_html__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! raw-loader!./app.component.html */ "VzVu");
/* harmony import */ var _app_component_scss__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./app.component.scss */ "ynWL");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/core */ "fXoL");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};



var AppComponent = /** @class */ (function () {
    function AppComponent() {
        this.title = 'argon-dashboard-angular';
    }
    AppComponent = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_2__["Component"])({
            selector: 'app-root',
            template: _raw_loader_app_component_html__WEBPACK_IMPORTED_MODULE_0__["default"],
            styles: [_app_component_scss__WEBPACK_IMPORTED_MODULE_1__["default"]]
        })
    ], AppComponent);
    return AppComponent;
}());



/***/ }),

/***/ "TK9H":
/*!**********************************************************************!*\
  !*** ./src/app/components/sidebar-admin/sidebar-admin.component.css ***!
  \**********************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = ("\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IiIsImZpbGUiOiJzcmMvYXBwL2NvbXBvbmVudHMvc2lkZWJhci1hZG1pbi9zaWRlYmFyLWFkbWluLmNvbXBvbmVudC5jc3MifQ== */");

/***/ }),

/***/ "VppA":
/*!***************************************************************************************************************!*\
  !*** ./node_modules/raw-loader/dist/cjs.js!./src/app/pages/vendedor/form-clients/form-clients.component.html ***!
  \***************************************************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = ("<div class=\"header pb-8 pt-5 pt-lg-8 d-flex align-items-center\"\r\n  style=\"min-height: 200px; background-image: url(https://geoinnova.org/cursos/wp-content/uploads/2016/10/1-11-1024x551.jpg); background-size: cover; background-position: center top;\">\r\n  <!-- Mask -->\r\n  <span class=\"mask bg-gradient-danger opacity-8\"></span>\r\n\r\n</div>\r\n<div class=\"container-fluid mt--7\">\r\n  <div class=\"row\">\r\n    <div class=\"col-xl-4 order-xl-2 mb-5 mb-xl-0\">\r\n      <div class=\"card card-profile shadow\">\r\n        <div class=\"row justify-content-center\">\r\n          <div class=\"col-lg-3 order-lg-2\">\r\n            <div class=\"card-profile-image\">\r\n              <a href=\"javascript:void(0)\">\r\n                <img src=\"https://logodix.com/logo/1707088.png\" class=\"rounded-circle\">\r\n              </a>\r\n            </div>\r\n          </div>\r\n        </div>\r\n        <div class=\"card-header text-center border-0 pt-8 pt-md-4 pb-0 pb-md-4\">\r\n          <div class=\"d-flex justify-content-between\">\r\n\r\n          </div>\r\n        </div>\r\n        <div class=\"card-body pt-0 pt-md-4\">\r\n          <div class=\"row\">\r\n            <div class=\"col\">\r\n              <div class=\"card-profile-stats d-flex justify-content-center mt-md-5\">\r\n\r\n              </div>\r\n            </div>\r\n          </div>\r\n          <div class=\"text-center\">\r\n\r\n          </div>\r\n        </div>\r\n      </div>\r\n    </div>\r\n    <div class=\"col-xl-8 order-xl-1\">\r\n      <div class=\"card bg-secondary shadow\">\r\n        <div class=\"card-header bg-white border-0\">\r\n          <div class=\"row align-items-center\">\r\n            <div class=\"col-8\">\r\n              <h3 class=\"mb-0\">REGISTER CLIENTS</h3>\r\n            </div>\r\n            <div class=\"col-4 text-right\">\r\n              <a (click)=\"save()\" class=\"btn btn-sm btn-primary\">Create</a>\r\n            </div>\r\n          </div>\r\n        </div>\r\n        <div class=\"card-body\">\r\n\r\n          <h6 class=\"heading-small text-muted mb-4\">Client information</h6>\r\n          <div class=\"pl-lg-4\">\r\n            <div class=\"row\">\r\n              <div class=\"col-lg-6\">\r\n                <div class=\"form-group\">\r\n                  <label class=\"form-control-label\" for=\"input-username\">Name</label>\r\n                  <input type=\"text\" id=\"input-username\" [(ngModel)]=\"nombre\"\r\n                    class=\"form-control form-control-alternative\" value=\"{{nombre}}\">\r\n                </div>\r\n              </div>\r\n              <div class=\"col-lg-6\">\r\n                <div class=\"form-group\">\r\n                  <label class=\"form-control-label\" for=\"input-email\">NIT</label>\r\n                  <input type=\"text\" [(ngModel)]=\"nit\" class=\"form-control form-control-alternative\" value=\"{{nit}}\">\r\n                </div>\r\n              </div>\r\n            </div>\r\n            <div class=\"row\">\r\n              <div class=\"col-lg-12\">\r\n                <div class=\"form-group\">\r\n                  <label class=\"form-control-label\" for=\"input-firste-name\">Address</label>\r\n                  <input type=\"text\" id=\"input-first-najme\" [(ngModel)]=\"direccion\"\r\n                    class=\"form-control form-control-alternative\" value=\"{{direccion}}\">\r\n                </div>\r\n              </div>\r\n            </div>\r\n\r\n            <div class=\"row\">\r\n              <div class=\"col-lg-6\">\r\n                <div class=\"form-group\">\r\n                  <label class=\"form-control-label\" for=\"input-last-name\">DPI</label>\r\n                  <input type=\"number\" id=\"input-last-namfe\" class=\"form-control form-control-alternative\"\r\n                    [(ngModel)]=\"dpi\">\r\n                </div>\r\n              </div>\r\n\r\n              <div class=\"col-lg-6\">\r\n                <div class=\"form-group\">\r\n                  <label class=\"form-control-label\" for=\"input-last-name\">Sede</label>\r\n                  <select class=\"form-control form-control-alternative\" id=\"selectPlan\" [(ngModel)]=\"sede\" name=\"selectPlan\" >\r\n                    <option [value]=\"sed.id\" *ngFor=\"let sed of sedes\">{{sed.Sede}}</option>\r\n                  </select>\r\n                </div>\r\n              </div>\r\n\r\n\r\n            </div>\r\n          </div>\r\n        </div>\r\n      </div>\r\n    </div>\r\n  </div>\r\n</div>\r\n");

/***/ }),

/***/ "VzVu":
/*!**************************************************************************!*\
  !*** ./node_modules/raw-loader/dist/cjs.js!./src/app/app.component.html ***!
  \**************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = ("<router-outlet></router-outlet>\r\n");

/***/ }),

/***/ "WSaj":
/*!******************************************************************************************************!*\
  !*** ./node_modules/raw-loader/dist/cjs.js!./src/app/layouts/auth-layout/auth-layout.component.html ***!
  \******************************************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = ("<div class=\"main-content\">\r\n  <nav class=\"navbar navbar-top navbar-horizontal navbar-expand-md navbar-dark\">\r\n    <div class=\"container px-4\">\r\n      <a class=\"navbar-brand\" routerLinkActive=\"active\" [routerLink]=\"['/dashboard']\">\r\n        <img src=\"\" />\r\n      </a>\r\n      <button class=\"navbar-toggler\" type=\"button\" (click)=\"isCollapsed=!isCollapsed\"\r\n         aria-controls=\"sidenav-collapse-main\">\r\n        <span class=\"navbar-toggler-icon\"></span>\r\n      </button>\r\n      <div class=\"collapse navbar-collapse\"  [ngbCollapse]=\"isCollapsed\" id=\"sidenav-collapse-main\">\r\n        <!-- Collapse header -->\r\n        <div class=\"navbar-collapse-header d-md-none\">\r\n          <div class=\"row\">\r\n            <div class=\"col-6 collapse-brand\">\r\n              <a routerLinkActive=\"active\" [routerLink]=\"['/dashboard']\">\r\n                <img src=\"\">\r\n              </a>\r\n            </div>\r\n            <div class=\"col-6 collapse-close\">\r\n              <button type=\"button\" class=\"navbar-toggler\" (click)=\"isCollapsed=!isCollapsed\" >\r\n                <span></span>\r\n                <span></span>\r\n              </button>\r\n            </div>\r\n          </div>\r\n        </div>\r\n        <!-- Navbar items -->\r\n       \r\n      </div>\r\n    </div>\r\n  </nav>\r\n  <router-outlet></router-outlet>\r\n</div>\r\n<footer class=\"py-5\">\r\n  <div class=\"container\">\r\n    <div class=\"row align-items-center justify-content-xl-between\">\r\n      <div class=\"col-xl-6\">\r\n        <div class=\"copyright text-center text-xl-left text-muted\">\r\n          &copy; {{ test | date: \"yyyy\" }} <a href=\"#\" class=\"font-weight-bold ml-1\" target=\"_blank\">Practicas Intermedias</a>\r\n        </div>\r\n      </div>\r\n    \r\n    </div>\r\n  </div>\r\n</footer>\r\n");

/***/ }),

/***/ "WwN9":
/*!***********************************************************************************************!*\
  !*** ./node_modules/raw-loader/dist/cjs.js!./src/app/components/footer/footer.component.html ***!
  \***********************************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = ("<footer class=\"footer\">\r\n  <div class=\"row align-items-center justify-content-xl-between\">\r\n    <div class=\"col-xl-6\">\r\n      <div class=\"copyright text-center text-xl-left text-muted\">\r\n        &copy; {{ test | date: \"yyyy\" }} <a href=\"#\" class=\"font-weight-bold ml-1\" target=\"_blank\">Practicas Intermedias</a>\r\n      </div>\r\n    </div>\r\n   \r\n  </div>\r\n</footer>\r\n");

/***/ }),

/***/ "Z4Bw":
/*!****************************************************************************************************!*\
  !*** ./node_modules/raw-loader/dist/cjs.js!./src/app/layouts/repartidor/repartidor.component.html ***!
  \****************************************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = ("<app-sidebar-repartidor></app-sidebar-repartidor>\r\n<div class=\"main-content\">\r\n  <!-- Top navbar -->\r\n  <app-navbar-bodeguero></app-navbar-bodeguero>\r\n  <!-- Pages -->\r\n  <router-outlet></router-outlet>\r\n  <div class=\"container-fluid\">\r\n    <app-footer></app-footer>\r\n  </div>\r\n</div>\r\n");

/***/ }),

/***/ "ZAI4":
/*!*******************************!*\
  !*** ./src/app/app.module.ts ***!
  \*******************************/
/*! exports provided: AppModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "AppModule", function() { return AppModule; });
/* harmony import */ var _angular_platform_browser_animations__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/platform-browser/animations */ "R1ws");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "fXoL");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/forms */ "3Pt+");
/* harmony import */ var _angular_common_http__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/common/http */ "tk/3");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @angular/router */ "tyNb");
/* harmony import */ var _app_component__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ./app.component */ "Sy1n");
/* harmony import */ var _layouts_admin_layout_admin_layout_component__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ./layouts/admin-layout/admin-layout.component */ "P6kD");
/* harmony import */ var _layouts_auth_layout_auth_layout_component__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! ./layouts/auth-layout/auth-layout.component */ "3TnI");
/* harmony import */ var _ng_bootstrap_ng_bootstrap__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! @ng-bootstrap/ng-bootstrap */ "1kSV");
/* harmony import */ var _app_routing__WEBPACK_IMPORTED_MODULE_9__ = __webpack_require__(/*! ./app.routing */ "beVS");
/* harmony import */ var _components_components_module__WEBPACK_IMPORTED_MODULE_10__ = __webpack_require__(/*! ./components/components.module */ "j1ZV");
/* harmony import */ var _layouts_admin_admin_component__WEBPACK_IMPORTED_MODULE_11__ = __webpack_require__(/*! ./layouts/admin/admin.component */ "R+tk");
/* harmony import */ var _layouts_bodeguero_bodeguero_component__WEBPACK_IMPORTED_MODULE_12__ = __webpack_require__(/*! ./layouts/bodeguero/bodeguero.component */ "k3QA");
/* harmony import */ var _layouts_vendedor_vendedor_component__WEBPACK_IMPORTED_MODULE_13__ = __webpack_require__(/*! ./layouts/vendedor/vendedor.component */ "Sjf0");
/* harmony import */ var _layouts_repartidor_repartidor_component__WEBPACK_IMPORTED_MODULE_14__ = __webpack_require__(/*! ./layouts/repartidor/repartidor.component */ "oipf");
/* harmony import */ var _pages_vendedor_form_clients_form_clients_component__WEBPACK_IMPORTED_MODULE_15__ = __webpack_require__(/*! ./pages/vendedor/form-clients/form-clients.component */ "k+Sr");
/* harmony import */ var _pages_vendedor_form_sales_form_sales_component__WEBPACK_IMPORTED_MODULE_16__ = __webpack_require__(/*! ./pages/vendedor/form-sales/form-sales.component */ "sF19");
/* harmony import */ var _pages_vendedor_sales_report_sales_report_component__WEBPACK_IMPORTED_MODULE_17__ = __webpack_require__(/*! ./pages/vendedor/sales-report/sales-report.component */ "edOk");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};


















var AppModule = /** @class */ (function () {
    function AppModule() {
    }
    AppModule = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
            imports: [
                _angular_platform_browser_animations__WEBPACK_IMPORTED_MODULE_0__["BrowserAnimationsModule"],
                _angular_forms__WEBPACK_IMPORTED_MODULE_2__["FormsModule"],
                _angular_common_http__WEBPACK_IMPORTED_MODULE_3__["HttpClientModule"],
                _components_components_module__WEBPACK_IMPORTED_MODULE_10__["ComponentsModule"],
                _ng_bootstrap_ng_bootstrap__WEBPACK_IMPORTED_MODULE_8__["NgbModule"],
                _angular_router__WEBPACK_IMPORTED_MODULE_4__["RouterModule"],
                _app_routing__WEBPACK_IMPORTED_MODULE_9__["AppRoutingModule"]
            ],
            declarations: [
                _app_component__WEBPACK_IMPORTED_MODULE_5__["AppComponent"],
                _layouts_admin_layout_admin_layout_component__WEBPACK_IMPORTED_MODULE_6__["AdminLayoutComponent"],
                _layouts_auth_layout_auth_layout_component__WEBPACK_IMPORTED_MODULE_7__["AuthLayoutComponent"],
                _layouts_admin_admin_component__WEBPACK_IMPORTED_MODULE_11__["AdminComponent"],
                _layouts_bodeguero_bodeguero_component__WEBPACK_IMPORTED_MODULE_12__["BodegueroComponent"],
                _layouts_vendedor_vendedor_component__WEBPACK_IMPORTED_MODULE_13__["VendedorComponent"],
                _layouts_repartidor_repartidor_component__WEBPACK_IMPORTED_MODULE_14__["RepartidorComponent"],
                _pages_vendedor_form_clients_form_clients_component__WEBPACK_IMPORTED_MODULE_15__["FormClientsComponent"],
                _pages_vendedor_form_sales_form_sales_component__WEBPACK_IMPORTED_MODULE_16__["FormSalesComponent"],
                _pages_vendedor_sales_report_sales_report_component__WEBPACK_IMPORTED_MODULE_17__["SalesReportComponent"],
            ],
            providers: [],
            bootstrap: [_app_component__WEBPACK_IMPORTED_MODULE_5__["AppComponent"]]
        })
    ], AppModule);
    return AppModule;
}());



/***/ }),

/***/ "beVS":
/*!********************************!*\
  !*** ./src/app/app.routing.ts ***!
  \********************************/
/*! exports provided: AppRoutingModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "AppRoutingModule", function() { return AppRoutingModule; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "fXoL");
/* harmony import */ var _angular_common__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/common */ "ofXK");
/* harmony import */ var _angular_platform_browser__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/platform-browser */ "jhN1");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/router */ "tyNb");
/* harmony import */ var _layouts_admin_layout_admin_layout_component__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ./layouts/admin-layout/admin-layout.component */ "P6kD");
/* harmony import */ var _layouts_auth_layout_auth_layout_component__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ./layouts/auth-layout/auth-layout.component */ "3TnI");
/* harmony import */ var _layouts_admin_admin_component__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ./layouts/admin/admin.component */ "R+tk");
/* harmony import */ var _layouts_bodeguero_bodeguero_component__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! ./layouts/bodeguero/bodeguero.component */ "k3QA");
/* harmony import */ var _layouts_vendedor_vendedor_component__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! ./layouts/vendedor/vendedor.component */ "Sjf0");
/* harmony import */ var _layouts_repartidor_repartidor_component__WEBPACK_IMPORTED_MODULE_9__ = __webpack_require__(/*! ./layouts/repartidor/repartidor.component */ "oipf");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};










var routes = [
    {
        path: '',
        redirectTo: 'login',
        pathMatch: 'full',
    }, {
        path: '',
        component: _layouts_admin_layout_admin_layout_component__WEBPACK_IMPORTED_MODULE_4__["AdminLayoutComponent"],
        children: [
            {
                path: '',
                loadChildren: './layouts/admin-layout/admin-layout.module#AdminLayoutModule'
            }
        ]
    },
    {
        path: '',
        component: _layouts_admin_admin_component__WEBPACK_IMPORTED_MODULE_6__["AdminComponent"],
        children: [
            {
                path: '',
                loadChildren: './layouts/admin/admin.module#AdminModule'
            }
        ]
    },
    {
        path: '',
        component: _layouts_bodeguero_bodeguero_component__WEBPACK_IMPORTED_MODULE_7__["BodegueroComponent"],
        children: [
            {
                path: '',
                loadChildren: './layouts/bodeguero/bodeguero.module#BodegueroModule'
            }
        ]
    },
    {
        path: '',
        component: _layouts_vendedor_vendedor_component__WEBPACK_IMPORTED_MODULE_8__["VendedorComponent"],
        children: [
            {
                path: '',
                loadChildren: './layouts/vendedor/vendedor.module#VendedorModule'
            }
        ]
    },
    {
        path: '',
        component: _layouts_repartidor_repartidor_component__WEBPACK_IMPORTED_MODULE_9__["RepartidorComponent"],
        children: [
            {
                path: '',
                loadChildren: './layouts/repartidor/repartidor.module#RepartidorModule'
            }
        ]
    },
    {
        path: '',
        component: _layouts_auth_layout_auth_layout_component__WEBPACK_IMPORTED_MODULE_5__["AuthLayoutComponent"],
        children: [
            {
                path: '',
                loadChildren: './layouts/auth-layout/auth-layout.module#AuthLayoutModule'
            }
        ]
    }, {
        path: '**',
        redirectTo: 'login'
    }
];
var AppRoutingModule = /** @class */ (function () {
    function AppRoutingModule() {
    }
    AppRoutingModule = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["NgModule"])({
            imports: [
                _angular_common__WEBPACK_IMPORTED_MODULE_1__["CommonModule"],
                _angular_platform_browser__WEBPACK_IMPORTED_MODULE_2__["BrowserModule"],
                _angular_router__WEBPACK_IMPORTED_MODULE_3__["RouterModule"].forRoot(routes, {
                    useHash: true,
                    relativeLinkResolution: 'legacy'
                })
            ],
            exports: [],
        })
    ], AppRoutingModule);
    return AppRoutingModule;
}());



/***/ }),

/***/ "btPN":
/*!************************************************************************!*\
  !*** ./src/app/pages/vendedor/sales-report/sales-report.component.css ***!
  \************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = ("\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IiIsImZpbGUiOiJzcmMvYXBwL3BhZ2VzL3ZlbmRlZG9yL3NhbGVzLXJlcG9ydC9zYWxlcy1yZXBvcnQuY29tcG9uZW50LmNzcyJ9 */");

/***/ }),

/***/ "cefF":
/*!***************************************************!*\
  !*** ./src/app/layouts/admin/admin.component.css ***!
  \***************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = ("\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IiIsImZpbGUiOiJzcmMvYXBwL2xheW91dHMvYWRtaW4vYWRtaW4uY29tcG9uZW50LmNzcyJ9 */");

/***/ }),

/***/ "crnd":
/*!**********************************************************!*\
  !*** ./src/$$_lazy_route_resource lazy namespace object ***!
  \**********************************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

var map = {
	"./layouts/admin-layout/admin-layout.module": [
		"IqXj",
		"default~layouts-admin-admin-module~layouts-admin-layout-admin-layout-module~layouts-bodeguero-bodegu~a1f1748c",
		"layouts-admin-layout-admin-layout-module"
	],
	"./layouts/admin/admin.module": [
		"ACLt",
		"default~layouts-admin-admin-module~layouts-admin-layout-admin-layout-module~layouts-bodeguero-bodegu~a1f1748c",
		"layouts-admin-admin-module"
	],
	"./layouts/auth-layout/auth-layout.module": [
		"PTPi",
		"layouts-auth-layout-auth-layout-module"
	],
	"./layouts/bodeguero/bodeguero.module": [
		"DkCZ",
		"default~layouts-admin-admin-module~layouts-admin-layout-admin-layout-module~layouts-bodeguero-bodegu~a1f1748c",
		"layouts-bodeguero-bodeguero-module"
	],
	"./layouts/repartidor/repartidor.module": [
		"nw+H",
		"default~layouts-admin-admin-module~layouts-admin-layout-admin-layout-module~layouts-bodeguero-bodegu~a1f1748c",
		"layouts-repartidor-repartidor-module"
	],
	"./layouts/vendedor/vendedor.module": [
		"QF2h",
		"default~layouts-admin-admin-module~layouts-admin-layout-admin-layout-module~layouts-bodeguero-bodegu~a1f1748c",
		"layouts-vendedor-vendedor-module"
	]
};
function webpackAsyncContext(req) {
	if(!__webpack_require__.o(map, req)) {
		return Promise.resolve().then(function() {
			var e = new Error("Cannot find module '" + req + "'");
			e.code = 'MODULE_NOT_FOUND';
			throw e;
		});
	}

	var ids = map[req], id = ids[0];
	return Promise.all(ids.slice(1).map(__webpack_require__.e)).then(function() {
		return __webpack_require__(id);
	});
}
webpackAsyncContext.keys = function webpackAsyncContextKeys() {
	return Object.keys(map);
};
webpackAsyncContext.id = "crnd";
module.exports = webpackAsyncContext;

/***/ }),

/***/ "dB3f":
/*!********************************************************************************!*\
  !*** ./src/app/components/sidebar-repartidor/sidebar-repartidor.component.css ***!
  \********************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = ("\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IiIsImZpbGUiOiJzcmMvYXBwL2NvbXBvbmVudHMvc2lkZWJhci1yZXBhcnRpZG9yL3NpZGViYXItcmVwYXJ0aWRvci5jb21wb25lbnQuY3NzIn0= */");

/***/ }),

/***/ "edOk":
/*!***********************************************************************!*\
  !*** ./src/app/pages/vendedor/sales-report/sales-report.component.ts ***!
  \***********************************************************************/
/*! exports provided: SalesReportComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "SalesReportComponent", function() { return SalesReportComponent; });
/* harmony import */ var _raw_loader_sales_report_component_html__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! raw-loader!./sales-report.component.html */ "7FMQ");
/* harmony import */ var _sales_report_component_css__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./sales-report.component.css */ "btPN");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/core */ "fXoL");
/* harmony import */ var _peticiones_service__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ../../../peticiones.service */ "kW39");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};




var SalesReportComponent = /** @class */ (function () {
    function SalesReportComponent(app) {
        this.app = app;
        this.ventas = [];
    }
    SalesReportComponent.prototype.ngOnInit = function () {
        var _this = this;
        this.usuario_actual = JSON.parse(sessionStorage.getItem("log"));
        this.app.getVentas(this.usuario_actual.id).subscribe(function (res) {
            _this.ventas = res;
            console.log(_this.ventas);
        });
    };
    SalesReportComponent.ctorParameters = function () { return [
        { type: _peticiones_service__WEBPACK_IMPORTED_MODULE_3__["PeticionesService"] }
    ]; };
    SalesReportComponent = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_2__["Component"])({
            selector: "app-sales-report",
            template: _raw_loader_sales_report_component_html__WEBPACK_IMPORTED_MODULE_0__["default"],
            styles: [_sales_report_component_css__WEBPACK_IMPORTED_MODULE_1__["default"]]
        }),
        __metadata("design:paramtypes", [_peticiones_service__WEBPACK_IMPORTED_MODULE_3__["PeticionesService"]])
    ], SalesReportComponent);
    return SalesReportComponent;
}());



/***/ }),

/***/ "hrlM":
/*!*******************************************************!*\
  !*** ./src/app/components/navbar/navbar.component.ts ***!
  \*******************************************************/
/*! exports provided: NavbarComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "NavbarComponent", function() { return NavbarComponent; });
/* harmony import */ var _raw_loader_navbar_component_html__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! raw-loader!./navbar.component.html */ "S6iF");
/* harmony import */ var _navbar_component_scss__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./navbar.component.scss */ "Ls9r");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/core */ "fXoL");
/* harmony import */ var _sidebar_sidebar_component__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ../sidebar/sidebar.component */ "zBoC");
/* harmony import */ var _angular_common__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @angular/common */ "ofXK");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! @angular/router */ "tyNb");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};






var NavbarComponent = /** @class */ (function () {
    function NavbarComponent(location, element, router) {
        this.element = element;
        this.router = router;
        this.location = location;
    }
    NavbarComponent.prototype.ngOnInit = function () {
        this.listTitles = _sidebar_sidebar_component__WEBPACK_IMPORTED_MODULE_3__["ROUTES"].filter(function (listTitle) { return listTitle; });
    };
    NavbarComponent.prototype.getTitle = function () {
        var titlee = this.location.prepareExternalUrl(this.location.path());
        if (titlee.charAt(0) === '#') {
            titlee = titlee.slice(1);
        }
        for (var item = 0; item < this.listTitles.length; item++) {
            if (this.listTitles[item].path === titlee) {
                return this.listTitles[item].title;
            }
        }
        return titlee.replace('/', '');
    };
    NavbarComponent.prototype.btnLogout = function () {
        localStorage.removeItem('log');
        this.router.navigate(['/login']);
    };
    NavbarComponent.ctorParameters = function () { return [
        { type: _angular_common__WEBPACK_IMPORTED_MODULE_4__["Location"] },
        { type: _angular_core__WEBPACK_IMPORTED_MODULE_2__["ElementRef"] },
        { type: _angular_router__WEBPACK_IMPORTED_MODULE_5__["Router"] }
    ]; };
    NavbarComponent = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_2__["Component"])({
            selector: 'app-navbar',
            template: _raw_loader_navbar_component_html__WEBPACK_IMPORTED_MODULE_0__["default"],
            styles: [_navbar_component_scss__WEBPACK_IMPORTED_MODULE_1__["default"]]
        }),
        __metadata("design:paramtypes", [_angular_common__WEBPACK_IMPORTED_MODULE_4__["Location"], _angular_core__WEBPACK_IMPORTED_MODULE_2__["ElementRef"], _angular_router__WEBPACK_IMPORTED_MODULE_5__["Router"]])
    ], NavbarComponent);
    return NavbarComponent;
}());



/***/ }),

/***/ "iHt8":
/*!************************************************************************************************!*\
  !*** ./node_modules/raw-loader/dist/cjs.js!./src/app/layouts/vendedor/vendedor.component.html ***!
  \************************************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = ("<app-sidebar-vendedor></app-sidebar-vendedor>\r\n<div class=\"main-content\">\r\n  <!-- Top navbar -->\r\n  <app-navbar-bodeguero></app-navbar-bodeguero>\r\n  <!-- Pages -->\r\n  <router-outlet></router-outlet>\r\n  <div class=\"container-fluid\">\r\n    <app-footer></app-footer>\r\n  </div>\r\n</div>");

/***/ }),

/***/ "j1ZV":
/*!*************************************************!*\
  !*** ./src/app/components/components.module.ts ***!
  \*************************************************/
/*! exports provided: ComponentsModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "ComponentsModule", function() { return ComponentsModule; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "fXoL");
/* harmony import */ var _angular_common__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/common */ "ofXK");
/* harmony import */ var _sidebar_sidebar_component__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./sidebar/sidebar.component */ "zBoC");
/* harmony import */ var _navbar_navbar_component__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./navbar/navbar.component */ "hrlM");
/* harmony import */ var _footer_footer_component__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ./footer/footer.component */ "LmEr");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! @angular/router */ "tyNb");
/* harmony import */ var _ng_bootstrap_ng_bootstrap__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! @ng-bootstrap/ng-bootstrap */ "1kSV");
/* harmony import */ var _sidebar_admin_sidebar_admin_component__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! ./sidebar-admin/sidebar-admin.component */ "Hysb");
/* harmony import */ var _sidebar_bodeguero_sidebar_bodeguero_component__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! ./sidebar-bodeguero/sidebar-bodeguero.component */ "Cwie");
/* harmony import */ var _navbar_bodeguero_navbar_bodeguero_component__WEBPACK_IMPORTED_MODULE_9__ = __webpack_require__(/*! ./navbar-bodeguero/navbar-bodeguero.component */ "6vv9");
/* harmony import */ var _sidebar_vendedor_sidebar_vendedor_component__WEBPACK_IMPORTED_MODULE_10__ = __webpack_require__(/*! ./sidebar-vendedor/sidebar-vendedor.component */ "C75L");
/* harmony import */ var _sidebar_repartidor_sidebar_repartidor_component__WEBPACK_IMPORTED_MODULE_11__ = __webpack_require__(/*! ./sidebar-repartidor/sidebar-repartidor.component */ "Qqav");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};












var ComponentsModule = /** @class */ (function () {
    function ComponentsModule() {
    }
    ComponentsModule = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["NgModule"])({
            imports: [
                _angular_common__WEBPACK_IMPORTED_MODULE_1__["CommonModule"],
                _angular_router__WEBPACK_IMPORTED_MODULE_5__["RouterModule"],
                _ng_bootstrap_ng_bootstrap__WEBPACK_IMPORTED_MODULE_6__["NgbModule"]
            ],
            declarations: [
                _footer_footer_component__WEBPACK_IMPORTED_MODULE_4__["FooterComponent"],
                _navbar_navbar_component__WEBPACK_IMPORTED_MODULE_3__["NavbarComponent"],
                _sidebar_sidebar_component__WEBPACK_IMPORTED_MODULE_2__["SidebarComponent"],
                _sidebar_admin_sidebar_admin_component__WEBPACK_IMPORTED_MODULE_7__["SidebarAdminComponent"],
                _sidebar_bodeguero_sidebar_bodeguero_component__WEBPACK_IMPORTED_MODULE_8__["SidebarBodegueroComponent"],
                _navbar_bodeguero_navbar_bodeguero_component__WEBPACK_IMPORTED_MODULE_9__["NavbarBodegueroComponent"],
                _sidebar_vendedor_sidebar_vendedor_component__WEBPACK_IMPORTED_MODULE_10__["SidebarVendedorComponent"],
                _sidebar_repartidor_sidebar_repartidor_component__WEBPACK_IMPORTED_MODULE_11__["SidebarRepartidorComponent"]
            ],
            exports: [
                _footer_footer_component__WEBPACK_IMPORTED_MODULE_4__["FooterComponent"],
                _navbar_navbar_component__WEBPACK_IMPORTED_MODULE_3__["NavbarComponent"],
                _sidebar_sidebar_component__WEBPACK_IMPORTED_MODULE_2__["SidebarComponent"],
                _sidebar_admin_sidebar_admin_component__WEBPACK_IMPORTED_MODULE_7__["SidebarAdminComponent"],
                _sidebar_bodeguero_sidebar_bodeguero_component__WEBPACK_IMPORTED_MODULE_8__["SidebarBodegueroComponent"],
                _navbar_bodeguero_navbar_bodeguero_component__WEBPACK_IMPORTED_MODULE_9__["NavbarBodegueroComponent"],
                _sidebar_vendedor_sidebar_vendedor_component__WEBPACK_IMPORTED_MODULE_10__["SidebarVendedorComponent"],
                _sidebar_repartidor_sidebar_repartidor_component__WEBPACK_IMPORTED_MODULE_11__["SidebarRepartidorComponent"]
            ]
        })
    ], ComponentsModule);
    return ComponentsModule;
}());



/***/ }),

/***/ "jcT0":
/*!***********************************************************!*\
  !*** ./src/app/components/sidebar/sidebar.component.scss ***!
  \***********************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = ("\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IiIsImZpbGUiOiJzcmMvYXBwL2NvbXBvbmVudHMvc2lkZWJhci9zaWRlYmFyLmNvbXBvbmVudC5zY3NzIn0= */");

/***/ }),

/***/ "k+Sr":
/*!***********************************************************************!*\
  !*** ./src/app/pages/vendedor/form-clients/form-clients.component.ts ***!
  \***********************************************************************/
/*! exports provided: FormClientsComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "FormClientsComponent", function() { return FormClientsComponent; });
/* harmony import */ var _raw_loader_form_clients_component_html__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! raw-loader!./form-clients.component.html */ "VppA");
/* harmony import */ var _form_clients_component_css__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./form-clients.component.css */ "m1lG");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/core */ "fXoL");
/* harmony import */ var _peticiones_service__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ../../../peticiones.service */ "kW39");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};




var FormClientsComponent = /** @class */ (function () {
    function FormClientsComponent(app) {
        this.app = app;
        this.sedes = [];
        this.nombre = "";
        this.nit = "";
        this.dpi = "";
        this.direccion = "";
    }
    FormClientsComponent.prototype.ngOnInit = function () {
        var _this = this;
        this.usuario_actual = JSON.parse(sessionStorage.getItem("log"));
        this.app.getSedesUsuario(this.usuario_actual.id).subscribe(function (res) {
            _this.sedes = res;
            console.log(_this.sedes);
        });
    };
    FormClientsComponent.prototype.save = function () {
        var _this = this;
        if (this.nombre != "" &&
            this.nit != "" &&
            this.dpi != "" &&
            this.direccion != "" &&
            this.sede != undefined) {
            if (this.sedes.length > 0) {
                this.app
                    .postCliente({
                    nombre: this.nombre,
                    nit: this.nit,
                    dpi: this.dpi,
                    direccion: this.direccion,
                    sede: this.sede,
                })
                    .subscribe(function (resp) {
                    console.log(resp);
                    alert(resp.descripcion);
                    _this.nombre = "";
                    _this.nit = "";
                    _this.dpi = "";
                    _this.direccion = "";
                    _this.sede = "";
                });
            }
            else {
                alert("No tienes ninguna sede asignada");
            }
        }
        else {
            alert("Debes llenar todos los campos");
        }
    };
    FormClientsComponent.ctorParameters = function () { return [
        { type: _peticiones_service__WEBPACK_IMPORTED_MODULE_3__["PeticionesService"] }
    ]; };
    FormClientsComponent = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_2__["Component"])({
            selector: "app-form-clients",
            template: _raw_loader_form_clients_component_html__WEBPACK_IMPORTED_MODULE_0__["default"],
            styles: [_form_clients_component_css__WEBPACK_IMPORTED_MODULE_1__["default"]]
        }),
        __metadata("design:paramtypes", [_peticiones_service__WEBPACK_IMPORTED_MODULE_3__["PeticionesService"]])
    ], FormClientsComponent);
    return FormClientsComponent;
}());



/***/ }),

/***/ "k3QA":
/*!**********************************************************!*\
  !*** ./src/app/layouts/bodeguero/bodeguero.component.ts ***!
  \**********************************************************/
/*! exports provided: BodegueroComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "BodegueroComponent", function() { return BodegueroComponent; });
/* harmony import */ var _raw_loader_bodeguero_component_html__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! raw-loader!./bodeguero.component.html */ "omVW");
/* harmony import */ var _bodeguero_component_css__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./bodeguero.component.css */ "vBN0");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/core */ "fXoL");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};



var BodegueroComponent = /** @class */ (function () {
    function BodegueroComponent() {
    }
    BodegueroComponent.prototype.ngOnInit = function () {
    };
    BodegueroComponent.ctorParameters = function () { return []; };
    BodegueroComponent = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_2__["Component"])({
            selector: 'app-bodeguero',
            template: _raw_loader_bodeguero_component_html__WEBPACK_IMPORTED_MODULE_0__["default"],
            styles: [_bodeguero_component_css__WEBPACK_IMPORTED_MODULE_1__["default"]]
        }),
        __metadata("design:paramtypes", [])
    ], BodegueroComponent);
    return BodegueroComponent;
}());



/***/ }),

/***/ "kW39":
/*!***************************************!*\
  !*** ./src/app/peticiones.service.ts ***!
  \***************************************/
/*! exports provided: PeticionesService */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "PeticionesService", function() { return PeticionesService; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "fXoL");
/* harmony import */ var _angular_common_http__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/common/http */ "tk/3");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};


var PeticionesService = /** @class */ (function () {
    function PeticionesService(http) {
        this.http = http;
    }
    PeticionesService.prototype.postLogin = function (datos) {
        return this.http.post("http://18.225.36.192:3000/login", datos);
    };
    PeticionesService.prototype.postRecuperar = function (datos) {
        return this.http.post("http://18.225.36.192:3000/recpass", datos);
    };
    PeticionesService.prototype.postModificar = function (datos) {
        return this.http.post("http://18.225.36.192:3000/editprofile", datos);
    };
    PeticionesService.prototype.postCrearUsuario = function (datos) {
        return this.http.post("http://18.225.36.192:3000/signup", datos);
    };
    //--------------------------------------------sedes-----------------------------------
    PeticionesService.prototype.postCrearSede = function (datos) {
        return this.http.post("http://18.225.36.192:3000/encargado/crearSede", datos);
    };
    PeticionesService.prototype.getSedes = function () {
        return this.http.get("http://18.225.36.192:3000/encargado/sedes");
    };
    PeticionesService.prototype.putModificarSede = function (datos) {
        return this.http.put("http://18.225.36.192:3000/encargado/updateSede", datos);
    };
    PeticionesService.prototype.deleteSede = function (datos) {
        return this.http.post("http://18.225.36.192:3000/encargado/deleteSede", datos);
    };
    PeticionesService.prototype.getSedesU = function (id) {
        return this.http.get("http://18.225.36.192:3000/encargado/sede" + id);
    };
    //-----------------bodegas--------------------------------------
    PeticionesService.prototype.getBodegasSede = function (id) {
        return this.http.get("http://18.225.36.192:3000/encargado/bodegasSede" + id);
    };
    PeticionesService.prototype.postCrearBodega = function (datos) {
        return this.http.post("http://18.225.36.192:3000/encargado/crearBodega", datos);
    };
    //---------------------bodegueros--------------------------------------
    PeticionesService.prototype.getProductoInventario = function (id) {
        return this.http.get("http://18.225.36.192:3000/bodeguero/productos_inventario" + id);
    };
    PeticionesService.prototype.getBodegasU = function (id) {
        return this.http.get("http://18.225.36.192:3000/encargado/bodegasUser" + id);
    };
    PeticionesService.prototype.actualizarInventario = function (datos) {
        return this.http.get("http://18.225.36.192:3000/actualizarInventario" + datos);
    };
    PeticionesService.prototype.postCrearSolicitud = function (datos) {
        return this.http.post("http://18.225.36.192:3000/solicitarTransferencia", datos);
    };
    PeticionesService.prototype.getTansferenciasInterna = function (id) {
        return this.http.get("http://18.225.36.192:3000/bodeguero/ordenes_internas" + id);
    };
    PeticionesService.prototype.actualizarTransferencia = function (datos) {
        return this.http.get("http://18.225.36.192:3000/bodeguero/actualizar_transferencia" + datos);
    };
    PeticionesService.prototype.getTansferenciasExterna = function (id) {
        return this.http.get("http://18.225.36.192:3000/bodeguero/ordenes_externas" + id);
    };
    //----------------------------- repartidor ----------------------------------------
    PeticionesService.prototype.getVentas = function (id) {
        return this.http.get("http://18.225.36.192:3000/repartidor/ventas" + id);
    };
    PeticionesService.prototype.getTrans = function (id) {
        return this.http.get("http://18.225.36.192:3000/repartidor/transferencias" + id);
    };
    PeticionesService.prototype.getSedesUsuario = function (id) {
        return this.http.get("http://18.225.36.192:3000/encargado/sede" + id);
    };
    //--------------------------------------------clientes-----------------------------------
    PeticionesService.prototype.postCliente = function (datos) {
        return this.http.post("http://18.225.36.192:3000/registrarCliente", datos);
    };
    PeticionesService.prototype.getClientes = function () {
        return this.http.get("http://18.225.36.192:3000/getClientes");
    };
    //--------------------------------------------ventas-----------------------------------
    PeticionesService.prototype.postVenta = function (datos) {
        console.log(datos);
        return this.http.post("http://18.225.36.192:3000/registrarVenta", datos);
    };
    //--------------------------------------------asignar rol-----------------------------------
    PeticionesService.prototype.postAsignarRol = function (datos) {
        return this.http.post("http://18.225.36.192:3000/asignarRol", datos);
    };
    //--------------------------------------------asignar rol-----------------------------------
    PeticionesService.prototype.postQuitarRol = function (datos) {
        return this.http.post("http://18.225.36.192:3000/quitarRol", datos);
    };
    //--------------------------------------------actualizar estado bodega-----------------------------------
    PeticionesService.prototype.activarBodega = function (datos) {
        return this.http.post("http://18.225.36.192:3000/activarBodega", datos);
    };
    //--------------------------------------------actualizar estado bodega-----------------------------------
    PeticionesService.prototype.sendEmail = function (datos) {
        return this.http.post("http://18.225.36.192:3000/notificacion", datos);
    };
    PeticionesService.prototype.getVentasClient = function (datos) {
        return this.http.post("http://18.225.36.192:3000/getSales", datos);
    };
    PeticionesService.ctorParameters = function () { return [
        { type: _angular_common_http__WEBPACK_IMPORTED_MODULE_1__["HttpClient"] }
    ]; };
    PeticionesService = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Injectable"])({
            providedIn: 'root'
        }),
        __metadata("design:paramtypes", [_angular_common_http__WEBPACK_IMPORTED_MODULE_1__["HttpClient"]])
    ], PeticionesService);
    return PeticionesService;
}());



/***/ }),

/***/ "l0hG":
/*!*********************************************************************************************************************!*\
  !*** ./node_modules/raw-loader/dist/cjs.js!./src/app/components/sidebar-bodeguero/sidebar-bodeguero.component.html ***!
  \*********************************************************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = ("<nav class=\"navbar navbar-vertical navbar-expand-md navbar-light bg-white\" id=\"sidenav-main\">\r\n    <div class=\"container-fluid\">\r\n      <!-- Toggler -->\r\n      <button class=\"navbar-toggler\" type=\"button\" (click)=\"isCollapsed=!isCollapsed\"\r\n         aria-controls=\"sidenav-collapse-main\">\r\n        <span class=\"navbar-toggler-icon\"></span>\r\n      </button>\r\n      <!-- Brand -->\r\n      <a class=\"navbar-brand pt-0\" routerLinkActive=\"active\">\r\n        <img src=\"https://us.123rf.com/450wm/dragomirescu/dragomirescu1807/dragomirescu180700290/104917812-gold-alphabet-letter-sb-s-b-logo-combination-design-suitable-for-a-company-or-business.jpg\" class=\"navbar-brand-img\" alt=\"...\">\r\n      </a>\r\n      <!-- User -->\r\n      <ul class=\"nav align-items-center d-md-none\">\r\n        <li class=\"nav-item\" ngbDropdown placement=\"bottom-right\">\r\n          <a class=\"nav-link nav-link-icon\" role=\"button\" ngbDropdownToggle>\r\n            <i class=\"ni ni-bell-55\"></i>\r\n          </a>\r\n          <div class=\"dropdown-menu-arrow dropdown-menu-right\" ngbDropdownMenu>\r\n            <a class=\"dropdown-item\" href=\"javascript:void(0)\">Action</a>\r\n            <a class=\"dropdown-item\" href=\"javascript:void(0)\">Another action</a>\r\n            <div class=\"dropdown-divider\"></div>\r\n            <a class=\"dropdown-item\" href=\"javascript:void(0)\">Something else here</a>\r\n          </div>\r\n        </li>\r\n        <li class=\"nav-item\" ngbDropdown placement=\"bottom-right\">\r\n          <a class=\"nav-link\" role=\"button\" ngbDropdownToggle>\r\n            <div class=\"media align-items-center\">\r\n              <span class=\"avatar avatar-sm rounded-circle\">\r\n                <img alt=\"Image placeholder\" src=\"./assets/img/theme/team-1-800x800.jpg\">\r\n              </span>\r\n            </div>\r\n          </a>\r\n          <div class=\"dropdown-menu-arrow dropdown-menu-right\" ngbDropdownMenu>\r\n            <div class=\" dropdown-header noti-title\">\r\n              <h6 class=\"text-overflow m-0\">Welcome!</h6>\r\n            </div>\r\n            <a routerLinkActive=\"active\" [routerLink]=\"['/user-profile']\" class=\"dropdown-item\">\r\n              <i class=\"ni ni-single-02\"></i>\r\n              <span>My profile</span>\r\n            </a>\r\n            <a routerLinkActive=\"active\" [routerLink]=\"['/user-profile']\" class=\"dropdown-item\">\r\n              <i class=\"ni ni-settings-gear-65\"></i>\r\n              <span>Settings</span>\r\n            </a>\r\n            <a routerLinkActive=\"active\" [routerLink]=\"['/user-profile']\" class=\"dropdown-item\">\r\n              <i class=\"ni ni-calendar-grid-58\"></i>\r\n              <span>Activity</span>\r\n            </a>\r\n            <a routerLinkActive=\"active\" [routerLink]=\"['/user-profile']\" class=\"dropdown-item\">\r\n              <i class=\"ni ni-support-16\"></i>\r\n              <span>Support</span>\r\n            </a>\r\n            <div class=\"dropdown-divider\"></div>\r\n            <a href=\"#!\" class=\"dropdown-item\">\r\n              <i class=\"ni ni-user-run\"></i>\r\n              <span>Logout</span>\r\n            </a>\r\n          </div>\r\n        </li>\r\n      </ul>\r\n      <!-- Collapse -->\r\n      <div class=\"collapse navbar-collapse\"  [ngbCollapse]=\"isCollapsed\" id=\"sidenav-collapse-main\">\r\n        <!-- Collapse header -->\r\n        <div class=\"navbar-collapse-header d-md-none\">\r\n          <div class=\"row\">\r\n            <div class=\"col-6 collapse-brand\">\r\n              <a  routerLinkActive=\"active\" [routerLink]=\"['/dashboard']\">\r\n                <img src=\"./assets/img/brand/blue.png\">\r\n              </a>\r\n            </div>\r\n            <div class=\"col-6 collapse-close\">\r\n              <button type=\"button\" class=\"navbar-toggler\" (click)=\"isCollapsed=!isCollapsed\">\r\n                <span></span>\r\n                <span></span>\r\n              </button>\r\n            </div>\r\n          </div>\r\n        </div>\r\n        <!-- Form -->\r\n        <form class=\"mt-4 mb-3 d-md-none\">\r\n          <div class=\"input-group input-group-rounded input-group-merge\">\r\n            <input type=\"search\" class=\"form-control form-control-rounded form-control-prepended\" placeholder=\"Search\" aria-label=\"Search\">\r\n            <div class=\"input-group-prepend\">\r\n              <div class=\"input-group-text\">\r\n                <span class=\"fa fa-search\"></span>\r\n              </div>\r\n            </div>\r\n          </div>\r\n        </form>\r\n        <!-- Navigation -->\r\n        <ul class=\"navbar-nav\">\r\n            <li *ngFor=\"let menuItem of menuItems\" class=\"{{menuItem.class}} nav-item\">\r\n                <a routerLinkActive=\"active\" [routerLink]=\"[menuItem.path]\" class=\"nav-link\">\r\n                    <i class=\"ni {{menuItem.icon}}\"></i>\r\n                    {{menuItem.title}}\r\n                </a>\r\n            </li>\r\n        </ul>\r\n        <!-- Divider -->\r\n        <hr class=\"my-3\">\r\n        <!-- Heading -->\r\n       \r\n       \r\n      </div>\r\n    </div>\r\n  </nav>\r\n  ");

/***/ }),

/***/ "m1lG":
/*!************************************************************************!*\
  !*** ./src/app/pages/vendedor/form-clients/form-clients.component.css ***!
  \************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = ("\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IiIsImZpbGUiOiJzcmMvYXBwL3BhZ2VzL3ZlbmRlZG9yL2Zvcm0tY2xpZW50cy9mb3JtLWNsaWVudHMuY29tcG9uZW50LmNzcyJ9 */");

/***/ }),

/***/ "oipf":
/*!************************************************************!*\
  !*** ./src/app/layouts/repartidor/repartidor.component.ts ***!
  \************************************************************/
/*! exports provided: RepartidorComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "RepartidorComponent", function() { return RepartidorComponent; });
/* harmony import */ var _raw_loader_repartidor_component_html__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! raw-loader!./repartidor.component.html */ "Z4Bw");
/* harmony import */ var _repartidor_component_css__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./repartidor.component.css */ "J6UQ");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/core */ "fXoL");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};



var RepartidorComponent = /** @class */ (function () {
    function RepartidorComponent() {
    }
    RepartidorComponent.prototype.ngOnInit = function () {
    };
    RepartidorComponent.ctorParameters = function () { return []; };
    RepartidorComponent = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_2__["Component"])({
            selector: 'app-repartidor',
            template: _raw_loader_repartidor_component_html__WEBPACK_IMPORTED_MODULE_0__["default"],
            styles: [_repartidor_component_css__WEBPACK_IMPORTED_MODULE_1__["default"]]
        }),
        __metadata("design:paramtypes", [])
    ], RepartidorComponent);
    return RepartidorComponent;
}());



/***/ }),

/***/ "omVW":
/*!**************************************************************************************************!*\
  !*** ./node_modules/raw-loader/dist/cjs.js!./src/app/layouts/bodeguero/bodeguero.component.html ***!
  \**************************************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = ("<app-sidebar-bodeguero></app-sidebar-bodeguero>\r\n<div class=\"main-content\">\r\n  <!-- Top navbar -->\r\n  <app-navbar-bodeguero></app-navbar-bodeguero>\r\n  <!-- Pages -->\r\n  <router-outlet></router-outlet>\r\n  <div class=\"container-fluid\">\r\n    <app-footer></app-footer>\r\n  </div>\r\n</div>");

/***/ }),

/***/ "sF19":
/*!*******************************************************************!*\
  !*** ./src/app/pages/vendedor/form-sales/form-sales.component.ts ***!
  \*******************************************************************/
/*! exports provided: FormSalesComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "FormSalesComponent", function() { return FormSalesComponent; });
/* harmony import */ var _raw_loader_form_sales_component_html__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! raw-loader!./form-sales.component.html */ "tba0");
/* harmony import */ var _form_sales_component_css__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./form-sales.component.css */ "Ptez");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/core */ "fXoL");
/* harmony import */ var _peticiones_service__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ../../../peticiones.service */ "kW39");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};




var FormSalesComponent = /** @class */ (function () {
    function FormSalesComponent(app) {
        this.app = app;
        this.entrega = "";
        this.factura = "";
        this.total = 0;
        this.recargo = 0;
        this.direccion = "";
        this.domicilio = false;
        this.estado = "Entregado";
        this.id_cliente = "";
        this.id_repartidor = "15";
        this.clientes = [];
    }
    FormSalesComponent.prototype.ngOnInit = function () {
        var _this = this;
        var today = new Date();
        var dd = String(today.getDate()).padStart(2, "0");
        var mm = String(today.getMonth() + 1).padStart(2, "0"); //January is 0!
        var yyyy = today.getFullYear();
        this.factura = dd + "/" + mm + "/" + yyyy;
        this.entrega = this.factura;
        this.usuario_actual = JSON.parse(sessionStorage.getItem("log"));
        this.app.getClientes().subscribe(function (res) {
            _this.clientes = res;
            console.log(res);
        });
    };
    FormSalesComponent.prototype.save = function () {
        var _this = this;
        if (this.domicilio) {
            this.estado = "Pendiente";
            this.entrega = this.factura;
            this.recargo = 15;
            this.total += this.recargo;
        }
        else {
            this.direccion = "Local";
        }
        if (this.direccion != "" &&
            this.entrega != "" &&
            this.entrega != "" &&
            this.total != 0 &&
            this.estado != "" &&
            this.id_cliente != "" &&
            this.id_repartidor != "") {
            this.app.postVenta({
                "fechafacturacion": this.factura,
                "fechaentrega": this.entrega,
                "id_usuario": this.usuario_actual.id,
                "id_cliente": this.id_cliente,
                "total": this.total,
                "estado": this.estado,
                "r_venta": this.recargo,
                "id_repartidor": this.id_repartidor,
                "ubicacion": this.direccion
            }).subscribe(function (resp) {
                alert(resp.descripcion);
                _this.total = 0;
                _this.recargo = 0;
                _this.direccion = "";
                _this.domicilio = false;
                _this.estado = "Entregado";
            });
        }
        else {
            alert("Debes llenar todos los campos");
        }
    };
    FormSalesComponent.ctorParameters = function () { return [
        { type: _peticiones_service__WEBPACK_IMPORTED_MODULE_3__["PeticionesService"] }
    ]; };
    FormSalesComponent = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_2__["Component"])({
            selector: "app-form-sales",
            template: _raw_loader_form_sales_component_html__WEBPACK_IMPORTED_MODULE_0__["default"],
            styles: [_form_sales_component_css__WEBPACK_IMPORTED_MODULE_1__["default"]]
        }),
        __metadata("design:paramtypes", [_peticiones_service__WEBPACK_IMPORTED_MODULE_3__["PeticionesService"]])
    ], FormSalesComponent);
    return FormSalesComponent;
}());



/***/ }),

/***/ "tba0":
/*!***********************************************************************************************************!*\
  !*** ./node_modules/raw-loader/dist/cjs.js!./src/app/pages/vendedor/form-sales/form-sales.component.html ***!
  \***********************************************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = ("<div class=\"header pb-8 pt-5 pt-lg-8 d-flex align-items-center\" style=\"min-height: 200px; background-image: url(https://geoinnova.org/cursos/wp-content/uploads/2016/10/1-11-1024x551.jpg); background-size: cover; background-position: center top;\">\r\n  <!-- Mask -->\r\n  <span class=\"mask bg-gradient-danger opacity-8\"></span>\r\n\r\n</div>\r\n<div class=\"container-fluid mt--7\">\r\n  <div class=\"row\">\r\n    <div class=\"col-xl-4 order-xl-2 mb-5 mb-xl-0\">\r\n      <div class=\"card card-profile shadow\">\r\n        <div class=\"row justify-content-center\">\r\n          <div class=\"col-lg-3 order-lg-2\">\r\n            <div class=\"card-profile-image\">\r\n              <a href=\"javascript:void(0)\">\r\n                <img src=\"https://logodix.com/logo/1707088.png\" class=\"rounded-circle\">\r\n              </a>\r\n            </div>\r\n          </div>\r\n        </div>\r\n        <div class=\"card-header text-center border-0 pt-8 pt-md-4 pb-0 pb-md-4\">\r\n          <div class=\"d-flex justify-content-between\">\r\n\r\n          </div>\r\n        </div>\r\n        <div class=\"card-body pt-0 pt-md-4\">\r\n          <div class=\"row\">\r\n            <div class=\"col\">\r\n              <div class=\"card-profile-stats d-flex justify-content-center mt-md-5\">\r\n\r\n              </div>\r\n            </div>\r\n          </div>\r\n          <div class=\"text-center\">\r\n\r\n          </div>\r\n        </div>\r\n      </div>\r\n    </div>\r\n    <div class=\"col-xl-8 order-xl-1\">\r\n      <div class=\"card bg-secondary shadow\">\r\n        <div class=\"card-header bg-white border-0\">\r\n          <div class=\"row align-items-center\">\r\n            <div class=\"col-8\">\r\n              <h3 class=\"mb-0\">REGISTER SALE</h3>\r\n            </div>\r\n            <div class=\"col-4 text-right\">\r\n              <a (click)=\"save()\" class=\"btn btn-sm btn-primary\">Create</a>\r\n            </div>\r\n          </div>\r\n        </div>\r\n          <div class=\"card-body\">\r\n\r\n            <h6 class=\"heading-small text-muted mb-4\">Sale information</h6>\r\n            <div class=\"pl-lg-4\">\r\n\r\n              <div class=\"row\">\r\n\r\n                <div class=\"col-lg-6\">\r\n                  <div class=\"form-group\">\r\n                    <label class=\"form-control-label\" for=\"input-username\">Fecha Facturación</label>\r\n                    <input type=\"text\" id=\"input-username\" [(ngModel)]=\"factura\" class=\"form-control form-control-alternative\"  value=\"{{factura}}\" disabled={{true}}>\r\n                  </div>\r\n                </div>\r\n\r\n                <div class=\"col-lg-6\">\r\n                  <div class=\"form-group\">\r\n                    <label class=\"form-control-label\" for=\"input-username\">Fecha Entrega</label>\r\n                    <input type=\"text\" id=\"input-username\" [(ngModel)]=\"entrega\" class=\"form-control form-control-alternative\"  value=\"{{entrega}}\" disabled={{!domicilio}}>\r\n                  </div>\r\n                </div>\r\n\r\n              </div>\r\n\r\n              <div class=\"row\">\r\n\r\n                <div class=\"col-lg-4\">\r\n                  <div class=\"form-group\">\r\n                    <label class=\"form-control-label\" for=\"input-username\">Total</label>\r\n                    <input type=\"text\" id=\"input-username\" [(ngModel)]=\"total\" class=\"form-control form-control-alternative\"  value=\"{{total}}\">\r\n                  </div>\r\n                </div>\r\n\r\n                <div class=\"col-lg-4\">\r\n                  <div class=\"form-group\">\r\n                    <label class=\"form-control-label\" for=\"input-last-name\">Cliente</label>\r\n                    <select class=\"form-control form-control-alternative\" id=\"selectPlan\" [(ngModel)]=\"id_cliente\" name=\"selectPlan\" >\r\n                      <option [value]=\"client.idCliente\" *ngFor=\"let client of clientes\">{{client.Nombre + \" - \" + client.Nit}}</option>\r\n                    </select>\r\n                  </div>\r\n                </div>\r\n\r\n                <div class=\"col-lg-4\">\r\n                  <div class=\"form-group\">\r\n                    <label class=\"form-control-label\" for=\"input-username\">Entrega a domicilio</label>\r\n                    <input class=\"form-control \" type=\"checkbox\" id=\"cbox2\" [(ngModel)]=\"domicilio\" value=\"{{domicilio}}\">\r\n                  </div>\r\n                </div>\r\n\r\n              </div>\r\n\r\n              <div class=\"row\">\r\n\r\n                <div class=\"col-lg-12\">\r\n                  <div class=\"form-group\">\r\n                    <label class=\"form-control-label\" for=\"input-username\">Dirección Entrega</label>\r\n                    <input type=\"text\" id=\"input-username\" [(ngModel)]=\"direccion\" class=\"form-control form-control-alternative\"  value=\"{{direccion}}\" disabled={{!domicilio}}>\r\n                  </div>\r\n                </div>\r\n\r\n              </div>\r\n\r\n\r\n            </div>\r\n\r\n          </div>\r\n        </div>\r\n      </div>\r\n    </div>\r\n  </div>\r\n\r\n");

/***/ }),

/***/ "v5Bj":
/*!****************************************************************************!*\
  !*** ./src/app/components/sidebar-vendedor/sidebar-vendedor.component.css ***!
  \****************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = ("\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IiIsImZpbGUiOiJzcmMvYXBwL2NvbXBvbmVudHMvc2lkZWJhci12ZW5kZWRvci9zaWRlYmFyLXZlbmRlZG9yLmNvbXBvbmVudC5jc3MifQ== */");

/***/ }),

/***/ "vBN0":
/*!***********************************************************!*\
  !*** ./src/app/layouts/bodeguero/bodeguero.component.css ***!
  \***********************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = ("\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IiIsImZpbGUiOiJzcmMvYXBwL2xheW91dHMvYm9kZWd1ZXJvL2JvZGVndWVyby5jb21wb25lbnQuY3NzIn0= */");

/***/ }),

/***/ "vtrx":
/*!******************************************************************!*\
  !*** ./src/app/layouts/admin-layout/admin-layout.component.scss ***!
  \******************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = ("\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IiIsImZpbGUiOiJzcmMvYXBwL2xheW91dHMvYWRtaW4tbGF5b3V0L2FkbWluLWxheW91dC5jb21wb25lbnQuc2NzcyJ9 */");

/***/ }),

/***/ "yZN6":
/*!*********************************************************!*\
  !*** ./src/app/components/footer/footer.component.scss ***!
  \*********************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = ("\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IiIsImZpbGUiOiJzcmMvYXBwL2NvbXBvbmVudHMvZm9vdGVyL2Zvb3Rlci5jb21wb25lbnQuc2NzcyJ9 */");

/***/ }),

/***/ "ynWL":
/*!************************************!*\
  !*** ./src/app/app.component.scss ***!
  \************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = ("\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IiIsImZpbGUiOiJzcmMvYXBwL2FwcC5jb21wb25lbnQuc2NzcyJ9 */");

/***/ }),

/***/ "zBoC":
/*!*********************************************************!*\
  !*** ./src/app/components/sidebar/sidebar.component.ts ***!
  \*********************************************************/
/*! exports provided: ROUTES, SidebarComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "ROUTES", function() { return ROUTES; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "SidebarComponent", function() { return SidebarComponent; });
/* harmony import */ var _raw_loader_sidebar_component_html__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! raw-loader!./sidebar.component.html */ "KKA+");
/* harmony import */ var _sidebar_component_scss__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./sidebar.component.scss */ "jcT0");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/core */ "fXoL");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/router */ "tyNb");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};




var ROUTES = [
    { path: '/user-profile', title: 'User profile', icon: 'ni-single-02 text-yellow', class: '' },
    { path: '/users-encargado', title: 'Users', icon: 'ni-single-02 text-yellow', class: '' },
    { path: '/view-bodegas', title: 'bodegas', icon: 'ni-single-02 text-yellow', class: '' },
];
var SidebarComponent = /** @class */ (function () {
    function SidebarComponent(router) {
        this.router = router;
        this.isCollapsed = true;
    }
    SidebarComponent.prototype.ngOnInit = function () {
        var _this = this;
        this.menuItems = ROUTES.filter(function (menuItem) { return menuItem; });
        this.router.events.subscribe(function (event) {
            _this.isCollapsed = true;
        });
    };
    SidebarComponent.ctorParameters = function () { return [
        { type: _angular_router__WEBPACK_IMPORTED_MODULE_3__["Router"] }
    ]; };
    SidebarComponent = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_2__["Component"])({
            selector: 'app-sidebar',
            template: _raw_loader_sidebar_component_html__WEBPACK_IMPORTED_MODULE_0__["default"],
            styles: [_sidebar_component_scss__WEBPACK_IMPORTED_MODULE_1__["default"]]
        }),
        __metadata("design:paramtypes", [_angular_router__WEBPACK_IMPORTED_MODULE_3__["Router"]])
    ], SidebarComponent);
    return SidebarComponent;
}());



/***/ }),

/***/ "zRj8":
/*!*************************************************************************************************************!*\
  !*** ./node_modules/raw-loader/dist/cjs.js!./src/app/components/sidebar-admin/sidebar-admin.component.html ***!
  \*************************************************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = ("<nav class=\"navbar navbar-vertical navbar-expand-md navbar-light bg-white\" id=\"sidenav-main\">\r\n  <div class=\"container-fluid\">\r\n    <!-- Toggler -->\r\n    <button class=\"navbar-toggler\" type=\"button\" (click)=\"isCollapsed=!isCollapsed\"\r\n       aria-controls=\"sidenav-collapse-main\">\r\n      <span class=\"navbar-toggler-icon\"></span>\r\n    </button>\r\n    <!-- Brand -->\r\n    <a class=\"navbar-brand pt-0\" routerLinkActive=\"active\">\r\n      <img src=\"https://us.123rf.com/450wm/dragomirescu/dragomirescu1807/dragomirescu180700290/104917812-gold-alphabet-letter-sb-s-b-logo-combination-design-suitable-for-a-company-or-business.jpg\" class=\"navbar-brand-img\" alt=\"...\">\r\n    </a>\r\n    \r\n    <!-- Collapse -->\r\n    <div class=\"collapse navbar-collapse\"  [ngbCollapse]=\"isCollapsed\" id=\"sidenav-collapse-main\">\r\n      <!-- Collapse header -->\r\n      <div class=\"navbar-collapse-header d-md-none\">\r\n        <div class=\"row\">\r\n          <div class=\"col-6 collapse-brand\">\r\n            <a  routerLinkActive=\"active\" [routerLink]=\"['/dashboard']\">\r\n              <img src=\"./assets/img/brand/blue.png\">\r\n            </a>\r\n          </div>\r\n          <div class=\"col-6 collapse-close\">\r\n            <button type=\"button\" class=\"navbar-toggler\" (click)=\"isCollapsed=!isCollapsed\">\r\n              <span></span>\r\n              <span></span>\r\n            </button>\r\n          </div>\r\n        </div>\r\n      </div>\r\n      <!-- Form -->\r\n      <form class=\"mt-4 mb-3 d-md-none\">\r\n        <div class=\"input-group input-group-rounded input-group-merge\">\r\n          <input type=\"search\" class=\"form-control form-control-rounded form-control-prepended\" placeholder=\"Search\" aria-label=\"Search\">\r\n          <div class=\"input-group-prepend\">\r\n            <div class=\"input-group-text\">\r\n              <span class=\"fa fa-search\"></span>\r\n            </div>\r\n          </div>\r\n        </div>\r\n      </form>\r\n      <!-- Navigation -->\r\n      <ul class=\"navbar-nav\">\r\n          <li *ngFor=\"let menuItem of menuItems\" class=\"{{menuItem.class}} nav-item\">\r\n              <a routerLinkActive=\"active\" [routerLink]=\"[menuItem.path]\" class=\"nav-link\">\r\n                  <i class=\"ni {{menuItem.icon}}\"></i>\r\n                  {{menuItem.title}}\r\n              </a>\r\n             \r\n          </li>\r\n          <a href=\"#!\" (click)=\"btnLogout()\" class=\"nav-link\">\r\n            <i class=\"ni ni-user-run ni-tv-2 text-primary\"></i>\r\n             Logout\r\n          </a>\r\n      </ul>\r\n      <!-- Divider -->\r\n      <hr class=\"my-3\">\r\n      <!-- Heading -->\r\n     \r\n    </div>\r\n  </div>\r\n</nav>\r\n\r\n\r\n");

/***/ }),

/***/ "zUnb":
/*!*********************!*\
  !*** ./src/main.ts ***!
  \*********************/
/*! no exports provided */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "fXoL");
/* harmony import */ var _angular_platform_browser_dynamic__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/platform-browser-dynamic */ "a3Wg");
/* harmony import */ var _app_app_module__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./app/app.module */ "ZAI4");
/* harmony import */ var _environments_environment__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./environments/environment */ "AytR");
/*!

=========================================================
* Argon Dashboard Angular - v1.1.0
=========================================================

* Product Page: https://www.creative-tim.com/product/argon-dashboard-angular
* Copyright 2019 Creative Tim (https://www.creative-tim.com)
* Licensed under MIT (https://github.com/creativetimofficial/argon-dashboard-angular/blob/master/LICENSE.md)

* Coded by Creative Tim

=========================================================

* The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.

*/




if (_environments_environment__WEBPACK_IMPORTED_MODULE_3__["environment"].production) {
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["enableProdMode"])();
}
Object(_angular_platform_browser_dynamic__WEBPACK_IMPORTED_MODULE_1__["platformBrowserDynamic"])().bootstrapModule(_app_app_module__WEBPACK_IMPORTED_MODULE_2__["AppModule"])
    .catch(function (err) { return console.error(err); });


/***/ })

},[[0,"runtime","vendor"]]]);
//# sourceMappingURL=main.js.map