(window["webpackJsonp"] = window["webpackJsonp"] || []).push([["layouts-admin-layout-admin-layout-module"],{

/***/ "05Zr":
/*!*************************************************************************!*\
  !*** ./src/app/pages/encargado/view-bodegas/view-bodegas.component.css ***!
  \*************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = ("\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IiIsImZpbGUiOiJzcmMvYXBwL3BhZ2VzL2VuY2FyZ2Fkby92aWV3LWJvZGVnYXMvdmlldy1ib2RlZ2FzLmNvbXBvbmVudC5jc3MifQ== */");

/***/ }),

/***/ "2VCV":
/*!**************************************************************************************************!*\
  !*** ./node_modules/raw-loader/dist/cjs.js!./src/app/pages/encargado/users/users.component.html ***!
  \**************************************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = ("<div class=\"header pb-8 pt-5 pt-lg-8 d-flex align-items-center\"\r\n  style=\"min-height: 200px; background-image: url(https://geoinnova.org/cursos/wp-content/uploads/2016/10/1-11-1024x551.jpg); background-size: cover; background-position: center top;\">\r\n  <!-- Mask -->\r\n  <span class=\"mask bg-gradient-danger opacity-8\"></span>\r\n\r\n</div>\r\n<div class=\"container-fluid mt--7\">\r\n  <div class=\"row\">\r\n    <div class=\"col-xl-4 order-xl-2 mb-5 mb-xl-0\">\r\n      <div class=\"card card-profile shadow\">\r\n        <div class=\"row justify-content-center\">\r\n          <div class=\"col-lg-3 order-lg-2\">\r\n            <div class=\"card-profile-image\">\r\n              <a href=\"javascript:void(0)\">\r\n                <img src=\"https://logodix.com/logo/1707088.png\" class=\"rounded-circle\">\r\n              </a>\r\n            </div>\r\n          </div>\r\n        </div>\r\n        <div class=\"card-header text-center border-0 pt-8 pt-md-4 pb-0 pb-md-4\">\r\n          <div class=\"d-flex justify-content-between\">\r\n\r\n          </div>\r\n        </div>\r\n        <div class=\"card-body pt-0 pt-md-4\">\r\n          <div class=\"row\">\r\n            <div class=\"col\">\r\n              <div class=\"card-profile-stats d-flex justify-content-center mt-md-5\">\r\n\r\n              </div>\r\n            </div>\r\n          </div>\r\n          <div class=\"text-center\">\r\n\r\n          </div>\r\n        </div>\r\n      </div>\r\n    </div>\r\n    <div class=\"col-xl-8 order-xl-1\">\r\n      <div class=\"card bg-secondary shadow\">\r\n        <div class=\"card-header bg-white border-0\">\r\n          <div class=\"row align-items-center\">\r\n            <div class=\"col-8\">\r\n              <h3 class=\"mb-0\">REGISTER USERS</h3>\r\n            </div>\r\n            <div class=\"col-4 text-right\">\r\n              <a (click)=\"save()\" class=\"btn btn-sm btn-primary\">save</a>\r\n            </div>\r\n          </div>\r\n        </div>\r\n        <div class=\"card-body\">\r\n\r\n          <h6 class=\"heading-small text-muted mb-4\">User information</h6>\r\n          <div class=\"pl-lg-4\">\r\n            <div class=\"row\">\r\n              <div class=\"col-lg-6\">\r\n                <div class=\"form-group\">\r\n                  <label class=\"form-control-label\" for=\"input-username\">Username</label>\r\n                  <input type=\"text\" id=\"input-username\" [(ngModel)]=\"name\"\r\n                    class=\"form-control form-control-alternative\" value=\"{{name}}\">\r\n                </div>\r\n              </div>\r\n              <div class=\"col-lg-6\">\r\n                <div class=\"form-group\">\r\n                  <label class=\"form-control-label\" for=\"input-email\">Email address</label>\r\n                  <input type=\"text\" [(ngModel)]=\"email\" class=\"form-control form-control-alternative\"\r\n                    value=\"{{email}}\">\r\n                </div>\r\n              </div>\r\n            </div>\r\n            <div class=\"row\">\r\n              <div class=\"col-lg-6\">\r\n                <div class=\"form-group\">\r\n                  <label class=\"form-control-label\" for=\"input-firste-name\">Date of Birth</label>\r\n                  <input type=\"text\" id=\"input-first-najme\" [(ngModel)]=\"fechaN\"\r\n                    class=\"form-control form-control-alternative\" value=\"{{fechaN}}\">\r\n                </div>\r\n              </div>\r\n              <div class=\"col-lg-6\">\r\n                <div class=\"form-group\">\r\n                  <label class=\"form-control-label\" for=\"input-last-name\">DPI</label>\r\n                  <input type=\"text\" id=\"input-last-namfe\" class=\"form-control form-control-alternative\"\r\n                    [(ngModel)]=\"dpi\">\r\n                </div>\r\n              </div>\r\n            </div>\r\n\r\n            <div class=\"row\">\r\n              <div class=\"col-lg-6\">\r\n                <div class=\"form-group\">\r\n                  <label class=\"form-control-label\" for=\"input-first-name\">Password</label>\r\n                  <input type=\"text\" id=\"input-first-name\" [(ngModel)]=\"password\"\r\n                    class=\"form-control form-control-alternative\" value=\"{{password}}\">\r\n                </div>\r\n              </div>\r\n              <div class=\"col-lg-6\">\r\n                <div class=\"form-group\">\r\n                  <label class=\"form-control-label\" for=\"input-last-namwe\">rol</label>\r\n                  <select [(ngModel)]=\"opcionSeleccionado\" class=\"form-control form-control-alternative\">\r\n                    <option *ngFor=\"let rol of roles\">\r\n                      {{rol}}\r\n                    </option>\r\n                  </select>\r\n                </div>\r\n              </div>\r\n\r\n            </div>\r\n\r\n            \r\n          </div>\r\n\r\n\r\n\r\n\r\n\r\n        </div>\r\n      </div>\r\n    </div>\r\n  </div>\r\n</div>");

/***/ }),

/***/ "9yqR":
/*!************************************************************************!*\
  !*** ./src/app/pages/encargado/view-bodegas/view-bodegas.component.ts ***!
  \************************************************************************/
/*! exports provided: ROUTES, ViewBodegasComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "ROUTES", function() { return ROUTES; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "ViewBodegasComponent", function() { return ViewBodegasComponent; });
/* harmony import */ var _raw_loader_view_bodegas_component_html__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! raw-loader!./view-bodegas.component.html */ "GvQs");
/* harmony import */ var _view_bodegas_component_css__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./view-bodegas.component.css */ "05Zr");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/core */ "fXoL");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/router */ "tyNb");
/* harmony import */ var _peticiones_service__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ../../../peticiones.service */ "kW39");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};





var ROUTES = [
    { path: '/view-bodegas', title: 'VIEW', icon: 'ni-tv-2 text-primary', class: '' },
    { path: '/bodegas', title: 'ADD', icon: 'ni-tv-2 text-primary', class: '' },
];
var ViewBodegasComponent = /** @class */ (function () {
    function ViewBodegasComponent(router, app) {
        var _this = this;
        this.router = router;
        this.app = app;
        this.isCollapsed = true;
        this.solicitudes = [];
        var InfoUsuario = JSON.parse(sessionStorage.getItem('log'));
        this.app.getSedesU(InfoUsuario.id).subscribe(function (resp1) {
            _this.app.getBodegasSede(resp1[0].id).subscribe(function (resp) {
                _this.solicitudes = resp;
            });
        });
    }
    ViewBodegasComponent.prototype.ngOnInit = function () {
        var _this = this;
        this.menuItems = ROUTES.filter(function (menuItem) { return menuItem; });
        this.router.events.subscribe(function (event) {
            _this.isCollapsed = true;
        });
    };
    ViewBodegasComponent.prototype.Actualizar = function (id, estado) {
        var _this = this;
        if (estado == "Desactivada") {
            this.app.activarBodega({ "idBodega": id, "estado": "Activa" }).subscribe(function (resp) {
                alert(resp.msj);
            });
        }
        else {
            //se Desactiva
            this.app.activarBodega({ "idBodega": id, "estado": "Desactivada" }).subscribe(function (resp) {
                alert(resp.msj);
            });
        }
        var InfoUsuario = JSON.parse(sessionStorage.getItem('log'));
        this.app.getSedesU(InfoUsuario.id).subscribe(function (resp1) {
            _this.app.getBodegasSede(resp1[0].id).subscribe(function (resp) {
                _this.solicitudes = resp;
            });
        });
    };
    ViewBodegasComponent.ctorParameters = function () { return [
        { type: _angular_router__WEBPACK_IMPORTED_MODULE_3__["Router"] },
        { type: _peticiones_service__WEBPACK_IMPORTED_MODULE_4__["PeticionesService"] }
    ]; };
    ViewBodegasComponent = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_2__["Component"])({
            selector: 'app-view-bodegas',
            template: _raw_loader_view_bodegas_component_html__WEBPACK_IMPORTED_MODULE_0__["default"],
            styles: [_view_bodegas_component_css__WEBPACK_IMPORTED_MODULE_1__["default"]]
        }),
        __metadata("design:paramtypes", [_angular_router__WEBPACK_IMPORTED_MODULE_3__["Router"], _peticiones_service__WEBPACK_IMPORTED_MODULE_4__["PeticionesService"]])
    ], ViewBodegasComponent);
    return ViewBodegasComponent;
}());



/***/ }),

/***/ "A8Te":
/*!******************************************************************************************************!*\
  !*** ./node_modules/raw-loader/dist/cjs.js!./src/app/pages/user-profile/user-profile.component.html ***!
  \******************************************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = ("<div class=\"header pb-8 pt-5 pt-lg-8 d-flex align-items-center\" style=\"min-height: 200px; background-image: url(https://geoinnova.org/cursos/wp-content/uploads/2016/10/1-11-1024x551.jpg); background-size: cover; background-position: center top;\">\r\n  <!-- Mask -->\r\n  <span class=\"mask bg-gradient-danger opacity-8\"></span>\r\n \r\n</div>\r\n<div class=\"container-fluid mt--7\">\r\n  <div class=\"row\">\r\n    <div class=\"col-xl-4 order-xl-2 mb-5 mb-xl-0\">\r\n      <div class=\"card card-profile shadow\">\r\n        <div class=\"row justify-content-center\">\r\n          <div class=\"col-lg-3 order-lg-2\">\r\n            <div class=\"card-profile-image\">\r\n              <a href=\"javascript:void(0)\">\r\n                <img src=\"https://www.softzone.es/app/uploads-softzone.es/2018/04/guest.png\" class=\"rounded-circle\">\r\n              </a>\r\n            </div>\r\n          </div>\r\n        </div>\r\n        <div class=\"card-header text-center border-0 pt-8 pt-md-4 pb-0 pb-md-4\">\r\n          <div class=\"d-flex justify-content-between\">\r\n           \r\n          </div>\r\n        </div>\r\n        <div class=\"card-body pt-0 pt-md-4\">\r\n          <div class=\"row\">\r\n            <div class=\"col\">\r\n              <div class=\"card-profile-stats d-flex justify-content-center mt-md-5\">\r\n               \r\n              </div>\r\n            </div>\r\n          </div>\r\n          <div class=\"text-center\">\r\n            \r\n          </div>\r\n        </div>\r\n      </div>\r\n    </div>\r\n    <div class=\"col-xl-8 order-xl-1\">\r\n      <div class=\"card bg-secondary shadow\">\r\n        <div class=\"card-header bg-white border-0\">\r\n          <div class=\"row align-items-center\">\r\n            <div class=\"col-8\">\r\n              <h3 class=\"mb-0\">My account</h3>\r\n            </div>\r\n            <div class=\"col-4 text-right\">\r\n              <a (click)=\"edit()\" class=\"btn btn-sm btn-primary\">UPDATE</a>\r\n            </div>\r\n          </div>\r\n        </div>\r\n        <div class=\"card-body\">\r\n        \r\n            <h6 class=\"heading-small text-muted mb-4\">User information</h6>\r\n            <div class=\"pl-lg-4\">\r\n              <div class=\"row\">\r\n                <div class=\"col-lg-6\">\r\n                  <div class=\"form-group\">\r\n                    <label class=\"form-control-label\" for=\"input-username\">Username</label>\r\n                    <input type=\"text\" id=\"input-username\" [(ngModel)]=\"name\" class=\"form-control form-control-alternative\"  value=\"{{name}}\">\r\n                  </div>\r\n                </div>\r\n                <div class=\"col-lg-6\">\r\n                  <div class=\"form-group\">\r\n                    <label class=\"form-control-label\" for=\"input-email\">Email address</label>\r\n                    <input type=\"text\" [(ngModel)]=\"email\" class=\"form-control form-control-alternative\" value=\"{{email}}\">\r\n                  </div>\r\n                </div>\r\n              </div>\r\n              <div class=\"row\">\r\n                <div class=\"col-lg-6\">\r\n                  <div class=\"form-group\">\r\n                    <label class=\"form-control-label\" for=\"input-firste-name\">Date of Birth</label>\r\n                    <input type=\"text\" id=\"input-first-najme\" [(ngModel)]=\"fechaN\" class=\"form-control form-control-alternative\"  value=\"{{fechaN}}\">\r\n                  </div>\r\n                </div>\r\n                <div class=\"col-lg-6\">\r\n                  <div class=\"form-group\">\r\n                    <label class=\"form-control-label\" for=\"input-last-name\">DPI</label>\r\n                    <input type=\"text\" id=\"input-last-namfe\" class=\"form-control form-control-alternative\"  value=\"{{dpi}}\"  disabled=\"disabled\">\r\n                  </div>\r\n                </div>\r\n              </div>\r\n\r\n              <div class=\"row\">\r\n                <div class=\"col-lg-6\">\r\n                  <div class=\"form-group\">\r\n                    <label class=\"form-control-label\" for=\"input-first-name\">Password</label>\r\n                    <input type=\"text\" id=\"input-first-name\" [(ngModel)]=\"password\" class=\"form-control form-control-alternative\"  value=\"{{password}}\">\r\n                  </div>\r\n                </div>\r\n                <div class=\"col-lg-6\">\r\n                  <div class=\"form-group\">\r\n                    <label class=\"form-control-label\" for=\"input-last-namwe\">roles</label>\r\n                    <select class=\"form-control form-control-alternative\">\r\n                      <option *ngFor=\"let rol of roles\">\r\n                        {{rol.descripcion}}\r\n                      </option>\r\n                    </select>\r\n                  </div>\r\n                </div>\r\n              </div>\r\n            </div>\r\n          \r\n           \r\n           \r\n         \r\n        </div>\r\n      </div>\r\n    </div>\r\n  </div>\r\n</div>\r\n");

/***/ }),

/***/ "ARmr":
/*!**************************************************************!*\
  !*** ./src/app/pages/encargado/bodegas/bodegas.component.ts ***!
  \**************************************************************/
/*! exports provided: ROUTES, BodegasComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "ROUTES", function() { return ROUTES; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "BodegasComponent", function() { return BodegasComponent; });
/* harmony import */ var _raw_loader_bodegas_component_html__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! raw-loader!./bodegas.component.html */ "TkXs");
/* harmony import */ var _bodegas_component_css__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./bodegas.component.css */ "H+Fi");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/core */ "fXoL");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/router */ "tyNb");
/* harmony import */ var _peticiones_service__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ../../../peticiones.service */ "kW39");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};





var ROUTES = [
    { path: '/view-bodegas', title: 'VIEW', icon: 'ni-tv-2 text-primary', class: '' },
    { path: '/bodegas', title: 'ADD', icon: 'ni-tv-2 text-primary', class: '' },
];
var BodegasComponent = /** @class */ (function () {
    function BodegasComponent(router, app) {
        this.router = router;
        this.app = app;
        this.isCollapsed = true;
    }
    BodegasComponent.prototype.ngOnInit = function () {
        var _this = this;
        this.menuItems = ROUTES.filter(function (menuItem) { return menuItem; });
        this.router.events.subscribe(function (event) {
            _this.isCollapsed = true;
        });
    };
    BodegasComponent.prototype.registrar = function () {
        var _this = this;
        var InfoUsuario = JSON.parse(sessionStorage.getItem('log'));
        this.app.getSedesU(InfoUsuario.id).subscribe(function (resp1) {
            _this.app.postCrearBodega({
                "nombre": _this.nombre, "descripcion": _this.descripcion, "estado": _this.estado,
                "sede": resp1[0].id, "usuario": _this.id_encargado
            }).subscribe(function (resp) {
                alert(resp.msj);
                _this.nombre = _this.descripcion = _this.estado = "";
                _this.id_sede = _this.id_encargado = 0;
            });
        });
    };
    BodegasComponent.ctorParameters = function () { return [
        { type: _angular_router__WEBPACK_IMPORTED_MODULE_3__["Router"] },
        { type: _peticiones_service__WEBPACK_IMPORTED_MODULE_4__["PeticionesService"] }
    ]; };
    BodegasComponent = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_2__["Component"])({
            selector: 'app-bodegas',
            template: _raw_loader_bodegas_component_html__WEBPACK_IMPORTED_MODULE_0__["default"],
            styles: [_bodegas_component_css__WEBPACK_IMPORTED_MODULE_1__["default"]]
        }),
        __metadata("design:paramtypes", [_angular_router__WEBPACK_IMPORTED_MODULE_3__["Router"], _peticiones_service__WEBPACK_IMPORTED_MODULE_4__["PeticionesService"]])
    ], BodegasComponent);
    return BodegasComponent;
}());



/***/ }),

/***/ "G9Dd":
/*!****************************************************************!*\
  !*** ./src/app/pages/user-profile/user-profile.component.scss ***!
  \****************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = ("\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IiIsImZpbGUiOiJzcmMvYXBwL3BhZ2VzL3VzZXItcHJvZmlsZS91c2VyLXByb2ZpbGUuY29tcG9uZW50LnNjc3MifQ== */");

/***/ }),

/***/ "G9k0":
/*!**************************************************************!*\
  !*** ./src/app/pages/user-profile/user-profile.component.ts ***!
  \**************************************************************/
/*! exports provided: UserProfileComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "UserProfileComponent", function() { return UserProfileComponent; });
/* harmony import */ var _raw_loader_user_profile_component_html__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! raw-loader!./user-profile.component.html */ "A8Te");
/* harmony import */ var _user_profile_component_scss__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./user-profile.component.scss */ "G9Dd");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/core */ "fXoL");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/router */ "tyNb");
/* harmony import */ var _peticiones_service__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ../../peticiones.service */ "kW39");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};





var UserProfileComponent = /** @class */ (function () {
    function UserProfileComponent(router, app) {
        this.router = router;
        this.app = app;
        this.InfoUsuario = null;
        this.roles = null;
        if (sessionStorage.length == 0) {
            this.router.navigate(['/login']);
        }
    }
    UserProfileComponent.prototype.ngOnInit = function () {
        this.InfoUsuario = JSON.parse(sessionStorage.getItem('log'));
        this.name = this.InfoUsuario.nombre;
        this.email = this.InfoUsuario.correo;
        this.password = this.InfoUsuario.password;
        this.dpi = this.InfoUsuario.dpi;
        this.fechaN = this.InfoUsuario.fechanacimiento;
        this.roles = this.InfoUsuario.rol;
    };
    UserProfileComponent.prototype.edit = function () {
        var _this = this;
        this.app.postModificar({ "dpi": this.dpi, "nombre": this.name, "fechanacimiento": this.fechaN, "correo": this.email, "pass": this.password }).subscribe(function (resp) {
            if (resp.status == 300) {
                var nuevo = void 0;
                nuevo = { "status": resp.status, "nombre": _this.name, "dpi": _this.dpi, "correo": _this.email, "fechanacimiento": _this.fechaN, "rol": _this.roles, "password": _this.password };
                sessionStorage.setItem('log', JSON.stringify(nuevo));
                alert("Moodify User!");
            }
            else if (resp.status == 100) {
                alert("Error");
            }
        });
    };
    UserProfileComponent.ctorParameters = function () { return [
        { type: _angular_router__WEBPACK_IMPORTED_MODULE_3__["Router"] },
        { type: _peticiones_service__WEBPACK_IMPORTED_MODULE_4__["PeticionesService"] }
    ]; };
    UserProfileComponent = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_2__["Component"])({
            selector: 'app-user-profile',
            template: _raw_loader_user_profile_component_html__WEBPACK_IMPORTED_MODULE_0__["default"],
            styles: [_user_profile_component_scss__WEBPACK_IMPORTED_MODULE_1__["default"]]
        }),
        __metadata("design:paramtypes", [_angular_router__WEBPACK_IMPORTED_MODULE_3__["Router"], _peticiones_service__WEBPACK_IMPORTED_MODULE_4__["PeticionesService"]])
    ], UserProfileComponent);
    return UserProfileComponent;
}());



/***/ }),

/***/ "GvQs":
/*!****************************************************************************************************************!*\
  !*** ./node_modules/raw-loader/dist/cjs.js!./src/app/pages/encargado/view-bodegas/view-bodegas.component.html ***!
  \****************************************************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = ("<div class=\"header pb-5 pt-5 pt-lg-5 d-flex align-items-center\" style=\"min-height: 100px; background-image: url(https://geoinnova.org/cursos/wp-content/uploads/2016/10/1-11-1024x551.jpg); background-size: cover; background-position: center top;\">\r\n    <!-- Mask -->\r\n    <span class=\"mask bg-gradient-danger opacity-8\"></span>\r\n   \r\n  </div>\r\n\r\n  <nav class=\"navbar navbar-horizontal navbar-expand-md navbar-light bg-white\" id=\"sidenav-main\">\r\n    <div class=\"container-fluid\">\r\n      <!-- Toggler -->\r\n      <button class=\"navbar-toggler\" type=\"button\" (click)=\"isCollapsed=!isCollapsed\"\r\n         aria-controls=\"sidenav-collapse-main\">\r\n        <span class=\"navbar-toggler-icon\"></span>\r\n      </button>\r\n    \r\n     \r\n     \r\n      <!-- Collapse -->\r\n      <div class=\"collapse navbar-collapse\"  [ngbCollapse]=\"isCollapsed\" id=\"sidenav-collapse-main\">\r\n        <!-- Collapse header -->\r\n        <div class=\"navbar-collapse-header d-md-none\">\r\n          <div class=\"row\">\r\n            <div class=\"col-6 collapse-brand\">\r\n              <a  routerLinkActive=\"active\" [routerLink]=\"['/dashboard']\">\r\n                <img src=\"./assets/img/brand/blue.png\">\r\n              </a>\r\n            </div>\r\n            <div class=\"col-6 collapse-close\">\r\n              <button type=\"button\" class=\"navbar-toggler\" (click)=\"isCollapsed=!isCollapsed\">\r\n                <span></span>\r\n                <span></span>\r\n              </button>\r\n            </div>\r\n          </div>\r\n        </div>\r\n        <!-- Form -->\r\n        <form class=\"mt-4 mb-3 d-md-none\">\r\n          <div class=\"input-group input-group-rounded input-group-merge\">\r\n            <input type=\"search\" class=\"form-control form-control-rounded form-control-prepended\" placeholder=\"Search\" aria-label=\"Search\">\r\n            <div class=\"input-group-prepend\">\r\n              <div class=\"input-group-text\">\r\n                <span class=\"fa fa-search\"></span>\r\n              </div>\r\n            </div>\r\n          </div>\r\n        </form>\r\n        <!-- Navigation -->\r\n        <ul class=\"navbar-nav\">\r\n            <li *ngFor=\"let menuItem of menuItems\" class=\"{{menuItem.class}} nav-item\">\r\n                <a routerLinkActive=\"active\" [routerLink]=\"[menuItem.path]\" class=\"nav-link\">\r\n                    <i class=\"ni {{menuItem.icon}}\"></i>\r\n                    {{menuItem.title}}\r\n                </a>\r\n               \r\n            </li>\r\n            \r\n        </ul>\r\n        <!-- Divider -->\r\n        <hr class=\"my-3\">\r\n        <!-- Heading -->\r\n       \r\n      </div>\r\n    </div>\r\n  </nav>\r\n\r\n\r\n  \r\n    \r\n\r\n  <div class=\"dark\">\r\n    <div class=\"row\">\r\n        <div *ngFor=\"let solicitud of solicitudes\" class=\"col-sm-3\">\r\n            <div class=\"card space\" style=\"width: 20rem;\">\r\n                <div class=\"card-header\">\r\n                    BODEGAS\r\n                </div>\r\n                <div class=\"card-body\">\r\n                    \r\n                    <div class=\"input-group input-group-alternative\">\r\n                        <div class=\"input-group-prepend\">\r\n                          <span class=\"input-group-text\">ID:</span>\r\n                        </div>\r\n                        <input class=\"form-control\" [(ngModel)] =\"solicitud.id\" type=\"text\">\r\n                      </div>\r\n                   \r\n                      <div class=\"input-group input-group-alternative\">\r\n                        <div class=\"input-group-prepend\">\r\n                          <span class=\"input-group-text\">Bodega:</span>\r\n                        </div>\r\n                        <input class=\"form-control\" [(ngModel)] =\"solicitud.Bodega\" type=\"text\">\r\n                      </div>\r\n\r\n                      <div class=\"input-group input-group-alternative\">\r\n                        <div class=\"input-group-prepend\">\r\n                          <span class=\"input-group-text\">Descripcion:</span>\r\n                        </div>\r\n                        <input class=\"form-control\" [(ngModel)] =\"solicitud.Descripcion\" type=\"text\">\r\n                      </div>\r\n\r\n                      <div class=\"input-group input-group-alternative\">\r\n                        <div class=\"input-group-prepend\">\r\n                          <span class=\"input-group-text\">Estado:</span>\r\n                        </div>\r\n                        <input class=\"form-control\" [(ngModel)]=\"solicitud.Estado\" type=\"text\">\r\n                      </div>\r\n\r\n                      <div class=\"input-group input-group-alternative\">\r\n                        <div class=\"input-group-prepend\">\r\n                          <span class=\"input-group-text\">Sede:</span>\r\n                        </div>\r\n                        <input class=\"form-control\" [(ngModel)]=\"solicitud.Sede\" type=\"text\">\r\n                      </div>\r\n                   \r\n                   \r\n                      <div class=\"input-group input-group-alternative\">\r\n                        <div class=\"input-group-prepend\">\r\n                          <span class=\"input-group-text\">Encargado:</span>\r\n                        </div>\r\n                        <input class=\"form-control\" [(ngModel)] =\"solicitud.Encargado\" type=\"text\">\r\n                      </div>\r\n                      <button  class=\"btn btn-warning space\"  (click)=\"Actualizar(solicitud.id,solicitud.Estado)\">Cambiar estado</button>\r\n                </div>\r\n\r\n            \r\n            </div> \r\n        </div>\r\n    </div>");

/***/ }),

/***/ "H+Fi":
/*!***************************************************************!*\
  !*** ./src/app/pages/encargado/bodegas/bodegas.component.css ***!
  \***************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = ("\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IiIsImZpbGUiOiJzcmMvYXBwL3BhZ2VzL2VuY2FyZ2Fkby9ib2RlZ2FzL2JvZGVnYXMuY29tcG9uZW50LmNzcyJ9 */");

/***/ }),

/***/ "I0jI":
/*!**********************************************************!*\
  !*** ./src/app/pages/encargado/users/users.component.ts ***!
  \**********************************************************/
/*! exports provided: UsersComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "UsersComponent", function() { return UsersComponent; });
/* harmony import */ var _raw_loader_users_component_html__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! raw-loader!./users.component.html */ "2VCV");
/* harmony import */ var _users_component_css__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./users.component.css */ "oJ6d");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/core */ "fXoL");
/* harmony import */ var _peticiones_service__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ../../../peticiones.service */ "kW39");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @angular/router */ "tyNb");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};





var UsersComponent = /** @class */ (function () {
    function UsersComponent(router, app) {
        this.router = router;
        this.app = app;
        this.roles = ['Bodeguero', 'Vendedor', 'Repartidor'];
    }
    UsersComponent.prototype.ngOnInit = function () {
    };
    UsersComponent.prototype.save = function () {
        var _this = this;
        this.app.postCrearUsuario({ "dpi": this.dpi, "nombre": this.name, "fechanac": this.fechaN, "correo": this.email, "password": this.password, "rol": this.opcionSeleccionado }).subscribe(function (resp) {
            alert(resp.msj);
            _this.dpi = 0;
            _this.name = _this.fechaN = _this.email = _this.password = "";
        });
    };
    UsersComponent.ctorParameters = function () { return [
        { type: _angular_router__WEBPACK_IMPORTED_MODULE_4__["Router"] },
        { type: _peticiones_service__WEBPACK_IMPORTED_MODULE_3__["PeticionesService"] }
    ]; };
    UsersComponent = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_2__["Component"])({
            selector: 'app-users',
            template: _raw_loader_users_component_html__WEBPACK_IMPORTED_MODULE_0__["default"],
            styles: [_users_component_css__WEBPACK_IMPORTED_MODULE_1__["default"]]
        }),
        __metadata("design:paramtypes", [_angular_router__WEBPACK_IMPORTED_MODULE_4__["Router"], _peticiones_service__WEBPACK_IMPORTED_MODULE_3__["PeticionesService"]])
    ], UsersComponent);
    return UsersComponent;
}());



/***/ }),

/***/ "IqXj":
/*!*************************************************************!*\
  !*** ./src/app/layouts/admin-layout/admin-layout.module.ts ***!
  \*************************************************************/
/*! exports provided: AdminLayoutModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "AdminLayoutModule", function() { return AdminLayoutModule; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "fXoL");
/* harmony import */ var _angular_common_http__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/common/http */ "tk/3");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/router */ "tyNb");
/* harmony import */ var _angular_common__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/common */ "ofXK");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @angular/forms */ "3Pt+");
/* harmony import */ var ngx_clipboard__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ngx-clipboard */ "Dvla");
/* harmony import */ var _admin_layout_routing__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ./admin-layout.routing */ "qZ7x");
/* harmony import */ var _pages_user_profile_user_profile_component__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! ../../pages/user-profile/user-profile.component */ "G9k0");
/* harmony import */ var _ng_bootstrap_ng_bootstrap__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! @ng-bootstrap/ng-bootstrap */ "1kSV");
/* harmony import */ var _pages_encargado_users_users_component__WEBPACK_IMPORTED_MODULE_9__ = __webpack_require__(/*! ../../pages/encargado/users/users.component */ "I0jI");
/* harmony import */ var _pages_encargado_bodegas_bodegas_component__WEBPACK_IMPORTED_MODULE_10__ = __webpack_require__(/*! ../../pages/encargado/bodegas/bodegas.component */ "ARmr");
/* harmony import */ var _pages_encargado_view_bodegas_view_bodegas_component__WEBPACK_IMPORTED_MODULE_11__ = __webpack_require__(/*! ../../pages/encargado/view-bodegas/view-bodegas.component */ "9yqR");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};









// import { ToastrModule } from 'ngx-toastr';



var AdminLayoutModule = /** @class */ (function () {
    function AdminLayoutModule() {
    }
    AdminLayoutModule = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["NgModule"])({
            imports: [
                _angular_common__WEBPACK_IMPORTED_MODULE_3__["CommonModule"],
                _angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"].forChild(_admin_layout_routing__WEBPACK_IMPORTED_MODULE_6__["AdminLayoutRoutes"]),
                _angular_forms__WEBPACK_IMPORTED_MODULE_4__["FormsModule"],
                _angular_common_http__WEBPACK_IMPORTED_MODULE_1__["HttpClientModule"],
                _ng_bootstrap_ng_bootstrap__WEBPACK_IMPORTED_MODULE_8__["NgbModule"],
                ngx_clipboard__WEBPACK_IMPORTED_MODULE_5__["ClipboardModule"]
            ],
            declarations: [
                _pages_user_profile_user_profile_component__WEBPACK_IMPORTED_MODULE_7__["UserProfileComponent"],
                _pages_encargado_users_users_component__WEBPACK_IMPORTED_MODULE_9__["UsersComponent"],
                _pages_encargado_bodegas_bodegas_component__WEBPACK_IMPORTED_MODULE_10__["BodegasComponent"],
                _pages_encargado_view_bodegas_view_bodegas_component__WEBPACK_IMPORTED_MODULE_11__["ViewBodegasComponent"],
            ]
        })
    ], AdminLayoutModule);
    return AdminLayoutModule;
}());



/***/ }),

/***/ "TkXs":
/*!******************************************************************************************************!*\
  !*** ./node_modules/raw-loader/dist/cjs.js!./src/app/pages/encargado/bodegas/bodegas.component.html ***!
  \******************************************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = ("<div class=\"header pb-5 pt-5 pt-lg-5 d-flex align-items-center\" style=\"min-height: 100px; background-image: url(https://geoinnova.org/cursos/wp-content/uploads/2016/10/1-11-1024x551.jpg); background-size: cover; background-position: center top;\">\r\n    <!-- Mask -->\r\n    <span class=\"mask bg-gradient-danger opacity-8\"></span>\r\n   \r\n  </div>\r\n  \r\n  <nav class=\"navbar navbar-horizontal navbar-expand-md navbar-light bg-white\" id=\"sidenav-main\">\r\n    <div class=\"container-fluid\">\r\n      <!-- Toggler -->\r\n      <button class=\"navbar-toggler\" type=\"button\" (click)=\"isCollapsed=!isCollapsed\"\r\n         aria-controls=\"sidenav-collapse-main\">\r\n        <span class=\"navbar-toggler-icon\"></span>\r\n      </button>\r\n    \r\n     \r\n     \r\n      <!-- Collapse -->\r\n      <div class=\"collapse navbar-collapse\"  [ngbCollapse]=\"isCollapsed\" id=\"sidenav-collapse-main\">\r\n        <!-- Collapse header -->\r\n        <div class=\"navbar-collapse-header d-md-none\">\r\n          <div class=\"row\">\r\n            <div class=\"col-6 collapse-brand\">\r\n              <a  routerLinkActive=\"active\" [routerLink]=\"['/']\">\r\n                <img src=\"./assets/img/brand/blue.png\">\r\n              </a>\r\n            </div>\r\n            <div class=\"col-6 collapse-close\">\r\n              <button type=\"button\" class=\"navbar-toggler\" (click)=\"isCollapsed=!isCollapsed\">\r\n                <span></span>\r\n                <span></span>\r\n              </button>\r\n            </div>\r\n          </div>\r\n        </div>\r\n        <!-- Form -->\r\n        <form class=\"mt-4 mb-3 d-md-none\">\r\n          <div class=\"input-group input-group-rounded input-group-merge\">\r\n            <input type=\"search\" class=\"form-control form-control-rounded form-control-prepended\" placeholder=\"Search\" aria-label=\"Search\">\r\n            <div class=\"input-group-prepend\">\r\n              <div class=\"input-group-text\">\r\n                <span class=\"fa fa-search\"></span>\r\n              </div>\r\n            </div>\r\n          </div>\r\n        </form>\r\n        <!-- Navigation -->\r\n        <ul class=\"navbar-nav\">\r\n            <li *ngFor=\"let menuItem of menuItems\" class=\"{{menuItem.class}} nav-item\">\r\n                <a routerLinkActive=\"active\" [routerLink]=\"[menuItem.path]\" class=\"nav-link\">\r\n                    <i class=\"ni {{menuItem.icon}}\"></i>\r\n                    {{menuItem.title}}\r\n                </a>\r\n               \r\n            </li>\r\n           \r\n        </ul>\r\n        <!-- Divider -->\r\n        <hr class=\"my-3\">\r\n        <!-- Heading -->\r\n       \r\n      </div>\r\n    </div>\r\n  </nav>\r\n  \r\n  \r\n  \r\n    <div class=\"container-fluid mt-2\">\r\n      <div class=\"row\">\r\n        <div class=\"col-xl-4 order-xl-2 mb-5 mb-xl-0\">\r\n          <div class=\"card card-profile shadow\">\r\n            <div class=\"row justify-content-center\">\r\n              <div class=\"col-lg-3 order-lg-2\">\r\n                <div class=\"card-profile-image\">\r\n                  <a href=\"javascript:void(0)\">\r\n                    <img src=\"https://image.flaticon.com/icons/png/512/16/16199.png\" class=\"rounded-circle\">\r\n                  </a>\r\n                </div>\r\n              </div>\r\n            </div>\r\n            <div class=\"card-header text-center border-0 pt-8 pt-md-4 pb-0 pb-md-4\">\r\n              <div class=\"d-flex justify-content-between\">\r\n               \r\n              </div>\r\n            </div>\r\n            <div class=\"card-body pt-0 pt-md-4\">\r\n              <div class=\"row\">\r\n                <div class=\"col\">\r\n                  <div class=\"card-profile-stats d-flex justify-content-center mt-md-5\">\r\n                   \r\n                  </div>\r\n                </div>\r\n              </div>\r\n              <div class=\"text-center\">\r\n                \r\n              </div>\r\n            </div>\r\n          </div>\r\n        </div>\r\n        <div class=\"col-xl-8 order-xl-1\">\r\n          <div class=\"card bg-secondary shadow\">\r\n            <div class=\"card-header bg-white border-0\">\r\n              <div class=\"row align-items-center\">\r\n                <div class=\"col-8\">\r\n                  <h3 class=\"mb-0\">REGISTER BODEGA</h3>\r\n                </div>\r\n                <div class=\"col-4 text-right\">\r\n                  <a  (click)=\"registrar()\" class=\"btn btn-sm btn-primary\">save</a>\r\n                </div>\r\n              </div>\r\n            </div>\r\n              <div class=\"card-body\">\r\n              \r\n                  <h6 class=\"heading-small text-muted mb-4\">DATA</h6>\r\n                  <div class=\"pl-lg-4\">\r\n                    <div class=\"row\">\r\n                      <div class=\"col-lg-6\">\r\n                        <div class=\"form-group\">\r\n                          <label class=\"form-control-label\" for=\"input-username\">Nombre</label>\r\n                          <input type=\"text\" [(ngModel)]=\"nombre\" class=\"form-control form-control-alternative\"  >\r\n                        </div>\r\n                      </div>\r\n                      <div class=\"col-lg-6\">\r\n                        <div class=\"form-group\">\r\n                          <label class=\"form-control-label\" for=\"input-email\">Decripcion</label>\r\n                          <input type=\"text\" [(ngModel)]=\"descripcion\" class=\"form-control form-control-alternative\" >\r\n                        </div>\r\n                      </div>\r\n                    </div>\r\n                    <div class=\"row\">\r\n                      <div class=\"col-lg-6\">\r\n                        <div class=\"form-group\">\r\n                          <label class=\"form-control-label\" for=\"input-first-name\">Estado</label>\r\n                          <input type=\"text\"  [(ngModel)]=\"estado\" class=\"form-control form-control-alternative\"  >\r\n                        </div>\r\n                      </div>\r\n                      <div class=\"col-lg-6\">\r\n                        <div class=\"form-group\">\r\n                          <label class=\"form-control-label\" for=\"input-last-name\">ID_Encargado</label>\r\n                          <input type=\"number\" [(ngModel)]=\"id_encargado\" class=\"form-control form-control-alternative\" >\r\n                        </div>\r\n                      </div>\r\n                    </div>\r\n      \r\n                   \r\n                  </div>\r\n                \r\n                 \r\n                 \r\n               \r\n              </div>\r\n            </div>\r\n          </div>\r\n        </div>\r\n      </div>\r\n      \r\n    ");

/***/ }),

/***/ "oJ6d":
/*!***********************************************************!*\
  !*** ./src/app/pages/encargado/users/users.component.css ***!
  \***********************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = ("\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IiIsImZpbGUiOiJzcmMvYXBwL3BhZ2VzL2VuY2FyZ2Fkby91c2Vycy91c2Vycy5jb21wb25lbnQuY3NzIn0= */");

/***/ }),

/***/ "qZ7x":
/*!**************************************************************!*\
  !*** ./src/app/layouts/admin-layout/admin-layout.routing.ts ***!
  \**************************************************************/
/*! exports provided: AdminLayoutRoutes */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "AdminLayoutRoutes", function() { return AdminLayoutRoutes; });
/* harmony import */ var _pages_user_profile_user_profile_component__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ../../pages/user-profile/user-profile.component */ "G9k0");
/* harmony import */ var _pages_encargado_users_users_component__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ../../pages/encargado/users/users.component */ "I0jI");
/* harmony import */ var _pages_encargado_bodegas_bodegas_component__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../../pages/encargado/bodegas/bodegas.component */ "ARmr");
/* harmony import */ var _pages_encargado_view_bodegas_view_bodegas_component__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ../../pages/encargado/view-bodegas/view-bodegas.component */ "9yqR");




var AdminLayoutRoutes = [
    { path: 'user-profile', component: _pages_user_profile_user_profile_component__WEBPACK_IMPORTED_MODULE_0__["UserProfileComponent"] },
    { path: 'users-encargado', component: _pages_encargado_users_users_component__WEBPACK_IMPORTED_MODULE_1__["UsersComponent"] },
    { path: 'bodegas', component: _pages_encargado_bodegas_bodegas_component__WEBPACK_IMPORTED_MODULE_2__["BodegasComponent"] },
    { path: 'view-bodegas', component: _pages_encargado_view_bodegas_view_bodegas_component__WEBPACK_IMPORTED_MODULE_3__["ViewBodegasComponent"] },
];


/***/ })

}]);
//# sourceMappingURL=layouts-admin-layout-admin-layout-module.js.map