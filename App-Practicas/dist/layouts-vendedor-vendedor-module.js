(window["webpackJsonp"] = window["webpackJsonp"] || []).push([["layouts-vendedor-vendedor-module"],{

/***/ "/6Mf":
/*!******************************************************!*\
  !*** ./src/app/layouts/vendedor/vendedor.routing.ts ***!
  \******************************************************/
/*! exports provided: VendedorRoutes */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "VendedorRoutes", function() { return VendedorRoutes; });
/* harmony import */ var _pages_vendedor_profile_profile_component__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ../../pages/vendedor/profile/profile.component */ "Kqhm");
/* harmony import */ var _pages_vendedor_form_clients_form_clients_component__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ../../pages/vendedor/form-clients/form-clients.component */ "k+Sr");
/* harmony import */ var _pages_vendedor_form_sales_form_sales_component__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../../pages/vendedor/form-sales/form-sales.component */ "sF19");
/* harmony import */ var _pages_vendedor_sales_report_sales_report_component__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ../../pages/vendedor/sales-report/sales-report.component */ "edOk");




var VendedorRoutes = [
    { path: 'profile-vendedor', component: _pages_vendedor_profile_profile_component__WEBPACK_IMPORTED_MODULE_0__["ProfileComponent"] },
    { path: 'form-clients', component: _pages_vendedor_form_clients_form_clients_component__WEBPACK_IMPORTED_MODULE_1__["FormClientsComponent"] },
    { path: 'form-sales', component: _pages_vendedor_form_sales_form_sales_component__WEBPACK_IMPORTED_MODULE_2__["FormSalesComponent"] },
    { path: 'sales-report', component: _pages_vendedor_sales_report_sales_report_component__WEBPACK_IMPORTED_MODULE_3__["SalesReportComponent"] },
];


/***/ }),

/***/ "Kqhm":
/*!*************************************************************!*\
  !*** ./src/app/pages/vendedor/profile/profile.component.ts ***!
  \*************************************************************/
/*! exports provided: ProfileComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "ProfileComponent", function() { return ProfileComponent; });
/* harmony import */ var _raw_loader_profile_component_html__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! raw-loader!./profile.component.html */ "V81z");
/* harmony import */ var _profile_component_css__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./profile.component.css */ "NI/I");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/core */ "fXoL");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/router */ "tyNb");
/* harmony import */ var _peticiones_service__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ../../../peticiones.service */ "kW39");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};





var ProfileComponent = /** @class */ (function () {
    function ProfileComponent(router, app) {
        this.router = router;
        this.app = app;
        this.InfoUsuario = null;
        this.roles = null;
        if (sessionStorage.length == 0) {
            this.router.navigate(['/login']);
        }
    }
    ProfileComponent.prototype.ngOnInit = function () {
        this.InfoUsuario = JSON.parse(sessionStorage.getItem('log'));
        this.name = this.InfoUsuario.nombre;
        this.email = this.InfoUsuario.correo;
        this.password = this.InfoUsuario.password;
        this.dpi = this.InfoUsuario.dpi;
        this.fechaN = this.InfoUsuario.fechanacimiento;
        this.roles = this.InfoUsuario.rol;
    };
    ProfileComponent.prototype.edit = function () {
        var _this = this;
        this.app.postModificar({ "dpi": this.dpi, "nombre": this.name, "fechanacimiento": this.fechaN, "correo": this.email, "pass": this.password }).subscribe(function (resp) {
            if (resp.status == 300) {
                var nuevo = void 0;
                nuevo = { "status": resp.status, "nombre": _this.name, "dpi": _this.dpi, "correo": _this.email, "fechanacimiento": _this.fechaN, "rol": _this.roles, "password": _this.password };
                sessionStorage.setItem('log', JSON.stringify(nuevo));
                alert("Moodify User!");
            }
            else if (resp.status == 100) {
                alert("Error");
            }
        });
    };
    ProfileComponent.ctorParameters = function () { return [
        { type: _angular_router__WEBPACK_IMPORTED_MODULE_3__["Router"] },
        { type: _peticiones_service__WEBPACK_IMPORTED_MODULE_4__["PeticionesService"] }
    ]; };
    ProfileComponent = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_2__["Component"])({
            selector: 'app-profile',
            template: _raw_loader_profile_component_html__WEBPACK_IMPORTED_MODULE_0__["default"],
            styles: [_profile_component_css__WEBPACK_IMPORTED_MODULE_1__["default"]]
        }),
        __metadata("design:paramtypes", [_angular_router__WEBPACK_IMPORTED_MODULE_3__["Router"], _peticiones_service__WEBPACK_IMPORTED_MODULE_4__["PeticionesService"]])
    ], ProfileComponent);
    return ProfileComponent;
}());



/***/ }),

/***/ "NI/I":
/*!**************************************************************!*\
  !*** ./src/app/pages/vendedor/profile/profile.component.css ***!
  \**************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = ("\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IiIsImZpbGUiOiJzcmMvYXBwL3BhZ2VzL3ZlbmRlZG9yL3Byb2ZpbGUvcHJvZmlsZS5jb21wb25lbnQuY3NzIn0= */");

/***/ }),

/***/ "QF2h":
/*!*****************************************************!*\
  !*** ./src/app/layouts/vendedor/vendedor.module.ts ***!
  \*****************************************************/
/*! exports provided: VendedorModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "VendedorModule", function() { return VendedorModule; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "fXoL");
/* harmony import */ var _angular_common_http__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/common/http */ "tk/3");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/router */ "tyNb");
/* harmony import */ var _angular_common__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/common */ "ofXK");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @angular/forms */ "3Pt+");
/* harmony import */ var _vendedor_routing__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ./vendedor.routing */ "/6Mf");
/* harmony import */ var ngx_clipboard__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ngx-clipboard */ "Dvla");
/* harmony import */ var _ng_bootstrap_ng_bootstrap__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! @ng-bootstrap/ng-bootstrap */ "1kSV");
/* harmony import */ var _pages_vendedor_profile_profile_component__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! ../../pages/vendedor/profile/profile.component */ "Kqhm");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};









var VendedorModule = /** @class */ (function () {
    function VendedorModule() {
    }
    VendedorModule = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["NgModule"])({
            imports: [
                _angular_common__WEBPACK_IMPORTED_MODULE_3__["CommonModule"],
                _angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"].forChild(_vendedor_routing__WEBPACK_IMPORTED_MODULE_5__["VendedorRoutes"]),
                _angular_forms__WEBPACK_IMPORTED_MODULE_4__["FormsModule"],
                _angular_common_http__WEBPACK_IMPORTED_MODULE_1__["HttpClientModule"],
                _ng_bootstrap_ng_bootstrap__WEBPACK_IMPORTED_MODULE_7__["NgbModule"],
                ngx_clipboard__WEBPACK_IMPORTED_MODULE_6__["ClipboardModule"]
            ],
            declarations: [
                _pages_vendedor_profile_profile_component__WEBPACK_IMPORTED_MODULE_8__["ProfileComponent"],
            ]
        })
    ], VendedorModule);
    return VendedorModule;
}());



/***/ }),

/***/ "V81z":
/*!*****************************************************************************************************!*\
  !*** ./node_modules/raw-loader/dist/cjs.js!./src/app/pages/vendedor/profile/profile.component.html ***!
  \*****************************************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = ("<div class=\"header pb-8 pt-5 pt-lg-8 d-flex align-items-center\" style=\"min-height: 200px; background-image: url(https://geoinnova.org/cursos/wp-content/uploads/2016/10/1-11-1024x551.jpg); background-size: cover; background-position: center top;\">\r\n    <!-- Mask -->\r\n    <span class=\"mask bg-gradient-danger opacity-8\"></span>\r\n\r\n  </div>\r\n  <div class=\"container-fluid mt--7\">\r\n    <div class=\"row\">\r\n      <div class=\"col-xl-4 order-xl-2 mb-5 mb-xl-0\">\r\n        <div class=\"card card-profile shadow\">\r\n          <div class=\"row justify-content-center\">\r\n            <div class=\"col-lg-3 order-lg-2\">\r\n              <div class=\"card-profile-image\">\r\n                <a href=\"javascript:void(0)\">\r\n                  <img src=\"https://www.softzone.es/app/uploads-softzone.es/2018/04/guest.png\" class=\"rounded-circle\">\r\n                </a>\r\n              </div>\r\n            </div>\r\n          </div>\r\n          <div class=\"card-header text-center border-0 pt-8 pt-md-4 pb-0 pb-md-4\">\r\n            <div class=\"d-flex justify-content-between\">\r\n\r\n            </div>\r\n          </div>\r\n          <div class=\"card-body pt-0 pt-md-4\">\r\n            <div class=\"row\">\r\n              <div class=\"col\">\r\n                <div class=\"card-profile-stats d-flex justify-content-center mt-md-5\">\r\n\r\n                </div>\r\n              </div>\r\n            </div>\r\n            <div class=\"text-center\">\r\n\r\n            </div>\r\n          </div>\r\n        </div>\r\n      </div>\r\n      <div class=\"col-xl-8 order-xl-1\">\r\n        <div class=\"card bg-secondary shadow\">\r\n          <div class=\"card-header bg-white border-0\">\r\n            <div class=\"row align-items-center\">\r\n              <div class=\"col-8\">\r\n                <h3 class=\"mb-0\">My account</h3>\r\n              </div>\r\n              <div class=\"col-4 text-right\">\r\n                <a  (click)=\"edit()\" class=\"btn btn-sm btn-primary\">UPDATE</a>\r\n              </div>\r\n            </div>\r\n          </div>\r\n          <div class=\"card-body\">\r\n\r\n              <h6 class=\"heading-small text-muted mb-4\">User information</h6>\r\n              <div class=\"pl-lg-4\">\r\n                <div class=\"row\">\r\n                  <div class=\"col-lg-6\">\r\n                    <div class=\"form-group\">\r\n                      <label class=\"form-control-label\" for=\"input-username\">Username</label>\r\n                      <input type=\"text\" id=\"input-username\" [(ngModel)]=\"name\" class=\"form-control form-control-alternative\"  value=\"{{name}}\">\r\n                    </div>\r\n                  </div>\r\n                  <div class=\"col-lg-6\">\r\n                    <div class=\"form-group\">\r\n                      <label class=\"form-control-label\" for=\"input-email\">Email address</label>\r\n                      <input type=\"text\" [(ngModel)]=\"email\" class=\"form-control form-control-alternative\" value=\"{{email}}\">\r\n                    </div>\r\n                  </div>\r\n                </div>\r\n                <div class=\"row\">\r\n                  <div class=\"col-lg-6\">\r\n                    <div class=\"form-group\">\r\n                      <label class=\"form-control-label\" for=\"input-firste-name\">Date of Birth</label>\r\n                      <input type=\"text\" id=\"input-first-najme\" [(ngModel)]=\"fechaN\" class=\"form-control form-control-alternative\"  value=\"{{fechaN}}\">\r\n                    </div>\r\n                  </div>\r\n                  <div class=\"col-lg-6\">\r\n                    <div class=\"form-group\">\r\n                      <label class=\"form-control-label\" for=\"input-last-name\">DPI</label>\r\n                      <input type=\"text\" id=\"input-last-namfe\" class=\"form-control form-control-alternative\"  value=\"{{dpi}}\"  disabled=\"disabled\">\r\n                    </div>\r\n                  </div>\r\n                </div>\r\n\r\n                <div class=\"row\">\r\n                  <div class=\"col-lg-6\">\r\n                    <div class=\"form-group\">\r\n                      <label class=\"form-control-label\" for=\"input-first-name\">Password</label>\r\n                      <input type=\"text\" id=\"input-first-name\" [(ngModel)]=\"password\" class=\"form-control form-control-alternative\"  value=\"{{password}}\">\r\n                    </div>\r\n                  </div>\r\n                  <div class=\"col-lg-6\">\r\n                    <div class=\"form-group\">\r\n                      <label class=\"form-control-label\" for=\"input-last-namwe\">roles</label>\r\n                      <select class=\"form-control form-control-alternative\">\r\n                        <option *ngFor=\"let rol of roles\">\r\n                          {{rol.descripcion}}\r\n                        </option>\r\n                      </select>\r\n                    </div>\r\n                  </div>\r\n                </div>\r\n              </div>\r\n\r\n\r\n\r\n\r\n          </div>\r\n        </div>\r\n      </div>\r\n    </div>\r\n  </div>\r\n");

/***/ })

}]);
//# sourceMappingURL=layouts-vendedor-vendedor-module.js.map