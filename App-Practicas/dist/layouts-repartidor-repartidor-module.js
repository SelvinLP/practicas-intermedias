(window["webpackJsonp"] = window["webpackJsonp"] || []).push([["layouts-repartidor-repartidor-module"],{

/***/ "8IWD":
/*!**********************************************************!*\
  !*** ./src/app/layouts/repartidor/repartidor.routing.ts ***!
  \**********************************************************/
/*! exports provided: RepartidorRoutes */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "RepartidorRoutes", function() { return RepartidorRoutes; });
/* harmony import */ var _pages_repartidor_profile_profile_component__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ../../pages/repartidor/profile/profile.component */ "lCUN");
/* harmony import */ var _pages_repartidor_transferencias_transferencias_component__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ../../pages/repartidor/transferencias/transferencias.component */ "n4P7");
/* harmony import */ var _pages_repartidor_ventas_ventas_component__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../../pages/repartidor/ventas/ventas.component */ "tyEZ");



var RepartidorRoutes = [
    { path: 'profile-repartidor', component: _pages_repartidor_profile_profile_component__WEBPACK_IMPORTED_MODULE_0__["ProfileComponent"] },
    { path: 'repartidor-transferencias', component: _pages_repartidor_transferencias_transferencias_component__WEBPACK_IMPORTED_MODULE_1__["TransferenciasComponent"] },
    { path: 'repartidor-ventas', component: _pages_repartidor_ventas_ventas_component__WEBPACK_IMPORTED_MODULE_2__["VentasComponent"] },
];


/***/ }),

/***/ "Mw1N":
/*!******************************************************************************!*\
  !*** ./src/app/pages/repartidor/transferencias/transferencias.component.css ***!
  \******************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = ("\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IiIsImZpbGUiOiJzcmMvYXBwL3BhZ2VzL3JlcGFydGlkb3IvdHJhbnNmZXJlbmNpYXMvdHJhbnNmZXJlbmNpYXMuY29tcG9uZW50LmNzcyJ9 */");

/***/ }),

/***/ "OgTA":
/*!*********************************************************************************************************************!*\
  !*** ./node_modules/raw-loader/dist/cjs.js!./src/app/pages/repartidor/transferencias/transferencias.component.html ***!
  \*********************************************************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = ("<div class=\"header pb-5 pt-5 pt-lg-5 d-flex align-items-center\" style=\"min-height: 100px; background-image: url(https://geoinnova.org/cursos/wp-content/uploads/2016/10/1-11-1024x551.jpg); background-size: cover; background-position: center top;\">\r\n    <!-- Mask -->\r\n    <span class=\"mask bg-gradient-danger opacity-8\"></span>\r\n   \r\n  </div>\r\n\r\n  <nav class=\"navbar navbar-horizontal navbar-expand-md navbar-light bg-white\" id=\"sidenav-main\">\r\n    <div class=\"container-fluid\">\r\n      <!-- Toggler -->\r\n      <button class=\"navbar-toggler\" type=\"button\" (click)=\"isCollapsed=!isCollapsed\"\r\n         aria-controls=\"sidenav-collapse-main\">\r\n        <span class=\"navbar-toggler-icon\"></span>\r\n      </button>\r\n    \r\n     \r\n     \r\n      <!-- Collapse -->\r\n      <div class=\"collapse navbar-collapse\"  [ngbCollapse]=\"isCollapsed\" id=\"sidenav-collapse-main\">\r\n        <!-- Collapse header -->\r\n        <div class=\"navbar-collapse-header d-md-none\">\r\n          <div class=\"row\">\r\n            <div class=\"col-6 collapse-brand\">\r\n              <a  routerLinkActive=\"active\" [routerLink]=\"['/dashboard']\">\r\n                <img src=\"./assets/img/brand/blue.png\">\r\n              </a>\r\n            </div>\r\n            <div class=\"col-6 collapse-close\">\r\n              <button type=\"button\" class=\"navbar-toggler\" (click)=\"isCollapsed=!isCollapsed\">\r\n                <span></span>\r\n                <span></span>\r\n              </button>\r\n            </div>\r\n          </div>\r\n        </div>\r\n        <!-- Form -->\r\n        <form class=\"mt-4 mb-3 d-md-none\">\r\n          <div class=\"input-group input-group-rounded input-group-merge\">\r\n            <input type=\"search\" class=\"form-control form-control-rounded form-control-prepended\" placeholder=\"Search\" aria-label=\"Search\">\r\n            <div class=\"input-group-prepend\">\r\n              <div class=\"input-group-text\">\r\n                <span class=\"fa fa-search\"></span>\r\n              </div>\r\n            </div>\r\n          </div>\r\n        </form>\r\n        <!-- Navigation -->\r\n        <ul class=\"navbar-nav\">\r\n            <li *ngFor=\"let menuItem of menuItems\" class=\"{{menuItem.class}} nav-item\">\r\n                <a routerLinkActive=\"active\" [routerLink]=\"[menuItem.path]\" class=\"nav-link\">\r\n                    <i class=\"ni {{menuItem.icon}}\"></i>\r\n                    {{menuItem.title}}\r\n                </a>\r\n               \r\n            </li>\r\n            \r\n        </ul>\r\n        <!-- Divider -->\r\n        <hr class=\"my-3\">\r\n        <!-- Heading -->\r\n       \r\n      </div>\r\n    </div>\r\n  </nav>\r\n\r\n\r\n  \r\n    \r\n\r\n  <div class=\"dark\">\r\n    <div class=\"row\">\r\n        <div *ngFor=\"let solicitud of solicitudes\" class=\"col-sm-3\">\r\n            <div class=\"card space\" style=\"width: 20rem;\">\r\n                <div class=\"card-header\">\r\n                    Producto\r\n                </div>\r\n                <div class=\"card-body\">\r\n                    \r\n                    <div class=\"input-group input-group-alternative\">\r\n                        <div class=\"input-group-prepend\">\r\n                          <span class=\"input-group-text\">ID:</span>\r\n                        </div>\r\n                        <input class=\"form-control\" [(ngModel)] =\"solicitud.Id\" type=\"text\">\r\n                      </div>\r\n                   \r\n                      <div class=\"input-group input-group-alternative\">\r\n                        <div class=\"input-group-prepend\">\r\n                          <span class=\"input-group-text\">Estado:</span>\r\n                        </div>\r\n                        <input class=\"form-control\"  [(ngModel)] =\"solicitud.Estado\" type=\"text\">\r\n                      </div>\r\n\r\n                      <div class=\"input-group input-group-alternative\">\r\n                        <div class=\"input-group-prepend\">\r\n                          <span class=\"input-group-text\">Creador:</span>\r\n                        </div>\r\n                        <input class=\"form-control\"  [(ngModel)] =\"solicitud.Creador\" type=\"text\">\r\n                      </div>\r\n\r\n                      <div class=\"input-group input-group-alternative\">\r\n                        <div class=\"input-group-prepend\">\r\n                          <span class=\"input-group-text\">Bodega Origen:</span>\r\n                        </div>\r\n                        <input class=\"form-control\" [(ngModel)] =\"solicitud.Bodega_solicitante\" type=\"text\">\r\n                      </div>\r\n\r\n                      <div class=\"input-group input-group-alternative\">\r\n                        <div class=\"input-group-prepend\">\r\n                          <span class=\"input-group-text\">Descripcion:</span>\r\n                        </div>\r\n                        <input class=\"form-control\" [(ngModel)] =\"solicitud.Descripcion\" type=\"text\">\r\n                      </div>\r\n\r\n\r\n                      \r\n                   \r\n                   \r\n                      <button  class=\"btn btn-warning space\"  (click)=\"Actualizar(solicitud.Id)\">Aceptar</button>\r\n                      \r\n                </div>\r\n            </div> \r\n        </div>\r\n    </div>");

/***/ }),

/***/ "Z/Sj":
/*!*******************************************************************************************************!*\
  !*** ./node_modules/raw-loader/dist/cjs.js!./src/app/pages/repartidor/profile/profile.component.html ***!
  \*******************************************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = ("<div class=\"header pb-8 pt-5 pt-lg-8 d-flex align-items-center\" style=\"min-height: 200px; background-image: url(https://geoinnova.org/cursos/wp-content/uploads/2016/10/1-11-1024x551.jpg); background-size: cover; background-position: center top;\">\r\n    <!-- Mask -->\r\n    <span class=\"mask bg-gradient-danger opacity-8\"></span>\r\n   \r\n  </div>\r\n  <div class=\"container-fluid mt--7\">\r\n    <div class=\"row\">\r\n      <div class=\"col-xl-4 order-xl-2 mb-5 mb-xl-0\">\r\n        <div class=\"card card-profile shadow\">\r\n          <div class=\"row justify-content-center\">\r\n            <div class=\"col-lg-3 order-lg-2\">\r\n              <div class=\"card-profile-image\">\r\n                <a href=\"javascript:void(0)\">\r\n                  <img src=\"https://www.softzone.es/app/uploads-softzone.es/2018/04/guest.png\" class=\"rounded-circle\">\r\n                </a>\r\n              </div>\r\n            </div>\r\n          </div>\r\n          <div class=\"card-header text-center border-0 pt-8 pt-md-4 pb-0 pb-md-4\">\r\n            <div class=\"d-flex justify-content-between\">\r\n             \r\n            </div>\r\n          </div>\r\n          <div class=\"card-body pt-0 pt-md-4\">\r\n            <div class=\"row\">\r\n              <div class=\"col\">\r\n                <div class=\"card-profile-stats d-flex justify-content-center mt-md-5\">\r\n                 \r\n                </div>\r\n              </div>\r\n            </div>\r\n            <div class=\"text-center\">\r\n              \r\n            </div>\r\n          </div>\r\n        </div>\r\n      </div>\r\n      <div class=\"col-xl-8 order-xl-1\">\r\n        <div class=\"card bg-secondary shadow\">\r\n          <div class=\"card-header bg-white border-0\">\r\n            <div class=\"row align-items-center\">\r\n              <div class=\"col-8\">\r\n                <h3 class=\"mb-0\">My account</h3>\r\n              </div>\r\n              <div class=\"col-4 text-right\">\r\n                <a  (click)=\"edit()\" class=\"btn btn-sm btn-primary\">UPDATE</a>\r\n              </div>\r\n            </div>\r\n          </div>\r\n          <div class=\"card-body\">\r\n          \r\n              <h6 class=\"heading-small text-muted mb-4\">User information</h6>\r\n              <div class=\"pl-lg-4\">\r\n                <div class=\"row\">\r\n                  <div class=\"col-lg-6\">\r\n                    <div class=\"form-group\">\r\n                      <label class=\"form-control-label\" for=\"input-username\">Username</label>\r\n                      <input type=\"text\" id=\"input-username\" [(ngModel)]=\"name\" class=\"form-control form-control-alternative\"  value=\"{{name}}\">\r\n                    </div>\r\n                  </div>\r\n                  <div class=\"col-lg-6\">\r\n                    <div class=\"form-group\">\r\n                      <label class=\"form-control-label\" for=\"input-email\">Email address</label>\r\n                      <input type=\"text\" [(ngModel)]=\"email\" class=\"form-control form-control-alternative\" value=\"{{email}}\">\r\n                    </div>\r\n                  </div>\r\n                </div>\r\n                <div class=\"row\">\r\n                  <div class=\"col-lg-6\">\r\n                    <div class=\"form-group\">\r\n                      <label class=\"form-control-label\" for=\"input-firste-name\">Date of Birth</label>\r\n                      <input type=\"text\" id=\"input-first-najme\" [(ngModel)]=\"fechaN\" class=\"form-control form-control-alternative\"  value=\"{{fechaN}}\">\r\n                    </div>\r\n                  </div>\r\n                  <div class=\"col-lg-6\">\r\n                    <div class=\"form-group\">\r\n                      <label class=\"form-control-label\" for=\"input-last-name\">DPI</label>\r\n                      <input type=\"text\" id=\"input-last-namfe\" class=\"form-control form-control-alternative\"  value=\"{{dpi}}\"  disabled=\"disabled\">\r\n                    </div>\r\n                  </div>\r\n                </div>\r\n  \r\n                <div class=\"row\">\r\n                  <div class=\"col-lg-6\">\r\n                    <div class=\"form-group\">\r\n                      <label class=\"form-control-label\" for=\"input-first-name\">Password</label>\r\n                      <input type=\"text\" id=\"input-first-name\" [(ngModel)]=\"password\" class=\"form-control form-control-alternative\"  value=\"{{password}}\">\r\n                    </div>\r\n                  </div>\r\n                  <div class=\"col-lg-6\">\r\n                    <div class=\"form-group\">\r\n                      <label class=\"form-control-label\" for=\"input-last-namwe\">roles</label>\r\n                      <select class=\"form-control form-control-alternative\">\r\n                        <option *ngFor=\"let rol of roles\">\r\n                          {{rol.descripcion}}\r\n                        </option>\r\n                      </select>\r\n                    </div>\r\n                  </div>\r\n                </div>\r\n              </div>\r\n            \r\n             \r\n             \r\n           \r\n          </div>\r\n        </div>\r\n      </div>\r\n    </div>\r\n  </div>\r\n  ");

/***/ }),

/***/ "cxGI":
/*!**************************************************************!*\
  !*** ./src/app/pages/repartidor/ventas/ventas.component.css ***!
  \**************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = ("\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IiIsImZpbGUiOiJzcmMvYXBwL3BhZ2VzL3JlcGFydGlkb3IvdmVudGFzL3ZlbnRhcy5jb21wb25lbnQuY3NzIn0= */");

/***/ }),

/***/ "lCUN":
/*!***************************************************************!*\
  !*** ./src/app/pages/repartidor/profile/profile.component.ts ***!
  \***************************************************************/
/*! exports provided: ProfileComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "ProfileComponent", function() { return ProfileComponent; });
/* harmony import */ var _raw_loader_profile_component_html__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! raw-loader!./profile.component.html */ "Z/Sj");
/* harmony import */ var _profile_component_css__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./profile.component.css */ "s89l");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/core */ "fXoL");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/router */ "tyNb");
/* harmony import */ var _peticiones_service__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ../../../peticiones.service */ "kW39");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};





var ProfileComponent = /** @class */ (function () {
    function ProfileComponent(router, app) {
        this.router = router;
        this.app = app;
        this.InfoUsuario = null;
        this.roles = null;
        if (sessionStorage.length == 0) {
            this.router.navigate(['/login']);
        }
    }
    ProfileComponent.prototype.ngOnInit = function () {
        this.InfoUsuario = JSON.parse(sessionStorage.getItem('log'));
        this.name = this.InfoUsuario.nombre;
        this.email = this.InfoUsuario.correo;
        this.password = this.InfoUsuario.password;
        this.dpi = this.InfoUsuario.dpi;
        this.fechaN = this.InfoUsuario.fechanacimiento;
        this.roles = this.InfoUsuario.rol;
    };
    ProfileComponent.prototype.edit = function () {
        var _this = this;
        this.app.postModificar({ "dpi": this.dpi, "nombre": this.name, "fechanacimiento": this.fechaN, "correo": this.email, "pass": this.password }).subscribe(function (resp) {
            if (resp.status == 300) {
                _this.app.postLogin({ "email": _this.email, "password": _this.password }).subscribe(function (resp1) {
                    if (resp1.status == 300) {
                        var nuevo = void 0;
                        nuevo = { "status": resp1.status, "nombre": resp1.nombre, "dpi": resp1.dpi, "correo": resp1.correo, "fechanacimiento": resp1.fechanacimiento, "rol": resp1.rol, "password": _this.password };
                        sessionStorage.setItem('log', JSON.stringify(nuevo));
                        alert("Moodify User!");
                        _this.router.navigate(['/login']);
                    }
                });
            }
            else if (resp.status == 100) {
                alert("Error");
            }
        });
    };
    ProfileComponent.ctorParameters = function () { return [
        { type: _angular_router__WEBPACK_IMPORTED_MODULE_3__["Router"] },
        { type: _peticiones_service__WEBPACK_IMPORTED_MODULE_4__["PeticionesService"] }
    ]; };
    ProfileComponent = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_2__["Component"])({
            selector: 'app-profile',
            template: _raw_loader_profile_component_html__WEBPACK_IMPORTED_MODULE_0__["default"],
            styles: [_profile_component_css__WEBPACK_IMPORTED_MODULE_1__["default"]]
        }),
        __metadata("design:paramtypes", [_angular_router__WEBPACK_IMPORTED_MODULE_3__["Router"], _peticiones_service__WEBPACK_IMPORTED_MODULE_4__["PeticionesService"]])
    ], ProfileComponent);
    return ProfileComponent;
}());



/***/ }),

/***/ "n4P7":
/*!*****************************************************************************!*\
  !*** ./src/app/pages/repartidor/transferencias/transferencias.component.ts ***!
  \*****************************************************************************/
/*! exports provided: ROUTES, TransferenciasComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "ROUTES", function() { return ROUTES; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "TransferenciasComponent", function() { return TransferenciasComponent; });
/* harmony import */ var _raw_loader_transferencias_component_html__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! raw-loader!./transferencias.component.html */ "OgTA");
/* harmony import */ var _transferencias_component_css__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./transferencias.component.css */ "Mw1N");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/core */ "fXoL");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/router */ "tyNb");
/* harmony import */ var _peticiones_service__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ../../../peticiones.service */ "kW39");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};





var ROUTES = [
    { path: '/repartidor-ventas', title: 'Ventas', icon: 'ni-tv-2 text-primary', class: '' },
    { path: '/repartidor-transferencias', title: 'Transferencias', icon: 'ni-tv-2 text-primary', class: '' },
];
var TransferenciasComponent = /** @class */ (function () {
    function TransferenciasComponent(router, app) {
        var _this = this;
        this.router = router;
        this.app = app;
        this.isCollapsed = true;
        this.solicitudes = [];
        var InfoUsuario = JSON.parse(sessionStorage.getItem('log'));
        this.app.getTrans(InfoUsuario.id).subscribe(function (resp) {
            _this.solicitudes = resp.trans;
        });
    }
    TransferenciasComponent.prototype.ngOnInit = function () {
        var _this = this;
        this.menuItems = ROUTES.filter(function (menuItem) { return menuItem; });
        this.router.events.subscribe(function (event) {
            _this.isCollapsed = true;
        });
    };
    TransferenciasComponent.prototype.Actualizar = function (id) {
        this.app.actualizarTransferencia(id).subscribe(function (resp) {
            alert(resp.msj);
        });
    };
    TransferenciasComponent.ctorParameters = function () { return [
        { type: _angular_router__WEBPACK_IMPORTED_MODULE_3__["Router"] },
        { type: _peticiones_service__WEBPACK_IMPORTED_MODULE_4__["PeticionesService"] }
    ]; };
    TransferenciasComponent = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_2__["Component"])({
            selector: 'app-transferencias',
            template: _raw_loader_transferencias_component_html__WEBPACK_IMPORTED_MODULE_0__["default"],
            styles: [_transferencias_component_css__WEBPACK_IMPORTED_MODULE_1__["default"]]
        }),
        __metadata("design:paramtypes", [_angular_router__WEBPACK_IMPORTED_MODULE_3__["Router"], _peticiones_service__WEBPACK_IMPORTED_MODULE_4__["PeticionesService"]])
    ], TransferenciasComponent);
    return TransferenciasComponent;
}());



/***/ }),

/***/ "nw+H":
/*!*********************************************************!*\
  !*** ./src/app/layouts/repartidor/repartidor.module.ts ***!
  \*********************************************************/
/*! exports provided: RepartidorModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "RepartidorModule", function() { return RepartidorModule; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "fXoL");
/* harmony import */ var _angular_common_http__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/common/http */ "tk/3");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/router */ "tyNb");
/* harmony import */ var _angular_common__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/common */ "ofXK");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @angular/forms */ "3Pt+");
/* harmony import */ var _repartidor_routing__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ./repartidor.routing */ "8IWD");
/* harmony import */ var ngx_clipboard__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ngx-clipboard */ "Dvla");
/* harmony import */ var _ng_bootstrap_ng_bootstrap__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! @ng-bootstrap/ng-bootstrap */ "1kSV");
/* harmony import */ var _pages_repartidor_profile_profile_component__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! ../../pages/repartidor/profile/profile.component */ "lCUN");
/* harmony import */ var _pages_repartidor_transferencias_transferencias_component__WEBPACK_IMPORTED_MODULE_9__ = __webpack_require__(/*! ../../pages/repartidor/transferencias/transferencias.component */ "n4P7");
/* harmony import */ var _pages_repartidor_ventas_ventas_component__WEBPACK_IMPORTED_MODULE_10__ = __webpack_require__(/*! ../../pages/repartidor/ventas/ventas.component */ "tyEZ");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};











var RepartidorModule = /** @class */ (function () {
    function RepartidorModule() {
    }
    RepartidorModule = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["NgModule"])({
            imports: [
                _angular_common__WEBPACK_IMPORTED_MODULE_3__["CommonModule"],
                _angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"].forChild(_repartidor_routing__WEBPACK_IMPORTED_MODULE_5__["RepartidorRoutes"]),
                _angular_forms__WEBPACK_IMPORTED_MODULE_4__["FormsModule"],
                _angular_common_http__WEBPACK_IMPORTED_MODULE_1__["HttpClientModule"],
                _ng_bootstrap_ng_bootstrap__WEBPACK_IMPORTED_MODULE_7__["NgbModule"],
                ngx_clipboard__WEBPACK_IMPORTED_MODULE_6__["ClipboardModule"]
            ],
            declarations: [
                _pages_repartidor_profile_profile_component__WEBPACK_IMPORTED_MODULE_8__["ProfileComponent"],
                _pages_repartidor_transferencias_transferencias_component__WEBPACK_IMPORTED_MODULE_9__["TransferenciasComponent"],
                _pages_repartidor_ventas_ventas_component__WEBPACK_IMPORTED_MODULE_10__["VentasComponent"]
            ]
        })
    ], RepartidorModule);
    return RepartidorModule;
}());



/***/ }),

/***/ "psQr":
/*!*****************************************************************************************************!*\
  !*** ./node_modules/raw-loader/dist/cjs.js!./src/app/pages/repartidor/ventas/ventas.component.html ***!
  \*****************************************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = ("<div class=\"header pb-5 pt-5 pt-lg-5 d-flex align-items-center\" style=\"min-height: 100px; background-image: url(https://geoinnova.org/cursos/wp-content/uploads/2016/10/1-11-1024x551.jpg); background-size: cover; background-position: center top;\">\r\n    <!-- Mask -->\r\n    <span class=\"mask bg-gradient-danger opacity-8\"></span>\r\n   \r\n  </div>\r\n\r\n  <nav class=\"navbar navbar-horizontal navbar-expand-md navbar-light bg-white\" id=\"sidenav-main\">\r\n    <div class=\"container-fluid\">\r\n      <!-- Toggler -->\r\n      <button class=\"navbar-toggler\" type=\"button\" (click)=\"isCollapsed=!isCollapsed\"\r\n         aria-controls=\"sidenav-collapse-main\">\r\n        <span class=\"navbar-toggler-icon\"></span>\r\n      </button>\r\n    \r\n     \r\n     \r\n      <!-- Collapse -->\r\n      <div class=\"collapse navbar-collapse\"  [ngbCollapse]=\"isCollapsed\" id=\"sidenav-collapse-main\">\r\n        <!-- Collapse header -->\r\n        <div class=\"navbar-collapse-header d-md-none\">\r\n          <div class=\"row\">\r\n            <div class=\"col-6 collapse-brand\">\r\n              <a  routerLinkActive=\"active\" [routerLink]=\"['/dashboard']\">\r\n                <img src=\"./assets/img/brand/blue.png\">\r\n              </a>\r\n            </div>\r\n            <div class=\"col-6 collapse-close\">\r\n              <button type=\"button\" class=\"navbar-toggler\" (click)=\"isCollapsed=!isCollapsed\">\r\n                <span></span>\r\n                <span></span>\r\n              </button>\r\n            </div>\r\n          </div>\r\n        </div>\r\n        <!-- Form -->\r\n        <form class=\"mt-4 mb-3 d-md-none\">\r\n          <div class=\"input-group input-group-rounded input-group-merge\">\r\n            <input type=\"search\" class=\"form-control form-control-rounded form-control-prepended\" placeholder=\"Search\" aria-label=\"Search\">\r\n            <div class=\"input-group-prepend\">\r\n              <div class=\"input-group-text\">\r\n                <span class=\"fa fa-search\"></span>\r\n              </div>\r\n            </div>\r\n          </div>\r\n        </form>\r\n        <!-- Navigation -->\r\n        <ul class=\"navbar-nav\">\r\n            <li *ngFor=\"let menuItem of menuItems\" class=\"{{menuItem.class}} nav-item\">\r\n                <a routerLinkActive=\"active\" [routerLink]=\"[menuItem.path]\" class=\"nav-link\">\r\n                    <i class=\"ni {{menuItem.icon}}\"></i>\r\n                    {{menuItem.title}}\r\n                </a>\r\n               \r\n            </li>\r\n            \r\n        </ul>\r\n        <!-- Divider -->\r\n        <hr class=\"my-3\">\r\n        <!-- Heading -->\r\n       \r\n      </div>\r\n    </div>\r\n  </nav>\r\n\r\n\r\n  \r\n    \r\n\r\n  <div class=\"dark\">\r\n    <div class=\"row\">\r\n        <div *ngFor=\"let solicitud of solicitudes\" class=\"col-sm-3\">\r\n            <div class=\"card space\" style=\"width: 20rem;\">\r\n                <div class=\"card-header\">\r\n                    Producto\r\n                </div>\r\n                <div class=\"card-body\">\r\n                    \r\n                    <div class=\"input-group input-group-alternative\">\r\n                        <div class=\"input-group-prepend\">\r\n                          <span class=\"input-group-text\">Fecha:</span>\r\n                        </div>\r\n                        <input class=\"form-control\" [(ngModel)] =\"solicitud.FechaFacturacion\" type=\"text\">\r\n                      </div>\r\n                   \r\n                      <div class=\"input-group input-group-alternative\">\r\n                        <div class=\"input-group-prepend\">\r\n                          <span class=\"input-group-text\">FechaEntrega:</span>\r\n                        </div>\r\n                        <input class=\"form-control\"  [(ngModel)] =\"solicitud.FechaEntrega\" type=\"text\">\r\n                      </div>\r\n\r\n                      <div class=\"input-group input-group-alternative\">\r\n                        <div class=\"input-group-prepend\">\r\n                          <span class=\"input-group-text\">Total:</span>\r\n                        </div>\r\n                        <input class=\"form-control\"  [(ngModel)] =\"solicitud.Total\" type=\"text\">\r\n                      </div>\r\n\r\n                                       \r\n                   \r\n                   \r\n                      <button  class=\"btn btn-warning space\"  (click)=\"Actualizar(solicitud.idVenta)\">Aceptar</button>\r\n                      \r\n                </div>\r\n            </div> \r\n        </div>\r\n    </div>");

/***/ }),

/***/ "s89l":
/*!****************************************************************!*\
  !*** ./src/app/pages/repartidor/profile/profile.component.css ***!
  \****************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = ("\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IiIsImZpbGUiOiJzcmMvYXBwL3BhZ2VzL3JlcGFydGlkb3IvcHJvZmlsZS9wcm9maWxlLmNvbXBvbmVudC5jc3MifQ== */");

/***/ }),

/***/ "tyEZ":
/*!*************************************************************!*\
  !*** ./src/app/pages/repartidor/ventas/ventas.component.ts ***!
  \*************************************************************/
/*! exports provided: ROUTES, VentasComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "ROUTES", function() { return ROUTES; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "VentasComponent", function() { return VentasComponent; });
/* harmony import */ var _raw_loader_ventas_component_html__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! raw-loader!./ventas.component.html */ "psQr");
/* harmony import */ var _ventas_component_css__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./ventas.component.css */ "cxGI");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/core */ "fXoL");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/router */ "tyNb");
/* harmony import */ var _peticiones_service__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ../../../peticiones.service */ "kW39");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};





var ROUTES = [
    { path: '/repartidor-ventas', title: 'Ventas', icon: 'ni-tv-2 text-primary', class: '' },
    { path: '/repartidor-transferencias', title: 'Transferencias', icon: 'ni-tv-2 text-primary', class: '' },
];
var VentasComponent = /** @class */ (function () {
    function VentasComponent(router, app) {
        var _this = this;
        this.router = router;
        this.app = app;
        this.isCollapsed = true;
        this.solicitudes = [];
        var InfoUsuario = JSON.parse(sessionStorage.getItem('log'));
        this.app.getVentas(InfoUsuario.id).subscribe(function (resp) {
            _this.solicitudes = resp;
        });
    }
    VentasComponent.prototype.ngOnInit = function () {
        var _this = this;
        this.menuItems = ROUTES.filter(function (menuItem) { return menuItem; });
        this.router.events.subscribe(function (event) {
            _this.isCollapsed = true;
        });
    };
    VentasComponent.prototype.Actualizar = function (id) {
    };
    VentasComponent.ctorParameters = function () { return [
        { type: _angular_router__WEBPACK_IMPORTED_MODULE_3__["Router"] },
        { type: _peticiones_service__WEBPACK_IMPORTED_MODULE_4__["PeticionesService"] }
    ]; };
    VentasComponent = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_2__["Component"])({
            selector: 'app-ventas',
            template: _raw_loader_ventas_component_html__WEBPACK_IMPORTED_MODULE_0__["default"],
            styles: [_ventas_component_css__WEBPACK_IMPORTED_MODULE_1__["default"]]
        }),
        __metadata("design:paramtypes", [_angular_router__WEBPACK_IMPORTED_MODULE_3__["Router"], _peticiones_service__WEBPACK_IMPORTED_MODULE_4__["PeticionesService"]])
    ], VentasComponent);
    return VentasComponent;
}());



/***/ })

}]);
//# sourceMappingURL=layouts-repartidor-repartidor-module.js.map