(window["webpackJsonp"] = window["webpackJsonp"] || []).push([["default~layouts-admin-admin-module~layouts-admin-layout-admin-layout-module~layouts-bodeguero-bodegu~a1f1748c"],{

/***/ "Dvla":
/*!***************************************************************************!*\
  !*** ./node_modules/ngx-clipboard/__ivy_ngcc__/fesm2015/ngx-clipboard.js ***!
  \***************************************************************************/
/*! exports provided: ClipboardDirective, ClipboardIfSupportedDirective, ClipboardModule, ClipboardService */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "ClipboardDirective", function() { return ClipboardDirective; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "ClipboardIfSupportedDirective", function() { return ClipboardIfSupportedDirective; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "ClipboardModule", function() { return ClipboardModule; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "ClipboardService", function() { return ClipboardService; });
/* harmony import */ var C_Users_Luis_Pineda_Desktop_PRACTICAS_practicas_intermedias_App_Practicas_node_modules_babel_runtime_helpers_esm_classCallCheck__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./node_modules/@babel/runtime/helpers/esm/classCallCheck */ "1OyB");
/* harmony import */ var C_Users_Luis_Pineda_Desktop_PRACTICAS_practicas_intermedias_App_Practicas_node_modules_babel_runtime_helpers_esm_createClass__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./node_modules/@babel/runtime/helpers/esm/createClass */ "vuIU");
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! tslib */ "mrSG");
/* harmony import */ var _angular_common__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/common */ "ofXK");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @angular/core */ "fXoL");
/* harmony import */ var ngx_window_token__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ngx-window-token */ "Qoup");
/* harmony import */ var rxjs__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! rxjs */ "qCKp");







/**
 * The following code is heavily copied from https://github.com/zenorocha/clipboard.js
 */



var ClipboardService = /*#__PURE__*/function () {
  function ClipboardService(document, window) {
    Object(C_Users_Luis_Pineda_Desktop_PRACTICAS_practicas_intermedias_App_Practicas_node_modules_babel_runtime_helpers_esm_classCallCheck__WEBPACK_IMPORTED_MODULE_0__["default"])(this, ClipboardService);

    this.document = document;
    this.window = window;
    this.copySubject = new rxjs__WEBPACK_IMPORTED_MODULE_6__["Subject"]();
    this.copyResponse$ = this.copySubject.asObservable();
    this.config = {};
  }

  Object(C_Users_Luis_Pineda_Desktop_PRACTICAS_practicas_intermedias_App_Practicas_node_modules_babel_runtime_helpers_esm_createClass__WEBPACK_IMPORTED_MODULE_1__["default"])(ClipboardService, [{
    key: "configure",
    value: function configure(config) {
      this.config = config;
    }
  }, {
    key: "copy",
    value: function copy(content) {
      if (!this.isSupported || !content) {
        return this.pushCopyResponse({
          isSuccess: false,
          content: content
        });
      }

      var copyResult = this.copyFromContent(content);

      if (copyResult) {
        return this.pushCopyResponse({
          content: content,
          isSuccess: copyResult
        });
      }

      return this.pushCopyResponse({
        isSuccess: false,
        content: content
      });
    }
  }, {
    key: "isSupported",
    get: function get() {
      return !!this.document.queryCommandSupported && !!this.document.queryCommandSupported('copy') && !!this.window;
    }
  }, {
    key: "isTargetValid",
    value: function isTargetValid(element) {
      if (element instanceof HTMLInputElement || element instanceof HTMLTextAreaElement) {
        if (element.hasAttribute('disabled')) {
          throw new Error('Invalid "target" attribute. Please use "readonly" instead of "disabled" attribute');
        }

        return true;
      }

      throw new Error('Target should be input or textarea');
    }
    /**
     * Attempts to copy from an input `targetElm`
     */

  }, {
    key: "copyFromInputElement",
    value: function copyFromInputElement(targetElm) {
      var isFocus = arguments.length > 1 && arguments[1] !== undefined ? arguments[1] : true;

      try {
        this.selectTarget(targetElm);
        var re = this.copyText();
        this.clearSelection(isFocus ? targetElm : undefined, this.window);
        return re && this.isCopySuccessInIE11();
      } catch (error) {
        return false;
      }
    }
    /**
     * This is a hack for IE11 to return `true` even if copy fails.
     */

  }, {
    key: "isCopySuccessInIE11",
    value: function isCopySuccessInIE11() {
      var clipboardData = this.window['clipboardData'];

      if (clipboardData && clipboardData.getData) {
        if (!clipboardData.getData('Text')) {
          return false;
        }
      }

      return true;
    }
    /**
     * Creates a fake textarea element, sets its value from `text` property,
     * and makes a selection on it.
     */

  }, {
    key: "copyFromContent",
    value: function copyFromContent(content) {
      var container = arguments.length > 1 && arguments[1] !== undefined ? arguments[1] : this.document.body;

      // check if the temp textarea still belongs to the current container.
      // In case we have multiple places using ngx-clipboard, one is in a modal using container but the other one is not.
      if (this.tempTextArea && !container.contains(this.tempTextArea)) {
        this.destroy(this.tempTextArea.parentElement || undefined);
      }

      if (!this.tempTextArea) {
        this.tempTextArea = this.createTempTextArea(this.document, this.window);

        try {
          container.appendChild(this.tempTextArea);
        } catch (error) {
          throw new Error('Container should be a Dom element');
        }
      }

      this.tempTextArea.value = content;
      var toReturn = this.copyFromInputElement(this.tempTextArea, false);

      if (this.config.cleanUpAfterCopy) {
        this.destroy(this.tempTextArea.parentElement || undefined);
      }

      return toReturn;
    }
    /**
     * Remove temporary textarea if any exists.
     */

  }, {
    key: "destroy",
    value: function destroy() {
      var container = arguments.length > 0 && arguments[0] !== undefined ? arguments[0] : this.document.body;

      if (this.tempTextArea) {
        container.removeChild(this.tempTextArea); // removeChild doesn't remove the reference from memory

        this.tempTextArea = undefined;
      }
    }
    /**
     * Select the target html input element.
     */

  }, {
    key: "selectTarget",
    value: function selectTarget(inputElement) {
      inputElement.select();
      inputElement.setSelectionRange(0, inputElement.value.length);
      return inputElement.value.length;
    }
  }, {
    key: "copyText",
    value: function copyText() {
      return this.document.execCommand('copy');
    }
    /**
     * Moves focus away from `target` and back to the trigger, removes current selection.
     */

  }, {
    key: "clearSelection",
    value: function clearSelection(inputElement, window) {
      var _a;

      inputElement && inputElement.focus();
      (_a = window.getSelection()) === null || _a === void 0 ? void 0 : _a.removeAllRanges();
    }
    /**
     * Creates a fake textarea for copy command.
     */

  }, {
    key: "createTempTextArea",
    value: function createTempTextArea(doc, window) {
      var isRTL = doc.documentElement.getAttribute('dir') === 'rtl';
      var ta;
      ta = doc.createElement('textarea'); // Prevent zooming on iOS

      ta.style.fontSize = '12pt'; // Reset box model

      ta.style.border = '0';
      ta.style.padding = '0';
      ta.style.margin = '0'; // Move element out of screen horizontally

      ta.style.position = 'absolute';
      ta.style[isRTL ? 'right' : 'left'] = '-9999px'; // Move element to the same position vertically

      var yPosition = window.pageYOffset || doc.documentElement.scrollTop;
      ta.style.top = yPosition + 'px';
      ta.setAttribute('readonly', '');
      return ta;
    }
    /**
     * Pushes copy operation response to copySubject, to provide global access
     * to the response.
     */

  }, {
    key: "pushCopyResponse",
    value: function pushCopyResponse(response) {
      this.copySubject.next(response);
    }
    /**
     * @deprecated use pushCopyResponse instead.
     */

  }, {
    key: "pushCopyReponse",
    value: function pushCopyReponse(response) {
      this.pushCopyResponse(response);
    }
  }]);

  return ClipboardService;
}();

ClipboardService.ɵfac = function ClipboardService_Factory(t) {
  return new (t || ClipboardService)(_angular_core__WEBPACK_IMPORTED_MODULE_4__["ɵɵinject"](_angular_common__WEBPACK_IMPORTED_MODULE_3__["DOCUMENT"]), _angular_core__WEBPACK_IMPORTED_MODULE_4__["ɵɵinject"](ngx_window_token__WEBPACK_IMPORTED_MODULE_5__["WINDOW"], 8));
};

ClipboardService.ctorParameters = function () {
  return [{
    type: undefined,
    decorators: [{
      type: _angular_core__WEBPACK_IMPORTED_MODULE_4__["Inject"],
      args: [_angular_common__WEBPACK_IMPORTED_MODULE_3__["DOCUMENT"]]
    }]
  }, {
    type: undefined,
    decorators: [{
      type: _angular_core__WEBPACK_IMPORTED_MODULE_4__["Optional"]
    }, {
      type: _angular_core__WEBPACK_IMPORTED_MODULE_4__["Inject"],
      args: [ngx_window_token__WEBPACK_IMPORTED_MODULE_5__["WINDOW"]]
    }]
  }];
};

ClipboardService.ɵprov = Object(_angular_core__WEBPACK_IMPORTED_MODULE_4__["ɵɵdefineInjectable"])({
  factory: function ClipboardService_Factory() {
    return new ClipboardService(Object(_angular_core__WEBPACK_IMPORTED_MODULE_4__["ɵɵinject"])(_angular_common__WEBPACK_IMPORTED_MODULE_3__["DOCUMENT"]), Object(_angular_core__WEBPACK_IMPORTED_MODULE_4__["ɵɵinject"])(ngx_window_token__WEBPACK_IMPORTED_MODULE_5__["WINDOW"], 8));
  },
  token: ClipboardService,
  providedIn: "root"
});
ClipboardService = Object(tslib__WEBPACK_IMPORTED_MODULE_2__["__decorate"])([Object(tslib__WEBPACK_IMPORTED_MODULE_2__["__param"])(0, Object(_angular_core__WEBPACK_IMPORTED_MODULE_4__["Inject"])(_angular_common__WEBPACK_IMPORTED_MODULE_3__["DOCUMENT"])), Object(tslib__WEBPACK_IMPORTED_MODULE_2__["__param"])(1, Object(_angular_core__WEBPACK_IMPORTED_MODULE_4__["Optional"])()), Object(tslib__WEBPACK_IMPORTED_MODULE_2__["__param"])(1, Object(_angular_core__WEBPACK_IMPORTED_MODULE_4__["Inject"])(ngx_window_token__WEBPACK_IMPORTED_MODULE_5__["WINDOW"]))], ClipboardService);

var ClipboardDirective = /*#__PURE__*/function () {
  function ClipboardDirective(clipboardSrv) {
    Object(C_Users_Luis_Pineda_Desktop_PRACTICAS_practicas_intermedias_App_Practicas_node_modules_babel_runtime_helpers_esm_classCallCheck__WEBPACK_IMPORTED_MODULE_0__["default"])(this, ClipboardDirective);

    this.clipboardSrv = clipboardSrv;
    this.cbOnSuccess = new _angular_core__WEBPACK_IMPORTED_MODULE_4__["EventEmitter"]();
    this.cbOnError = new _angular_core__WEBPACK_IMPORTED_MODULE_4__["EventEmitter"]();
  } // tslint:disable-next-line:no-empty


  Object(C_Users_Luis_Pineda_Desktop_PRACTICAS_practicas_intermedias_App_Practicas_node_modules_babel_runtime_helpers_esm_createClass__WEBPACK_IMPORTED_MODULE_1__["default"])(ClipboardDirective, [{
    key: "ngOnInit",
    value: function ngOnInit() {}
  }, {
    key: "ngOnDestroy",
    value: function ngOnDestroy() {
      this.clipboardSrv.destroy(this.container);
    }
  }, {
    key: "onClick",
    value: function onClick(event) {
      if (!this.clipboardSrv.isSupported) {
        this.handleResult(false, undefined, event);
      } else if (this.targetElm && this.clipboardSrv.isTargetValid(this.targetElm)) {
        this.handleResult(this.clipboardSrv.copyFromInputElement(this.targetElm), this.targetElm.value, event);
      } else if (this.cbContent) {
        this.handleResult(this.clipboardSrv.copyFromContent(this.cbContent, this.container), this.cbContent, event);
      }
    }
    /**
     * Fires an event based on the copy operation result.
     * @param succeeded
     */

  }, {
    key: "handleResult",
    value: function handleResult(succeeded, copiedContent, event) {
      var response = {
        isSuccess: succeeded,
        event: event
      };

      if (succeeded) {
        response = Object.assign(response, {
          content: copiedContent,
          successMessage: this.cbSuccessMsg
        });
        this.cbOnSuccess.emit(response);
      } else {
        this.cbOnError.emit(response);
      }

      this.clipboardSrv.pushCopyResponse(response);
    }
  }]);

  return ClipboardDirective;
}();

ClipboardDirective.ɵfac = function ClipboardDirective_Factory(t) {
  return new (t || ClipboardDirective)(_angular_core__WEBPACK_IMPORTED_MODULE_4__["ɵɵdirectiveInject"](ClipboardService));
};

ClipboardDirective.ɵdir = _angular_core__WEBPACK_IMPORTED_MODULE_4__["ɵɵdefineDirective"]({
  type: ClipboardDirective,
  selectors: [["", "ngxClipboard", ""]],
  hostBindings: function ClipboardDirective_HostBindings(rf, ctx) {
    if (rf & 1) {
      _angular_core__WEBPACK_IMPORTED_MODULE_4__["ɵɵlistener"]("click", function ClipboardDirective_click_HostBindingHandler($event) {
        return ctx.onClick($event.target);
      });
    }
  },
  inputs: {
    targetElm: ["ngxClipboard", "targetElm"],
    container: "container",
    cbContent: "cbContent",
    cbSuccessMsg: "cbSuccessMsg"
  },
  outputs: {
    cbOnSuccess: "cbOnSuccess",
    cbOnError: "cbOnError"
  }
});

ClipboardDirective.ctorParameters = function () {
  return [{
    type: ClipboardService
  }];
};

Object(tslib__WEBPACK_IMPORTED_MODULE_2__["__decorate"])([Object(_angular_core__WEBPACK_IMPORTED_MODULE_4__["Input"])('ngxClipboard')], ClipboardDirective.prototype, "targetElm", void 0);

Object(tslib__WEBPACK_IMPORTED_MODULE_2__["__decorate"])([Object(_angular_core__WEBPACK_IMPORTED_MODULE_4__["Input"])()], ClipboardDirective.prototype, "container", void 0);

Object(tslib__WEBPACK_IMPORTED_MODULE_2__["__decorate"])([Object(_angular_core__WEBPACK_IMPORTED_MODULE_4__["Input"])()], ClipboardDirective.prototype, "cbContent", void 0);

Object(tslib__WEBPACK_IMPORTED_MODULE_2__["__decorate"])([Object(_angular_core__WEBPACK_IMPORTED_MODULE_4__["Input"])()], ClipboardDirective.prototype, "cbSuccessMsg", void 0);

Object(tslib__WEBPACK_IMPORTED_MODULE_2__["__decorate"])([Object(_angular_core__WEBPACK_IMPORTED_MODULE_4__["Output"])()], ClipboardDirective.prototype, "cbOnSuccess", void 0);

Object(tslib__WEBPACK_IMPORTED_MODULE_2__["__decorate"])([Object(_angular_core__WEBPACK_IMPORTED_MODULE_4__["Output"])()], ClipboardDirective.prototype, "cbOnError", void 0);

Object(tslib__WEBPACK_IMPORTED_MODULE_2__["__decorate"])([Object(_angular_core__WEBPACK_IMPORTED_MODULE_4__["HostListener"])('click', ['$event.target'])], ClipboardDirective.prototype, "onClick", null);

var ClipboardIfSupportedDirective = /*#__PURE__*/function () {
  function ClipboardIfSupportedDirective(_clipboardService, _viewContainerRef, _templateRef) {
    Object(C_Users_Luis_Pineda_Desktop_PRACTICAS_practicas_intermedias_App_Practicas_node_modules_babel_runtime_helpers_esm_classCallCheck__WEBPACK_IMPORTED_MODULE_0__["default"])(this, ClipboardIfSupportedDirective);

    this._clipboardService = _clipboardService;
    this._viewContainerRef = _viewContainerRef;
    this._templateRef = _templateRef;
  }

  Object(C_Users_Luis_Pineda_Desktop_PRACTICAS_practicas_intermedias_App_Practicas_node_modules_babel_runtime_helpers_esm_createClass__WEBPACK_IMPORTED_MODULE_1__["default"])(ClipboardIfSupportedDirective, [{
    key: "ngOnInit",
    value: function ngOnInit() {
      if (this._clipboardService.isSupported) {
        this._viewContainerRef.createEmbeddedView(this._templateRef);
      }
    }
  }]);

  return ClipboardIfSupportedDirective;
}();

ClipboardIfSupportedDirective.ɵfac = function ClipboardIfSupportedDirective_Factory(t) {
  return new (t || ClipboardIfSupportedDirective)(_angular_core__WEBPACK_IMPORTED_MODULE_4__["ɵɵdirectiveInject"](ClipboardService), _angular_core__WEBPACK_IMPORTED_MODULE_4__["ɵɵdirectiveInject"](_angular_core__WEBPACK_IMPORTED_MODULE_4__["ViewContainerRef"]), _angular_core__WEBPACK_IMPORTED_MODULE_4__["ɵɵdirectiveInject"](_angular_core__WEBPACK_IMPORTED_MODULE_4__["TemplateRef"]));
};

ClipboardIfSupportedDirective.ɵdir = _angular_core__WEBPACK_IMPORTED_MODULE_4__["ɵɵdefineDirective"]({
  type: ClipboardIfSupportedDirective,
  selectors: [["", "ngxClipboardIfSupported", ""]]
});

ClipboardIfSupportedDirective.ctorParameters = function () {
  return [{
    type: ClipboardService
  }, {
    type: _angular_core__WEBPACK_IMPORTED_MODULE_4__["ViewContainerRef"]
  }, {
    type: _angular_core__WEBPACK_IMPORTED_MODULE_4__["TemplateRef"]
  }];
};

var ClipboardModule = function ClipboardModule() {
  Object(C_Users_Luis_Pineda_Desktop_PRACTICAS_practicas_intermedias_App_Practicas_node_modules_babel_runtime_helpers_esm_classCallCheck__WEBPACK_IMPORTED_MODULE_0__["default"])(this, ClipboardModule);
};

ClipboardModule.ɵmod = _angular_core__WEBPACK_IMPORTED_MODULE_4__["ɵɵdefineNgModule"]({
  type: ClipboardModule
});
ClipboardModule.ɵinj = _angular_core__WEBPACK_IMPORTED_MODULE_4__["ɵɵdefineInjector"]({
  factory: function ClipboardModule_Factory(t) {
    return new (t || ClipboardModule)();
  },
  imports: [[_angular_common__WEBPACK_IMPORTED_MODULE_3__["CommonModule"]]]
});
/*@__PURE__*/

(function () {
  _angular_core__WEBPACK_IMPORTED_MODULE_4__["ɵsetClassMetadata"](ClipboardService, [{
    type: _angular_core__WEBPACK_IMPORTED_MODULE_4__["Injectable"],
    args: [{
      providedIn: 'root'
    }]
  }], function () {
    return [{
      type: undefined,
      decorators: [{
        type: _angular_core__WEBPACK_IMPORTED_MODULE_4__["Inject"],
        args: [_angular_common__WEBPACK_IMPORTED_MODULE_3__["DOCUMENT"]]
      }]
    }, {
      type: undefined,
      decorators: [{
        type: _angular_core__WEBPACK_IMPORTED_MODULE_4__["Optional"]
      }, {
        type: _angular_core__WEBPACK_IMPORTED_MODULE_4__["Inject"],
        args: [ngx_window_token__WEBPACK_IMPORTED_MODULE_5__["WINDOW"]]
      }]
    }];
  }, null);
})();
/*@__PURE__*/


(function () {
  _angular_core__WEBPACK_IMPORTED_MODULE_4__["ɵsetClassMetadata"](ClipboardDirective, [{
    type: _angular_core__WEBPACK_IMPORTED_MODULE_4__["Directive"],
    args: [{
      selector: '[ngxClipboard]'
    }]
  }], function () {
    return [{
      type: ClipboardService
    }];
  }, {
    cbOnSuccess: [{
      type: _angular_core__WEBPACK_IMPORTED_MODULE_4__["Output"]
    }],
    cbOnError: [{
      type: _angular_core__WEBPACK_IMPORTED_MODULE_4__["Output"]
    }],
    onClick: [{
      type: _angular_core__WEBPACK_IMPORTED_MODULE_4__["HostListener"],
      args: ['click', ['$event.target']]
    }],
    targetElm: [{
      type: _angular_core__WEBPACK_IMPORTED_MODULE_4__["Input"],
      args: ['ngxClipboard']
    }],
    container: [{
      type: _angular_core__WEBPACK_IMPORTED_MODULE_4__["Input"]
    }],
    cbContent: [{
      type: _angular_core__WEBPACK_IMPORTED_MODULE_4__["Input"]
    }],
    cbSuccessMsg: [{
      type: _angular_core__WEBPACK_IMPORTED_MODULE_4__["Input"]
    }]
  });
})();
/*@__PURE__*/


(function () {
  _angular_core__WEBPACK_IMPORTED_MODULE_4__["ɵsetClassMetadata"](ClipboardIfSupportedDirective, [{
    type: _angular_core__WEBPACK_IMPORTED_MODULE_4__["Directive"],
    args: [{
      selector: '[ngxClipboardIfSupported]'
    }]
  }], function () {
    return [{
      type: ClipboardService
    }, {
      type: _angular_core__WEBPACK_IMPORTED_MODULE_4__["ViewContainerRef"]
    }, {
      type: _angular_core__WEBPACK_IMPORTED_MODULE_4__["TemplateRef"]
    }];
  }, null);
})();

(function () {
  (typeof ngJitMode === "undefined" || ngJitMode) && _angular_core__WEBPACK_IMPORTED_MODULE_4__["ɵɵsetNgModuleScope"](ClipboardModule, {
    declarations: function declarations() {
      return [ClipboardDirective, ClipboardIfSupportedDirective];
    },
    imports: function imports() {
      return [_angular_common__WEBPACK_IMPORTED_MODULE_3__["CommonModule"]];
    },
    exports: function exports() {
      return [ClipboardDirective, ClipboardIfSupportedDirective];
    }
  });
})();
/*@__PURE__*/


(function () {
  _angular_core__WEBPACK_IMPORTED_MODULE_4__["ɵsetClassMetadata"](ClipboardModule, [{
    type: _angular_core__WEBPACK_IMPORTED_MODULE_4__["NgModule"],
    args: [{
      imports: [_angular_common__WEBPACK_IMPORTED_MODULE_3__["CommonModule"]],
      declarations: [ClipboardDirective, ClipboardIfSupportedDirective],
      exports: [ClipboardDirective, ClipboardIfSupportedDirective]
    }]
  }], null, null);
})();
/*
 * Public API Surface of ngx-clipboard
 */

/**
 * Generated bundle index. Do not edit.
 */




/***/ }),

/***/ "Qoup":
/*!*********************************************************************************!*\
  !*** ./node_modules/ngx-window-token/__ivy_ngcc__/fesm2015/ngx-window-token.js ***!
  \*********************************************************************************/
/*! exports provided: WINDOW */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "WINDOW", function() { return WINDOW; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "fXoL");

var WINDOW = new _angular_core__WEBPACK_IMPORTED_MODULE_0__["InjectionToken"]('WindowToken', typeof window !== 'undefined' && window.document ? {
  providedIn: 'root',
  factory: function factory() {
    return window;
  }
} : {
  providedIn: 'root',
  factory: function factory() {
    return undefined;
  }
});
/*
 * Public API Surface of ngx-window-token
 */

/**
 * Generated bundle index. Do not edit.
 */



/***/ })

}]);
//# sourceMappingURL=default~layouts-admin-admin-module~layouts-admin-layout-admin-layout-module~layouts-bodeguero-bodegu~a1f1748c.js.map