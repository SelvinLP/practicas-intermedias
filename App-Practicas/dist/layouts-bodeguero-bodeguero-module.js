(window["webpackJsonp"] = window["webpackJsonp"] || []).push([["layouts-bodeguero-bodeguero-module"],{

/***/ "/3df":
/*!**************************************************************************!*\
  !*** ./src/app/pages/bodeguero/transferencia/transferencia.component.ts ***!
  \**************************************************************************/
/*! exports provided: TransferenciaComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "TransferenciaComponent", function() { return TransferenciaComponent; });
/* harmony import */ var _raw_loader_transferencia_component_html__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! raw-loader!./transferencia.component.html */ "Ru8h");
/* harmony import */ var _transferencia_component_css__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./transferencia.component.css */ "bBu9");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/core */ "fXoL");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/router */ "tyNb");
/* harmony import */ var _peticiones_service__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ../../../peticiones.service */ "kW39");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};





var TransferenciaComponent = /** @class */ (function () {
    function TransferenciaComponent(router, app) {
        var _this = this;
        this.router = router;
        this.app = app;
        this.tipos = ['Interna', 'Externa'];
        this.estado = "Pendiente";
        var InfoUsuario = JSON.parse(sessionStorage.getItem('log'));
        this.app.getBodegasU(InfoUsuario.id).subscribe(function (resp1) {
            _this.bodega = resp1[0].id;
        });
    }
    TransferenciaComponent.prototype.ngOnInit = function () {
    };
    TransferenciaComponent.prototype.save = function () {
        var _this = this;
        this.app.postCrearSolicitud({ "descripcion": this.descripcion, "estado": this.estado, "creador": this.creador, "bodega": this.bodega, "bodega_dest": this.bodega_dest, "tipo": this.tipoA }).subscribe(function (resp) {
            alert(resp.descripcion);
            var InfoUsuario = JSON.parse(sessionStorage.getItem('log'));
            console.log(InfoUsuario.correo);
            _this.app.sendEmail({ "descripcion": "Transferencia Solicitada", "email": InfoUsuario.correo }).subscribe(function (resp1) {
            });
            _this.descripcion = "";
            _this.creador = "";
            _this.bodega_dest = null;
        });
    };
    TransferenciaComponent.ctorParameters = function () { return [
        { type: _angular_router__WEBPACK_IMPORTED_MODULE_3__["Router"] },
        { type: _peticiones_service__WEBPACK_IMPORTED_MODULE_4__["PeticionesService"] }
    ]; };
    TransferenciaComponent = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_2__["Component"])({
            selector: 'app-transferencia',
            template: _raw_loader_transferencia_component_html__WEBPACK_IMPORTED_MODULE_0__["default"],
            styles: [_transferencia_component_css__WEBPACK_IMPORTED_MODULE_1__["default"]]
        }),
        __metadata("design:paramtypes", [_angular_router__WEBPACK_IMPORTED_MODULE_3__["Router"], _peticiones_service__WEBPACK_IMPORTED_MODULE_4__["PeticionesService"]])
    ], TransferenciaComponent);
    return TransferenciaComponent;
}());



/***/ }),

/***/ "/AE7":
/*!**************************************************************!*\
  !*** ./src/app/pages/bodeguero/profile/profile.component.ts ***!
  \**************************************************************/
/*! exports provided: ProfileComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "ProfileComponent", function() { return ProfileComponent; });
/* harmony import */ var _raw_loader_profile_component_html__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! raw-loader!./profile.component.html */ "7fse");
/* harmony import */ var _profile_component_css__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./profile.component.css */ "iJ4Y");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/core */ "fXoL");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/router */ "tyNb");
/* harmony import */ var _peticiones_service__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ../../../peticiones.service */ "kW39");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};





var ProfileComponent = /** @class */ (function () {
    function ProfileComponent(router, app) {
        this.router = router;
        this.app = app;
        this.InfoUsuario = null;
        this.roles = null;
        if (sessionStorage.length == 0) {
            this.router.navigate(['/login']);
        }
    }
    ProfileComponent.prototype.ngOnInit = function () {
        this.InfoUsuario = JSON.parse(sessionStorage.getItem('log'));
        this.name = this.InfoUsuario.nombre;
        this.email = this.InfoUsuario.correo;
        this.password = this.InfoUsuario.password;
        this.dpi = this.InfoUsuario.dpi;
        this.fechaN = this.InfoUsuario.fechanacimiento;
        this.roles = this.InfoUsuario.rol;
    };
    ProfileComponent.prototype.edit = function () {
        var _this = this;
        this.app.postModificar({ "dpi": this.dpi, "nombre": this.name, "fechanacimiento": this.fechaN, "correo": this.email, "pass": this.password }).subscribe(function (resp) {
            if (resp.status == 300) {
                _this.app.postLogin({ "email": _this.email, "password": _this.password }).subscribe(function (resp1) {
                    if (resp1.status == 300) {
                        var nuevo = void 0;
                        nuevo = { "status": resp1.status, "nombre": resp1.nombre, "dpi": resp1.dpi, "correo": resp1.correo, "fechanacimiento": resp1.fechanacimiento, "rol": resp1.rol, "password": _this.password };
                        sessionStorage.setItem('log', JSON.stringify(nuevo));
                        alert("Moodify User!");
                        _this.router.navigate(['/login']);
                    }
                });
            }
            else if (resp.status == 100) {
                alert("Error");
            }
        });
    };
    ProfileComponent.ctorParameters = function () { return [
        { type: _angular_router__WEBPACK_IMPORTED_MODULE_3__["Router"] },
        { type: _peticiones_service__WEBPACK_IMPORTED_MODULE_4__["PeticionesService"] }
    ]; };
    ProfileComponent = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_2__["Component"])({
            selector: 'app-profile',
            template: _raw_loader_profile_component_html__WEBPACK_IMPORTED_MODULE_0__["default"],
            styles: [_profile_component_css__WEBPACK_IMPORTED_MODULE_1__["default"]]
        }),
        __metadata("design:paramtypes", [_angular_router__WEBPACK_IMPORTED_MODULE_3__["Router"], _peticiones_service__WEBPACK_IMPORTED_MODULE_4__["PeticionesService"]])
    ], ProfileComponent);
    return ProfileComponent;
}());



/***/ }),

/***/ "7fse":
/*!******************************************************************************************************!*\
  !*** ./node_modules/raw-loader/dist/cjs.js!./src/app/pages/bodeguero/profile/profile.component.html ***!
  \******************************************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = ("<div class=\"header pb-8 pt-5 pt-lg-8 d-flex align-items-center\" style=\"min-height: 200px; background-image: url(https://geoinnova.org/cursos/wp-content/uploads/2016/10/1-11-1024x551.jpg); background-size: cover; background-position: center top;\">\r\n    <!-- Mask -->\r\n    <span class=\"mask bg-gradient-danger opacity-8\"></span>\r\n   \r\n  </div>\r\n  <div class=\"container-fluid mt--7\">\r\n    <div class=\"row\">\r\n      <div class=\"col-xl-4 order-xl-2 mb-5 mb-xl-0\">\r\n        <div class=\"card card-profile shadow\">\r\n          <div class=\"row justify-content-center\">\r\n            <div class=\"col-lg-3 order-lg-2\">\r\n              <div class=\"card-profile-image\">\r\n                <a href=\"javascript:void(0)\">\r\n                  <img src=\"https://www.softzone.es/app/uploads-softzone.es/2018/04/guest.png\" class=\"rounded-circle\">\r\n                </a>\r\n              </div>\r\n            </div>\r\n          </div>\r\n          <div class=\"card-header text-center border-0 pt-8 pt-md-4 pb-0 pb-md-4\">\r\n            <div class=\"d-flex justify-content-between\">\r\n             \r\n            </div>\r\n          </div>\r\n          <div class=\"card-body pt-0 pt-md-4\">\r\n            <div class=\"row\">\r\n              <div class=\"col\">\r\n                <div class=\"card-profile-stats d-flex justify-content-center mt-md-5\">\r\n                 \r\n                </div>\r\n              </div>\r\n            </div>\r\n            <div class=\"text-center\">\r\n              \r\n            </div>\r\n          </div>\r\n        </div>\r\n      </div>\r\n      <div class=\"col-xl-8 order-xl-1\">\r\n        <div class=\"card bg-secondary shadow\">\r\n          <div class=\"card-header bg-white border-0\">\r\n            <div class=\"row align-items-center\">\r\n              <div class=\"col-8\">\r\n                <h3 class=\"mb-0\">My account</h3>\r\n              </div>\r\n              <div class=\"col-4 text-right\">\r\n                <a href=\"#!\" (click)=\"edit()\" class=\"btn btn-sm btn-primary\">UPDATE</a>\r\n              </div>\r\n            </div>\r\n          </div>\r\n          <div class=\"card-body\">\r\n          \r\n              <h6 class=\"heading-small text-muted mb-4\">User information</h6>\r\n              <div class=\"pl-lg-4\">\r\n                <div class=\"row\">\r\n                  <div class=\"col-lg-6\">\r\n                    <div class=\"form-group\">\r\n                      <label class=\"form-control-label\" for=\"input-username\">Username</label>\r\n                      <input type=\"text\" id=\"input-username\" [(ngModel)]=\"name\" class=\"form-control form-control-alternative\"  value=\"{{name}}\">\r\n                    </div>\r\n                  </div>\r\n                  <div class=\"col-lg-6\">\r\n                    <div class=\"form-group\">\r\n                      <label class=\"form-control-label\" for=\"input-email\">Email address</label>\r\n                      <input type=\"text\" [(ngModel)]=\"email\" class=\"form-control form-control-alternative\" value=\"{{email}}\">\r\n                    </div>\r\n                  </div>\r\n                </div>\r\n                <div class=\"row\">\r\n                  <div class=\"col-lg-6\">\r\n                    <div class=\"form-group\">\r\n                      <label class=\"form-control-label\" for=\"input-firste-name\">Date of Birth</label>\r\n                      <input type=\"text\" id=\"input-first-najme\" [(ngModel)]=\"fechaN\" class=\"form-control form-control-alternative\"  value=\"{{fechaN}}\">\r\n                    </div>\r\n                  </div>\r\n                  <div class=\"col-lg-6\">\r\n                    <div class=\"form-group\">\r\n                      <label class=\"form-control-label\" for=\"input-last-name\">DPI</label>\r\n                      <input type=\"text\" id=\"input-last-namfe\" class=\"form-control form-control-alternative\"  value=\"{{dpi}}\"  disabled=\"disabled\">\r\n                    </div>\r\n                  </div>\r\n                </div>\r\n  \r\n                <div class=\"row\">\r\n                  <div class=\"col-lg-6\">\r\n                    <div class=\"form-group\">\r\n                      <label class=\"form-control-label\" for=\"input-first-name\">Password</label>\r\n                      <input type=\"text\" id=\"input-first-name\" [(ngModel)]=\"password\" class=\"form-control form-control-alternative\"  value=\"{{password}}\">\r\n                    </div>\r\n                  </div>\r\n                  <div class=\"col-lg-6\">\r\n                    <div class=\"form-group\">\r\n                      <label class=\"form-control-label\" for=\"input-last-namwe\">roles</label>\r\n                      <select class=\"form-control form-control-alternative\">\r\n                        <option *ngFor=\"let rol of roles\">\r\n                          {{rol.descripcion}}\r\n                        </option>\r\n                      </select>\r\n                    </div>\r\n                  </div>\r\n                </div>\r\n              </div>\r\n            \r\n             \r\n             \r\n           \r\n          </div>\r\n        </div>\r\n      </div>\r\n    </div>\r\n  </div>\r\n  ");

/***/ }),

/***/ "Cj97":
/*!******************************************************************************************************************!*\
  !*** ./node_modules/raw-loader/dist/cjs.js!./src/app/pages/bodeguero/actualizacion/actualizacion.component.html ***!
  \******************************************************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = ("<div class=\"header pb-5 pt-5 pt-lg-5 d-flex align-items-center\" style=\"min-height: 100px; background-image: url(https://geoinnova.org/cursos/wp-content/uploads/2016/10/1-11-1024x551.jpg); background-size: cover; background-position: center top;\">\r\n    <!-- Mask -->\r\n    <span class=\"mask bg-gradient-danger opacity-8\"></span>\r\n   \r\n  </div>\r\n\r\n  <nav class=\"navbar navbar-horizontal navbar-expand-md navbar-light bg-white\" id=\"sidenav-main\">\r\n    <div class=\"container-fluid\">\r\n      <!-- Toggler -->\r\n      <button class=\"navbar-toggler\" type=\"button\" (click)=\"isCollapsed=!isCollapsed\"\r\n         aria-controls=\"sidenav-collapse-main\">\r\n        <span class=\"navbar-toggler-icon\"></span>\r\n      </button>\r\n    \r\n     \r\n     \r\n      <!-- Collapse -->\r\n      <div class=\"collapse navbar-collapse\"  [ngbCollapse]=\"isCollapsed\" id=\"sidenav-collapse-main\">\r\n        <!-- Collapse header -->\r\n        <div class=\"navbar-collapse-header d-md-none\">\r\n          <div class=\"row\">\r\n            <div class=\"col-6 collapse-brand\">\r\n              <a  routerLinkActive=\"active\" [routerLink]=\"['/dashboard']\">\r\n                <img src=\"./assets/img/brand/blue.png\">\r\n              </a>\r\n            </div>\r\n            <div class=\"col-6 collapse-close\">\r\n              <button type=\"button\" class=\"navbar-toggler\" (click)=\"isCollapsed=!isCollapsed\">\r\n                <span></span>\r\n                <span></span>\r\n              </button>\r\n            </div>\r\n          </div>\r\n        </div>\r\n        <!-- Form -->\r\n        <form class=\"mt-4 mb-3 d-md-none\">\r\n          <div class=\"input-group input-group-rounded input-group-merge\">\r\n            <input type=\"search\" class=\"form-control form-control-rounded form-control-prepended\" placeholder=\"Search\" aria-label=\"Search\">\r\n            <div class=\"input-group-prepend\">\r\n              <div class=\"input-group-text\">\r\n                <span class=\"fa fa-search\"></span>\r\n              </div>\r\n            </div>\r\n          </div>\r\n        </form>\r\n        <!-- Navigation -->\r\n        <ul class=\"navbar-nav\">\r\n            <li *ngFor=\"let menuItem of menuItems\" class=\"{{menuItem.class}} nav-item\">\r\n                <a routerLinkActive=\"active\" [routerLink]=\"[menuItem.path]\" class=\"nav-link\">\r\n                    <i class=\"ni {{menuItem.icon}}\"></i>\r\n                    {{menuItem.title}}\r\n                </a>\r\n               \r\n            </li>\r\n            \r\n        </ul>\r\n        <!-- Divider -->\r\n        <hr class=\"my-3\">\r\n        <!-- Heading -->\r\n       \r\n      </div>\r\n    </div>\r\n  </nav>\r\n\r\n\r\n  \r\n    \r\n\r\n  <div class=\"dark\">\r\n    <div class=\"row\">\r\n        <div *ngFor=\"let solicitud of solicitudes\" class=\"col-sm-3\">\r\n            <div class=\"card space\" style=\"width: 20rem;\">\r\n                <div class=\"card-header\">\r\n                    Producto\r\n                </div>\r\n                <div class=\"card-body\">\r\n                    \r\n                    <div class=\"input-group input-group-alternative\">\r\n                        <div class=\"input-group-prepend\">\r\n                          <span class=\"input-group-text\">ID:</span>\r\n                        </div>\r\n                        <input class=\"form-control\" disabled=\"disabled\" [(ngModel)] =\"solicitud.idInventario\" type=\"text\">\r\n                      </div>\r\n                   \r\n                      <div class=\"input-group input-group-alternative\">\r\n                        <div class=\"input-group-prepend\">\r\n                          <span class=\"input-group-text\">Nombre Producto:</span>\r\n                        </div>\r\n                        <input class=\"form-control\" disabled=\"disabled\" [(ngModel)] =\"solicitud.Nombre\" type=\"text\">\r\n                      </div>\r\n\r\n                      <div class=\"input-group input-group-alternative\">\r\n                        <div class=\"input-group-prepend\">\r\n                          <span class=\"input-group-text\">Cantidad Anterior:</span>\r\n                        </div>\r\n                        <input class=\"form-control\" disabled=\"disabled\" [(ngModel)] =\"solicitud.cantidad\" type=\"text\">\r\n                      </div>\r\n\r\n                      <div class=\"input-group input-group-alternative\">\r\n                        <div class=\"input-group-prepend\">\r\n                          <span class=\"input-group-text\">Cantidad Nueva:</span>\r\n                        </div>\r\n                        <input class=\"form-control\" [(ngModel)] =\"solicitud.cant\" [(ngModel)]=\"solicitud.Estado\" type=\"number\">\r\n                      </div>\r\n\r\n                      <div class=\"input-group input-group-alternative\">\r\n                        <div class=\"input-group-prepend\">\r\n                          <span class=\"input-group-text\">Motivo:</span>\r\n                        </div>\r\n                        <input class=\"form-control\" [(ngModel)]=\"motivo\" type=\"text\">\r\n                      </div>\r\n                   \r\n                   \r\n                      <button  class=\"btn btn-warning space\"  (click)=\"Actualizar(solicitud.idInventario,solicitud.cant)\">Actualizar</button>\r\n                      \r\n                </div>\r\n            </div> \r\n        </div>\r\n    </div>");

/***/ }),

/***/ "DICF":
/*!******************************************************************************************************************************!*\
  !*** ./node_modules/raw-loader/dist/cjs.js!./src/app/pages/bodeguero/visualizar-externas/visualizar-externas.component.html ***!
  \******************************************************************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = ("<div class=\"header pb-5 pt-5 pt-lg-5 d-flex align-items-center\" style=\"min-height: 100px; background-image: url(https://geoinnova.org/cursos/wp-content/uploads/2016/10/1-11-1024x551.jpg); background-size: cover; background-position: center top;\">\r\n  <!-- Mask -->\r\n  <span class=\"mask bg-gradient-danger opacity-8\"></span>\r\n \r\n</div>\r\n\r\n<nav class=\"navbar navbar-horizontal navbar-expand-md navbar-light bg-white\" id=\"sidenav-main\">\r\n  <div class=\"container-fluid\">\r\n    <!-- Toggler -->\r\n    <button class=\"navbar-toggler\" type=\"button\" (click)=\"isCollapsed=!isCollapsed\"\r\n       aria-controls=\"sidenav-collapse-main\">\r\n      <span class=\"navbar-toggler-icon\"></span>\r\n    </button>\r\n  \r\n   \r\n   \r\n    <!-- Collapse -->\r\n    <div class=\"collapse navbar-collapse\"  [ngbCollapse]=\"isCollapsed\" id=\"sidenav-collapse-main\">\r\n      <!-- Collapse header -->\r\n      <div class=\"navbar-collapse-header d-md-none\">\r\n        <div class=\"row\">\r\n          <div class=\"col-6 collapse-brand\">\r\n            <a  routerLinkActive=\"active\" [routerLink]=\"['/dashboard']\">\r\n              <img src=\"./assets/img/brand/blue.png\">\r\n            </a>\r\n          </div>\r\n          <div class=\"col-6 collapse-close\">\r\n            <button type=\"button\" class=\"navbar-toggler\" (click)=\"isCollapsed=!isCollapsed\">\r\n              <span></span>\r\n              <span></span>\r\n            </button>\r\n          </div>\r\n        </div>\r\n      </div>\r\n      <!-- Form -->\r\n      <form class=\"mt-4 mb-3 d-md-none\">\r\n        <div class=\"input-group input-group-rounded input-group-merge\">\r\n          <input type=\"search\" class=\"form-control form-control-rounded form-control-prepended\" placeholder=\"Search\" aria-label=\"Search\">\r\n          <div class=\"input-group-prepend\">\r\n            <div class=\"input-group-text\">\r\n              <span class=\"fa fa-search\"></span>\r\n            </div>\r\n          </div>\r\n        </div>\r\n      </form>\r\n      <!-- Navigation -->\r\n      <ul class=\"navbar-nav\">\r\n          <li *ngFor=\"let menuItem of menuItems\" class=\"{{menuItem.class}} nav-item\">\r\n              <a routerLinkActive=\"active\" [routerLink]=\"[menuItem.path]\" class=\"nav-link\">\r\n                  <i class=\"ni {{menuItem.icon}}\"></i>\r\n                  {{menuItem.title}}\r\n              </a>\r\n             \r\n          </li>\r\n          \r\n      </ul>\r\n      <!-- Divider -->\r\n      <hr class=\"my-3\">\r\n      <!-- Heading -->\r\n     \r\n    </div>\r\n  </div>\r\n</nav>\r\n\r\n\r\n\r\n  \r\n\r\n<div class=\"dark\">\r\n  <div class=\"row\">\r\n      <div *ngFor=\"let solicitud of solicitudes\" class=\"col-sm-3\">\r\n          <div class=\"card space\" style=\"width: 20rem;\">\r\n              <div class=\"card-header\">\r\n                  Producto\r\n              </div>\r\n              <div class=\"card-body\">\r\n                  \r\n                  <div class=\"input-group input-group-alternative\">\r\n                      <div class=\"input-group-prepend\">\r\n                        <span class=\"input-group-text\">ID:</span>\r\n                      </div>\r\n                      <input class=\"form-control\" [(ngModel)] =\"solicitud.Id\" type=\"text\">\r\n                    </div>\r\n                 \r\n                    <div class=\"input-group input-group-alternative\">\r\n                      <div class=\"input-group-prepend\">\r\n                        <span class=\"input-group-text\">Estado:</span>\r\n                      </div>\r\n                      <input class=\"form-control\"  [(ngModel)] =\"solicitud.Estado\" type=\"text\">\r\n                    </div>\r\n\r\n                    <div class=\"input-group input-group-alternative\">\r\n                      <div class=\"input-group-prepend\">\r\n                        <span class=\"input-group-text\">Creador:</span>\r\n                      </div>\r\n                      <input class=\"form-control\"  [(ngModel)] =\"solicitud.Creador\" type=\"text\">\r\n                    </div>\r\n\r\n                    <div class=\"input-group input-group-alternative\">\r\n                      <div class=\"input-group-prepend\">\r\n                        <span class=\"input-group-text\">Bodega Origen:</span>\r\n                      </div>\r\n                      <input class=\"form-control\" [(ngModel)] =\"solicitud.Bodega_solicitante\" type=\"text\">\r\n                    </div>\r\n\r\n                    <div class=\"input-group input-group-alternative\">\r\n                      <div class=\"input-group-prepend\">\r\n                        <span class=\"input-group-text\">Descripcion:</span>\r\n                      </div>\r\n                      <input class=\"form-control\" [(ngModel)] =\"solicitud.Descripcion\" type=\"text\">\r\n                    </div>\r\n\r\n\r\n                    \r\n                 \r\n                 \r\n                    <button  class=\"btn btn-warning space\"  (click)=\"Actualizar(solicitud.Id)\">Aceptar</button>\r\n                    \r\n              </div>\r\n          </div> \r\n      </div>\r\n  </div>");

/***/ }),

/***/ "DkCZ":
/*!*******************************************************!*\
  !*** ./src/app/layouts/bodeguero/bodeguero.module.ts ***!
  \*******************************************************/
/*! exports provided: BodegueroModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "BodegueroModule", function() { return BodegueroModule; });
/* harmony import */ var _pages_bodeguero_profile_profile_component__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ../../pages/bodeguero/profile/profile.component */ "/AE7");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "fXoL");
/* harmony import */ var _angular_common_http__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/common/http */ "tk/3");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/router */ "tyNb");
/* harmony import */ var _angular_common__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @angular/common */ "ofXK");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! @angular/forms */ "3Pt+");
/* harmony import */ var _bodeguero_routing__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ./bodeguero.routing */ "pYIQ");
/* harmony import */ var ngx_clipboard__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! ngx-clipboard */ "Dvla");
/* harmony import */ var _ng_bootstrap_ng_bootstrap__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! @ng-bootstrap/ng-bootstrap */ "1kSV");
/* harmony import */ var _pages_bodeguero_actualizacion_actualizacion_component__WEBPACK_IMPORTED_MODULE_9__ = __webpack_require__(/*! ../../pages/bodeguero/actualizacion/actualizacion.component */ "mgA3");
/* harmony import */ var _pages_bodeguero_transferencia_transferencia_component__WEBPACK_IMPORTED_MODULE_10__ = __webpack_require__(/*! ../../pages/bodeguero/transferencia/transferencia.component */ "/3df");
/* harmony import */ var _pages_bodeguero_visualizar_internas_visualizar_internas_component__WEBPACK_IMPORTED_MODULE_11__ = __webpack_require__(/*! ../../pages/bodeguero/visualizar-internas/visualizar-internas.component */ "bRtU");
/* harmony import */ var _pages_bodeguero_visualizar_externas_visualizar_externas_component__WEBPACK_IMPORTED_MODULE_12__ = __webpack_require__(/*! ../../pages/bodeguero/visualizar-externas/visualizar-externas.component */ "Z5rp");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};













var BodegueroModule = /** @class */ (function () {
    function BodegueroModule() {
    }
    BodegueroModule = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
            imports: [
                _angular_common__WEBPACK_IMPORTED_MODULE_4__["CommonModule"],
                _angular_router__WEBPACK_IMPORTED_MODULE_3__["RouterModule"].forChild(_bodeguero_routing__WEBPACK_IMPORTED_MODULE_6__["BodegueroRoutes"]),
                _angular_forms__WEBPACK_IMPORTED_MODULE_5__["FormsModule"],
                _angular_common_http__WEBPACK_IMPORTED_MODULE_2__["HttpClientModule"],
                _ng_bootstrap_ng_bootstrap__WEBPACK_IMPORTED_MODULE_8__["NgbModule"],
                ngx_clipboard__WEBPACK_IMPORTED_MODULE_7__["ClipboardModule"]
            ],
            declarations: [
                _pages_bodeguero_profile_profile_component__WEBPACK_IMPORTED_MODULE_0__["ProfileComponent"],
                _pages_bodeguero_actualizacion_actualizacion_component__WEBPACK_IMPORTED_MODULE_9__["ActualizacionComponent"],
                _pages_bodeguero_transferencia_transferencia_component__WEBPACK_IMPORTED_MODULE_10__["TransferenciaComponent"],
                _pages_bodeguero_visualizar_internas_visualizar_internas_component__WEBPACK_IMPORTED_MODULE_11__["VisualizarInternasComponent"],
                _pages_bodeguero_visualizar_externas_visualizar_externas_component__WEBPACK_IMPORTED_MODULE_12__["VisualizarExternasComponent"]
            ]
        })
    ], BodegueroModule);
    return BodegueroModule;
}());



/***/ }),

/***/ "Ru8h":
/*!******************************************************************************************************************!*\
  !*** ./node_modules/raw-loader/dist/cjs.js!./src/app/pages/bodeguero/transferencia/transferencia.component.html ***!
  \******************************************************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = ("<div class=\"header pb-8 pt-5 pt-lg-8 d-flex align-items-center\"\r\n  style=\"min-height: 200px; background-image: url(https://geoinnova.org/cursos/wp-content/uploads/2016/10/1-11-1024x551.jpg); background-size: cover; background-position: center top;\">\r\n  <!-- Mask -->\r\n  <span class=\"mask bg-gradient-danger opacity-8\"></span>\r\n\r\n</div>\r\n<div class=\"container-fluid mt--7\">\r\n  <div class=\"row\">\r\n    <div class=\"col-xl-4 order-xl-2 mb-5 mb-xl-0\">\r\n      <div class=\"card card-profile shadow\">\r\n        <div class=\"row justify-content-center\">\r\n          <div class=\"col-lg-3 order-lg-2\">\r\n            <div class=\"card-profile-image\">\r\n              <a href=\"javascript:void(0)\">\r\n                <img src=\"https://i.pinimg.com/originals/f0/44/c2/f044c2d34c38ac73891f3f0e9806d08e.png\"\r\n                  class=\"rounded-circle\">\r\n              </a>\r\n            </div>\r\n          </div>\r\n        </div>\r\n        <div class=\"card-header text-center border-0 pt-8 pt-md-4 pb-0 pb-md-4\">\r\n          <div class=\"d-flex justify-content-between\">\r\n\r\n          </div>\r\n        </div>\r\n        <div class=\"card-body pt-0 pt-md-4\">\r\n          <div class=\"row\">\r\n            <div class=\"col\">\r\n              <div class=\"card-profile-stats d-flex justify-content-center mt-md-5\">\r\n\r\n              </div>\r\n            </div>\r\n          </div>\r\n          <div class=\"text-center\">\r\n\r\n          </div>\r\n        </div>\r\n      </div>\r\n    </div>\r\n    <div class=\"col-xl-8 order-xl-1\">\r\n      <div class=\"card bg-secondary shadow\">\r\n        <div class=\"card-header bg-white border-0\">\r\n          <div class=\"row align-items-center\">\r\n            <div class=\"col-8\">\r\n              <h3 class=\"mb-0\">REGISTER USERS</h3>\r\n            </div>\r\n            <div class=\"col-4 text-right\">\r\n              <a (click)=\"save()\" class=\"btn btn-sm btn-primary\">save</a>\r\n            </div>\r\n          </div>\r\n        </div>\r\n        <div class=\"card-body\">\r\n\r\n          <h6 class=\"heading-small text-muted mb-4\">Solicitud</h6>\r\n          <div class=\"pl-lg-4\">\r\n            <div class=\"row\">\r\n              <div class=\"col-lg-6\">\r\n                <div class=\"form-group\">\r\n                  <label class=\"form-control-label\" for=\"input-username\">Descripcion:</label>\r\n                  <input type=\"text\" id=\"input-username\" [(ngModel)]=\"descripcion\"\r\n                    class=\"form-control form-control-alternative\">\r\n                </div>\r\n              </div>\r\n              <div class=\"col-lg-6\">\r\n                <div class=\"form-group\">\r\n                  <label class=\"form-control-label\" for=\"input-email\">Estado:</label>\r\n                  <input type=\"text\" [(ngModel)]=\"estado\" disabled=\"disabled\"\r\n                    class=\"form-control form-control-alternative\">\r\n                </div>\r\n              </div>\r\n            </div>\r\n            <div class=\"row\">\r\n              <div class=\"col-lg-6\">\r\n                <div class=\"form-group\">\r\n                  <label class=\"form-control-label\" for=\"input-firste-name\">Creador:</label>\r\n                  <input type=\"text\" id=\"input-first-najme\" [(ngModel)]=\"creador\"\r\n                    class=\"form-control form-control-alternative\">\r\n                </div>\r\n              </div>\r\n              <div class=\"col-lg-6\">\r\n                <div class=\"form-group\">\r\n                  <label class=\"form-control-label\" for=\"input-last-name\">Bodega</label>\r\n                  <input type=\"text\" id=\"input-last-namfe\" disabled=\"disabled\"\r\n                    class=\"form-control form-control-alternative\" [(ngModel)]=\"bodega\">\r\n                </div>\r\n              </div>\r\n            </div>\r\n\r\n\r\n            <div class=\"row\">\r\n\r\n              <div class=\"col-lg-6\">\r\n                <div class=\"form-group\">\r\n                  <label class=\"form-control-label\" for=\"input-last-namwe\">Tipo</label>\r\n                  <select [(ngModel)]=\"tipoA\" class=\"form-control form-control-alternative\">\r\n                    <option *ngFor=\"let tipo of tipos\">\r\n                      {{tipo}}\r\n                    </option>\r\n                  </select>\r\n                </div>\r\n\r\n\r\n              </div>\r\n              <div class=\"col-lg-6\">\r\n                <div class=\"form-group\">\r\n                  <label class=\"form-control-label\" for=\"input-last-name\">Bodega Destino</label>\r\n                  <input type=\"number\" id=\"input-last-namfe\" class=\"form-control form-control-alternative\"\r\n                    [(ngModel)]=\"bodega_dest\">\r\n                </div>\r\n              </div>\r\n\r\n            </div>\r\n\r\n          </div>\r\n        </div>\r\n      </div>\r\n    </div>\r\n  </div>\r\n</div>");

/***/ }),

/***/ "VBgP":
/*!***************************************************************************************!*\
  !*** ./src/app/pages/bodeguero/visualizar-internas/visualizar-internas.component.css ***!
  \***************************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = ("\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IiIsImZpbGUiOiJzcmMvYXBwL3BhZ2VzL2JvZGVndWVyby92aXN1YWxpemFyLWludGVybmFzL3Zpc3VhbGl6YXItaW50ZXJuYXMuY29tcG9uZW50LmNzcyJ9 */");

/***/ }),

/***/ "Z5rp":
/*!**************************************************************************************!*\
  !*** ./src/app/pages/bodeguero/visualizar-externas/visualizar-externas.component.ts ***!
  \**************************************************************************************/
/*! exports provided: ROUTES, VisualizarExternasComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "ROUTES", function() { return ROUTES; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "VisualizarExternasComponent", function() { return VisualizarExternasComponent; });
/* harmony import */ var _raw_loader_visualizar_externas_component_html__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! raw-loader!./visualizar-externas.component.html */ "DICF");
/* harmony import */ var _visualizar_externas_component_css__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./visualizar-externas.component.css */ "eAP3");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/core */ "fXoL");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/router */ "tyNb");
/* harmony import */ var _peticiones_service__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ../../../peticiones.service */ "kW39");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};





var ROUTES = [
    { path: '/visualizar-interna-bodeguero', title: 'Interna', icon: 'ni-tv-2 text-primary', class: '' },
    { path: '/visualizar-externa-bodeguero', title: 'Externa', icon: 'ni-tv-2 text-primary', class: '' },
];
var VisualizarExternasComponent = /** @class */ (function () {
    function VisualizarExternasComponent(router, app) {
        var _this = this;
        this.router = router;
        this.app = app;
        this.isCollapsed = true;
        this.solicitudes = [];
        var InfoUsuario = JSON.parse(sessionStorage.getItem('log'));
        this.app.getBodegasU(InfoUsuario.id).subscribe(function (resp1) {
            _this.app.getTansferenciasExterna(resp1[0].id).subscribe(function (resp) {
                _this.solicitudes = resp;
            });
        });
    }
    VisualizarExternasComponent.prototype.ngOnInit = function () {
        var _this = this;
        this.menuItems = ROUTES.filter(function (menuItem) { return menuItem; });
        this.router.events.subscribe(function (event) {
            _this.isCollapsed = true;
        });
    };
    VisualizarExternasComponent.prototype.Actualizar = function (id) {
        this.app.actualizarTransferencia(id).subscribe(function (resp) {
            alert(resp.msj);
        });
    };
    VisualizarExternasComponent.ctorParameters = function () { return [
        { type: _angular_router__WEBPACK_IMPORTED_MODULE_3__["Router"] },
        { type: _peticiones_service__WEBPACK_IMPORTED_MODULE_4__["PeticionesService"] }
    ]; };
    VisualizarExternasComponent = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_2__["Component"])({
            selector: 'app-visualizar-externas',
            template: _raw_loader_visualizar_externas_component_html__WEBPACK_IMPORTED_MODULE_0__["default"],
            styles: [_visualizar_externas_component_css__WEBPACK_IMPORTED_MODULE_1__["default"]]
        }),
        __metadata("design:paramtypes", [_angular_router__WEBPACK_IMPORTED_MODULE_3__["Router"], _peticiones_service__WEBPACK_IMPORTED_MODULE_4__["PeticionesService"]])
    ], VisualizarExternasComponent);
    return VisualizarExternasComponent;
}());



/***/ }),

/***/ "bBu9":
/*!***************************************************************************!*\
  !*** ./src/app/pages/bodeguero/transferencia/transferencia.component.css ***!
  \***************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = ("\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IiIsImZpbGUiOiJzcmMvYXBwL3BhZ2VzL2JvZGVndWVyby90cmFuc2ZlcmVuY2lhL3RyYW5zZmVyZW5jaWEuY29tcG9uZW50LmNzcyJ9 */");

/***/ }),

/***/ "bRtU":
/*!**************************************************************************************!*\
  !*** ./src/app/pages/bodeguero/visualizar-internas/visualizar-internas.component.ts ***!
  \**************************************************************************************/
/*! exports provided: ROUTES, VisualizarInternasComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "ROUTES", function() { return ROUTES; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "VisualizarInternasComponent", function() { return VisualizarInternasComponent; });
/* harmony import */ var _raw_loader_visualizar_internas_component_html__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! raw-loader!./visualizar-internas.component.html */ "vruh");
/* harmony import */ var _visualizar_internas_component_css__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./visualizar-internas.component.css */ "VBgP");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/core */ "fXoL");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/router */ "tyNb");
/* harmony import */ var _peticiones_service__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ../../../peticiones.service */ "kW39");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};





var ROUTES = [
    { path: '/visualizar-interna-bodeguero', title: 'Interna', icon: 'ni-tv-2 text-primary', class: '' },
    { path: '/visualizar-externa-bodeguero', title: 'Externa', icon: 'ni-tv-2 text-primary', class: '' },
];
var VisualizarInternasComponent = /** @class */ (function () {
    function VisualizarInternasComponent(router, app) {
        var _this = this;
        this.router = router;
        this.app = app;
        this.isCollapsed = true;
        this.solicitudes = [];
        var InfoUsuario = JSON.parse(sessionStorage.getItem('log'));
        this.app.getBodegasU(InfoUsuario.id).subscribe(function (resp1) {
            _this.app.getTansferenciasInterna(resp1[0].id).subscribe(function (resp) {
                _this.solicitudes = resp;
            });
        });
    }
    VisualizarInternasComponent.prototype.ngOnInit = function () {
        var _this = this;
        this.menuItems = ROUTES.filter(function (menuItem) { return menuItem; });
        this.router.events.subscribe(function (event) {
            _this.isCollapsed = true;
        });
    };
    VisualizarInternasComponent.prototype.Actualizar = function (id) {
        this.app.actualizarTransferencia(id).subscribe(function (resp) {
            alert(resp.msj);
        });
    };
    VisualizarInternasComponent.ctorParameters = function () { return [
        { type: _angular_router__WEBPACK_IMPORTED_MODULE_3__["Router"] },
        { type: _peticiones_service__WEBPACK_IMPORTED_MODULE_4__["PeticionesService"] }
    ]; };
    VisualizarInternasComponent = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_2__["Component"])({
            selector: 'app-visualizar-internas',
            template: _raw_loader_visualizar_internas_component_html__WEBPACK_IMPORTED_MODULE_0__["default"],
            styles: [_visualizar_internas_component_css__WEBPACK_IMPORTED_MODULE_1__["default"]]
        }),
        __metadata("design:paramtypes", [_angular_router__WEBPACK_IMPORTED_MODULE_3__["Router"], _peticiones_service__WEBPACK_IMPORTED_MODULE_4__["PeticionesService"]])
    ], VisualizarInternasComponent);
    return VisualizarInternasComponent;
}());



/***/ }),

/***/ "dcej":
/*!***************************************************************************!*\
  !*** ./src/app/pages/bodeguero/actualizacion/actualizacion.component.css ***!
  \***************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = ("\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IiIsImZpbGUiOiJzcmMvYXBwL3BhZ2VzL2JvZGVndWVyby9hY3R1YWxpemFjaW9uL2FjdHVhbGl6YWNpb24uY29tcG9uZW50LmNzcyJ9 */");

/***/ }),

/***/ "eAP3":
/*!***************************************************************************************!*\
  !*** ./src/app/pages/bodeguero/visualizar-externas/visualizar-externas.component.css ***!
  \***************************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = ("\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IiIsImZpbGUiOiJzcmMvYXBwL3BhZ2VzL2JvZGVndWVyby92aXN1YWxpemFyLWV4dGVybmFzL3Zpc3VhbGl6YXItZXh0ZXJuYXMuY29tcG9uZW50LmNzcyJ9 */");

/***/ }),

/***/ "iJ4Y":
/*!***************************************************************!*\
  !*** ./src/app/pages/bodeguero/profile/profile.component.css ***!
  \***************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = ("\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IiIsImZpbGUiOiJzcmMvYXBwL3BhZ2VzL2JvZGVndWVyby9wcm9maWxlL3Byb2ZpbGUuY29tcG9uZW50LmNzcyJ9 */");

/***/ }),

/***/ "mgA3":
/*!**************************************************************************!*\
  !*** ./src/app/pages/bodeguero/actualizacion/actualizacion.component.ts ***!
  \**************************************************************************/
/*! exports provided: ROUTES, ActualizacionComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "ROUTES", function() { return ROUTES; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "ActualizacionComponent", function() { return ActualizacionComponent; });
/* harmony import */ var _raw_loader_actualizacion_component_html__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! raw-loader!./actualizacion.component.html */ "Cj97");
/* harmony import */ var _actualizacion_component_css__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./actualizacion.component.css */ "dcej");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/core */ "fXoL");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/router */ "tyNb");
/* harmony import */ var _peticiones_service__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ../../../peticiones.service */ "kW39");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};





var ROUTES = [
    { path: '/actualizacion-bodeguero', title: 'Actualizar', icon: 'ni-tv-2 text-primary', class: '' },
];
var ActualizacionComponent = /** @class */ (function () {
    function ActualizacionComponent(router, app) {
        var _this = this;
        this.router = router;
        this.app = app;
        this.isCollapsed = true;
        this.solicitudes = [];
        var InfoUsuario = JSON.parse(sessionStorage.getItem('log'));
        this.app.getBodegasU(InfoUsuario.id).subscribe(function (resp1) {
            _this.app.getProductoInventario(resp1[0].id).subscribe(function (resp) {
                _this.solicitudes = resp;
                console.log(resp1);
            });
        });
    }
    ActualizacionComponent.prototype.ngOnInit = function () {
        var _this = this;
        this.menuItems = ROUTES.filter(function (menuItem) { return menuItem; });
        this.router.events.subscribe(function (event) {
            _this.isCollapsed = true;
        });
    };
    ActualizacionComponent.prototype.Actualizar = function (id, cant, motivo) {
        this.app.actualizarInventario({ "inventario": id, "cantidad": cant }).subscribe(function (resp) {
            alert(resp.descripcion);
        });
    };
    ActualizacionComponent.prototype.Eliminar = function (id) {
        var _this = this;
        this.app.deleteSede({ "id": id }).subscribe(function (resp) {
            alert(resp.msj);
            _this.router.navigate(['/view-sedes']);
            _this.app.getSedes().subscribe(function (resp) {
                _this.solicitudes = resp;
            });
        });
    };
    ActualizacionComponent.ctorParameters = function () { return [
        { type: _angular_router__WEBPACK_IMPORTED_MODULE_3__["Router"] },
        { type: _peticiones_service__WEBPACK_IMPORTED_MODULE_4__["PeticionesService"] }
    ]; };
    ActualizacionComponent = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_2__["Component"])({
            selector: 'app-actualizacion',
            template: _raw_loader_actualizacion_component_html__WEBPACK_IMPORTED_MODULE_0__["default"],
            styles: [_actualizacion_component_css__WEBPACK_IMPORTED_MODULE_1__["default"]]
        }),
        __metadata("design:paramtypes", [_angular_router__WEBPACK_IMPORTED_MODULE_3__["Router"], _peticiones_service__WEBPACK_IMPORTED_MODULE_4__["PeticionesService"]])
    ], ActualizacionComponent);
    return ActualizacionComponent;
}());



/***/ }),

/***/ "pYIQ":
/*!********************************************************!*\
  !*** ./src/app/layouts/bodeguero/bodeguero.routing.ts ***!
  \********************************************************/
/*! exports provided: BodegueroRoutes */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "BodegueroRoutes", function() { return BodegueroRoutes; });
/* harmony import */ var _pages_bodeguero_profile_profile_component__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ../../pages/bodeguero/profile/profile.component */ "/AE7");
/* harmony import */ var _pages_bodeguero_actualizacion_actualizacion_component__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ../../pages/bodeguero/actualizacion/actualizacion.component */ "mgA3");
/* harmony import */ var _pages_bodeguero_transferencia_transferencia_component__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../../pages/bodeguero/transferencia/transferencia.component */ "/3df");
/* harmony import */ var _pages_bodeguero_visualizar_internas_visualizar_internas_component__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ../../pages/bodeguero/visualizar-internas/visualizar-internas.component */ "bRtU");
/* harmony import */ var _pages_bodeguero_visualizar_externas_visualizar_externas_component__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ../../pages/bodeguero/visualizar-externas/visualizar-externas.component */ "Z5rp");





var BodegueroRoutes = [
    { path: 'profile-bodeguero', component: _pages_bodeguero_profile_profile_component__WEBPACK_IMPORTED_MODULE_0__["ProfileComponent"] },
    { path: 'actualizacion-bodeguero', component: _pages_bodeguero_actualizacion_actualizacion_component__WEBPACK_IMPORTED_MODULE_1__["ActualizacionComponent"] },
    { path: 'transferencia-bodeguero', component: _pages_bodeguero_transferencia_transferencia_component__WEBPACK_IMPORTED_MODULE_2__["TransferenciaComponent"] },
    { path: 'visualizar-interna-bodeguero', component: _pages_bodeguero_visualizar_internas_visualizar_internas_component__WEBPACK_IMPORTED_MODULE_3__["VisualizarInternasComponent"] },
    { path: 'visualizar-externa-bodeguero', component: _pages_bodeguero_visualizar_externas_visualizar_externas_component__WEBPACK_IMPORTED_MODULE_4__["VisualizarExternasComponent"] },
];


/***/ }),

/***/ "vruh":
/*!******************************************************************************************************************************!*\
  !*** ./node_modules/raw-loader/dist/cjs.js!./src/app/pages/bodeguero/visualizar-internas/visualizar-internas.component.html ***!
  \******************************************************************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = ("<div class=\"header pb-5 pt-5 pt-lg-5 d-flex align-items-center\" style=\"min-height: 100px; background-image: url(https://geoinnova.org/cursos/wp-content/uploads/2016/10/1-11-1024x551.jpg); background-size: cover; background-position: center top;\">\r\n    <!-- Mask -->\r\n    <span class=\"mask bg-gradient-danger opacity-8\"></span>\r\n   \r\n  </div>\r\n\r\n  <nav class=\"navbar navbar-horizontal navbar-expand-md navbar-light bg-white\" id=\"sidenav-main\">\r\n    <div class=\"container-fluid\">\r\n      <!-- Toggler -->\r\n      <button class=\"navbar-toggler\" type=\"button\" (click)=\"isCollapsed=!isCollapsed\"\r\n         aria-controls=\"sidenav-collapse-main\">\r\n        <span class=\"navbar-toggler-icon\"></span>\r\n      </button>\r\n    \r\n     \r\n     \r\n      <!-- Collapse -->\r\n      <div class=\"collapse navbar-collapse\"  [ngbCollapse]=\"isCollapsed\" id=\"sidenav-collapse-main\">\r\n        <!-- Collapse header -->\r\n        <div class=\"navbar-collapse-header d-md-none\">\r\n          <div class=\"row\">\r\n            <div class=\"col-6 collapse-brand\">\r\n              <a  routerLinkActive=\"active\" [routerLink]=\"['/dashboard']\">\r\n                <img src=\"./assets/img/brand/blue.png\">\r\n              </a>\r\n            </div>\r\n            <div class=\"col-6 collapse-close\">\r\n              <button type=\"button\" class=\"navbar-toggler\" (click)=\"isCollapsed=!isCollapsed\">\r\n                <span></span>\r\n                <span></span>\r\n              </button>\r\n            </div>\r\n          </div>\r\n        </div>\r\n        <!-- Form -->\r\n        <form class=\"mt-4 mb-3 d-md-none\">\r\n          <div class=\"input-group input-group-rounded input-group-merge\">\r\n            <input type=\"search\" class=\"form-control form-control-rounded form-control-prepended\" placeholder=\"Search\" aria-label=\"Search\">\r\n            <div class=\"input-group-prepend\">\r\n              <div class=\"input-group-text\">\r\n                <span class=\"fa fa-search\"></span>\r\n              </div>\r\n            </div>\r\n          </div>\r\n        </form>\r\n        <!-- Navigation -->\r\n        <ul class=\"navbar-nav\">\r\n            <li *ngFor=\"let menuItem of menuItems\" class=\"{{menuItem.class}} nav-item\">\r\n                <a routerLinkActive=\"active\" [routerLink]=\"[menuItem.path]\" class=\"nav-link\">\r\n                    <i class=\"ni {{menuItem.icon}}\"></i>\r\n                    {{menuItem.title}}\r\n                </a>\r\n               \r\n            </li>\r\n            \r\n        </ul>\r\n        <!-- Divider -->\r\n        <hr class=\"my-3\">\r\n        <!-- Heading -->\r\n       \r\n      </div>\r\n    </div>\r\n  </nav>\r\n\r\n\r\n  \r\n    \r\n\r\n  <div class=\"dark\">\r\n    <div class=\"row\">\r\n        <div *ngFor=\"let solicitud of solicitudes\" class=\"col-sm-3\">\r\n            <div class=\"card space\" style=\"width: 20rem;\">\r\n                <div class=\"card-header\">\r\n                    Producto\r\n                </div>\r\n                <div class=\"card-body\">\r\n                    \r\n                    <div class=\"input-group input-group-alternative\">\r\n                        <div class=\"input-group-prepend\">\r\n                          <span class=\"input-group-text\">ID:</span>\r\n                        </div>\r\n                        <input class=\"form-control\" [(ngModel)] =\"solicitud.Id\" type=\"text\">\r\n                      </div>\r\n                   \r\n                      <div class=\"input-group input-group-alternative\">\r\n                        <div class=\"input-group-prepend\">\r\n                          <span class=\"input-group-text\">Estado:</span>\r\n                        </div>\r\n                        <input class=\"form-control\"  [(ngModel)] =\"solicitud.Estado\" type=\"text\">\r\n                      </div>\r\n\r\n                      <div class=\"input-group input-group-alternative\">\r\n                        <div class=\"input-group-prepend\">\r\n                          <span class=\"input-group-text\">Creador:</span>\r\n                        </div>\r\n                        <input class=\"form-control\"  [(ngModel)] =\"solicitud.Creador\" type=\"text\">\r\n                      </div>\r\n\r\n                      <div class=\"input-group input-group-alternative\">\r\n                        <div class=\"input-group-prepend\">\r\n                          <span class=\"input-group-text\">Bodega Origen:</span>\r\n                        </div>\r\n                        <input class=\"form-control\" [(ngModel)] =\"solicitud.Bodega_solicitante\" type=\"text\">\r\n                      </div>\r\n\r\n                      <div class=\"input-group input-group-alternative\">\r\n                        <div class=\"input-group-prepend\">\r\n                          <span class=\"input-group-text\">Descripcion:</span>\r\n                        </div>\r\n                        <input class=\"form-control\" [(ngModel)] =\"solicitud.Descripcion\" type=\"text\">\r\n                      </div>\r\n\r\n\r\n                      \r\n                   \r\n                   \r\n                      <button  class=\"btn btn-warning space\"  (click)=\"Actualizar(solicitud.Id)\">Aceptar</button>\r\n                      \r\n                </div>\r\n            </div> \r\n        </div>\r\n    </div>");

/***/ })

}]);
//# sourceMappingURL=layouts-bodeguero-bodeguero-module.js.map