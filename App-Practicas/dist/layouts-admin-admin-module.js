(window["webpackJsonp"] = window["webpackJsonp"] || []).push([["layouts-admin-admin-module"],{

/***/ "1Mxm":
/*!******************************************************************************************!*\
  !*** ./node_modules/raw-loader/dist/cjs.js!./src/app/pages/admin/rol/rol.component.html ***!
  \******************************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = ("<div class=\"header pb-8 pt-5 pt-lg-8 d-flex align-items-center\"\n  style=\"min-height: 200px; background-image: url(https://geoinnova.org/cursos/wp-content/uploads/2016/10/1-11-1024x551.jpg); background-size: cover; background-position: center top;\">\n  <!-- Mask -->\n  <span class=\"mask bg-gradient-danger opacity-8\"></span>\n\n</div>\n<div class=\"container-fluid mt--7\">\n  <div class=\"row\">\n    <div class=\"col-xl-4 order-xl-2 mb-5 mb-xl-0\">\n      <div class=\"card card-profile shadow\">\n        <div class=\"row justify-content-center\">\n          <div class=\"col-lg-3 order-lg-2\">\n            <div class=\"card-profile-image\">\n              <a href=\"javascript:void(0)\">\n                <img src=\"https://logodix.com/logo/1707088.png\" class=\"rounded-circle\">\n              </a>\n            </div>\n          </div>\n        </div>\n        <div class=\"card-header text-center border-0 pt-8 pt-md-4 pb-0 pb-md-4\">\n          <div class=\"d-flex justify-content-between\">\n\n          </div>\n        </div>\n        <div class=\"card-body pt-0 pt-md-4\">\n          <div class=\"row\">\n            <div class=\"col\">\n              <div class=\"card-profile-stats d-flex justify-content-center mt-md-5\">\n\n              </div>\n            </div>\n          </div>\n          <div class=\"text-center\">\n\n          </div>\n        </div>\n      </div>\n    </div>\n    <div class=\"col-xl-8 order-xl-1\">\n      <div class=\"card bg-secondary shadow\">\n        <div class=\"card-header bg-white border-0\">\n          <div class=\"row align-items-center\">\n            <div class=\"col-8\">\n              <h3 class=\"mb-0\">REGISTER USERS</h3>\n            </div>\n            <div class=\"col-4 text-right\">\n              <a (click)=\"save()\" class=\"btn btn-sm btn-primary\">save</a>\n            </div>\n          </div>\n        </div>\n        <div class=\"card-body\">\n\n          <h6 class=\"heading-small text-muted mb-4\">User information</h6>\n          <div class=\"pl-lg-4\">\n                     \n\n            <div class=\"row\">\n              <div class=\"col-lg-6\">\n                <div class=\"form-group\">\n                  <label class=\"form-control-label\" for=\"input-first-name\">idUser</label>\n                  <input type=\"number\" id=\"input-first-name\" [(ngModel)]=\"idU\"\n                    class=\"form-control form-control-alternative\" value=\"{{idU}}\">\n                </div>\n              </div>\n              <div class=\"col-lg-6\">\n                <div class=\"form-group\">\n                  <label class=\"form-control-label\" for=\"input-last-namwe\">rol</label>\n                  <select [(ngModel)]=\"opcionSeleccionadoRol\" class=\"form-control form-control-alternative\">\n                    <option *ngFor=\"let rol of roles\">\n                      {{rol}}\n                    </option>\n                  </select>\n                </div>\n              </div>\n\n            </div>\n\n\n            <div class=\"row\">\n               \n                <div class=\"col-lg-6\">\n                  <div class=\"form-group\">\n                    <label class=\"form-control-label\" for=\"input-last-namwe\">Accion</label>\n                    <select [(ngModel)]=\"opcionSeleccionado\" class=\"form-control form-control-alternative\">\n                      <option>\n                        Quitar\n                      </option>\n\n                      <option>\n                        Agregar\n                      </option>\n                    </select>\n                  </div>\n                </div>\n  \n              </div>\n\n\n          </div>\n\n\n\n\n\n        </div>\n      </div>\n    </div>\n  </div>\n</div>");

/***/ }),

/***/ "23Hi":
/*!****************************************************************!*\
  !*** ./src/app/pages/admin/form-users/form-users.component.ts ***!
  \****************************************************************/
/*! exports provided: FormUsersComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "FormUsersComponent", function() { return FormUsersComponent; });
/* harmony import */ var _raw_loader_form_users_component_html__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! raw-loader!./form-users.component.html */ "m3dm");
/* harmony import */ var _form_users_component_css__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./form-users.component.css */ "JksJ");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/core */ "fXoL");
/* harmony import */ var _peticiones_service__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ../../../peticiones.service */ "kW39");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @angular/router */ "tyNb");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};





var FormUsersComponent = /** @class */ (function () {
    function FormUsersComponent(router, app) {
        this.router = router;
        this.app = app;
        this.roles = ['Encargado', 'Bodeguero', 'Vendedor', 'Repartidor'];
    }
    FormUsersComponent.prototype.ngOnInit = function () {
    };
    FormUsersComponent.prototype.save = function () {
        var _this = this;
        this.app.postCrearUsuario({ "dpi": this.dpi, "nombre": this.name, "fechanac": this.fechaN, "correo": this.email, "password": this.password, "rol": this.opcionSeleccionado }).subscribe(function (resp) {
            alert(resp.msj);
            _this.dpi = 0;
            _this.name = _this.fechaN = _this.email = _this.password = "";
        });
    };
    FormUsersComponent.ctorParameters = function () { return [
        { type: _angular_router__WEBPACK_IMPORTED_MODULE_4__["Router"] },
        { type: _peticiones_service__WEBPACK_IMPORTED_MODULE_3__["PeticionesService"] }
    ]; };
    FormUsersComponent = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_2__["Component"])({
            selector: 'app-form-users',
            template: _raw_loader_form_users_component_html__WEBPACK_IMPORTED_MODULE_0__["default"],
            styles: [_form_users_component_css__WEBPACK_IMPORTED_MODULE_1__["default"]]
        }),
        __metadata("design:paramtypes", [_angular_router__WEBPACK_IMPORTED_MODULE_4__["Router"], _peticiones_service__WEBPACK_IMPORTED_MODULE_3__["PeticionesService"]])
    ], FormUsersComponent);
    return FormUsersComponent;
}());



/***/ }),

/***/ "2yPx":
/*!*****************************************************************!*\
  !*** ./src/app/pages/admin/view-sedes/view-sedes.component.css ***!
  \*****************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = (".t{\r\n    width: 300px;\r\n    \r\n}\r\n\r\n.esp{\r\n    width: 25px;\r\n}\r\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbInNyYy9hcHAvcGFnZXMvYWRtaW4vdmlldy1zZWRlcy92aWV3LXNlZGVzLmNvbXBvbmVudC5jc3MiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IkFBQUE7SUFDSSxZQUFZOztBQUVoQjs7QUFFQTtJQUNJLFdBQVc7QUFDZiIsImZpbGUiOiJzcmMvYXBwL3BhZ2VzL2FkbWluL3ZpZXctc2VkZXMvdmlldy1zZWRlcy5jb21wb25lbnQuY3NzIiwic291cmNlc0NvbnRlbnQiOlsiLnR7XHJcbiAgICB3aWR0aDogMzAwcHg7XHJcbiAgICBcclxufVxyXG5cclxuLmVzcHtcclxuICAgIHdpZHRoOiAyNXB4O1xyXG59Il19 */");

/***/ }),

/***/ "95zn":
/*!***************************************************!*\
  !*** ./src/app/pages/admin/rol/rol.component.css ***!
  \***************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = ("\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IiIsImZpbGUiOiJzcmMvYXBwL3BhZ2VzL2FkbWluL3JvbC9yb2wuY29tcG9uZW50LmNzcyJ9 */");

/***/ }),

/***/ "ACLt":
/*!***********************************************!*\
  !*** ./src/app/layouts/admin/admin.module.ts ***!
  \***********************************************/
/*! exports provided: AdminModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "AdminModule", function() { return AdminModule; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "fXoL");
/* harmony import */ var _angular_common_http__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/common/http */ "tk/3");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/router */ "tyNb");
/* harmony import */ var _angular_common__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/common */ "ofXK");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @angular/forms */ "3Pt+");
/* harmony import */ var ngx_clipboard__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ngx-clipboard */ "Dvla");
/* harmony import */ var _admin_routing__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ./admin.routing */ "qr1/");
/* harmony import */ var _ng_bootstrap_ng_bootstrap__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! @ng-bootstrap/ng-bootstrap */ "1kSV");
/* harmony import */ var _pages_admin_form_users_form_users_component__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! ../../pages/admin/form-users/form-users.component */ "23Hi");
/* harmony import */ var _pages_admin_form_sedes_form_sedes_component__WEBPACK_IMPORTED_MODULE_9__ = __webpack_require__(/*! ../../pages/admin/form-sedes/form-sedes.component */ "jJig");
/* harmony import */ var _pages_admin_view_sedes_view_sedes_component__WEBPACK_IMPORTED_MODULE_10__ = __webpack_require__(/*! ../../pages/admin/view-sedes/view-sedes.component */ "UK8G");
/* harmony import */ var _pages_admin_rol_rol_component__WEBPACK_IMPORTED_MODULE_11__ = __webpack_require__(/*! ../../pages/admin/rol/rol.component */ "oB2U");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};












var AdminModule = /** @class */ (function () {
    function AdminModule() {
    }
    AdminModule = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["NgModule"])({
            imports: [
                _angular_common__WEBPACK_IMPORTED_MODULE_3__["CommonModule"],
                _angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"].forChild(_admin_routing__WEBPACK_IMPORTED_MODULE_6__["AdminRoutes"]),
                _angular_forms__WEBPACK_IMPORTED_MODULE_4__["FormsModule"],
                _angular_common_http__WEBPACK_IMPORTED_MODULE_1__["HttpClientModule"],
                _ng_bootstrap_ng_bootstrap__WEBPACK_IMPORTED_MODULE_7__["NgbModule"],
                ngx_clipboard__WEBPACK_IMPORTED_MODULE_5__["ClipboardModule"]
            ],
            declarations: [
                _pages_admin_form_users_form_users_component__WEBPACK_IMPORTED_MODULE_8__["FormUsersComponent"],
                _pages_admin_form_sedes_form_sedes_component__WEBPACK_IMPORTED_MODULE_9__["FormSedesComponent"],
                _pages_admin_view_sedes_view_sedes_component__WEBPACK_IMPORTED_MODULE_10__["ViewSedesComponent"],
                _pages_admin_rol_rol_component__WEBPACK_IMPORTED_MODULE_11__["RolComponent"]
            ]
        })
    ], AdminModule);
    return AdminModule;
}());



/***/ }),

/***/ "G4zp":
/*!********************************************************************************************************!*\
  !*** ./node_modules/raw-loader/dist/cjs.js!./src/app/pages/admin/view-sedes/view-sedes.component.html ***!
  \********************************************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = ("<div class=\"header pb-5 pt-5 pt-lg-5 d-flex align-items-center\" style=\"min-height: 100px; background-image: url(https://geoinnova.org/cursos/wp-content/uploads/2016/10/1-11-1024x551.jpg); background-size: cover; background-position: center top;\">\r\n    <!-- Mask -->\r\n    <span class=\"mask bg-gradient-danger opacity-8\"></span>\r\n   \r\n  </div>\r\n\r\n  <nav class=\"navbar navbar-horizontal navbar-expand-md navbar-light bg-white\" id=\"sidenav-main\">\r\n    <div class=\"container-fluid\">\r\n      <!-- Toggler -->\r\n      <button class=\"navbar-toggler\" type=\"button\" (click)=\"isCollapsed=!isCollapsed\"\r\n         aria-controls=\"sidenav-collapse-main\">\r\n        <span class=\"navbar-toggler-icon\"></span>\r\n      </button>\r\n    \r\n     \r\n     \r\n      <!-- Collapse -->\r\n      <div class=\"collapse navbar-collapse\"  [ngbCollapse]=\"isCollapsed\" id=\"sidenav-collapse-main\">\r\n        <!-- Collapse header -->\r\n        <div class=\"navbar-collapse-header d-md-none\">\r\n          <div class=\"row\">\r\n            <div class=\"col-6 collapse-brand\">\r\n              <a  routerLinkActive=\"active\" [routerLink]=\"['/dashboard']\">\r\n                <img src=\"./assets/img/brand/blue.png\">\r\n              </a>\r\n            </div>\r\n            <div class=\"col-6 collapse-close\">\r\n              <button type=\"button\" class=\"navbar-toggler\" (click)=\"isCollapsed=!isCollapsed\">\r\n                <span></span>\r\n                <span></span>\r\n              </button>\r\n            </div>\r\n          </div>\r\n        </div>\r\n        <!-- Form -->\r\n        <form class=\"mt-4 mb-3 d-md-none\">\r\n          <div class=\"input-group input-group-rounded input-group-merge\">\r\n            <input type=\"search\" class=\"form-control form-control-rounded form-control-prepended\" placeholder=\"Search\" aria-label=\"Search\">\r\n            <div class=\"input-group-prepend\">\r\n              <div class=\"input-group-text\">\r\n                <span class=\"fa fa-search\"></span>\r\n              </div>\r\n            </div>\r\n          </div>\r\n        </form>\r\n        <!-- Navigation -->\r\n        <ul class=\"navbar-nav\">\r\n            <li *ngFor=\"let menuItem of menuItems\" class=\"{{menuItem.class}} nav-item\">\r\n                <a routerLinkActive=\"active\" [routerLink]=\"[menuItem.path]\" class=\"nav-link\">\r\n                    <i class=\"ni {{menuItem.icon}}\"></i>\r\n                    {{menuItem.title}}\r\n                </a>\r\n               \r\n            </li>\r\n            \r\n        </ul>\r\n        <!-- Divider -->\r\n        <hr class=\"my-3\">\r\n        <!-- Heading -->\r\n       \r\n      </div>\r\n    </div>\r\n  </nav>\r\n\r\n\r\n  \r\n    \r\n\r\n  <div class=\"dark\">\r\n    <div class=\"row\">\r\n        <div *ngFor=\"let solicitud of solicitudes\" class=\"col-sm-3\">\r\n            <div class=\"card space\" style=\"width: 20rem;\">\r\n                <div class=\"card-header\">\r\n                    SEDE\r\n                </div>\r\n                <div class=\"card-body\">\r\n                    \r\n                    <div class=\"input-group input-group-alternative\">\r\n                        <div class=\"input-group-prepend\">\r\n                          <span class=\"input-group-text\">Alias:</span>\r\n                        </div>\r\n                        <input class=\"form-control\" [(ngModel)] =\"solicitud.Sede\" type=\"text\">\r\n                      </div>\r\n                   \r\n                      <div class=\"input-group input-group-alternative\">\r\n                        <div class=\"input-group-prepend\">\r\n                          <span class=\"input-group-text\">Direccion:</span>\r\n                        </div>\r\n                        <input class=\"form-control\" [(ngModel)] =\"solicitud.Direccion\" type=\"text\">\r\n                      </div>\r\n\r\n                      <div class=\"input-group input-group-alternative\">\r\n                        <div class=\"input-group-prepend\">\r\n                          <span class=\"input-group-text\">Departamento:</span>\r\n                        </div>\r\n                        <input class=\"form-control\" [(ngModel)] =\"solicitud.Departamento\" type=\"text\">\r\n                      </div>\r\n\r\n                      <div class=\"input-group input-group-alternative\">\r\n                        <div class=\"input-group-prepend\">\r\n                          <span class=\"input-group-text\">Municipio:</span>\r\n                        </div>\r\n                        <input class=\"form-control\" [(ngModel)]=\"solicitud.Municipio\" type=\"text\">\r\n                      </div>\r\n                   \r\n                      <div class=\"input-group input-group-alternative\">\r\n                        <div class=\"input-group-prepend\">\r\n                          <span class=\"input-group-text\">Encargado:</span>\r\n                        </div>\r\n                        <input class=\"form-control\" [(ngModel)] =\"solicitud.Encargado\" type=\"text\">\r\n                      </div>\r\n                    \r\n                    <button  class=\"btn btn-warning space\"  (click)=\"Actualizar(solicitud.id,solicitud.Sede,solicitud.Direccion, solicitud.Departamento,solicitud.Municipio,solicitud.Encargado)\">Actualizar</button>\r\n                    <button  class=\"btn btn-danger space\" (click)=\"Eliminar(solicitud.id)\"> Eliminar</button>\r\n                </div>\r\n            </div> \r\n        </div>\r\n    </div>\r\n</div>");

/***/ }),

/***/ "JksJ":
/*!*****************************************************************!*\
  !*** ./src/app/pages/admin/form-users/form-users.component.css ***!
  \*****************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = ("\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IiIsImZpbGUiOiJzcmMvYXBwL3BhZ2VzL2FkbWluL2Zvcm0tdXNlcnMvZm9ybS11c2Vycy5jb21wb25lbnQuY3NzIn0= */");

/***/ }),

/***/ "UK8G":
/*!****************************************************************!*\
  !*** ./src/app/pages/admin/view-sedes/view-sedes.component.ts ***!
  \****************************************************************/
/*! exports provided: ROUTES, ViewSedesComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "ROUTES", function() { return ROUTES; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "ViewSedesComponent", function() { return ViewSedesComponent; });
/* harmony import */ var _raw_loader_view_sedes_component_html__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! raw-loader!./view-sedes.component.html */ "G4zp");
/* harmony import */ var _view_sedes_component_css__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./view-sedes.component.css */ "2yPx");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/core */ "fXoL");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/router */ "tyNb");
/* harmony import */ var _peticiones_service__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ../../../peticiones.service */ "kW39");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};





var ROUTES = [
    {
        path: "/view-sedes",
        title: "VIEW",
        icon: "ni-tv-2 text-primary",
        class: "",
    },
    {
        path: "/form-sedes",
        title: "ADD",
        icon: "ni-tv-2 text-primary",
        class: "",
    },
];
var ViewSedesComponent = /** @class */ (function () {
    function ViewSedesComponent(router, app) {
        var _this = this;
        this.router = router;
        this.app = app;
        this.isCollapsed = true;
        this.solicitudes = [];
        this.app.getSedes().subscribe(function (resp) {
            _this.solicitudes = resp;
        });
    }
    ViewSedesComponent.prototype.ngOnInit = function () {
        var _this = this;
        this.menuItems = ROUTES.filter(function (menuItem) { return menuItem; });
        this.router.events.subscribe(function (event) {
            _this.isCollapsed = true;
        });
    };
    ViewSedesComponent.prototype.Actualizar = function (id, alias, direc, dep, mun, enc) {
        this.app
            .putModificarSede({
            alias: alias,
            direccion: direc,
            departamento: dep,
            municipio: mun,
            id: id,
        })
            .subscribe(function (resp) {
            alert(resp.msj);
        });
    };
    ViewSedesComponent.prototype.Eliminar = function (id) {
        var _this = this;
        this.app.deleteSede({ id: id }).subscribe(function (resp) {
            alert(resp.msj);
            _this.router.navigate(["/view-sedes"]);
            _this.app.getSedes().subscribe(function (resp) {
                _this.solicitudes = resp;
            });
        });
    };
    ViewSedesComponent.ctorParameters = function () { return [
        { type: _angular_router__WEBPACK_IMPORTED_MODULE_3__["Router"] },
        { type: _peticiones_service__WEBPACK_IMPORTED_MODULE_4__["PeticionesService"] }
    ]; };
    ViewSedesComponent = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_2__["Component"])({
            selector: "app-view-sedes",
            template: _raw_loader_view_sedes_component_html__WEBPACK_IMPORTED_MODULE_0__["default"],
            styles: [_view_sedes_component_css__WEBPACK_IMPORTED_MODULE_1__["default"]]
        }),
        __metadata("design:paramtypes", [_angular_router__WEBPACK_IMPORTED_MODULE_3__["Router"], _peticiones_service__WEBPACK_IMPORTED_MODULE_4__["PeticionesService"]])
    ], ViewSedesComponent);
    return ViewSedesComponent;
}());



/***/ }),

/***/ "WEv8":
/*!*****************************************************************!*\
  !*** ./src/app/pages/admin/form-sedes/form-sedes.component.css ***!
  \*****************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = ("\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IiIsImZpbGUiOiJzcmMvYXBwL3BhZ2VzL2FkbWluL2Zvcm0tc2VkZXMvZm9ybS1zZWRlcy5jb21wb25lbnQuY3NzIn0= */");

/***/ }),

/***/ "isNQ":
/*!********************************************************************************************************!*\
  !*** ./node_modules/raw-loader/dist/cjs.js!./src/app/pages/admin/form-sedes/form-sedes.component.html ***!
  \********************************************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = ("<div class=\"header pb-5 pt-5 pt-lg-5 d-flex align-items-center\"\r\n  style=\"min-height: 100px; background-image: url(https://geoinnova.org/cursos/wp-content/uploads/2016/10/1-11-1024x551.jpg); background-size: cover; background-position: center top;\">\r\n  <!-- Mask -->\r\n  <span class=\"mask bg-gradient-danger opacity-8\"></span>\r\n\r\n</div>\r\n\r\n<nav class=\"navbar navbar-horizontal navbar-expand-md navbar-light bg-white\" id=\"sidenav-main\">\r\n  <div class=\"container-fluid\">\r\n    <!-- Toggler -->\r\n    <button class=\"navbar-toggler\" type=\"button\" (click)=\"isCollapsed=!isCollapsed\"\r\n      aria-controls=\"sidenav-collapse-main\">\r\n      <span class=\"navbar-toggler-icon\"></span>\r\n    </button>\r\n\r\n\r\n\r\n    <!-- Collapse -->\r\n    <div class=\"collapse navbar-collapse\" [ngbCollapse]=\"isCollapsed\" id=\"sidenav-collapse-main\">\r\n      <!-- Collapse header -->\r\n      <div class=\"navbar-collapse-header d-md-none\">\r\n        <div class=\"row\">\r\n          <div class=\"col-6 collapse-brand\">\r\n            <a routerLinkActive=\"active\" [routerLink]=\"['/dashboard']\">\r\n              <img src=\"./assets/img/brand/blue.png\">\r\n            </a>\r\n          </div>\r\n          <div class=\"col-6 collapse-close\">\r\n            <button type=\"button\" class=\"navbar-toggler\" (click)=\"isCollapsed=!isCollapsed\">\r\n              <span></span>\r\n              <span></span>\r\n            </button>\r\n          </div>\r\n        </div>\r\n      </div>\r\n      <!-- Form -->\r\n      <form class=\"mt-4 mb-3 d-md-none\">\r\n        <div class=\"input-group input-group-rounded input-group-merge\">\r\n          <input type=\"search\" class=\"form-control form-control-rounded form-control-prepended\" placeholder=\"Search\"\r\n            aria-label=\"Search\">\r\n          <div class=\"input-group-prepend\">\r\n            <div class=\"input-group-text\">\r\n              <span class=\"fa fa-search\"></span>\r\n            </div>\r\n          </div>\r\n        </div>\r\n      </form>\r\n      <!-- Navigation -->\r\n      <ul class=\"navbar-nav\">\r\n        <li *ngFor=\"let menuItem of menuItems\" class=\"{{menuItem.class}} nav-item\">\r\n          <a routerLinkActive=\"active\" [routerLink]=\"[menuItem.path]\" class=\"nav-link\">\r\n            <i class=\"ni {{menuItem.icon}}\"></i>\r\n            {{menuItem.title}}\r\n          </a>\r\n\r\n        </li>\r\n\r\n      </ul>\r\n      <!-- Divider -->\r\n      <hr class=\"my-3\">\r\n      <!-- Heading -->\r\n\r\n    </div>\r\n  </div>\r\n</nav>\r\n\r\n\r\n\r\n<div class=\"container-fluid mt-2\">\r\n  <div class=\"row\">\r\n    <div class=\"col-xl-4 order-xl-2 mb-5 mb-xl-0\">\r\n      <div class=\"card card-profile shadow\">\r\n        <div class=\"row justify-content-center\">\r\n          <div class=\"col-lg-3 order-lg-2\">\r\n            <div class=\"card-profile-image\">\r\n              <a href=\"javascript:void(0)\">\r\n                <img src=\"https://image.flaticon.com/icons/png/512/16/16199.png\" class=\"rounded-circle\">\r\n              </a>\r\n            </div>\r\n          </div>\r\n        </div>\r\n        <div class=\"card-header text-center border-0 pt-8 pt-md-4 pb-0 pb-md-4\">\r\n          <div class=\"d-flex justify-content-between\">\r\n\r\n          </div>\r\n        </div>\r\n        <div class=\"card-body pt-0 pt-md-4\">\r\n          <div class=\"row\">\r\n            <div class=\"col\">\r\n              <div class=\"card-profile-stats d-flex justify-content-center mt-md-5\">\r\n\r\n              </div>\r\n            </div>\r\n          </div>\r\n          <div class=\"text-center\">\r\n\r\n          </div>\r\n        </div>\r\n      </div>\r\n    </div>\r\n    <div class=\"col-xl-8 order-xl-1\">\r\n      <div class=\"card bg-secondary shadow\">\r\n        <div class=\"card-header bg-white border-0\">\r\n          <div class=\"row align-items-center\">\r\n            <div class=\"col-8\">\r\n              <h3 class=\"mb-0\">REGISTER SEDE</h3>\r\n            </div>\r\n            <div class=\"col-4 text-right\">\r\n              <a (click)=\"registrar()\" class=\"btn btn-sm btn-primary\">save</a>\r\n            </div>\r\n          </div>\r\n        </div>\r\n        <div class=\"card-body\">\r\n\r\n          <h6 class=\"heading-small text-muted mb-4\">DATA</h6>\r\n          <div class=\"pl-lg-4\">\r\n            <div class=\"row\">\r\n              <div class=\"col-lg-6\">\r\n                <div class=\"form-group\">\r\n                  <label class=\"form-control-label\" for=\"input-username\">Alias</label>\r\n                  <input type=\"text\" [(ngModel)]=\"alias\" class=\"form-control form-control-alternative\">\r\n                </div>\r\n              </div>\r\n              <div class=\"col-lg-6\">\r\n                <div class=\"form-group\">\r\n                  <label class=\"form-control-label\" for=\"input-email\">Direccion</label>\r\n                  <input type=\"text\" [(ngModel)]=\"direccion\" class=\"form-control form-control-alternative\">\r\n                </div>\r\n              </div>\r\n            </div>\r\n            <div class=\"row\">\r\n              <div class=\"col-lg-6\">\r\n                <div class=\"form-group\">\r\n                  <label class=\"form-control-label\" for=\"input-first-name\">Departamento</label>\r\n                  <input type=\"text\" [(ngModel)]=\"departamento\" class=\"form-control form-control-alternative\">\r\n                </div>\r\n              </div>\r\n              <div class=\"col-lg-6\">\r\n                <div class=\"form-group\">\r\n                  <label class=\"form-control-label\" for=\"input-last-name\">Municipio</label>\r\n                  <input type=\"text\" [(ngModel)]=\"municipio\" class=\"form-control form-control-alternative\">\r\n                </div>\r\n              </div>\r\n            </div>\r\n\r\n            <div class=\"row\">\r\n              <div class=\"col-lg-6\">\r\n                <div class=\"form-group\">\r\n                  <label class=\"form-control-label\" for=\"input-first-name\">Encargado</label>\r\n                  <input type=\"number\" [(ngModel)]=\"encargado\" class=\"form-control form-control-alternative\">\r\n                </div>\r\n              </div>\r\n\r\n            </div>\r\n          </div>\r\n\r\n\r\n\r\n\r\n        </div>\r\n      </div>\r\n    </div>\r\n  </div>\r\n</div>");

/***/ }),

/***/ "jJig":
/*!****************************************************************!*\
  !*** ./src/app/pages/admin/form-sedes/form-sedes.component.ts ***!
  \****************************************************************/
/*! exports provided: ROUTES, FormSedesComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "ROUTES", function() { return ROUTES; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "FormSedesComponent", function() { return FormSedesComponent; });
/* harmony import */ var _raw_loader_form_sedes_component_html__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! raw-loader!./form-sedes.component.html */ "isNQ");
/* harmony import */ var _form_sedes_component_css__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./form-sedes.component.css */ "WEv8");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/core */ "fXoL");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/router */ "tyNb");
/* harmony import */ var _peticiones_service__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ../../../peticiones.service */ "kW39");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};





var ROUTES = [
    { path: '/view-sedes', title: 'VIEW', icon: 'ni-tv-2 text-primary', class: '' },
    { path: '/form-sedes', title: 'ADD', icon: 'ni-tv-2 text-primary', class: '' },
];
var FormSedesComponent = /** @class */ (function () {
    function FormSedesComponent(router, app) {
        this.router = router;
        this.app = app;
        this.isCollapsed = true;
    }
    FormSedesComponent.prototype.ngOnInit = function () {
        var _this = this;
        this.menuItems = ROUTES.filter(function (menuItem) { return menuItem; });
        this.router.events.subscribe(function (event) {
            _this.isCollapsed = true;
        });
    };
    FormSedesComponent.prototype.registrar = function () {
        var _this = this;
        this.app.postCrearSede({ "alias": this.alias, "direccion": this.direccion, "departamento": this.departamento,
            "municipio": this.municipio, "usuario": this.encargado }).subscribe(function (resp) {
            alert(resp.msj);
            _this.alias = _this.direccion = _this.departamento = _this.municipio = "";
            _this.encargado = 0;
        });
    };
    FormSedesComponent.ctorParameters = function () { return [
        { type: _angular_router__WEBPACK_IMPORTED_MODULE_3__["Router"] },
        { type: _peticiones_service__WEBPACK_IMPORTED_MODULE_4__["PeticionesService"] }
    ]; };
    FormSedesComponent = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_2__["Component"])({
            selector: 'app-form-sedes',
            template: _raw_loader_form_sedes_component_html__WEBPACK_IMPORTED_MODULE_0__["default"],
            styles: [_form_sedes_component_css__WEBPACK_IMPORTED_MODULE_1__["default"]]
        }),
        __metadata("design:paramtypes", [_angular_router__WEBPACK_IMPORTED_MODULE_3__["Router"], _peticiones_service__WEBPACK_IMPORTED_MODULE_4__["PeticionesService"]])
    ], FormSedesComponent);
    return FormSedesComponent;
}());



/***/ }),

/***/ "m3dm":
/*!********************************************************************************************************!*\
  !*** ./node_modules/raw-loader/dist/cjs.js!./src/app/pages/admin/form-users/form-users.component.html ***!
  \********************************************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = ("<div class=\"header pb-8 pt-5 pt-lg-8 d-flex align-items-center\"\r\n  style=\"min-height: 200px; background-image: url(https://geoinnova.org/cursos/wp-content/uploads/2016/10/1-11-1024x551.jpg); background-size: cover; background-position: center top;\">\r\n  <!-- Mask -->\r\n  <span class=\"mask bg-gradient-danger opacity-8\"></span>\r\n\r\n</div>\r\n<div class=\"container-fluid mt--7\">\r\n  <div class=\"row\">\r\n    <div class=\"col-xl-4 order-xl-2 mb-5 mb-xl-0\">\r\n      <div class=\"card card-profile shadow\">\r\n        <div class=\"row justify-content-center\">\r\n          <div class=\"col-lg-3 order-lg-2\">\r\n            <div class=\"card-profile-image\">\r\n              <a href=\"javascript:void(0)\">\r\n                <img src=\"https://logodix.com/logo/1707088.png\" class=\"rounded-circle\">\r\n              </a>\r\n            </div>\r\n          </div>\r\n        </div>\r\n        <div class=\"card-header text-center border-0 pt-8 pt-md-4 pb-0 pb-md-4\">\r\n          <div class=\"d-flex justify-content-between\">\r\n\r\n          </div>\r\n        </div>\r\n        <div class=\"card-body pt-0 pt-md-4\">\r\n          <div class=\"row\">\r\n            <div class=\"col\">\r\n              <div class=\"card-profile-stats d-flex justify-content-center mt-md-5\">\r\n\r\n              </div>\r\n            </div>\r\n          </div>\r\n          <div class=\"text-center\">\r\n\r\n          </div>\r\n        </div>\r\n      </div>\r\n    </div>\r\n    <div class=\"col-xl-8 order-xl-1\">\r\n      <div class=\"card bg-secondary shadow\">\r\n        <div class=\"card-header bg-white border-0\">\r\n          <div class=\"row align-items-center\">\r\n            <div class=\"col-8\">\r\n              <h3 class=\"mb-0\">REGISTER USERS</h3>\r\n            </div>\r\n            <div class=\"col-4 text-right\">\r\n              <a (click)=\"save()\" class=\"btn btn-sm btn-primary\">save</a>\r\n            </div>\r\n          </div>\r\n        </div>\r\n        <div class=\"card-body\">\r\n\r\n          <h6 class=\"heading-small text-muted mb-4\">User information</h6>\r\n          <div class=\"pl-lg-4\">\r\n            <div class=\"row\">\r\n              <div class=\"col-lg-6\">\r\n                <div class=\"form-group\">\r\n                  <label class=\"form-control-label\" for=\"input-username\">Username</label>\r\n                  <input type=\"text\" id=\"input-username\" [(ngModel)]=\"name\"\r\n                    class=\"form-control form-control-alternative\" value=\"{{name}}\">\r\n                </div>\r\n              </div>\r\n              <div class=\"col-lg-6\">\r\n                <div class=\"form-group\">\r\n                  <label class=\"form-control-label\" for=\"input-email\">Email address</label>\r\n                  <input type=\"text\" [(ngModel)]=\"email\" class=\"form-control form-control-alternative\"\r\n                    value=\"{{email}}\">\r\n                </div>\r\n              </div>\r\n            </div>\r\n            <div class=\"row\">\r\n              <div class=\"col-lg-6\">\r\n                <div class=\"form-group\">\r\n                  <label class=\"form-control-label\" for=\"input-firste-name\">Date of Birth</label>\r\n                  <input type=\"text\" id=\"input-first-najme\" [(ngModel)]=\"fechaN\"\r\n                    class=\"form-control form-control-alternative\" value=\"{{fechaN}}\">\r\n                </div>\r\n              </div>\r\n              <div class=\"col-lg-6\">\r\n                <div class=\"form-group\">\r\n                  <label class=\"form-control-label\" for=\"input-last-name\">DPI</label>\r\n                  <input type=\"text\" id=\"input-last-namfe\" class=\"form-control form-control-alternative\"\r\n                    [(ngModel)]=\"dpi\">\r\n                </div>\r\n              </div>\r\n            </div>\r\n\r\n            <div class=\"row\">\r\n              <div class=\"col-lg-6\">\r\n                <div class=\"form-group\">\r\n                  <label class=\"form-control-label\" for=\"input-first-name\">Password</label>\r\n                  <input type=\"text\" id=\"input-first-name\" [(ngModel)]=\"password\"\r\n                    class=\"form-control form-control-alternative\" value=\"{{password}}\">\r\n                </div>\r\n              </div>\r\n              <div class=\"col-lg-6\">\r\n                <div class=\"form-group\">\r\n                  <label class=\"form-control-label\" for=\"input-last-namwe\">rol</label>\r\n                  <select [(ngModel)]=\"opcionSeleccionado\" class=\"form-control form-control-alternative\">\r\n                    <option *ngFor=\"let rol of roles\">\r\n                      {{rol}}\r\n                    </option>\r\n                  </select>\r\n                </div>\r\n              </div>\r\n\r\n            </div>\r\n\r\n\r\n          </div>\r\n\r\n\r\n\r\n\r\n\r\n        </div>\r\n      </div>\r\n    </div>\r\n  </div>\r\n</div>");

/***/ }),

/***/ "oB2U":
/*!**************************************************!*\
  !*** ./src/app/pages/admin/rol/rol.component.ts ***!
  \**************************************************/
/*! exports provided: RolComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "RolComponent", function() { return RolComponent; });
/* harmony import */ var _raw_loader_rol_component_html__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! raw-loader!./rol.component.html */ "1Mxm");
/* harmony import */ var _rol_component_css__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./rol.component.css */ "95zn");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/core */ "fXoL");
/* harmony import */ var _peticiones_service__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ../../../peticiones.service */ "kW39");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @angular/router */ "tyNb");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};





var RolComponent = /** @class */ (function () {
    function RolComponent(router, app) {
        this.router = router;
        this.app = app;
        this.roles = ['Encargado', 'Bodeguero', 'Vendedor', 'Repartidor'];
    }
    RolComponent.prototype.ngOnInit = function () {
    };
    RolComponent.prototype.save = function () {
        var _this = this;
        if (this.opcionSeleccionado == 'Agregar') {
            this.app.postAsignarRol({ "rol": this.opcionSeleccionadoRol, "idUsuario": this.idU }).subscribe(function (resp) {
                alert(resp.msj);
                _this.idU = null;
            });
        }
        else if (this.opcionSeleccionado == 'Quitar') {
            this.app.postQuitarRol({ "rol": this.opcionSeleccionadoRol, "idUsuario": this.idU }).subscribe(function (resp) {
                alert(resp.msj);
                _this.idU = null;
            });
        }
    };
    RolComponent.ctorParameters = function () { return [
        { type: _angular_router__WEBPACK_IMPORTED_MODULE_4__["Router"] },
        { type: _peticiones_service__WEBPACK_IMPORTED_MODULE_3__["PeticionesService"] }
    ]; };
    RolComponent = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_2__["Component"])({
            selector: 'app-rol',
            template: _raw_loader_rol_component_html__WEBPACK_IMPORTED_MODULE_0__["default"],
            styles: [_rol_component_css__WEBPACK_IMPORTED_MODULE_1__["default"]]
        }),
        __metadata("design:paramtypes", [_angular_router__WEBPACK_IMPORTED_MODULE_4__["Router"], _peticiones_service__WEBPACK_IMPORTED_MODULE_3__["PeticionesService"]])
    ], RolComponent);
    return RolComponent;
}());



/***/ }),

/***/ "qr1/":
/*!************************************************!*\
  !*** ./src/app/layouts/admin/admin.routing.ts ***!
  \************************************************/
/*! exports provided: AdminRoutes */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "AdminRoutes", function() { return AdminRoutes; });
/* harmony import */ var _pages_admin_form_users_form_users_component__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ../../pages/admin/form-users/form-users.component */ "23Hi");
/* harmony import */ var _pages_admin_form_sedes_form_sedes_component__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ../../pages/admin/form-sedes/form-sedes.component */ "jJig");
/* harmony import */ var _pages_admin_view_sedes_view_sedes_component__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../../pages/admin/view-sedes/view-sedes.component */ "UK8G");
/* harmony import */ var _pages_admin_rol_rol_component__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ../../pages/admin/rol/rol.component */ "oB2U");




var AdminRoutes = [
    { path: 'form-users', component: _pages_admin_form_users_form_users_component__WEBPACK_IMPORTED_MODULE_0__["FormUsersComponent"] },
    { path: 'form-sedes', component: _pages_admin_form_sedes_form_sedes_component__WEBPACK_IMPORTED_MODULE_1__["FormSedesComponent"] },
    { path: 'view-sedes', component: _pages_admin_view_sedes_view_sedes_component__WEBPACK_IMPORTED_MODULE_2__["ViewSedesComponent"] },
    { path: 'rol', component: _pages_admin_rol_rol_component__WEBPACK_IMPORTED_MODULE_3__["RolComponent"] },
];


/***/ })

}]);
//# sourceMappingURL=layouts-admin-admin-module.js.map