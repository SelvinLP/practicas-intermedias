import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';

declare interface RouteInfo {
    path: string;
    title: string;
    icon: string;
    class: string;
}
export const ROUTES: RouteInfo[] = [

    { path: '/profile-bodeguero', title: 'User profile',  icon:'ni-single-02 text-yellow', class: '' },
    { path: '/actualizacion-bodeguero', title: 'Actualizacion Inventario',  icon:'ni-single-02 text-yellow', class: '' }, 
    { path: '/transferencia-bodeguero', title: 'Transferencias',  icon:'ni-single-02 text-yellow', class: '' }, 
    { path: '/visualizar-interna-bodeguero', title: 'Visualizar',  icon:'ni-single-02 text-yellow', class: '' },
];
@Component({
  selector: 'app-sidebar-bodeguero',
  templateUrl: './sidebar-bodeguero.component.html',
  styleUrls: ['./sidebar-bodeguero.component.css']
})
export class SidebarBodegueroComponent implements OnInit {

  public menuItems: any[];
  public isCollapsed = true;

  constructor(private router: Router) { }

  ngOnInit() {
    this.menuItems = ROUTES.filter(menuItem => menuItem);
    this.router.events.subscribe((event) => {
      this.isCollapsed = true;
   });
  }

}
