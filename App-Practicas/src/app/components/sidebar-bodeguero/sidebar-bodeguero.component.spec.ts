import { ComponentFixture, TestBed } from '@angular/core/testing';

import { SidebarBodegueroComponent } from './sidebar-bodeguero.component';

describe('SidebarBodegueroComponent', () => {
  let component: SidebarBodegueroComponent;
  let fixture: ComponentFixture<SidebarBodegueroComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ SidebarBodegueroComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(SidebarBodegueroComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
