import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';

declare interface RouteInfo {
    path: string;
    title: string;
    icon: string;
    class: string;
}
export const ROUTES: RouteInfo[] = [

    { path: '/profile-vendedor', title: 'User profile',  icon:'ni-single-02 text-yellow', class: '' },
    { path: '/form-clients', title: 'Register client', icon:'ni-circle-08 text-yellow', class: '' },
    { path: '/form-sales', title: 'Register sale', icon:'ni-books text-yellow', class: '' },
    { path: '/sales-report', title: 'Sales report', icon:'ni-money-coins text-yellow', class: '' },

];
@Component({
  selector: 'app-sidebar-vendedor',
  templateUrl: './sidebar-vendedor.component.html',
  styleUrls: ['./sidebar-vendedor.component.css']
})
export class SidebarVendedorComponent implements OnInit {


  public menuItems: any[];
  public isCollapsed = true;

  constructor(private router: Router) { }

  ngOnInit() {
    this.menuItems = ROUTES.filter(menuItem => menuItem);
    this.router.events.subscribe((event) => {
      this.isCollapsed = true;
   });
  }

}
