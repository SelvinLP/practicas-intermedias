import { ComponentFixture, TestBed } from '@angular/core/testing';

import { SidebarVendedorComponent } from './sidebar-vendedor.component';

describe('SidebarVendedorComponent', () => {
  let component: SidebarVendedorComponent;
  let fixture: ComponentFixture<SidebarVendedorComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ SidebarVendedorComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(SidebarVendedorComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
