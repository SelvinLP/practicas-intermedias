import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';

declare interface RouteInfo {
    path: string;
    title: string;
    icon: string;
    class: string;
}
export const ROUTES: RouteInfo[] = [
  
    { path: '/profile-repartidor', title: 'User profile',  icon:'ni-single-02 text-yellow', class: '' },
    { path: '/repartidor-ventas', title: 'ORDENES',  icon:'ni-single-02 text-yellow', class: '' },
    
      
];
@Component({
  selector: 'app-sidebar-repartidor',
  templateUrl: './sidebar-repartidor.component.html',
  styleUrls: ['./sidebar-repartidor.component.css']
})
export class SidebarRepartidorComponent implements OnInit {

  
  
  public menuItems: any[];
  public isCollapsed = true;

  constructor(private router: Router) { }

  ngOnInit() {
    this.menuItems = ROUTES.filter(menuItem => menuItem);
    this.router.events.subscribe((event) => {
      this.isCollapsed = true;
   });
  }

}
