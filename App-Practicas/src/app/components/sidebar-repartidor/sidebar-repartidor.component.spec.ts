import { ComponentFixture, TestBed } from '@angular/core/testing';

import { SidebarRepartidorComponent } from './sidebar-repartidor.component';

describe('SidebarRepartidorComponent', () => {
  let component: SidebarRepartidorComponent;
  let fixture: ComponentFixture<SidebarRepartidorComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ SidebarRepartidorComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(SidebarRepartidorComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
