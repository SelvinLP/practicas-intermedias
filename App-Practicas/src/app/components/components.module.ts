import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { SidebarComponent } from './sidebar/sidebar.component';
import { NavbarComponent } from './navbar/navbar.component';
import { FooterComponent } from './footer/footer.component';
import { RouterModule } from '@angular/router';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { SidebarAdminComponent } from './sidebar-admin/sidebar-admin.component';
import { SidebarBodegueroComponent } from './sidebar-bodeguero/sidebar-bodeguero.component';
import { NavbarBodegueroComponent } from './navbar-bodeguero/navbar-bodeguero.component';
import { SidebarVendedorComponent } from './sidebar-vendedor/sidebar-vendedor.component';
import { SidebarRepartidorComponent } from './sidebar-repartidor/sidebar-repartidor.component';


@NgModule({
  imports: [
    CommonModule,
    RouterModule,
    NgbModule
  ],
  declarations: [
    FooterComponent,
    NavbarComponent,
    SidebarComponent,
    SidebarAdminComponent,
    SidebarBodegueroComponent,
    NavbarBodegueroComponent,
    SidebarVendedorComponent,
    SidebarRepartidorComponent
  ],
  exports: [
    FooterComponent,
    NavbarComponent,
    SidebarComponent,
    SidebarAdminComponent,
    SidebarBodegueroComponent,
    NavbarBodegueroComponent,
    SidebarVendedorComponent,
    SidebarRepartidorComponent
  ]
})
export class ComponentsModule { }
