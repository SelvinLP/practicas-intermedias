import { ComponentFixture, TestBed } from '@angular/core/testing';

import { NavbarBodegueroComponent } from './navbar-bodeguero.component';

describe('NavbarBodegueroComponent', () => {
  let component: NavbarBodegueroComponent;
  let fixture: ComponentFixture<NavbarBodegueroComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ NavbarBodegueroComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(NavbarBodegueroComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
