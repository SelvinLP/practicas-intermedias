import { NgModule } from '@angular/core';
import { CommonModule, } from '@angular/common';
import { BrowserModule  } from '@angular/platform-browser';
import { Routes, RouterModule } from '@angular/router';

import { AdminLayoutComponent } from './layouts/admin-layout/admin-layout.component';
import { AuthLayoutComponent } from './layouts/auth-layout/auth-layout.component';
import { AdminComponent } from './layouts/admin/admin.component';
import { BodegueroComponent } from './layouts/bodeguero/bodeguero.component';
import { VendedorComponent } from './layouts/vendedor/vendedor.component';
import { RepartidorComponent } from './layouts/repartidor/repartidor.component';
const routes: Routes =[
  {
    path: '',
    redirectTo: 'login',
    pathMatch: 'full',
  }, {
    path: '',
    component: AdminLayoutComponent,
    children: [
      {
        path: '',
        loadChildren: './layouts/admin-layout/admin-layout.module#AdminLayoutModule'
      }
    ]
  },
  {
    path: '',
    component: AdminComponent,
    children: [
      {
        path: '',
        loadChildren: './layouts/admin/admin.module#AdminModule'
      }
    ]
  }, 
  {
    path: '',
    component: BodegueroComponent ,
    children: [
      {
        path: '',
        loadChildren: './layouts/bodeguero/bodeguero.module#BodegueroModule'
      }
    ]
  },
  {
    path: '',
    component: VendedorComponent ,
    children: [
      {
        path: '',
        loadChildren: './layouts/vendedor/vendedor.module#VendedorModule'
      }
    ]
  },

  {
    path: '',
    component: RepartidorComponent ,
    children: [
      {
        path: '',
        loadChildren: './layouts/repartidor/repartidor.module#RepartidorModule'
      }
    ]
  },
  
  
  {
    path: '',
    component: AuthLayoutComponent,
    children: [
      {
        path: '',
        loadChildren: './layouts/auth-layout/auth-layout.module#AuthLayoutModule'
      }
    ]
  }, {
    path: '**',
    redirectTo: 'login'
  }
];

@NgModule({
  imports: [
    CommonModule,
    BrowserModule,
    RouterModule.forRoot(routes,{
    useHash: true,
    relativeLinkResolution: 'legacy'
})
  ],
  exports: [
  ],
})
export class AppRoutingModule { }
