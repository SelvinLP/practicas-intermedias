import { Component, OnInit, OnDestroy } from "@angular/core";
import { PeticionesService } from "../../peticiones.service";
import { Router } from "@angular/router";
import { resetFakeAsyncZone } from "@angular/core/testing";
@Component({
  selector: "app-login",
  templateUrl: "./login.component.html",
  styleUrls: ["./login.component.scss"],
})
export class LoginComponent implements OnInit, OnDestroy {
  constructor(private router: Router, private app: PeticionesService) { }

  ngOnInit() { }
  ngOnDestroy() { }
  roles = ["Encargado", "Bodeguero", "Vendedor", "Repartidor"];
  email: string;
  password: string;

  opcionSeleccionado: string;

  recuperar() {
    this.router.navigate(["/register"]);
  }

  btnLogin() {
    if (this.email == "admin") {
      if (this.password == "admin") {
        sessionStorage.setItem("log", '{"email":"admin"}');
        this.router.navigate(["/form-users"]);
      }
    } else {
      this.app.postLogin({ "email": this.email, "password": +this.password }).subscribe(
        (resp: any) => {

          if (resp.status == 300) {
            let array = []
            for (let index = 0; index < resp.rol.length; index++) {
              array[index] = resp.rol[index].descripcion

            }
            if (array.includes(this.opcionSeleccionado)) {
              if (this.opcionSeleccionado == "Encargado") {
                let nuevo: any
                nuevo = { "status": resp.status, "nombre": resp.nombre, "dpi": resp.dpi, "id": resp.id, "correo": resp.correo, "fechanacimiento": resp.fechanacimiento, "rol": resp.rol, "password": this.password }
                sessionStorage.setItem('log', JSON.stringify(nuevo));
                this.router.navigate(['/user-profile']);

              } else if (this.opcionSeleccionado == "Bodeguero") {
                let nuevo: any
                nuevo = { "status": resp.status, "nombre": resp.nombre, "dpi": resp.dpi, "id": resp.id, "correo": resp.correo, "fechanacimiento": resp.fechanacimiento, "rol": resp.rol, "password": this.password }
                sessionStorage.setItem('log', JSON.stringify(nuevo));
                this.router.navigate(['/profile-bodeguero']);

              } else if (this.opcionSeleccionado == "Vendedor") {
                let nuevo: any
                nuevo = { "status": resp.status, "nombre": resp.nombre, "dpi": resp.dpi, "id": resp.id, "correo": resp.correo, "fechanacimiento": resp.fechanacimiento, "rol": resp.rol, "password": this.password }
                sessionStorage.setItem('log', JSON.stringify(nuevo));
                this.router.navigate(['/profile-vendedor']);

              } else if (this.opcionSeleccionado == "Repartidor") {

                let nuevo: any
                nuevo = { "status": resp.status, "nombre": resp.nombre, "dpi": resp.dpi, "id": resp.id, "correo": resp.correo, "fechanacimiento": resp.fechanacimiento, "rol": resp.rol, "password": this.password }
                sessionStorage.setItem('log', JSON.stringify(nuevo));
                this.router.navigate(['/profile-repartidor']);

              }


            } else {
              alert("El usuario no tiene el rol.")
            }
          }else{
            alert("Contraseña o correo incorrecto.")
          }
        })
    }
  }
}
