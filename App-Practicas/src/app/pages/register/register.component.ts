import { Component, OnInit } from '@angular/core';
import { PeticionesService } from '../../peticiones.service';
import { Router } from '@angular/router';
@Component({
  selector: 'app-register',
  templateUrl: './register.component.html',
  styleUrls: ['./register.component.scss']
})
export class RegisterComponent implements OnInit {

  constructor(private router: Router,private app:PeticionesService) { }

  ngOnInit() {
  }
  email:string;
send(){
  this.app.postRecuperar({"email":this.email}).subscribe( 
    (resp: any) =>{
      if(resp.status==300){
        alert("Correo de recuperacion enviado.")
        this.router.navigate([ '/login']);
      }else if(resp.status==100){
        alert("Correo de recuperacion invalido.")
      }
    });
}
cancel(){
  this.router.navigate([ '/login']);
}
}
