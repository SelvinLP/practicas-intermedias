import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { PeticionesService } from '../../../peticiones.service';

@Component({
  selector: 'app-profile',
  templateUrl: './profile.component.html',
  styleUrls: ['./profile.component.css']
})
export class ProfileComponent implements OnInit {

 
  constructor(private router: Router,private app:PeticionesService) { 

    if(sessionStorage.length==0){
      this.router.navigate([ '/login']);
    }
  }
  name:string;
  email:string;
  password:string;
  dpi:number;
  fechaN:string;
  InfoUsuario:any = null;
  roles:any=null;
  ngOnInit() {
this.InfoUsuario= JSON.parse(sessionStorage.getItem('log'));
this.name = this.InfoUsuario.nombre;
this.email= this.InfoUsuario.correo;
this.password= this.InfoUsuario.password;
this.dpi = this.InfoUsuario.dpi;
this.fechaN= this.InfoUsuario.fechanacimiento;
this.roles=this.InfoUsuario.rol;
  }

  edit(){
    this.app.postModificar({"dpi":this.dpi,"nombre":this.name,"fechanacimiento":this.fechaN,"correo":this.email,"pass":this.password}).subscribe( 
      (resp: any) =>{
        if(resp.status==300){
        
          this.app.postLogin({"email":this.email,"password":this.password}).subscribe( 
            (resp1: any) =>{
           
            if(resp1.status==300){
            let nuevo:any
            nuevo = {"status":resp1.status,"nombre":resp1.nombre,"dpi":resp1.dpi,"correo":resp1.correo,"fechanacimiento":resp1.fechanacimiento,"rol":resp1.rol,"password":this.password}
            sessionStorage.setItem('log',JSON.stringify(nuevo));
            alert("Moodify User!")
            this.router.navigate([ '/login']);
            
            }
          });
         
        }else if(resp.status==100){
          alert("Error")
        }
      });
  }

}
