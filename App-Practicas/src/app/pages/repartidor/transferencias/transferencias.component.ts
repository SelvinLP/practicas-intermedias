import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { PeticionesService } from '../../../peticiones.service';
declare interface RouteInfo {
  path: string;
  title: string;
  icon: string;
  class: string;
}

export const ROUTES: RouteInfo[] = [
  { path: '/repartidor-ventas', title: 'Ventas',  icon: 'ni-tv-2 text-primary', class: '' },
  { path: '/repartidor-transferencias', title: 'Transferencias',  icon: 'ni-tv-2 text-primary', class: '' },
];


@Component({
  selector: 'app-transferencias',
  templateUrl: './transferencias.component.html',
  styleUrls: ['./transferencias.component.css']
})
export class TransferenciasComponent implements OnInit {

  cantidad:number
 
  public menuItems: any[];
  public isCollapsed = true;
  constructor(private router: Router,private app:PeticionesService) {
    let InfoUsuario= JSON.parse(sessionStorage.getItem('log'));
    
    this.app.getTrans(InfoUsuario.id).subscribe( 
      (resp: any) =>{
     
        this.solicitudes=resp.trans;
       
        
      }
     );


   
   }
  solicitudes  = []

  ngOnInit(): void {
    this.menuItems = ROUTES.filter(menuItem => menuItem);
    this.router.events.subscribe((event) => {
      this.isCollapsed = true;
   });

   
  }
  motivo:string;
  Actualizar(id:number){
   
this.app.actualizarTransferencia(id).subscribe(
(resp:any)=>{
alert(resp.msj)
});

  }
  

}
