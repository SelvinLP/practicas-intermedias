import { Component, OnInit } from "@angular/core";
import { PeticionesService } from "../../../peticiones.service";

@Component({
  selector: "app-form-sales",
  templateUrl: "./form-sales.component.html",
  styleUrls: ["./form-sales.component.css"],
})
export class FormSalesComponent implements OnInit {
  constructor(private app: PeticionesService) {}

  entrega: string = "";
  factura: string = "";

  total: number = 0;
  recargo: number = 0;

  direccion: string = "";
  domicilio: boolean = false;
  estado: string = "Entregado";

  usuario_actual: any;
  id_cliente: string = "";
  id_repartidor: string = "15";
  clientes:any = [];

  ngOnInit(): void {
    let today = new Date();
    let dd = String(today.getDate()).padStart(2, "0");
    let mm = String(today.getMonth() + 1).padStart(2, "0"); //January is 0!
    let yyyy = today.getFullYear();

    this.factura = dd + "/" + mm + "/" + yyyy;
    this.entrega = this.factura;

    this.usuario_actual = JSON.parse(sessionStorage.getItem("log"));

    this.app.getClientes().subscribe((res: any) => {
      this.clientes = res;
      console.log(res);
    });
  }

  save() {
    if (this.domicilio) {
      this.estado = "Pendiente";
      this.entrega = this.factura;
      this.recargo = 15;
      this.total += this.recargo;
    } else {
      this.direccion = "Local";
    }

    if (
      this.direccion != "" &&
      this.entrega != "" &&
      this.entrega != "" &&
      this.total != 0 &&
      this.estado != "" &&
      this.id_cliente != "" &&
      this.id_repartidor != ""
    ) {

      this.app.postVenta({
        "fechafacturacion" : this.factura,
        "fechaentrega": this.entrega,
        "id_usuario": this.usuario_actual.id,
        "id_cliente": this.id_cliente,
        "total": this.total,
        "estado": this.estado,
        "r_venta": this.recargo,
        "id_repartidor": this.id_repartidor,
        "ubicacion": this.direccion
      }).subscribe((resp:any) => {
        alert(resp.descripcion);
        this.total = 0;
        this.recargo = 0;

        this.direccion = "";
        this.domicilio = false;
        this.estado = "Entregado";
      });

    } else {
      alert("Debes llenar todos los campos");
    }
  }
}
