import { Component, OnInit } from "@angular/core";
import { PeticionesService } from "../../../peticiones.service";

@Component({
  selector: "app-sales-report",
  templateUrl: "./sales-report.component.html",
  styleUrls: ["./sales-report.component.css"],
})
export class SalesReportComponent implements OnInit {
  constructor(private app: PeticionesService) {}

  ventas: any = [];
  usuario_actual: any;

  ngOnInit(): void {
    this.usuario_actual = JSON.parse(sessionStorage.getItem("log"));

    this.app.getVentas({cliente: this.usuario_actual.id}).subscribe((res: any) => {
      this.ventas = res;
      console.log(this.ventas);
    });
  }
}
