import { Component, OnInit } from "@angular/core";
import { PeticionesService } from "../../../peticiones.service";

@Component({
  selector: "app-form-clients",
  templateUrl: "./form-clients.component.html",
  styleUrls: ["./form-clients.component.css"],
})
export class FormClientsComponent implements OnInit {
  constructor(private app: PeticionesService) {}

  sedes = [];
  nombre: string = "";
  nit: string = "";
  dpi: string = "";
  direccion: string = "";
  sede: any;
  usuario_actual: any;

  ngOnInit(): void {
    this.usuario_actual = JSON.parse(sessionStorage.getItem("log"));

    this.app.getSedesUsuario(this.usuario_actual.id).subscribe((res: any) => {
      this.sedes = res;
      console.log(this.sedes);
    });
  }

  save() {
    if (
      this.nombre != "" &&
      this.nit != "" &&
      this.dpi != "" &&
      this.direccion != "" &&
      this.sede != undefined
    ) {
      if (this.sedes.length > 0) {
        this.app
          .postCliente({
            nombre: this.nombre,
            nit: this.nit,
            dpi: this.dpi,
            direccion: this.direccion,
            sede: this.sede,
          })
          .subscribe((resp: any) => {
            console.log(resp);
            alert(resp.descripcion);
            this.nombre = "";
            this.nit = "";
            this.dpi = "";
            this.direccion = "";
            this.sede = "";
          });
      } else {
        alert("No tienes ninguna sede asignada");
      }
    } else {
      alert("Debes llenar todos los campos");
    }
  }
}
