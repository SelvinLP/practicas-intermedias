import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { PeticionesService } from '../../../peticiones.service';
declare interface RouteInfo {
  path: string;
  title: string;
  icon: string;
  class: string;
}

export const ROUTES: RouteInfo[] = [
  { path: '/actualizacion-bodeguero', title: 'Actualizar',  icon: 'ni-tv-2 text-primary', class: '' },
  
];

@Component({
  selector: 'app-actualizacion',
  templateUrl: './actualizacion.component.html',
  styleUrls: ['./actualizacion.component.css']
})
export class ActualizacionComponent implements OnInit {
cantidad:number
 
  public menuItems: any[];
  public isCollapsed = true;
  constructor(private router: Router,private app:PeticionesService) {
    let InfoUsuario= JSON.parse(sessionStorage.getItem('log'));
    this.app.getBodegasU(InfoUsuario.id).subscribe(

      (resp1:any)=>{
       
        this.app.getProductoInventario(resp1[0].id).subscribe( 
          (resp: any) =>{
          
            this.solicitudes=resp;
            console.log(resp1)
            
          }
         );
      }
    );



   
   }
  solicitudes  = []

  ngOnInit(): void {
    this.menuItems = ROUTES.filter(menuItem => menuItem);
    this.router.events.subscribe((event) => {
      this.isCollapsed = true;
   });

   
  }
  motivo:string;
  Actualizar(id:number,cant:number,motivo:string){
   
this.app.actualizarInventario({"inventario":id,"cantidad":cant}).subscribe(
(resp:any)=>{
alert(resp.descripcion)
});

  }
  Eliminar(id:number){
    
    this.app.deleteSede({"id":id}).subscribe(
      (resp:any)=>{
      alert(resp.msj)
      this.router.navigate([ '/view-sedes']);
      this.app.getSedes().subscribe( 
        (resp: any) =>{
          this.solicitudes=resp;
          
        }
       );
      });
  }
}
