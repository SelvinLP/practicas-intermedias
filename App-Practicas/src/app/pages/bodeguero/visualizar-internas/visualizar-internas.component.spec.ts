import { ComponentFixture, TestBed } from '@angular/core/testing';

import { VisualizarInternasComponent } from './visualizar-internas.component';

describe('VisualizarInternasComponent', () => {
  let component: VisualizarInternasComponent;
  let fixture: ComponentFixture<VisualizarInternasComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ VisualizarInternasComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(VisualizarInternasComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
