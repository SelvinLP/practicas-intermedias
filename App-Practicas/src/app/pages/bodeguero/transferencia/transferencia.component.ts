import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { PeticionesService } from '../../../peticiones.service';

@Component({
  selector: 'app-transferencia',
  templateUrl: './transferencia.component.html',
  styleUrls: ['./transferencia.component.css']
})
export class TransferenciaComponent implements OnInit {

  constructor(private router: Router, private app: PeticionesService) { 
    this.estado="Pendiente"

    let InfoUsuario= JSON.parse(sessionStorage.getItem('log'));
    this.app.getBodegasU(InfoUsuario.id).subscribe(

      (resp1:any)=>{

        this.bodega=resp1[0].id;
      })
  }

  tipos=['Interna','Externa']
  descripcion: string;
  estado: string;
  
  creador: any;
  bodega: number;
 tipoA:string
 bodega_dest:0
 
  opcionSeleccionado:string;
  ngOnInit(): void {
  }

  save() {
    this.app.postCrearSolicitud({ "descripcion": this.descripcion, "estado": this.estado, "creador": this.creador, "bodega": this.bodega,"bodega_dest":this.bodega_dest,"tipo":this.tipoA}).subscribe(
      (resp: any) => {
        alert(resp.descripcion);

        let InfoUsuario= JSON.parse(sessionStorage.getItem('log'));
console.log(InfoUsuario.correo);
        this.app.sendEmail({ "descripcion": "Transferencia Solicitada", "email":InfoUsuario.correo}).subscribe(
          (resp1: any) => {
       
          });
        this.descripcion=""
        this.creador="";
        this.bodega_dest=null
      });
  }
}
