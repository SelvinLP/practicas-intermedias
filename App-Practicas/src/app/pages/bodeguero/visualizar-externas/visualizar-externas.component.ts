import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { PeticionesService } from '../../../peticiones.service';
declare interface RouteInfo {
  path: string;
  title: string;
  icon: string;
  class: string;
}

export const ROUTES: RouteInfo[] = [
  { path: '/visualizar-interna-bodeguero', title: 'Interna',  icon: 'ni-tv-2 text-primary', class: '' },
  { path: '/visualizar-externa-bodeguero', title: 'Externa',  icon: 'ni-tv-2 text-primary', class: '' },
];



@Component({
  selector: 'app-visualizar-externas',
  templateUrl: './visualizar-externas.component.html',
  styleUrls: ['./visualizar-externas.component.css']
})
export class VisualizarExternasComponent implements OnInit {
  cantidad:number
 
  public menuItems: any[];
  public isCollapsed = true;
  constructor(private router: Router,private app:PeticionesService) {
    let InfoUsuario= JSON.parse(sessionStorage.getItem('log'));
    this.app.getBodegasU(InfoUsuario.id).subscribe(

      (resp1:any)=>{
       
        this.app.getTansferenciasExterna(resp1[0].id).subscribe( 
          (resp: any) =>{
        
            this.solicitudes=resp;
           
            
          }
         );
      }
    );



   
   }
  solicitudes  = []

  ngOnInit(): void {
    this.menuItems = ROUTES.filter(menuItem => menuItem);
    this.router.events.subscribe((event) => {
      this.isCollapsed = true;
   });

   
  }
  motivo:string;
  Actualizar(id:number){
   
this.app.actualizarTransferencia(id).subscribe(
(resp:any)=>{
alert(resp.msj)
});

  }
  
}
