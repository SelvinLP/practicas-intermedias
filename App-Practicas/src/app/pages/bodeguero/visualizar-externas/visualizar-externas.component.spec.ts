import { ComponentFixture, TestBed } from '@angular/core/testing';

import { VisualizarExternasComponent } from './visualizar-externas.component';

describe('VisualizarExternasComponent', () => {
  let component: VisualizarExternasComponent;
  let fixture: ComponentFixture<VisualizarExternasComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ VisualizarExternasComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(VisualizarExternasComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
