import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { PeticionesService } from '../../../peticiones.service';
declare interface RouteInfo {
  path: string;
  title: string;
  icon: string;
  class: string;
}

export const ROUTES: RouteInfo[] = [
  { path: '/view-bodegas', title: 'VIEW', icon: 'ni-tv-2 text-primary', class: '' },
  { path: '/bodegas', title: 'ADD', icon: 'ni-tv-2 text-primary', class: '' },


];
@Component({
  selector: 'app-bodegas',
  templateUrl: './bodegas.component.html',
  styleUrls: ['./bodegas.component.css']
})
export class BodegasComponent implements OnInit {


  public menuItems: any[];
  public isCollapsed = true;
  constructor(private router: Router, private app: PeticionesService) { }

  ngOnInit(): void {
    this.menuItems = ROUTES.filter(menuItem => menuItem);
    this.router.events.subscribe((event) => {
      this.isCollapsed = true;
    });
  }

  nombre: string
  descripcion: string
  estado: string
  id_sede: number
  id_encargado: number
  registrar() {

    let InfoUsuario= JSON.parse(sessionStorage.getItem('log'));


 this.app.getSedesU(InfoUsuario.id).subscribe(

(resp1:any)=>{
  this.app.postCrearBodega({
    "nombre": this.nombre, "descripcion": this.descripcion, "estado": this.estado
    , "sede": resp1[0].id, "usuario": this.id_encargado
  }).subscribe(
    (resp: any) => {

      alert(resp.msj)
      this.nombre = this.descripcion = this.estado="";
      this.id_sede = this.id_encargado=0;

    }
  );



}

 );
   
  }

}
