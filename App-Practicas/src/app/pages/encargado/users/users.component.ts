import { Component, OnInit } from '@angular/core';
import { PeticionesService } from '../../../peticiones.service';
import { Router } from '@angular/router';
@Component({
  selector: 'app-users',
  templateUrl: './users.component.html',
  styleUrls: ['./users.component.css']
})
export class UsersComponent implements OnInit {
  constructor(private router: Router, private app: PeticionesService) { }
  name: string;
  email: string;
  password: string;
  dpi: number;
  fechaN: string;
  roles=['Bodeguero','Vendedor','Repartidor']
  opcionSeleccionado:string;
  ngOnInit(): void {
  }

  save() {
    this.app.postCrearUsuario({ "dpi": this.dpi, "nombre": this.name, "fechanac": this.fechaN, "correo": this.email, "password": this.password,"rol":this.opcionSeleccionado}).subscribe(
      (resp: any) => {
        alert(resp.msj);
        this.dpi = 0
        this.name = this.fechaN = this.email = this.password = ""
      });
  }
}
