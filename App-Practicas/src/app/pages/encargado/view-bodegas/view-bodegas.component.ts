import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { PeticionesService } from '../../../peticiones.service';
declare interface RouteInfo {
  path: string;
  title: string;
  icon: string;
  class: string;
}

export const ROUTES: RouteInfo[] = [
  { path: '/view-bodegas', title: 'VIEW',  icon: 'ni-tv-2 text-primary', class: '' },
  { path: '/bodegas', title: 'ADD',  icon: 'ni-tv-2 text-primary', class: '' },
  
  
];

@Component({
  selector: 'app-view-bodegas',
  templateUrl: './view-bodegas.component.html',
  styleUrls: ['./view-bodegas.component.css']
})
export class ViewBodegasComponent implements OnInit {

  public menuItems: any[];
  public isCollapsed = true;
  constructor(private router: Router,private app:PeticionesService) {
    let InfoUsuario= JSON.parse(sessionStorage.getItem('log'));
    this.app.getSedesU(InfoUsuario.id).subscribe(

      (resp1:any)=>{
       
        this.app.getBodegasSede(resp1[0].id).subscribe( 
          (resp: any) =>{
          
            this.solicitudes=resp;
            
          }
         );
      }
    );



   
   }
  solicitudes  = []

  ngOnInit(): void {
    this.menuItems = ROUTES.filter(menuItem => menuItem);
    this.router.events.subscribe((event) => {
      this.isCollapsed = true;
   });

   
  }
 
 

  Actualizar(id: any, estado:string){

    if(estado=="Desactivada"){

 this.app.activarBodega({"idBodega":id,"estado":"Activa"}).subscribe( 
        (resp: any) =>{
         alert(resp.msj)          
        }
       );
    }else{
      //se Desactiva
      this.app.activarBodega({"idBodega":id,"estado":"Desactivada"}).subscribe( 
        (resp: any) =>{
         alert(resp.msj)          
        }
       );
    }
    let InfoUsuario= JSON.parse(sessionStorage.getItem('log'));
    this.app.getSedesU(InfoUsuario.id).subscribe(

      (resp1:any)=>{
       
        this.app.getBodegasSede(resp1[0].id).subscribe( 
          (resp: any) =>{
          
            this.solicitudes=resp;
            
          }
         );
      }
    );

  }

}
