import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ViewBodegasComponent } from './view-bodegas.component';

describe('ViewBodegasComponent', () => {
  let component: ViewBodegasComponent;
  let fixture: ComponentFixture<ViewBodegasComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ ViewBodegasComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(ViewBodegasComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
