import { Component, OnInit } from '@angular/core';
import { PeticionesService } from '../../../peticiones.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-rol',
  templateUrl: './rol.component.html',
  styleUrls: ['./rol.component.css']
})
export class RolComponent implements OnInit {

  
  constructor(private router: Router, private app: PeticionesService) { }
  
  idU: number;
  fechaN: string;
  roles=['Encargado','Bodeguero','Vendedor','Repartidor']
  opcionSeleccionado:string;
  opcionSeleccionadoRol:string;
  ngOnInit(): void {
  }

  save() {
    if(this.opcionSeleccionado=='Agregar'){
      this.app.postAsignarRol({"rol":this.opcionSeleccionadoRol,"idUsuario":this.idU}).subscribe(
        (resp: any) => {
          alert(resp.msj);
          this.idU = null      
        });
    }else if(this.opcionSeleccionado=='Quitar'){
      this.app.postQuitarRol({"rol":this.opcionSeleccionadoRol,"idUsuario":this.idU}).subscribe(
        (resp: any) => {
          alert(resp.msj);
          this.idU = null      
        });
    }
   
  }

}
