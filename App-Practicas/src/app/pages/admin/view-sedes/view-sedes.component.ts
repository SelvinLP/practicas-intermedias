import { Component, OnInit } from "@angular/core";
import { Router } from "@angular/router";
import { PeticionesService } from "../../../peticiones.service";
declare interface RouteInfo {
  path: string;
  title: string;
  icon: string;
  class: string;
}

export const ROUTES: RouteInfo[] = [
  {
    path: "/view-sedes",
    title: "VIEW",
    icon: "ni-tv-2 text-primary",
    class: "",
  },
  {
    path: "/form-sedes",
    title: "ADD",
    icon: "ni-tv-2 text-primary",
    class: "",
  },
];

@Component({
  selector: "app-view-sedes",
  templateUrl: "./view-sedes.component.html",
  styleUrls: ["./view-sedes.component.css"],
})
export class ViewSedesComponent implements OnInit {
  public menuItems: any[];
  public isCollapsed = true;
  constructor(private router: Router, private app: PeticionesService) {
    this.app.getSedes().subscribe((resp: any) => {
      this.solicitudes = resp;
    });
  }
  solicitudes = [];

  ngOnInit(): void {
    this.menuItems = ROUTES.filter((menuItem) => menuItem);
    this.router.events.subscribe((event) => {
      this.isCollapsed = true;
    });
  }
  Actualizar(
    id: number,
    alias: string,
    direc: string,
    dep: string,
    mun: string,
    enc: string
  ) {
    this.app
      .putModificarSede({
        alias: alias,
        direccion: direc,
        departamento: dep,
        municipio: mun,
        id: id,
      })
      .subscribe((resp: any) => {
        alert(resp.msj);
      });
  }
  Eliminar(id: number) {
    this.app.deleteSede({ id: id }).subscribe((resp: any) => {
      alert(resp.msj);
      this.router.navigate(["/view-sedes"]);
      this.app.getSedes().subscribe((resp: any) => {
        this.solicitudes = resp;
      });
    });
  }
}
