import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { PeticionesService } from '../../../peticiones.service';
declare interface RouteInfo {
  path: string;
  title: string;
  icon: string;
  class: string;
}

export const ROUTES: RouteInfo[] = [
  { path: '/view-sedes', title: 'VIEW',  icon: 'ni-tv-2 text-primary', class: '' },
  { path: '/form-sedes', title: 'ADD',  icon: 'ni-tv-2 text-primary', class: '' },
  
  
];
@Component({
  selector: 'app-form-sedes',
  templateUrl: './form-sedes.component.html',
  styleUrls: ['./form-sedes.component.css']
})
export class FormSedesComponent implements OnInit {

  public menuItems: any[];
  public isCollapsed = true;
  constructor(private router: Router,private app:PeticionesService) { }

  ngOnInit(): void {
    this.menuItems = ROUTES.filter(menuItem => menuItem);
    this.router.events.subscribe((event) => {
      this.isCollapsed = true;
   });
  }

  alias:string
  direccion:string
  departamento:string
  municipio:string
  encargado:number
registrar(){
  this.app.postCrearSede({"alias":this.alias,"direccion":this.direccion,"departamento":this.departamento
,"municipio":this.municipio,"usuario":this.encargado}).subscribe( 
    (resp: any) =>{
     
      alert(resp.msj)
      this.alias=this.direccion=this.departamento=this.municipio="";
      this.encargado=0;
    }
   );
}
}
