import { Routes } from '@angular/router';
import { UserProfileComponent } from '../../pages/user-profile/user-profile.component';
import { UsersComponent } from '../../pages/encargado/users/users.component';
import { BodegasComponent } from '../../pages/encargado/bodegas/bodegas.component';
import { ViewBodegasComponent } from '../../pages/encargado/view-bodegas/view-bodegas.component';
export const AdminLayoutRoutes: Routes = [
    
    { path: 'user-profile',   component: UserProfileComponent },
    { path: 'users-encargado',   component: UsersComponent  },
    { path: 'bodegas',   component: BodegasComponent  },
    { path: 'view-bodegas',   component: ViewBodegasComponent  },
   
];
