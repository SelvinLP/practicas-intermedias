import { NgModule } from '@angular/core';
import { HttpClientModule } from '@angular/common/http';import { RouterModule } from '@angular/router';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';

import { ClipboardModule } from 'ngx-clipboard';

import { AdminLayoutRoutes } from './admin-layout.routing';

import { UserProfileComponent } from '../../pages/user-profile/user-profile.component';

import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
// import { ToastrModule } from 'ngx-toastr';
import { UsersComponent } from '../../pages/encargado/users/users.component';
import { BodegasComponent } from '../../pages/encargado/bodegas/bodegas.component';
import { ViewBodegasComponent } from '../../pages/encargado/view-bodegas/view-bodegas.component';



@NgModule({
  imports: [
    CommonModule,
    RouterModule.forChild(AdminLayoutRoutes),
    FormsModule,
    HttpClientModule,
    NgbModule,
    ClipboardModule
  ],
  declarations: [
   
    UserProfileComponent,
    UsersComponent,
    BodegasComponent,
    ViewBodegasComponent,
    
   
  ]
})

export class AdminLayoutModule {}
