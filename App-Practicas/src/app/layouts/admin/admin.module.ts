import { NgModule } from '@angular/core';
import { HttpClientModule } from '@angular/common/http';import { RouterModule } from '@angular/router';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';

import { ClipboardModule } from 'ngx-clipboard';

import { AdminRoutes } from './admin.routing';

import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { FormUsersComponent } from '../../pages/admin/form-users/form-users.component';

import { FormSedesComponent } from '../../pages/admin/form-sedes/form-sedes.component';
import { ViewSedesComponent } from '../../pages/admin/view-sedes/view-sedes.component';
import { RolComponent } from '../../pages/admin/rol/rol.component';
@NgModule({
    imports: [
        CommonModule,
        RouterModule.forChild(AdminRoutes),
        FormsModule,
        HttpClientModule,
        NgbModule,
        ClipboardModule
      ],
      declarations: [

        FormUsersComponent,
        FormSedesComponent,
        ViewSedesComponent,
        RolComponent

      ]
})
export class AdminModule { }
