import { Routes } from '@angular/router';

import { FormUsersComponent } from '../../pages/admin/form-users/form-users.component';

import { FormSedesComponent } from '../../pages/admin/form-sedes/form-sedes.component';
import { ViewSedesComponent } from '../../pages/admin/view-sedes/view-sedes.component';
import { RolComponent } from '../../pages/admin/rol/rol.component';

export const AdminRoutes: Routes = [
    { path: 'form-users',      component: FormUsersComponent },  
    { path: 'form-sedes',      component: FormSedesComponent },
    { path: 'view-sedes',      component: ViewSedesComponent },   
    { path: 'rol',      component: RolComponent },  
];
