
import { NgModule } from '@angular/core';
import { HttpClientModule } from '@angular/common/http';import { RouterModule } from '@angular/router';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { VendedorRoutes } from './vendedor.routing';
import { ClipboardModule } from 'ngx-clipboard';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { ProfileComponent } from '../../pages/vendedor/profile/profile.component';

@NgModule({
    imports: [
      CommonModule,
      RouterModule.forChild(VendedorRoutes),
      FormsModule,
      HttpClientModule,
      NgbModule,
      ClipboardModule
    ],
    declarations: [

        ProfileComponent,

    ]
  })

  export class VendedorModule {}


