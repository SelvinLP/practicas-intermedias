import { Routes } from '@angular/router';
import { ProfileComponent } from '../../pages/vendedor/profile/profile.component';
import { FormClientsComponent } from '../../pages/vendedor/form-clients/form-clients.component';
import { FormSalesComponent } from '../../pages/vendedor/form-sales/form-sales.component';
import { SalesReportComponent } from '../../pages/vendedor/sales-report/sales-report.component';


export const VendedorRoutes: Routes = [
    { path: 'profile-vendedor',      component: ProfileComponent  },
    { path: 'form-clients',      component: FormClientsComponent  },
    { path: 'form-sales',      component: FormSalesComponent  },
    { path: 'sales-report',      component: SalesReportComponent  },
];
