import { NgModule } from '@angular/core';
import { HttpClientModule } from '@angular/common/http';import { RouterModule } from '@angular/router';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { RepartidorRoutes } from './repartidor.routing';
import { ClipboardModule } from 'ngx-clipboard';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { ProfileComponent } from '../../pages/repartidor/profile/profile.component';
import { TransferenciasComponent } from '../../pages/repartidor/transferencias/transferencias.component';
import { VentasComponent } from '../../pages/repartidor/ventas/ventas.component';

@NgModule({
    imports: [
      CommonModule,
      RouterModule.forChild(RepartidorRoutes),
      FormsModule,
      HttpClientModule,
      NgbModule,
      ClipboardModule
    ],
    declarations: [
     
        ProfileComponent,  
        TransferenciasComponent,
        VentasComponent
     
    ]
  })
  
  export class RepartidorModule {}