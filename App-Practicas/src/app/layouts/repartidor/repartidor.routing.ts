import { Routes } from '@angular/router';
import { ProfileComponent } from '../../pages/repartidor/profile/profile.component';
import { TransferenciasComponent } from '../../pages/repartidor/transferencias/transferencias.component';
import { VentasComponent } from '../../pages/repartidor/ventas/ventas.component';

export const RepartidorRoutes: Routes = [
    { path: 'profile-repartidor',      component: ProfileComponent  },  
    { path: 'repartidor-transferencias',      component: TransferenciasComponent   }, 
    { path: 'repartidor-ventas',      component: VentasComponent  }, 
    
];
