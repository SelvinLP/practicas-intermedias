import { Routes } from '@angular/router';

import { ProfileComponent } from '../../pages/bodeguero/profile/profile.component';
import { ActualizacionComponent } from '../../pages/bodeguero/actualizacion/actualizacion.component';
import { TransferenciaComponent } from '../../pages/bodeguero/transferencia/transferencia.component';
import { VisualizarInternasComponent } from '../../pages/bodeguero/visualizar-internas/visualizar-internas.component';
import { VisualizarExternasComponent } from '../../pages/bodeguero/visualizar-externas/visualizar-externas.component';
export const BodegueroRoutes: Routes = [
    { path: 'profile-bodeguero',      component: ProfileComponent  },  
    { path: 'actualizacion-bodeguero',      component: ActualizacionComponent  }, 
    { path: 'transferencia-bodeguero',      component: TransferenciaComponent  },
    { path: 'visualizar-interna-bodeguero',      component: VisualizarInternasComponent  },
    { path: 'visualizar-externa-bodeguero',      component: VisualizarExternasComponent  },
];

