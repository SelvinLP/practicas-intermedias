import { ProfileComponent } from '../../pages/bodeguero/profile/profile.component';
import { NgModule } from '@angular/core';
import { HttpClientModule } from '@angular/common/http';import { RouterModule } from '@angular/router';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { BodegueroRoutes } from './bodeguero.routing';
import { ClipboardModule } from 'ngx-clipboard';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { ActualizacionComponent } from '../../pages/bodeguero/actualizacion/actualizacion.component';
import { TransferenciaComponent } from '../../pages/bodeguero/transferencia/transferencia.component';
import { VisualizarInternasComponent } from '../../pages/bodeguero/visualizar-internas/visualizar-internas.component';
import { VisualizarExternasComponent } from '../../pages/bodeguero/visualizar-externas/visualizar-externas.component';
@NgModule({
    imports: [
      CommonModule,
      RouterModule.forChild(BodegueroRoutes),
      FormsModule,
      HttpClientModule,
      NgbModule,
      ClipboardModule
    ],
    declarations: [
     
        ProfileComponent,     
        ActualizacionComponent,
        TransferenciaComponent,
        VisualizarInternasComponent,
        VisualizarExternasComponent
     
    ]
  })
  
  export class BodegueroModule {}
  


