import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
@Injectable({
  providedIn: 'root'
})
export class PeticionesService {

  constructor(private http:HttpClient) { }

  postLogin(datos:any){
    return this.http.post("http://18.225.36.192:3000/login",datos)
  }
  postRecuperar(datos:any){
    return this.http.post("http://18.225.36.192:3000/recpass",datos)
  }

  postModificar(datos:any){
    return this.http.post("http://18.225.36.192:3000/editprofile",datos)
  }

  postCrearUsuario(datos:any){
    return this.http.post("http://18.225.36.192:3000/signup",datos)
  }

  //--------------------------------------------sedes-----------------------------------
  postCrearSede(datos:any){
    return this.http.post("http://18.225.36.192:3000/encargado/crearSede",datos)
  }

  getSedes(){
    return this.http.get("http://18.225.36.192:3000/encargado/sedes")
  }

  putModificarSede(datos:any){
    return this.http.put("http://18.225.36.192:3000/encargado/updateSede",datos)
  }

  deleteSede(datos:any){
    return this.http.post("http://18.225.36.192:3000/encargado/deleteSede",datos)
  }
  getSedesU(id:any){
    return this.http.get("http://18.225.36.192:3000/encargado/sede"+id)
  }

  //-----------------bodegas--------------------------------------
  getBodegasSede(id:any){
    return this.http.get("http://18.225.36.192:3000/encargado/bodegasSede"+id)
  }

  postCrearBodega(datos:any){
    return this.http.post("http://18.225.36.192:3000/encargado/crearBodega",datos)
  }

  //---------------------bodegueros--------------------------------------

  getProductoInventario(id:any){
    return this.http.get("http://18.225.36.192:3000/bodeguero/productos_inventario"+id)
  }

  getBodegasU(id:any){
    return this.http.get("http://18.225.36.192:3000/encargado/bodegasUser"+id)
  }

  actualizarInventario(datos:any){
    return this.http.get("http://18.225.36.192:3000/actualizarInventario"+datos)
  }
  postCrearSolicitud(datos:any){
    return this.http.post("http://18.225.36.192:3000/solicitarTransferencia",datos)
  }

  getTansferenciasInterna(id:any){
    return this.http.get("http://18.225.36.192:3000/bodeguero/ordenes_internas"+id)
  }

  actualizarTransferencia(datos:any){
    return this.http.get("http://18.225.36.192:3000/bodeguero/actualizar_transferencia"+datos)
  }

  getTansferenciasExterna(id:any){
    return this.http.get("http://18.225.36.192:3000/bodeguero/ordenes_externas"+id)
  }

  //----------------------------- repartidor ----------------------------------------
  getVentas(id:any){
    return this.http.get("http://18.225.36.192:3000/repartidor/ventas"+id)
  }

  getTrans(id:any){
    return this.http.get("http://18.225.36.192:3000/repartidor/transferencias"+id)
  }


  getSedesUsuario(id:string){
    return this.http.get(`http://18.225.36.192:3000/encargado/sede${id}`);
  }

  //--------------------------------------------clientes-----------------------------------
  postCliente(datos:any){
    return this.http.post("http://18.225.36.192:3000/registrarCliente", datos);
  }

  getClientes(){
    return this.http.get("http://18.225.36.192:3000/getClientes");
  }

  //--------------------------------------------ventas-----------------------------------
  postVenta(datos:any){
    console.log(datos);
    return this.http.post("http://18.225.36.192:3000/registrarVenta", datos);
  }



   //--------------------------------------------asignar rol-----------------------------------
   postAsignarRol(datos:any){
    return this.http.post("http://18.225.36.192:3000/asignarRol", datos);
  }

  //--------------------------------------------asignar rol-----------------------------------
  postQuitarRol(datos:any){
    return this.http.post("http://18.225.36.192:3000/quitarRol", datos);
  }

  //--------------------------------------------actualizar estado bodega-----------------------------------
  activarBodega(datos:any){
    return this.http.post("http://18.225.36.192:3000/activarBodega", datos);
  }

  //--------------------------------------------actualizar estado bodega-----------------------------------
  sendEmail(datos:any){
    return this.http.post("http://18.225.36.192:3000/notificacion", datos);
  }
  getVentasClient(datos:any){
    return this.http.post("http://18.225.36.192:3000/getSales", datos);
  }

}
