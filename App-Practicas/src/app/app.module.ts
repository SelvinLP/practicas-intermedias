import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { HttpClientModule } from '@angular/common/http';
import { RouterModule } from '@angular/router';

import { AppComponent } from './app.component';
import { AdminLayoutComponent } from './layouts/admin-layout/admin-layout.component';
import { AuthLayoutComponent } from './layouts/auth-layout/auth-layout.component';


import { NgbModule } from '@ng-bootstrap/ng-bootstrap';

import { AppRoutingModule } from './app.routing';
import { ComponentsModule } from './components/components.module';
import { AdminComponent } from './layouts/admin/admin.component';
import { BodegueroComponent } from './layouts/bodeguero/bodeguero.component';
import { VendedorComponent } from './layouts/vendedor/vendedor.component';
import { RepartidorComponent } from './layouts/repartidor/repartidor.component';
import { FormClientsComponent } from './pages/vendedor/form-clients/form-clients.component';
import { FormSalesComponent } from './pages/vendedor/form-sales/form-sales.component';
import { SalesReportComponent } from './pages/vendedor/sales-report/sales-report.component';






@NgModule({
  imports: [
    BrowserAnimationsModule,
    FormsModule,
    HttpClientModule,
    ComponentsModule,
    NgbModule,
    RouterModule,
    AppRoutingModule
  ],
  declarations: [
    AppComponent,
    AdminLayoutComponent,
    AuthLayoutComponent,
    AdminComponent,
    BodegueroComponent,
    VendedorComponent,
    RepartidorComponent,
    FormClientsComponent,
    FormSalesComponent,
    SalesReportComponent,
     
  
   
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
